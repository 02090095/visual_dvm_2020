#include "CompilationSupervisor.h"    
#include "RunSupervisor.h"    
#include "Global.h"   
int main(int argc, char ** argv)      
{ 		   
	userWorkspace = String(argv[1]);   
	packageWorkspace = String(argv[2]);   
	maxKernels = atoi(argv[3]);   
	dvm_drv = String(argv[4]); 
	//--   
	freeKernels = maxKernels;   
	busyKernels= 0;	   
	//--   
	chdir(packageWorkspace.getCharArray());   
	userWorkspace.println();   
	packageWorkspace.println();   
	printf("%d\n", maxKernels); 
	 
	CompilationSupervisor * compilationSupervisor = new CompilationSupervisor();	    
	printf("%ld\n", compilationSupervisor->getLength());   
	compilationSupervisor->Do();
	   
	RunSupervisor * runSupervisor = new RunSupervisor(compilationSupervisor);   
	printf("%ld\n", runSupervisor->getLength());	    
	runSupervisor->print(); 
	runSupervisor->Do();  
	return 0;      
}      

#include "CompilationTask.h"   
#pragma once   
class RunTask : public Task {   
	long testcompilationtask_id; 	 
	String binary_name;  
	String matrix;   
	String environments;   
	String usr_par;   
	String args; 
	CompilationTask* parent;	 
public:	  	  
	virtual void print(){   
		printf("id=%ld; maxtime=%d; testcompilationtask_id=%ld; matrix=%s; environments=%s; usr_par=%s; args=%s kernels=%d\n",   
		id,   
		maxtime,   
		testcompilationtask_id,   
		matrix.getCharArray(),   
		environments.getCharArray(),   
		usr_par.getCharArray(),   
		args.getCharArray(), 
		kernels
		);	   
	}   
	int setKernels(String * kernels_s){
		return kernels = atoi(kernels_s->getCharArray());
	}	
	long setTestCompilationTaskId(String * id_s){	   
		return testcompilationtask_id=strtol(id_s->getCharArray(), NULL, 10);   
	}  
	long getTestCompilationTaskId(){  
		return testcompilationtask_id;  
	}  
	void setMatrix(String * matrix_in){   
		matrix= String (matrix_in->getCharArray());   
	}   
	void setEnvironments(String * environments_in){   
		environments= String(environments_in->getCharArray());   
	}   
	void setUsrPar(String * usr_par_in){   
		usr_par= String(usr_par_in->getCharArray(),'|');   
	}   
	void setArgs(String * args_in){   
		args= String(args_in->getCharArray());   
	}
	void setParent(CompilationTask * parent_in){ 
		parent = parent_in;	 
		binary_name = "spf_"+ String(id)+"_"+matrix.Replace(' ','_'); 
	} 
	CompilationTask * getParent(){ 
		return parent; 
	} 
	RunTask(Text * lines, int offset):Task(lines,offset) {   
		setTestCompilationTaskId(lines->get(offset+2));    
		setMatrix(lines->get(offset+3));    
		setEnvironments(lines->get(offset+4));    
		setUsrPar(lines->get(offset+5));    
		setArgs(lines->get(offset+6));
		setKernels(lines->get(offset+7));
	}   
	 
	virtual String getLaunchScriptText(){  
		String modules = userWorkspace+"/modules";  
		String starterCall  = modules+"/starter";	  
		String launcherCall = modules+"/launcher";  
		//- 
		String dvm_start = String::DQuotes(dvm_drv) + " run "; 
		if (!matrix.isEmpty()) 
            	dvm_start = dvm_start+matrix + " ";             
            dvm_start = dvm_start+ String::DQuotes("./" + binary_name); 
            if (!args.isEmpty()) 
            	dvm_start = dvm_start+ " " + args; 
		return String::DQuotes(starterCall)+" "+  
			String::DQuotes(launcherCall)+" "+  
			String(maxtime)+" "+  
			String::DQuotes("killall -SIGKILL " + binary_name)+" "+  
			dvm_start; 
	}  
	virtual void prepareWorkspace(){  
		String binary_src = parent->getWorkspace()+"/0"; 
		String binary_dst = workspace+"/"+binary_name; 
		Utils::Copy(binary_src, binary_dst); 
		if (!usr_par.isEmpty()){ 
			String parPath = String(id)+"/usr.par";  
			File parFile = File(parPath, usr_par);  
		} 
	}    
	virtual String getStartCommand(){ 				 
		String res = workspace+"/run"; 
		if (!environments.isEmpty()) 
			res = environments+" "+res; 
		printf("START %ld: %s\n", id, res.getCharArray()); 
		return res; 
	}
};
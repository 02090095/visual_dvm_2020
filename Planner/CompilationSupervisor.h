#include "Supervisor.h"   
#include "CompilationTask.h" 
#pragma once   
class CompilationSupervisor: public Supervisor<CompilationTask> { 
public:  
	CompilationSupervisor(){ 
		this->init("compilationTasks", 4); 
	}
	CompilationTask * getTaskById(long task_id){
		for (long i=0; i< length; ++i){
			CompilationTask * task = get(i);
			if (task->getId()==task_id)
				return task;
		}
		return NULL;
	}
	virtual String getStatePrefix(){
		return String("Compilation");
	}	
};
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include "String.h"
#pragma once
class Utils {
public:
	static int max(int a, int b){
		return (a > b)? a:b;  		
	} 
	static int min(int a, int b){
  		return (a > b)? b:a; 
	}
	static void Mkdir(const String& path){
		mkdir(path.getCharArray(), 0777);
	}	
	//https://stackoverflow.com/questions/4568681/using-chmod-in-a-c-program
	static void Chmod(const String& path){
		String command = "chmod 777 "+String::DQuotes(path);
		system(command.getCharArray());
	}	
	//https://stackoverflow.com/questions/230062/whats-the-best-way-to-check-if-a-file-exists-in-c
	static bool Exists(const String& path){		
		struct stat buffer;   
  		return (stat (path.getCharArray(), &buffer) == 0);
	}
	static void Sleep(int s){
		usleep(s* 1000000);
	}	
	static void Copy(const String& src, const String& dst){
		String command = "cp "+String::DQuotes(src)+" "+String::DQuotes(dst);		
		system(command.getCharArray());		
	}
	static long getAbsoluteTime(){
   		return time (NULL);
	}
	static String getDate(){
		long int ttime;
   		ttime = time (NULL);
   		String res(ctime (&ttime));
   		return res;
	}
	static void ZipFolder(const String& src, const String& dst){
		String command = "zip -r "+String::DQuotes(dst)+" "+String::DQuotes(src);
		system(command.getCharArray());
	}
};
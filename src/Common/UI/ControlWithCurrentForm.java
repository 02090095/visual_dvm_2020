package Common.UI;
import Common.Current;

import java.awt.*;
public class ControlWithCurrentForm<C extends Component> extends ControlForm<C> {
    public ControlWithCurrentForm(Class<C> class_in) {
        super(class_in);
    }
    //-
    public Current CurrentName() {
        return Current.Undefined;
    }
    public void ShowCurrentObject() throws Exception {
    }
    public void ShowNoCurrentObject() throws Exception {
    }
    public void MouseAction2() throws Exception {
    }
}

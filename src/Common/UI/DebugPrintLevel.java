package Common.UI;
public enum DebugPrintLevel {
    Undefined,
    Passes,
    Project;
    public String getDescription() {
        return toString();
    }
    public boolean isEnabled() {
        switch (this) {
            case Passes:
                return true;
            default:
                return false;
        }
    }
}
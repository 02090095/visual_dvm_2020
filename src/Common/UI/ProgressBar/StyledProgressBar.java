package Common.UI.ProgressBar;
import Common.Current;
import Common.UI.Themes.ThemeElement;

import javax.swing.*;
public class StyledProgressBar extends JProgressBar implements ThemeElement {
    public StyledProgressBar() {
        setStringPainted(true);
        applyTheme();
    }
    @Override
    public void applyTheme() {
        setBackground(Current.getTheme().bar_background);
        setForeground(Current.getTheme().bar_foreground);
    }
}

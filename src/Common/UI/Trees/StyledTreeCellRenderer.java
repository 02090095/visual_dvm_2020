package Common.UI.Trees;
import Common.Current;
import Common.UI.Themes.ThemeElement;

import javax.swing.tree.DefaultTreeCellRenderer;
public class StyledTreeCellRenderer extends DefaultTreeCellRenderer implements ThemeElement {
    public StyledTreeCellRenderer() {
        applyTheme();
    }
    @Override
    public void applyTheme() {
        setBackgroundNonSelectionColor(Current.getTheme().trees_background);
        setBackgroundSelectionColor(Current.getTheme().selection_background);
    }
}

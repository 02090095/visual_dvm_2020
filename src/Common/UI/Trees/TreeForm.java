package Common.UI.Trees;
import Common.UI.ControlForm;
import Common.UI.UI;

import java.awt.*;
public class TreeForm<C extends StyledTree> extends ControlForm<C> {
    public TreeForm(Class<C> class_in) {
        super(class_in);
    }
    //временно, чтобы не затрагивать коды раньше времени.
    public StyledTree getTree() {
        return control;
    }
    @Override
    protected void refresh() {
        getTree().revalidate();
        getTree().repaint();
    }
    @Override
    public void Show() {
        super.Show();
        content.add(scroll, BorderLayout.CENTER);
        content.updateUI();
    }
    @Override
    public void Clear() {
        super.Clear();
        UI.Clear(content);
    }
}

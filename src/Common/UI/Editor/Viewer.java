package Common.UI.Editor;
public class Viewer extends BaseEditor {
    public Viewer() {
        setLineWrap(true);
        setWrapStyleWord(true);
        setEditable(false);
    }
}

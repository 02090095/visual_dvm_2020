package Common.UI.Label;
import javax.swing.*;
public class ShortLabel extends JLabel {
    int max = 0;
    public ShortLabel(int max_in) {
        max = max_in;
    }
    @Override
    public void setText(String text_in) {
        if ((max > 0) && (text_in.length() > max)) {
            super.setText(text_in.substring(0, max - 1) + "...");
        } else super.setText(text_in);
        setToolTipText(text_in);
    }
}

package Common.UI.Windows;
public enum FormType {
    Undefined,

    Main,
    SearchReplace,
    Components,
    RemoteFileChooser,
    Profiles
}

package Common.UI.Windows.Dialog;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;
import java.awt.*;
public class DialogWrapText extends JTextPane implements DialogFields {
    public DialogWrapText(){
        setOpaque(true);
        setBackground(Color.WHITE);
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeBold));
        setEditable(false);
    }
    @Override
    public Component getContent() {
        return this;
    }
    /*
    public void setTextW(String text_in){
        String[] lines = text_in.split("\n");
        String labelText = "";
        if (lines.length == 1) {
            labelText = text_in;
        } else {
            int i = 0;
            for (String line : lines) {
                String fline = "";
                if (i == 0) {
                    fline = "<html><body>" + line + "<br>";
                } else if (i == lines.length - 1) {
                    fline = line + "</body></html>";
                } else {
                    fline = line + "<br>";
                }
                ++i;
                labelText += fline;
            }
        }
        setText(labelText);
    }
     */
}

package Common.UI.Windows.Dialog;
public abstract class SpinnerNumberForm extends NumberDialog<DialogSpinner> {
    public SpinnerNumberForm() {
        super(DialogSpinner.class);
    }
    @Override
    public void setNumber(int num_in) {
        fields.setValue(num_in);
    }
    @Override
    public int getDefaultWidth() {
        return 400;
    }
    @Override
    public int getDefaultHeight() {
        return 130;
    }
    @Override
    public void ProcessResult() {
        Result = (Integer) fields.getValue();
    }
}

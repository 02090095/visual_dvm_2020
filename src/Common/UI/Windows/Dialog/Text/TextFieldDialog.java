package Common.UI.Windows.Dialog.Text;
import Common.UI.Windows.Dialog.DialogTextField;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
public class TextFieldDialog extends TextDialog<DialogTextField> {
    public TextFieldDialog() {
        super(DialogTextField.class);
        setResizable(false);
        fields.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    onOK();
                }
            }
        });
    }
    @Override
    public int getDefaultWidth() {
        return 450;
    }
    @Override
    public int getDefaultHeight() {
        return 135;
    }
    @Override
    public void ProcessResult() {
        Result = fields.getText();
    }
    @Override
    public void setText(String text_in) {
        fields.setText(text_in);
    }
}

package Common.UI.Windows.Dialog.Text;
import Common.Utils.Utils;
public class FileNameForm extends TextFieldDialog {
    public FileNameForm() {
    }
    @Override
    public void validateFields() {
        Utils.validateFileShortNewName(fields.getText(), Log);
    }
}

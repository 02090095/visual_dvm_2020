package Common.UI.Windows.Dialog.Text;
import Common.UI.Editor.BaseEditor;
public class MultilineTextForm extends TextDialog<BaseEditor> {
    public MultilineTextForm() {
        super(BaseEditor.class);
    }
    //при наследовании по умолчанию поля не присваивать!
    //инициализация полей работает после конструктора предка!!
    @Override
    public void ProcessResult() {
        Result = fields.getText();
    }
    @Override
    public void InitFields() {
        fields.setSearchEnabled(false);
        fields.setLineWrap(true);
        fields.setWrapStyleWord(true);
        fields.setHighlightCurrentLine(false);
    }
    @Override
    public void setText(String text_in) {
        fields.setText(text_in);
        fields.setCaretPosition(0);
    }
}

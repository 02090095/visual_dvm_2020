package Common.UI.Windows.Dialog.Text;
public class ReadOnlyMultilineTextForm extends MultilineTextForm {
    public ReadOnlyMultilineTextForm() {
    }
    @Override
    public void InitFields() {
        fields.setEditable(false);
    }
    @Override
    public void CreateButtons() {
    }
}

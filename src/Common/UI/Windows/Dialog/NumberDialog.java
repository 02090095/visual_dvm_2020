package Common.UI.Windows.Dialog;
public abstract class NumberDialog<F extends DialogFields> extends Dialog<Integer, F> {
    public NumberDialog(Class<F> f) {
        super(f);
    }
    @Override
    public void Init(Object... params) {
        if (params.length > 0) setNumber((int) params[0]);
    }
    public abstract void setNumber(int num_in);
}

package Common.UI.Windows.Dialog;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;
import java.awt.*;
public class DialogTextComboBox extends JComboBox<String> implements DialogFields {
    public DialogTextComboBox() {
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        setEditable(false);
    }
    @Override
    public Component getContent() {
        return this;
    }
}

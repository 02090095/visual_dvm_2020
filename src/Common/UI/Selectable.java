package Common.UI;
import Common.Utils.Utils;

import javax.swing.*;
public interface Selectable {
    boolean isSelected();
    default void Select(boolean flag) {
        if (isSelectionEnabled())
            select(flag);
    }
    void select(boolean flag);
    //-
    default ImageIcon GetSelectionIcon() {
        return
                isSelectionEnabled() ?
                        Utils.getIcon("/icons/" + (isSelected() ? "Pick" : "NotPick") + ".png") :
                        GetDisabledIcon();
    }
    default ImageIcon GetDisabledIcon() {
        return Utils.getIcon("/icons/Arrays/Unknown.png");
    }
    default void SwitchSelection() {
        Select(!isSelected());
    }
    //строчный контент для передачи параметров проходам.
    default String getSelectionContent() {
        return toString();
    }
    //-
    default String getSelectionText() {
        return toString();
    }
    default boolean isSelectionEnabled() {
        return true;
    }
    default void SelectAllChildren(boolean select) {
    }
}

package Common.UI.Menus;
import GlobalData.Tasks.TaskState;

import javax.swing.*;
import java.awt.event.ActionListener;
public class TestsCompilationFilterMenu extends StyledPopupMenu{
    public void CreateStateItem(TaskState state, ActionListener listener){
        JMenuItem m = new VisualiserMenuItem(state.getDescription());
        m.addActionListener(listener);
        add(m);
    }
    public TestsCompilationFilterMenu(){
        for (TaskState taskState: TaskState.values()){
            switch (taskState){
                case Queued:
                case FailedToQueue:
                case NoSuchTask:
                case AbortingByUser:
                    break;
                case Waiting:
                    CreateStateItem(taskState, e -> {

                    });
                    break;
            }
        }
    }
}

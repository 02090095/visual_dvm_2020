package Common.UI.Menus;
import Common.Current;
import Common.Global;
import Common.UI.Menus_2023.LanguagesSubmenu;
import Common.UI.Menus_2023.StableMenuItem;
import Common.UI.Menus_2023.StylesSubmenu;
import Common.UI.Menus_2023.TypesSubmenu;
import Common.UI.Trees.StyledTree;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.FileType;
import ProjectData.Files.LanguageStyle;
import ProjectData.LanguageName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
public class ProjectFilesMenu extends GraphMenu {
    VisualiserMenuItem m_select_all;
    VisualiserMenuItem m_unselect_all;
    VisualiserMenuItem m_multiselection;
    JMenu mLanguage;
    JMenu mStyle;
    JMenu mType;
    public ProjectFilesMenu(StyledTree tree) {
        super(tree, "подпапки");
        addSeparator();
        JMenuItem m = new VisualiserMenuItem("Открыть в проводнике...", "/icons/Explorer.png");
        m.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Desktop.getDesktop().open(Current.getProject().Home);
                        } catch (Exception ex) {
                            Global.Log.PrintException(ex);
                        }
                    }
                });
        add(m);
        addSeparator();
        add(new PassesSubMenu("Добавить", "/icons/RedAdd.png",
                PassCode_2021.AddFile,
                PassCode_2021.CreateEmptyDirectory,
                PassCode_2021.ImportFiles));
        addSeparator();
        add(new PassesSubMenu("Переименовать", "/icons/Menu/Rename.png",
                PassCode_2021.RenameFile,
                PassCode_2021.RenameDirectory));
        add(new VisualiserMenuItem("Удалить текущий проект", "/icons/Delete.png") {
            {
                addActionListener(e -> {
                    if (Current.HasProject()) {
                        UI.getVersionsWindow().getVersionsForm().getTree().SelectNode(Current.getProject().node);
                        Pass_2021.passes.get(PassCode_2021.DeleteVersion).Do();
                    }
                });
            }
        });
        addSeparator();
        m_multiselection = new VisualiserMenuItem("Массовый режим работы с файлами");
        m_multiselection.setIcon(Utils.getIcon(Global.files_multiselection ? "/icons/Pick.png" : "/icons/NotPick.png"));
        m_multiselection.addActionListener(e -> {
            Global.files_multiselection = !Global.files_multiselection;
            m_multiselection.setIcon(Utils.getIcon(Global.files_multiselection ? "/icons/Pick.png" : "/icons/NotPick.png"));
            Current.getProject().SelectAllFiles(false);
            UI.getMainWindow().getProjectWindow().RefreshProjectFiles();

            //-
        });
        add(m_multiselection);
        addSeparator();
        //-
        m_select_all = new StableMenuItem("Выбрать всё", "/icons/SelectAll.png");
        m_select_all.addActionListener(e -> {
            Current.getProject().SelectAllFiles(true);
            tree.updateUI();
        });
        add(m_select_all);
        m_unselect_all = new StableMenuItem("Отменить всё", "/icons/UnselectAll.png");
        m_unselect_all.addActionListener(e -> {
            Current.getProject().SelectAllFiles(false);
            tree.updateUI();
        });
        add(m_unselect_all);
        //--------------------------------------------------
        add(mLanguage = new LanguagesSubmenu(PassCode_2021.SetSelectedFilesLanguage.getDescription()) {
            @Override
            public void action(LanguageName languageName) {
                Pass_2021.passes.get(PassCode_2021.SetSelectedFilesLanguage).Do(languageName);
            }
        });
        add(mStyle = new StylesSubmenu(PassCode_2021.SetSelectedFilesStyle.getDescription()) {
            @Override
            public void action(LanguageStyle languageStyle) {
                Pass_2021.passes.get(PassCode_2021.SetSelectedFilesStyle).Do(languageStyle);
            }
        });
        add(mType = new TypesSubmenu(PassCode_2021.SetSelectedFilesType.getDescription()) {
            @Override
            public void action(FileType fileType) {
                Pass_2021.passes.get(PassCode_2021.SetSelectedFilesType).Do(fileType);
            }
        });
        //--------------------------------------------------
        add(Pass_2021.passes.get(PassCode_2021.ExcludeSelectedFiles).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.IncludeSelectedFiles).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.ExcludeFile).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.IncludeFile).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteFile).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteDirectory).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteSelectedFiles).createMenuItem());
    }
    @Override
    public void CheckElementsVisibility() {
        m_select_all.setVisible(Global.files_multiselection);
        m_unselect_all.setVisible(Global.files_multiselection);
        mLanguage.setVisible(Global.files_multiselection);
        mStyle.setVisible(Global.files_multiselection);
        mType.setVisible(Global.files_multiselection);
        //-
        Pass_2021.passes.get(PassCode_2021.ExcludeFile).setControlsVisible(!Global.files_multiselection);
        Pass_2021.passes.get(PassCode_2021.IncludeFile).setControlsVisible(!Global.files_multiselection);
        //-
        Pass_2021.passes.get(PassCode_2021.DeleteFile).setControlsVisible(!Global.files_multiselection);
        Pass_2021.passes.get(PassCode_2021.DeleteDirectory).setControlsVisible(!Global.files_multiselection);
        //--
        Pass_2021.passes.get(PassCode_2021.ExcludeSelectedFiles).setControlsVisible(Global.files_multiselection);
        Pass_2021.passes.get(PassCode_2021.IncludeSelectedFiles).setControlsVisible(Global.files_multiselection);
        Pass_2021.passes.get(PassCode_2021.DeleteSelectedFiles).setControlsVisible(Global.files_multiselection);
    }
}


package Common.UI.Menus;
import Common.Current;
import Common.Global;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;

import javax.swing.*;
public class PropertiesSubmenu extends JMenu {
    public PropertiesSubmenu(String title, String icon, String... settings) {
        super(title);
        if (icon != null)
            setIcon(Utils.getIcon(icon));
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        for (String name : settings) {
            Global.properties.addFlagMenuItem(this, name);
        }
    }
}

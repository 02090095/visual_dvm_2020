package Common.UI.Menus;
import Common.Utils.Utils;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.util.Vector;
public class TextEditorMenu extends StyledPopupMenu {
    protected JTextComponent editor;
    protected String selectedText = null;
    //-------------------------------------------------
    JMenuItem m_cut;
    JMenuItem m_copy;
    JMenuItem m_paste;
    protected JMenuItem m_strike;
    protected JMenuItem m_unstrike;
    //-------------------------------------------------
    public TextEditorMenu(JTextComponent editor_in) {
        editor = editor_in;
        m_cut = new VisualiserMenuItem("Вырезать", "/icons/Editor/Cut.png");
        m_cut.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.cut();
            }
        });
        add(m_cut);
        m_copy = new VisualiserMenuItem("Копировать", "/icons/Editor/Copy.png");
        m_copy.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editor.copy();
                    }
                });
        add(m_copy);
        m_paste = new VisualiserMenuItem("Вставить", "/icons/Editor/Paste.png");
        m_paste.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editor.paste();
                    }
                });
        add(m_paste);
        //--
        m_strike = new VisualiserMenuItem("Вычеркнуть","/icons/Editor/Strikethrough.png");
        m_strike.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String[] data = selectedText.split("\n");
                        Vector<String> new_data = new Vector<>();
                        for (String line: data){
                            new_data.add(Utils.strikeThrough(line));
                        }
                        editor.replaceSelection(String.join("\n", new_data));
                    }
                });
        add(m_strike);
        m_unstrike = new VisualiserMenuItem("Отменить вычёркивание","/icons/Editor/NoStrike.png");
        m_unstrike.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String[] data = selectedText.split("\n");
                        Vector<String> new_data = new Vector<>();
                        for (String line: data){
                            new_data.add(Utils.noStrikeThrough(line));
                        }
                        editor.replaceSelection(String.join("\n", new_data));
                    }
                });
        add(m_unstrike);
    }
    @Override
    public void CheckElementsVisibility() {
        selectedText = editor.getSelectedText();
        m_cut.setVisible(editor.isEditable() && (selectedText != null));
        m_paste.setVisible(editor.isEditable());
        m_copy.setVisible(selectedText != null);
        m_strike.setVisible(editor.isEditable() && (selectedText != null));
        m_unstrike.setVisible(editor.isEditable() && (selectedText != null));
        super.CheckElementsVisibility();
    }
}

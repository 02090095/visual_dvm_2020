package Common.UI.Menus;
import Common.Current;
import Common.Global;
import Common.UI.Menus_2023.StableMenuItem;
import Common.UI.Trees.DataTree;
import Common.UI.UI;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class VersionsMenu extends GraphMenu<DataTree> {
    VisualiserMenuItem m_select_all;
    VisualiserMenuItem m_unselect_all;
    VisualiserMenuItem m_multiselection;
    public VersionsMenu(DataTree tree) {
        super(tree, "подверсии");
        add(Pass_2021.passes.get(PassCode_2021.DeleteSubversions).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteLonelyM).createMenuItem());
        addSeparator();
        m_multiselection = new VisualiserMenuItem("Массовый режим работы с версиями");
        m_multiselection.setIcon(Utils.getIcon(Global.versions_multiselection ? "/icons/Pick.png" : "/icons/NotPick.png"));
        m_multiselection.addActionListener(e -> {
            Global.versions_multiselection = !Global.versions_multiselection;
            m_multiselection.setIcon(Utils.getIcon(Global.versions_multiselection ? "/icons/Pick.png" : "/icons/NotPick.png"));
            Current.getRoot().SelectAllVersions(false);
            UI.getVersionsWindow().getVersionsForm().getTree().updateUI();
        });
        add(m_multiselection);
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.DeleteVersion).createMenuItem());
        //-
        m_select_all = new StableMenuItem("Выбрать всё, кроме резервных копий","/icons/SelectAll.png");
        m_select_all.addActionListener(e -> {
            Current.getRoot().SelectAllVersions(true);
            tree.updateUI();
        });
        add(m_select_all);
        m_unselect_all = new StableMenuItem("Отменить всё","/icons/UnselectAll.png");
        m_unselect_all.addActionListener(e -> {
            Current.getRoot().SelectAllVersions(false);
            tree.updateUI();
        });
        add(m_unselect_all);
        add(Pass_2021.passes.get(PassCode_2021.DeleteSelectedVersions).createMenuItem());
    }
    @Override
    public void CheckElementsVisibility() {
        Pass_2021.passes.get(PassCode_2021.DeleteSelectedVersions).setControlsVisible(!Global.versions_multiselection);
        Pass_2021.passes.get(PassCode_2021.DeleteSelectedVersions).setControlsVisible(Global.versions_multiselection);
        Pass_2021.passes.get(PassCode_2021.DeleteVersion).setControlsVisible(!Global.versions_multiselection);
        m_select_all.setVisible(Global.versions_multiselection);
        m_unselect_all.setVisible(Global.versions_multiselection);
    }
}

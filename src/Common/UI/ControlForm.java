package Common.UI;
import Common.Global;

import javax.swing.*;
import java.awt.*;
//класс, представляющий собой прокручиваемую панель, на которой лежит нечто.
public class ControlForm<C extends Component> {
    public C control = null;
    protected Class<C> control_class;
    protected JPanel content;  //задник.
    public JScrollPane scroll = null;
    public ControlForm(Class<C> class_in) {
        control_class = class_in;
        setContent(new JPanel(new BorderLayout()));
    }
    //нужно будет вывестии сделать нормальные формы для деревьев а не ручное создание.
    public JPanel getContent() {
        return content;
    }
    public void setContent(JPanel content_in) {

        content = content_in;
    }
    //-
    public void Show() {
        Clear();
        CreateControl();
        //------------------------
        scroll = new JScrollPane(control);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }
    public void CreateControl() {
        try {
            control = control_class.newInstance();
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
    }
    public boolean isShown() {
        return control != null;
    }
    public void Clear() {
        control = null; //очищено.
    }
    public void Refresh() {
        if (control != null)
            refresh();
    }
    //-
    protected void refresh() {
    } //перерисовать контрол.
}

package Common.UI;
import Common.Utils.Utils;

import javax.swing.*;
import java.util.Vector;
public class VisualiserStringList extends JList<String> {
    DefaultListModel<String> elements = null;
    //-
    public VisualiserStringList() {
        this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    public void fill(Vector<String> content) {
        elements = new DefaultListModel<>();
        for (String s : content)
            elements.addElement(Utils.Brackets(s));
        setModel(elements);
    }
    public String pack() {
        String res = "";
        for (int i = 0; i < elements.getSize(); ++i) {
            String s = elements.getElementAt(i);
            s = s.substring(1, s.length() - 1);
            res+=s+"\n";
        }
        return res;
    }
    public void addElement(String element) {
        String element_ = Utils.Brackets(element);
        if (!elements.contains(element_))
            elements.addElement(element_);
        else UI.Info("элемент " + element_ + " уже существует.");
    }
    public void removeElement(String element) {
        if (element != null)
            elements.removeElement(element);
        else UI.Info("элемент не выбран");
    }
}

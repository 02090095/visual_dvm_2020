package Common.UI.Menus_2023;
import Common.UI.Menus.VisualiserMenuItem;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.Pass_2021;
public class PassMenuItem extends VisualiserMenuItem implements PassControl {
    @Override
    public void setIcon(String icon_path) {
        setIcon(Utils.getIcon(icon_path));
    }
    public PassMenuItem(Pass_2021 pass) {
        setText(pass.getDescription());
        setToolTipText(pass.getDescription());
        if (pass.getIconPath() != null) setIcon(pass.getIconPath());
        addActionListener(pass.getControlAction());
        pass.controls.add(this);
    }
}

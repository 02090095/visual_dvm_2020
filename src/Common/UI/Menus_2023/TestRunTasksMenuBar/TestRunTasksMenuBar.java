package Common.UI.Menus_2023.TestRunTasksMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
public class TestRunTasksMenuBar extends DataMenuBar {
    JMenuBar filters= null;
    public void DropFilters() {
        if (filters != null) {
            remove(filters);
            filters = null;
        }
        revalidate();
        repaint();
    }
    public void addFilters(JMenu cFilterMenu, JMenu rFilterMenu) {
        filters= addMenus(cFilterMenu, rFilterMenu);
    }
    public TestRunTasksMenuBar() {
        super("задачи", PassCode_2021.DownloadTaskTest);
    }
}

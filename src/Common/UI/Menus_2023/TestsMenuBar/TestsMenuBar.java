package Common.UI.Menus_2023.TestsMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class TestsMenuBar extends DataMenuBar {
    public TestsMenuBar() {
        super("тесты",
                PassCode_2021.DownloadTest,
                PassCode_2021.PublishTest,
                PassCode_2021.EditTest,
                PassCode_2021.DeleteSelectedTests);
    }
}

package Common.UI.Menus_2023;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
//https://java-online.ru/swing-menu.xhtml
public class VisualiserMenuBar extends JToolBar {
    public VisualiserMenuBar() {
        setFloatable(false);
        setSizeLimits();
    }
    public void addPasses(PassCode_2021... codes) {
        //- кнопки. связать их с проходами. (!)
        for (PassCode_2021 code : codes)
            add(Pass_2021.passes.get(code).createButton());
    }
    public JMenuBar addMenus(JMenu... menus) {
        JMenuBar bar = new JMenuBar() {
            {
                for (JMenu menu : menus)
                    add(menu);
            }
        };
        add(bar);
        return bar;
    }
    public void setSizeLimits() {
        setPreferredSize(new Dimension(0, 30));
    }
}

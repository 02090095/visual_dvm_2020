package Common.UI.Menus_2023;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.Pass_2021;
public class StablePassMenuItem extends StableMenuItem implements PassControl {
    @Override
    public void setIcon(String icon_path) {
        setIcon(Utils.getIcon(icon_path));
    }
    public StablePassMenuItem(Pass_2021 pass) {
        setText(pass.getDescription());
        setToolTipText(pass.getDescription());
        if (pass.getIconPath() != null) setIcon(pass.getIconPath());
        addActionListener(pass.getControlAction());
        pass.controls.add(this);
    }
}

package Common.UI.Menus_2023.VariantsMenuBar;
import Common.UI.Menus.VisualiserMenuItem;
import Common.UI.Menus_2023.DataMenuBar;
import Common.UI.Menus_2023.VisualiserMenu;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class VariantsMenuBar extends DataMenuBar {
    public VariantsMenuBar() {
        super("варианты");
        addMenus(new VisualiserMenu("Отображение параллельных вариантов",
                "/icons/ShowPassword.png") {
            {
                add(new VisualiserMenuItem("Все варианты") {
                    {
                        addActionListener(e -> Pass_2021.passes.get(PassCode_2021.GenerateParallelVariants).Do(true));
                    }
                });
                add(new VisualiserMenuItem("Минимальное покрытие вариантов") {
                    {
                        addActionListener(e -> Pass_2021.passes.get(PassCode_2021.GenerateParallelVariants).Do(false));
                    }
                });
            }
            ;
        });
        addPasses(PassCode_2021.PredictParallelVariants, PassCode_2021.CreateParallelVariants);
    }
}

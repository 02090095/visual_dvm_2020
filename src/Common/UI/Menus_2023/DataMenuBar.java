package Common.UI.Menus_2023;
import Common.Database.DataSet;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
public class DataMenuBar extends VisualiserMenuBar {
    public JLabel countLabel = null;
    JButton selectAllButton = null;
    JButton unselectAllButton = null;
    //-
    public ActionListener selectAllListener = null;
    public ActionListener unselectAllListener = null;
    //-
    public DataMenuBar(String dataName, PassCode_2021... passes) {
        //  Font font = Current.getTheme().Fonts.get(VisualiserFonts.TreeBoldItalic).deriveFont(12.0F);
        add(new JLabel(dataName + " "));
        add(countLabel = new JLabel("?"));
        addPasses(passes);
    }
    public void createSelectionButtons(DataSet dataSet) {
        java.awt.Dimension d = new Dimension(25, 25);
        if (selectAllButton == null) {
            add(selectAllButton = new MenuBarButton() {
                {
                    setIcon("/icons/SelectAll.png");
                    setToolTipText("Выбрать всё");
                    setPreferredSize(d);
                    setMinimumSize(d);
                    setMaximumSize(d);
                }
            }, 0);
        }
        if (unselectAllButton == null) {
            add(unselectAllButton = new MenuBarButton() {
                {
                    setIcon("/icons/UnselectAll.png");
                    setToolTipText("Отменить всё");
                    setPreferredSize(d);
                    setMinimumSize(d);
                    setMaximumSize(d);
                }
            }, 1);
        }
        if (selectAllListener != null) {
            selectAllButton.removeActionListener(selectAllListener);        }
        selectAllButton.addActionListener(selectAllListener = e -> dataSet.CheckAll(true));
        if (unselectAllListener != null) {
            unselectAllButton.removeActionListener(unselectAllListener);
        }
        unselectAllButton.addActionListener(unselectAllListener = e -> dataSet.CheckAll(false));
    }
}

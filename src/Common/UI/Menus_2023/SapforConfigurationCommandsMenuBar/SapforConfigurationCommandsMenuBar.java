package Common.UI.Menus_2023.SapforConfigurationCommandsMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class SapforConfigurationCommandsMenuBar extends DataMenuBar {
    public SapforConfigurationCommandsMenuBar() {
        super("команды",
                PassCode_2021.PublishSapforConfigurationCommand,
                PassCode_2021.EditSapforConfigurationCommand,
                PassCode_2021.DeleteSapforConfigurationCommand
        );
    }
}

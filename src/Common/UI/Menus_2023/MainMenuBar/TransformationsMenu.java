package Common.UI.Menus_2023.MainMenuBar;
import Common.UI.Menus.PassesSubMenu;
import Common.UI.Menus_2023.VisualiserMenu;
import Repository.Component.Sapfor.Sapfor;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class TransformationsMenu extends VisualiserMenu {
    public TransformationsMenu() {
        super("Преобразования", "/icons/Transformations.png");
        add(new PassesSubMenu("Циклы", "/icons/Menu/Loops.png",
                Sapfor.getLoopsTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("Приватные переменные", "/icons/Menu/Privates.png",
                Sapfor.getPrivatesTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("Процедуры", "/icons/Menu/Functions.png",
                Sapfor.getProceduresTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("DVM директивы", "/icons/Menu/Dvm.png",
                Sapfor.getDVMTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("Интервалы", "/icons/Menu/Intervals.png",
                Sapfor.getIntervalsTransformationsCodes()
        ));
        addSeparator();
        add(new PassesSubMenu("Области распараллеливания", "/icons/Menu/Regions.png",
                Sapfor.getRegionsTransformationsCodes()
        ));
        addSeparator();
        add(new PassesSubMenu("Предобработка проекта", "/icons/Menu/Preprocessing.png",
                        Sapfor.getPreparationTransformationsCodes()
                )
        );
        add(Pass_2021.passes.get(PassCode_2021.SPF_SharedMemoryParallelization).createMenuItem());
    }
}
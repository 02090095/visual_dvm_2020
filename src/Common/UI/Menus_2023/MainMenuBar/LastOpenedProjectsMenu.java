package Common.UI.Menus_2023.MainMenuBar;
import Common.Global;
import Common.UI.Menus.VisualiserMenuItem;
import Common.UI.Menus_2023.VisualiserMenu;
import GlobalData.DBLastProject.DBLastProject;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Vector;
public class LastOpenedProjectsMenu extends VisualiserMenu {
    public LastOpenedProjectsMenu() {
        super("Недавние проекты", "/icons/LastOpened.png");
        addMenuListener(new MenuListener() {
            @Override
            public void menuSelected(MenuEvent e) {
                removeAll();
                Vector<DBLastProject> projects = Global.db.lastProjects.getOrdered();
                int k = 1;
                for (DBLastProject p : projects) {
                    if (new File(p.HomePath).exists() && p.lastOpened != 0) {
                        VisualiserMenuItem i = new VisualiserMenuItem(p.HomePath);
                        i.addActionListener(new AbstractAction() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                Pass_2021.passes.get(PassCode_2021.OpenCurrentProject).Do(new File(p.HomePath));
                            }
                        });
                        add(i);
                        ++k;
                        if (k > (Global.db.settings.get(SettingName.LastOpenedProjectsCount).toInt32())) break;
                    }
                }
            }
            @Override
            public void menuDeselected(MenuEvent e) {
            }
            @Override
            public void menuCanceled(MenuEvent e) {
            }
        });
    }
}

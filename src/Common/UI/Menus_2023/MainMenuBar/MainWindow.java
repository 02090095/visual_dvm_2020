package Common.UI.Menus_2023.MainMenuBar;
import Visual_DVM_2021.UI.Interface.CallbackWindow;
import Visual_DVM_2021.UI.Interface.ProjectWindow;
import Visual_DVM_2021.UI.Interface.TestingWindow;
public interface MainWindow {
    void Show();
    void ShowProject();
    void ShowNoProject();
    ProjectWindow getProjectWindow();
    CallbackWindow getCallbackWindow();
    //-
    void ShowUpdatesIcon();
    void FocusProject();
    void FocusCallback();
    void FocusTesting();
    TestingWindow getTestingWindow();
    void ShowTestingTab();
    void HideTestingTab();
}

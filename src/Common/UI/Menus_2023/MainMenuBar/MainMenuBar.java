package Common.UI.Menus_2023.MainMenuBar;
import Common.Global;
import Common.UI.Menus_2023.MenuBarButton;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Common.UI.UI;
import Repository.Component.PerformanceAnalyzer.PerformanceAnalyzer;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
public class MainMenuBar extends VisualiserMenuBar {
    JMenu analyses;
    JMenu transformations;
    MenuBarButton components;
    public MainMenuBar() {
        addMenus(new LastOpenedProjectsMenu());
        addPasses(PassCode_2021.OpenCurrentProject, PassCode_2021.CreateEmptyProject);
        addMenus(
                analyses = new AnalysesMenu(),
                transformations = new TransformationsMenu(),
                new GlobalCleaningMenu(),
                new VisualiserSettingsMenu()
        );
        add(components = new MenuBarButton() {
            {
                setToolTipText("Компоненты");
                setIcon("/icons/ComponentsActual.png");
                addActionListener(e -> {
                    if (PerformanceAnalyzer.isActive) {
                        UI.Info("Перед работой с компонентами закройте анализатор производительности!");
                    } else {
                        Pass_2021.passes.get(PassCode_2021.GetComponentsActualVersions).Do();
                        Global.RefreshUpdatesStatus();
                        UI.ShowComponentsWindow();
                    }
                });
            }
        });
        add(new MenuBarButton() {
            {
                setIcon("/icons/Comparsion.png");
                setToolTipText("Анализатор статистик");
                addActionListener(e -> {
                    Global.performanceAnalyzer.Start();
                });
            }
        });
        addPasses(PassCode_2021.ShowInstruction);
        //-
        setPreferredSize(new Dimension(0, 30));
        //---
        /*
        add(new MenuBarButton() {
            {
                setIcon("/icons/Apply.png");
                setToolTipText("Test");
                addActionListener(e -> {
                    Current.getProject().hasSubdirectories();
                });
            }
        });
         */
        //---
        ShowProject(false);
    }
    public void ShowUpdatesIcon() {
        components.setIcon(
                (Global.need_update > 0) || (Global.bad_state > 0)
                        ? "/icons/ComponentsNeedUpdate.gif"
                        : (Global.need_publish > 0 ? "/icons/ComponentsNeedPublish_2023.gif" : "/icons/ComponentsActual.png"));
    }
    public void ShowProject(boolean flag) {
        analyses.setEnabled(flag);
        transformations.setEnabled(flag);
        Pass_2021[] cleaningPasses = new Pass_2021[]{
                Pass_2021.passes.get(PassCode_2021.DropAnalyses),
                Pass_2021.passes.get(PassCode_2021.DropSavedArrays),
                Pass_2021.passes.get(PassCode_2021.CleanAnalyses),
                Pass_2021.passes.get(PassCode_2021.DeleteDebugResults),
                Pass_2021.passes.get(PassCode_2021.ResetCurrentProject)
        };
        for (Pass_2021 pass: cleaningPasses){
            pass.setControlsEnabled(flag);
        }
    }
}

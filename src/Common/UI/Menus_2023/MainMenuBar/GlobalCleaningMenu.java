package Common.UI.Menus_2023.MainMenuBar;
import Common.UI.Menus_2023.VisualiserMenu;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class GlobalCleaningMenu extends VisualiserMenu {
    public GlobalCleaningMenu() {
        super("Очистка", "/icons/Clean.png", false);
        add(Pass_2021.passes.get(PassCode_2021.DropLastProjects).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DropFastAccess).createMenuItem());
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.DeleteDownloadedBugReports).createMenuItem());
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.DropAnalyses).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.CleanAnalyses).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DropSavedArrays).createMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteDebugResults).createMenuItem());
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.ResetCurrentProject).createMenuItem());
    }
}

package Common.UI.Menus_2023.MainMenuBar;
import Common.Global;
import Common.UI.Menus.PropertiesSubmenu;
import Common.UI.Menus_2023.SettingsSubmenu;
import Common.UI.Menus_2023.VisualiserMenu;
import GlobalData.Settings.SettingName;
public class VisualiserSettingsMenu extends VisualiserMenu {
    public VisualiserSettingsMenu() {
        super("Настройки визуализатора", "/icons/Settings.png");
        //-
        add(new PropertiesSubmenu("Подтверждения и уведомления", null,
                "ShowPassesDone",
                "ConfirmPassesStart",
                "FocusPassesResult"
        ));
        add(new SettingsSubmenu("Компактность отображения", null,
                SettingName.SmallScreen,
                SettingName.ShowFullTabsNames,
                SettingName.ShowFullArraysDeclarations,
                SettingName.FastAccessPassesCount,
                SettingName.LastOpenedProjectsCount
        ));
        if (Global.isWindows) {
            add(new SettingsSubmenu("Компиляция на локальной машине", null,
                    SettingName.LocalMakePathWindows,
                    SettingName.Kernels
            ));
        } else {
            add(new SettingsSubmenu("Компиляция на локальной машине", null,
                    SettingName.Kernels
            ));
        }
        add(new SettingsSubmenu("Синхронизация", null,
                SettingName.AutoBugReportsLoad,
                SettingName.AutoTestsLoad
        ));
        add(new SettingsSubmenu("Сравнение", null,
                SettingName.ExtensionsOn,
                SettingName.RegisterOn,
                SettingName.SpacesOn,
                SettingName.EmptyLinesOn,
                SettingName.FortranWrapsOn,
                SettingName.ComparsionDiffMergeOn
        ));
        add(Global.db.settings.get(SettingName.Workspace).getMenuItem());
    }
}

package Common.UI.Menus_2023.MainMenuBar;
import Common.UI.Menus_2023.VisualiserMenu;
import Repository.Component.Sapfor.Sapfor;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class AnalysesMenu extends VisualiserMenu {
    public AnalysesMenu() {
        super("Анализаторы","/icons/Analyses.png" );
        for (PassCode_2021 code : Sapfor.getAnalysesCodes())
            add(Pass_2021.passes.get(code).createMenuItem());
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.SPF_GetGCovInfo).createMenuItem());
    }
}

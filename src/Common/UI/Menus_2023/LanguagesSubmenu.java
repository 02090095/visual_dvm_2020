package Common.UI.Menus_2023;
import Common.Utils.Utils;
import ProjectData.LanguageName;

import javax.swing.*;
import java.awt.event.ActionEvent;
public abstract class LanguagesSubmenu extends VisualiserMenu {
    public LanguagesSubmenu() {
        this("Язык");
    }
    public LanguagesSubmenu(String text) {
        super(text, "/icons/Language.png", true);
        for (LanguageName languageName : LanguageName.values()) {
            if (languageName.equals(LanguageName.fortran) ||
                    (languageName.equals(LanguageName.c) ||
                            (languageName.equals(LanguageName.cpp)))) {

                JMenuItem languageItem = new StableMenuItem(languageName.getDescription());
                String li = languageName.getIcon();
                if (!li.isEmpty())
                    languageItem.setIcon(Utils.getIcon(li));
                languageItem.addActionListener(
                        new AbstractAction() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                action(languageName);
                            }
                        });
                add(languageItem);
            }
        }
    }
    public abstract void action(LanguageName languageName);
}

package Common.UI.Menus_2023.RunConfigurationsMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
public class RunConfigurationsMenuBar extends DataMenuBar {
    public RunConfigurationsMenuBar() {
        super("конфигурации запуска", PassCode_2021.Run,
                PassCode_2021.AddRunConfiguration, PassCode_2021.EditRunConfiguration, PassCode_2021.DeleteRunConfiguration);
        add(new JSeparator());
        addPasses(PassCode_2021.EditProjectRunMaxtime);
    }
}

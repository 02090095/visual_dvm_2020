package Common.UI.Menus_2023.FastAccessMenuBar;
import Common.Global;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Common.UI.UI;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
import java.util.LinkedHashMap;
public class FastAccessMenuBar extends VisualiserMenuBar {
    LinkedHashMap<PassCode_2021, JButton> passesButtons = new LinkedHashMap<>();
    public FastAccessMenuBar() {
        Refresh();
    }
    @Override
    public void setSizeLimits() {
            //если задавать PreffredSize 0, скролл НЕ РАБОТАЕТ. Магия!
    }
    public void showPass(Pass_2021 pass) {
        JButton button = null;
        if (passesButtons.containsKey(pass.code()))
            button = passesButtons.get((pass.code()));
        else {
            button = pass.createButton();
            passesButtons.put(pass.code(), button);
        }
        add(button);
        Dimension d = button.getPreferredSize();
        button.setPreferredSize(new Dimension(d.width, 30));
        revalidate();
        repaint();
    }
    public void Refresh() {
        UI.Clear(this);
        int i = 1;
        for (Pass_2021 pass : Pass_2021.FAPasses) {
            if (pass.stats.HasUsages()) {
                showPass(pass);
                ++i;
                if (i > (Global.db.settings.get(SettingName.FastAccessPassesCount).toInt32())) break;
            }
        }
    }
    public void Drop(){
        UI.Clear(this);
    }
}

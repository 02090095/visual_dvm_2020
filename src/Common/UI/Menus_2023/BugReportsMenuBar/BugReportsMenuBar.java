package Common.UI.Menus_2023.BugReportsMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class BugReportsMenuBar extends DataMenuBar {
    public BugReportsMenuBar() {
        super("отчёты об ошибках",
                PassCode_2021.SynchronizeBugReports,
                PassCode_2021.DownloadAllBugReportsArchives,
                PassCode_2021.AddBugReport,
                PassCode_2021.PublishBugReport,
                PassCode_2021.OpenBugReportTestProject,
                PassCode_2021.OpenBugReport,
                PassCode_2021.UpdateBugReportProgress,
                PassCode_2021.CloseBugReport,
                PassCode_2021.DeleteBugReport);
    }
}

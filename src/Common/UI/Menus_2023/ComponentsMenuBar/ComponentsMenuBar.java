package Common.UI.Menus_2023.ComponentsMenuBar;
import Common.Current;
import Common.UI.Menus_2023.DataMenuBar;
import Common.UI.Menus_2023.VisualiserMenu;
import Common.UI.Themes.VisualiserFonts;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class ComponentsMenuBar extends DataMenuBar {
    public ComponentsMenuBar() {
        super("компоненты");
        addMenus(
                new VisualiserMenu(
                        "Восстановление предыдущей версии компонента", "/icons/Resurrect.png") {
                    {
                        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
                        add(Pass_2021.passes.get(PassCode_2021.ResurrectComponent).createMenuItem());
                        add(Pass_2021.passes.get(PassCode_2021.ResurrectComponentFromServer).createMenuItem());
                    }
                }
        );
        addPasses(PassCode_2021.InstallComponentFromFolder,
                PassCode_2021.UpdateSelectedComponents,
                PassCode_2021.PublishComponent,
                PassCode_2021.ShowComponentChangesLog);
        Pass_2021.passes.get(PassCode_2021.PublishComponent).setControlsVisible(false);
    }
}

package Common.UI.Menus_2023;
public interface PassControl {
    void setIcon(String icon_path);
    void setEnabled(boolean flag);
    void setVisible(boolean flag);
    void setToolTipText(String text);
}

package Common.UI.Menus_2023.GroupsMenuBar;
import Common.Global;
import Common.UI.Menus_2023.DataMenuBar;
import Common.UI.Menus_2023.MenuBarButton;
import Common.Utils.Utils;
import TestingSystem.Group.GroupInterface;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
public class GroupsMenuBar extends DataMenuBar {
    public GroupsMenuBar() {
        super("группы", PassCode_2021.SynchronizeTests, PassCode_2021.DownloadGroup, PassCode_2021.ConvertCorrectnessTests, PassCode_2021.PublishGroup, PassCode_2021.CopyGroups, PassCode_2021.EditGroup, PassCode_2021.DeleteSelectedGroups);
        add(new JSeparator());
        add(new MenuBarButton() {
            {
                setText("Свои");
                setToolTipText("Отображать только группы тестов авторства пользователя");
                Mark();
                addActionListener(e -> {
                    GroupInterface.filterMyOnly = !GroupInterface.filterMyOnly;
                    Mark();
                    Global.testingServer.db.groups.ShowUI();
                });
            }
            public void Mark() {
                setIcon(Utils.getIcon(GroupInterface.filterMyOnly ? "/icons/Pick.png" : "/icons/NotPick.png"));
            }
        });
    }
    public void addFilters(JMenu typesFilterMenu, JMenu languagesFilterMenu) {
       filters =  addMenus(typesFilterMenu, languagesFilterMenu);
    }
    JMenuBar filters= null;
    public void DropFilters() {
        if (filters != null) {
            remove(filters);
            filters = null;
        }
        revalidate();
        repaint();
    }
}

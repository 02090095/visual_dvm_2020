package Common.UI.Menus_2023.RemoteSapforsMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class RemoteSapforsMenuBar extends DataMenuBar {
    public RemoteSapforsMenuBar() {
        super("SAPFOR",  PassCode_2021.InstallRemoteSapfor,
                PassCode_2021.AddSapfor,
                PassCode_2021.EditSapfor,
                PassCode_2021.DeleteSapfor);
    }
}

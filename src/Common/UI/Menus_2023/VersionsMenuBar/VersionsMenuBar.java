package Common.UI.Menus_2023.VersionsMenuBar;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class VersionsMenuBar extends VisualiserMenuBar {
    public VersionsMenuBar(){
        addPasses(PassCode_2021.CreateTestsGroupFromSelectedVersions);
    }
}

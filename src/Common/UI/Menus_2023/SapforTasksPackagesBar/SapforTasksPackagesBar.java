package Common.UI.Menus_2023.SapforTasksPackagesBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class SapforTasksPackagesBar extends DataMenuBar {
    public SapforTasksPackagesBar() {
        super("пакеты задач", PassCode_2021.DeleteSapforTasksPackage);
    }
}

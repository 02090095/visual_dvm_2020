package Common.UI.Menus_2023.TasksPackagesMenuBar;
import Common.Current;
import Common.UI.Menus_2023.DataMenuBar;
import Common.UI.Menus_2023.MenuBarButton;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;
import Common.Utils.Utils;
import TestingSystem.TestingServer;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
import java.awt.*;
public class TasksPackagesMenuBar extends DataMenuBar {
    JButton autorefreshButton;
    JSpinner sCheckTime;
    public TasksPackagesMenuBar() {
        super("пакеты задач", PassCode_2021.SynchronizeTestsTasks, PassCode_2021.AbortSelectedPackages);
        add(autorefreshButton = new MenuBarButton() {
            {
                setText("проверка раз в");
                setToolTipText("автоматическое обновление состояния пакета задач");
                Mark();
                addActionListener(e -> {
                    TestingServer.checkTasks = !TestingServer.checkTasks;
                    TestingServer.switchTimer(TestingServer.checkTasks);
                    Mark();
                });
            }
            public void Mark() {
                setIcon(Utils.getIcon(TestingServer.checkTasks ? "/icons/Pick.png" : "/icons/NotPick.png"));
            }
        });
        add(sCheckTime = new JSpinner());
        sCheckTime.setPreferredSize(new Dimension(60, 26));
        sCheckTime.setMaximumSize(new Dimension(60, 26));
        sCheckTime.setModel(new SpinnerNumberModel(TestingServer.checkIntervalSecond, 10, 3600, 1));
        UI.MakeSpinnerRapid(sCheckTime, e -> {
            TestingServer.checkIntervalSecond = (int) sCheckTime.getValue();
            if (TestingServer.checkTasks) TestingServer.ResetTimer();
        });
        add(new JLabel(" сек") {
            {
                setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeItalic));
            }
        });
    }

    public void ShowAutorefresh() {
        autorefreshButton.setIcon(Utils.getIcon(TestingServer.checkTasks ? "/icons/Pick.png" : "/icons/NotPick.png"));
    }
}

package Common.UI.Menus_2023.MakefilesMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class MakefilesMenuBar extends DataMenuBar {
    public MakefilesMenuBar() {
        super("мейкфайлы",
                PassCode_2021.Compile,
                PassCode_2021.AddMakefile,
                PassCode_2021.EditMakefile,
                PassCode_2021.DeleteMakefile);
        addSeparator();
        addPasses(PassCode_2021.ShowMakefilePreview, PassCode_2021.EditProjectCompilationMaxtime);
    }
}

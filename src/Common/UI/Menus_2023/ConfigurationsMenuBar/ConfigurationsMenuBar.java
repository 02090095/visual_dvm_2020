package Common.UI.Menus_2023.ConfigurationsMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class ConfigurationsMenuBar extends DataMenuBar {
    public ConfigurationsMenuBar() {
        super("конфигурации");
        /*
        add(new MenuBarButton() {
            {
                setText("Оповещение по email");
                setToolTipText("Оповещение о прогрессе выполнения пакета тестов");
                Mark();
                addActionListener(e -> {
                    email = !email;
                    Mark();
                });
            }
            public void Mark() {
                setIcon(Utils.getIcon(email ? "/icons/Pick.png" : "/icons/NotPick.png"));
            }

        });
         */
        addPasses(
                PassCode_2021.EditMachineKernels,
                PassCode_2021.StartTests,
                PassCode_2021.PublishConfiguration,
                PassCode_2021.CopyConfigurations,
                PassCode_2021.EditConfiguration,
                PassCode_2021.DeleteSelectedConfigurations
        );
    }
}

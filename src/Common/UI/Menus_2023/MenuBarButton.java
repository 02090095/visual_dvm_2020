package Common.UI.Menus_2023;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;

import javax.swing.*;
import java.awt.*;
//https://java-online.ru/swing-jbutton.xhtml
public class MenuBarButton extends JButton {
    public MenuBarButton() {
        super();
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        //
        setBorderPainted(false);
        setContentAreaFilled(false);
        setOpaque(false);
        //setFocusPainted(false);
        //-
        setMinimumSize(new Dimension(38, 30)); //иначе сужаются вертикально.
    }
    @Override
    protected void paintComponent(Graphics g) {
        if (getModel().isPressed()) {
            g.setColor(new Color(163, 184, 204));
            g.fillRect(0, 0, getWidth(), getHeight());
        }
        super.paintComponent(g);
    }
    public void setIcon(String icon_path) {
        setIcon(Utils.getIcon(icon_path));
    }
}

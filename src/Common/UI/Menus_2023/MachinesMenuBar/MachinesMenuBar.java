package Common.UI.Menus_2023.MachinesMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class MachinesMenuBar extends DataMenuBar {
    public MachinesMenuBar() {
        super("машины",
                PassCode_2021.AddMachine,
                PassCode_2021.EditMachine,
                PassCode_2021.DeleteMachine);
    }
}

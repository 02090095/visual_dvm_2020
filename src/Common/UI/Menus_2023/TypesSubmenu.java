package Common.UI.Menus_2023;
import ProjectData.Files.FileType;

import javax.swing.*;
import java.awt.event.ActionEvent;
public abstract class TypesSubmenu extends VisualiserMenu {
    public TypesSubmenu() {
        this("Тип");
    }
    public TypesSubmenu(String text) {
        super(text, "/icons/type.png", true);
        for (FileType fileType : FileType.values()) {
            if (fileType != FileType.forbidden) {
                JMenuItem m = new StableMenuItem(fileType.getDescription());
                m.addActionListener(new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        action(fileType);
                    }
                });
                add(m);
            }
        }
    }
    public abstract void action(FileType fileType);
}

package Common.UI.Menus_2023;
import Visual_DVM_2021.Passes.Pass_2021;

import java.awt.*;
public class PassButton extends MenuBarButton implements PassControl {
    public PassButton(Pass_2021 pass, boolean tab) {
        setText(pass.getButtonText());
        setToolTipText(pass.getDescription());
        if (pass.getIconPath() != null) {
            if (tab) {
                setIcon(pass.getTabIcon());
                setPreferredSize(new Dimension(18,18));
                setMaximumSize(new Dimension(18,18));
                setMinimumSize(new Dimension(18,18));
            }
            else
               setIcon(pass.getIconPath());
        }
        addActionListener(pass.getControlAction());
        pass.controls.add(this);
    }
    public PassButton(Pass_2021 pass) {
        this(pass, false);
    }
}

package Common.UI.Menus_2023.UsersMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class UsersMenuBar extends DataMenuBar {
    public UsersMenuBar() {
        super("пользователи",  PassCode_2021.AddUser,
                PassCode_2021.EditUser,
                PassCode_2021.InitialiseUser,
                PassCode_2021.DeleteUser);
    }
}

package Common.UI.Menus_2023.SapforTasksMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class SapforTasksMenuBar extends DataMenuBar {
    public SapforTasksMenuBar() {
        super("задачи", PassCode_2021.OpenSapforTest);
    }
}

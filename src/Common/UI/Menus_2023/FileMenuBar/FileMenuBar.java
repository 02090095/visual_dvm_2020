package Common.UI.Menus_2023.FileMenuBar;
import Common.UI.Menus_2023.MenuBarButton;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Common.UI.UI;
import ProjectData.Files.UI.Editor.SPFEditor;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
import java.awt.*;
public class FileMenuBar extends VisualiserMenuBar {
    public JSpinner sToGo;
    JLabel LineCountLabel;
    public JLabel CurrentSymbolLabel;
    SPFEditor editor;
    FileSettingsMenu fileSettingsMenu;
    public FileMenuBar(SPFEditor editor_in) {
        editor = editor_in;
        add(new MenuBarButton() {
            {
                setToolTipText("Поиск(Ctrl+F)");
                setIcon("/icons/LastOpened.png");
                addActionListener(e -> UI.ShowSearchForm());
            }
        });
        addPasses(PassCode_2021.Save);
        add(new MenuBarButton() {
            {
                setToolTipText("Увеличить шрифт(Ctrl+'+')");
                setIcon("/icons/Editor/Font+.png");
                addActionListener(e -> editor.FontUp());
            }
        });
        add(new MenuBarButton() {
            {
                setToolTipText("Уменьшить шрифт(Ctrl+'-')");
                setIcon("/icons/Editor/Font-.png");
                addActionListener(e -> editor.FontDown());
            }
        });
        add(new MenuBarButton() {
            boolean isOn = false;
            {
                setToolTipText("Отображать спецсимволы");
                setIcon("/icons/Editor/ShowNoSigns.png");
                addActionListener(e-> {
                    isOn = !isOn;
                    if (isOn) {
                        setIcon("/icons/Editor/ShowAllSigns.png");
                        setToolTipText("Скрыть спецсимволы");
                        editor.setWhitespaceVisible(true);
                        editor.setEOLMarkersVisible(true);
                    } else {
                        setIcon("/icons/Editor/ShowNoSigns.png");
                        setToolTipText("Отображать спецсимволы");
                        editor.setWhitespaceVisible(false);
                        editor.setEOLMarkersVisible(false);
                    }
                });
            }
        });
        add(new JLabel(" Строка "));
        add(sToGo = new JSpinner());
        sToGo.setPreferredSize(new Dimension(60, 25));
        sToGo.setMaximumSize(new Dimension(60, 25));
        add(new JLabel(" из "));
        add(LineCountLabel = new JLabel("?"));
        add(new JLabel(" | "));
        add(new JLabel("Позиция "));
        add(CurrentSymbolLabel = new JLabel());
        add(new JSeparator());
        addMenus(fileSettingsMenu = new FileSettingsMenu());
        //--
       // addPasses(PassCode_2021.CloseCurrentFile);
        //-
        setPreferredSize(new Dimension(0, 30));
    }
    public void ShowLinesCount() {
        LineCountLabel.setText(String.valueOf(editor.getLineCount()));
    }
    //-
    public void ShowLanguage(){fileSettingsMenu.ShowLanguage();}
    public void ShowType(){fileSettingsMenu.ShowType();}
    public void ShowStyle(){fileSettingsMenu.ShowStyle();}
}

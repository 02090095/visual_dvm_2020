package Common.UI.Menus_2023.FileMenuBar;
import Common.Current;
import Common.UI.Menus_2023.LanguagesSubmenu;
import Common.UI.Menus_2023.StylesSubmenu;
import Common.UI.Menus_2023.TypesSubmenu;
import Common.UI.Menus_2023.VisualiserMenu;
import Common.UI.UI;
import ProjectData.Files.FileType;
import ProjectData.Files.LanguageStyle;
import ProjectData.LanguageName;

import javax.swing.*;
public class FileSettingsMenu extends VisualiserMenu {
    JMenu mLanguage;
    JMenu mStyle;
    JMenu mType;
    public FileSettingsMenu() {
        super("Настройки файла", "/icons/Settings.png");
        add(mLanguage = new LanguagesSubmenu() {
            @Override
            public void action(LanguageName languageName) {
                if (Current.getFile().UpdateLanguage(languageName)) {
                    Current.getSapfor().ResetAllAnalyses();
                    Current.getFile().form.ShowLanguage();
                    UI.getMainWindow().getProjectWindow().getFilesTreeForm().getTree().RefreshNode(Current.getFile().node);
                }
            }
        });
        ShowLanguage();
        //--
        add(mStyle = new StylesSubmenu() {
            @Override
            public void action(LanguageStyle languageStyle) {
                if (Current.getFile().UpdateStyle(languageStyle)) {
                    Current.getSapfor().ResetAllAnalyses();
                    Current.getFile().form.ShowStyle();
                }
            }
        });
        ShowStyle();
        //--
        add(mType = new TypesSubmenu() {
            @Override
            public void action(FileType fileType) {
                if (Current.getFile().UpdateType(fileType)) {
                    Current.getSapfor().ResetAllAnalyses();
                    UI.getMainWindow().getProjectWindow().getFilesTreeForm().getTree().RefreshNode(Current.getFile().node);
                    Current.getFile().form.ShowType();
                }
            }
        });
        ShowType();
    }
    public void ShowLanguage() {
        mLanguage.setText("Язык: " + Current.getFile().languageName.getDescription());
    }
    public void ShowStyle() {
        mStyle.setText("Стиль: " + Current.getFile().style.getDescription());
    }
    public void ShowType() {
        mType.setText("Тип: " + Current.getFile().fileType.getDescription());
    }
}

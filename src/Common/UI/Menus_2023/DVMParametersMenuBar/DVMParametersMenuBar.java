package Common.UI.Menus_2023.DVMParametersMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class DVMParametersMenuBar extends DataMenuBar {
    public DVMParametersMenuBar() {
        super("параметры",
                PassCode_2021.AddDVMParameter,
                PassCode_2021.EditDVMParameter,
                PassCode_2021.DeleteDVMParameter);
    }
}

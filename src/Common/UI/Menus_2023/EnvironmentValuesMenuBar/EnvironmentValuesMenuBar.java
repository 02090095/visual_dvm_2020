package Common.UI.Menus_2023.EnvironmentValuesMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class EnvironmentValuesMenuBar extends DataMenuBar {
    public EnvironmentValuesMenuBar() {
        super("переменные окружения",  PassCode_2021.AddEnvironmentValue,
                PassCode_2021.EditEnvironmentValue,
                PassCode_2021.DeleteEnvironmentValue,
                PassCode_2021.PickCompilerEnvironments
                );
    }
}

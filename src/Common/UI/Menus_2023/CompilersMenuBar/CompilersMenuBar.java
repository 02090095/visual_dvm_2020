package Common.UI.Menus_2023.CompilersMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class CompilersMenuBar extends DataMenuBar {
    public CompilersMenuBar() {
        super("компиляторы",
                PassCode_2021.AddCompiler,
                PassCode_2021.EditCompiler,
                PassCode_2021.DeleteCompiler,
                PassCode_2021.ShowCompilerVersion,
                PassCode_2021.ShowCompilerHelp);
    }
}

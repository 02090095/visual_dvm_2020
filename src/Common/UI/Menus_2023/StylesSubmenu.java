package Common.UI.Menus_2023;
import ProjectData.Files.LanguageStyle;

import javax.swing.*;
import java.awt.event.ActionEvent;
public abstract class StylesSubmenu extends VisualiserMenu {
    public StylesSubmenu() {
        this("Стиль");
    }
    public StylesSubmenu(String text) {
        super(text, "/icons/Style.png", true);
        for (LanguageStyle languageStyle : LanguageStyle.values()) {
            JMenuItem m = new StableMenuItem(languageStyle.getDescription());
            m.addActionListener(
                    new AbstractAction() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            action(languageStyle);
                        }
                    });
            add(m);
        }
    }
    public abstract void action(LanguageStyle languageStyle);
}

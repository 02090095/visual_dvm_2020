package Common.UI.Menus_2023.SapforConfigurationsMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class SapforConfigurationsMenuBar extends DataMenuBar {
    public SapforConfigurationsMenuBar() {
        super("конфигурации",  PassCode_2021.StartSapforTests,
                PassCode_2021.PublishSapforConfiguration,
                PassCode_2021.EditSapforConfiguration,
                PassCode_2021.DeleteSapforConfiguration
        );
    }
}

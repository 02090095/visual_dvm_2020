package Common.UI.Menus_2023;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;

import javax.swing.*;
import java.awt.*;
public class VisualiserMenu extends JMenu {
    public VisualiserMenu(String text, String iconPath, boolean textVisible) {
        setMinimumSize(new Dimension(38, 30)); //иначе сужаются вертикально.
        setToolTipText(text);
        if (textVisible)
            setText(text);
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeItalic));
        if (!iconPath.isEmpty())
            setIcon(Utils.getIcon(iconPath));
    }
    public VisualiserMenu(String text, String iconPath) {
        this(text, iconPath, false);
    }
}

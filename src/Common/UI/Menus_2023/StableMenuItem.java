package Common.UI.Menus_2023;
import Common.UI.Menus.VisualiserMenuItem;
import Common.UI.UI;
import Common.Utils.Utils;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuItemUI;
//неичезающий меню итем. нужен для настроек
//https://translated.turbopages.org/proxy_u/en-ru.ru.64537f6c-6460c460-8e74a1ab-74722d776562/https/tips4java.wordpress.com/2010/09/12/keeping-menus-open/
class StableItemUI extends BasicMenuItemUI {
    public static ComponentUI createUI(JComponent c) {
        return new StableItemUI();
    }
    @Override
    protected void doClick(MenuSelectionManager msm) {
        menuItem.doClick(0);
        if (UI.last_menu_path != null)
            MenuSelectionManager.defaultManager().setSelectedPath(UI.last_menu_path);
    }
}
public class StableMenuItem extends VisualiserMenuItem {
    {
        getModel().addChangeListener(e -> {
            if (getModel().isArmed() && isShowing())
                UI.last_menu_path = MenuSelectionManager.defaultManager().getSelectedPath();
        });
    }
    public StableMenuItem(String text) {
        super(text);
        setUI(new StableItemUI());
    }
    public StableMenuItem(String text, String icon_path) {
        super(text);
        setIcon(Utils.getIcon(icon_path));
        setUI(new StableItemUI());
    }
    public StableMenuItem() {
        setUI(new StableItemUI());
    }
}

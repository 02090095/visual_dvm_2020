package Common.UI.Menus_2023.SubscribersMenuBar;
import Common.UI.Menus_2023.DataMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
public class SubscribersMenuBar extends DataMenuBar {
    public SubscribersMenuBar() {
        super("Адресаты",
                PassCode_2021.SaveBugReportExecutor,
                PassCode_2021.SaveBugReportRecipients,
                PassCode_2021.AddSubscriber,
                PassCode_2021.EditSubscriber,
                PassCode_2021.DeleteSubscriber);
    }
}

package Common.UI.Menus_2023.ProjectMenuBar;
import Common.Current;
import Common.Global;
import Common.UI.Menus_2023.LanguagesSubmenu;
import Common.UI.Menus_2023.SettingsSubmenu;
import Common.UI.Menus_2023.StylesSubmenu;
import Common.UI.Menus_2023.VisualiserMenu;
import GlobalData.Settings.SettingName;
import ProjectData.Files.LanguageStyle;
import ProjectData.LanguageName;

import javax.swing.*;
public class ProjectSettingsMenu extends VisualiserMenu {
    JMenu mLanguage;
    JMenu mStyle;
    public ProjectSettingsMenu() {
        super("Настройки проекта", "/icons/Settings.png");
        add(mLanguage = new LanguagesSubmenu() {
            @Override
            public void action(LanguageName languageName) {
                if (Current.getProject().UpdateLanguage(languageName)) {
                    Current.getSapfor().ResetAllAnalyses();
                    ShowLanguage();
                }
            }
        });
        ShowLanguage();
        //--
        add(mStyle = new StylesSubmenu() {
            @Override
            public void action(LanguageStyle languageStyle) {
                if (Current.getProject().UpdateStyle(languageStyle))
                    ShowStyle();
            }
        });
        ShowStyle();
        addSeparator();
        add(new SettingsSubmenu("Анализ", null,
                SettingName.Precompilation,
                SettingName.STATIC_SHADOW_ANALYSIS,
                SettingName.KEEP_DVM_DIRECTIVES,
                SettingName.IGNORE_IO_SAPFOR,
                SettingName.MPI_PROGRAM,
                SettingName.ANALYSIS_OPTIONS,
                SettingName.PARALLELIZE_FREE_LOOPS
        ));
        addSeparator();
        add(new SettingsSubmenu("Построение системы интервалов", null,
                SettingName.KEEP_LOOPS_CLOSE_NESTING,
                SettingName.KEEP_GCOV
        ));
        addSeparator();
        add(new SettingsSubmenu("Построение версий", null,
                SettingName.FREE_FORM,
                SettingName.KEEP_SPF_DIRECTIVES,
                SettingName.KEEP_SPF_DIRECTIVES_AMONG_TRANSFORMATIONS,
                SettingName.OUTPUT_UPPER,
                SettingName.MAX_SHADOW_WIDTH,
                SettingName.DVMConvertationOptions,
                SettingName.SaveModifications
        ));
        addSeparator();
        add(Global.db.settings.get(SettingName.TRANSLATE_MESSAGES).getMenuItem());
        add(Global.db.settings.get(SettingName.DEBUG_PRINT_ON).getMenuItem());
        add(Global.db.settings.get(SettingName.GCOVLimit).getMenuItem());
    }
    public void ShowLanguage() {
        mLanguage.setText("Язык: " + Current.getProject().languageName.getDescription());
    }
    public void ShowStyle() {
        mStyle.setText("Стиль: " + Current.getProject().style.getDescription());
    }
}

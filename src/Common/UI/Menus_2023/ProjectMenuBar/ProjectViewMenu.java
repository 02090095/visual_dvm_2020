package Common.UI.Menus_2023.ProjectMenuBar;
import Common.Current;
import Common.UI.Menus_2023.VisualiserMenu;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.ProjectView;

import javax.swing.*;
import java.util.LinkedHashMap;
public class ProjectViewMenu extends VisualiserMenu {
    LinkedHashMap<ProjectView, JMenuItem> views;
    public ProjectViewMenu() {
        super("", "");
        views = new LinkedHashMap<>();
        for (ProjectView view : ProjectView.values()) {
            JMenuItem m = new JMenuItem(view.getDescription()) {
                {
                    setIcon(Utils.getIcon(view.getIcon()));
                    setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeItalic));
                    addActionListener(e -> {
                        Current.set(Current.ProjectView, view);
                        UI.getMainWindow().getProjectWindow().ShowProjectView();
                    });
                }
            };
            add(m);
            views.put(view, m);
        }
    }
    public void SelectView(ProjectView view){
        views.get(view).doClick();
    }
}

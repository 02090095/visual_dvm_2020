package Common.UI.Menus_2023.ProjectMenuBar;
import Common.UI.Menus_2023.MenuBarButton;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Common.UI.UI;

import javax.swing.*;
import java.awt.*;
public class ProjectMenuBar extends VisualiserMenuBar {
    public ProjectViewMenu projectViewMenu;
    public ProjectMenuBar() {
        addMenus(projectViewMenu = new ProjectViewMenu());
        add(new JSeparator());
        addMenus(
                new ProjectSettingsMenu()
        );
        add(new MenuBarButton() {
            {
                setToolTipText("Профили");
                setIcon("/icons/Profiles.png");
                addActionListener(e -> {
                    UI.ShowProfilesWindow();
                });
            }
        });
    }
    public ProjectViewMenu getProjectViewMenu() {
        return projectViewMenu;
    }
    @Override
    public void setSizeLimits() {
        setPreferredSize(new Dimension(0, 32));
    }
}

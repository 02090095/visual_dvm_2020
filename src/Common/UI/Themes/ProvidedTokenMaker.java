package Common.UI.Themes;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMaker;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rsyntaxtextarea.TokenMap;

import javax.swing.text.Segment;
//объект создается 1 раз, при установке стиля редактора.
//НИ В КОЕМ СЛУЧАЕ НЕЛЬЗЯ ПЕРЕДАВАТЬ ПО ССЫЛКЕ методам ради рефакторинга
//все переменные должны быть сугубо локальными
//иначе возможны непредсказуемые последствия.
//Метод вызывается асинхронно, причем несколькими событиями)
public abstract class ProvidedTokenMaker extends AbstractTokenMaker {
    public static void fillTokenMap(TokenMap map, int type, String... words) {
        for (String word : words)
            map.put(word, type);
    }
    //сохранить не забирая текущий символ.
    public void SaveCurrent(TokenProvider provider) {
        addToken(provider.text, provider.currentTokenStart,
                provider.i - 1, provider.currentTokenType,
                provider.newStartOffset + provider.currentTokenStart);
    }
    //сохранить забирая текущий символ
    public void SaveCurrent_(TokenProvider provider) {
        addToken(provider.text, provider.currentTokenStart,
                provider.i, provider.currentTokenType,
                provider.newStartOffset + provider.currentTokenStart);
    }
    public abstract void Body(TokenProvider provider);
    public abstract void performFinish(TokenProvider provider);
    public Token getTokenList(Segment text, int startTokenType, int startOffset) {
        //  System.out.println(Utils.Brackets(text.toString()));
        resetTokenList();
        //структура для хранения индексов смещений, текущего состояния и т д.
        TokenProvider provider = new TokenProvider(text, startTokenType, startOffset);
        // provider.checkFortranWrap();
        // System.out.println(this.);
        while (provider.canRead()) {
            provider.readNext();
            Body(provider);
            provider.gotoNext();
        }
        performFinish(provider);
        // Return the first token in our linked list.
        return firstToken;
    }
}

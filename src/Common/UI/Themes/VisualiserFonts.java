package Common.UI.Themes;
public enum VisualiserFonts {
    GoodState,
    ReadyState,
    BadState,
    Fatal,
    ProgressState,
    UnknownState,
    Hyperlink,
    Disabled,
    //бесцветные
    Distribution,
    //---
    TreeItalic,
    TreePlain,
    TreeBold,
    TreeBoldItalic,
    BlueState,
    NewVersion, //---
    Menu
}

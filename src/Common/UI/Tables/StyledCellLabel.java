package Common.UI.Tables;
import Common.Current;
import Common.UI.Themes.ThemeElement;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;
//наиболее распространенный случай. переотображение текста и/или изображения в ячейке таблицы.
public class StyledCellLabel extends JLabel implements ThemeElement {
    public StyledCellLabel() {
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreePlain));
        setHorizontalAlignment(SwingConstants.LEFT);
        setVerticalAlignment(SwingConstants.CENTER);
        setOpaque(true);
        applyTheme();
    }
    @Override
    public void applyTheme() {
        setBackground(Current.getTheme().table_background);
        setForeground(Current.getTheme().foreground);
    }
}

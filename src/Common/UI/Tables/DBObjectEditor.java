package Common.UI.Tables;
import Common.Database.DBObject;
import Common.UI.DataControl;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.util.EventObject;
import java.util.Objects;
public abstract class DBObjectEditor<T extends DBObject> extends EditorCell implements TableCellEditor {
    //задается при редактировании клетки.
    public T value = null;
    protected transient ChangeEvent changeEvent;
    public abstract void Action();
    public void InitValue(JTable table, Object value_in, int row, int column) {
        value = (T) ((DataControl) table).getRowObject(row);
    }
    @Override
    public Component getTableCellEditorComponent(
            JTable table, Object value_in, boolean isSelected, int row, int column) {
        this.setBackground(table.getSelectionBackground());
        InitValue(table, value_in, row, column);
        Action();
        return this;
    }
    //Copied from AbstractCellEditor
    //protected EventListenerList listenerList = new EventListenerList();
    @Override
    public boolean isCellEditable(EventObject e) {
        return true;
    }
    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }
    @Override
    public boolean stopCellEditing() {
        fireEditingStopped();
        return true;
    }
    @Override
    public void cancelCellEditing() {
        fireEditingCanceled();
    }
    @Override
    public void addCellEditorListener(CellEditorListener l) {
        listenerList.add(CellEditorListener.class, l);
    }
    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        listenerList.remove(CellEditorListener.class, l);
    }
    public CellEditorListener[] getCellEditorListeners() {
        return listenerList.getListeners(CellEditorListener.class);
    }
    protected void fireEditingStopped() {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == CellEditorListener.class) {
                // Lazily create the event:
                if (Objects.isNull(changeEvent)) {
                    changeEvent = new ChangeEvent(this);
                }
                ((CellEditorListener) listeners[i + 1]).editingStopped(changeEvent);
            }
        }
    }
    protected void fireEditingCanceled() {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == CellEditorListener.class) {
                // Lazily create the event:
                if (Objects.isNull(changeEvent)) {
                    changeEvent = new ChangeEvent(this);
                }
                ((CellEditorListener) listeners[i + 1]).editingCanceled(changeEvent);
            }
        }
    }
}

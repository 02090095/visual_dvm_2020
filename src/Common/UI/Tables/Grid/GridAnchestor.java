package Common.UI.Tables.Grid;
import javax.swing.table.AbstractTableModel;
import java.util.Collection;
import java.util.Vector;
public abstract class GridAnchestor extends AbstractTableModel {
    public Vector<Object> data = new Vector<>();
    protected Vector<String> columnNames = new Vector<>(); //массив имен столбцов.
    public GridAnchestor(Collection columnNames_in, Collection data_in) {
        columnNames.addAll(columnNames_in);
        data.addAll(data_in);
    }
    @Override
    public String getColumnName(int col) {
        return columnNames.get(col);
    }
    @Override
    public int getColumnCount() {
        return columnNames.size();
    }
    @Override
    public int getRowCount() {
        return data.size();
    }
    @Override
    public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
    }
}

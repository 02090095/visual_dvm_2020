package Common.UI.Tables;
import Common.Database.DataSet;
import Common.UI.TextField.StyledTextField;
import Common.Utils.Utils;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
public class ColumnFilter {
    public JTextField textField;
    public JPopupMenu popup;
    public ColumnFilter(DataSet dataSet, int columnIndex) {
        textField = new StyledTextField() {
            {
                setBorder(null);
                addActionListener(e -> {
                    popup.setVisible(false);
                    dataSet.ui_.control.getTableHeader().repaint();
                });
                getDocument().addDocumentListener(new DocumentListener() {
                    @Override
                    public void insertUpdate(DocumentEvent e) {
                        dataSet.changeColumnFilterValue(columnIndex, getText());
                        dataSet.ShowUI();
                    }
                    @Override
                    public void removeUpdate(DocumentEvent e) {
                        dataSet.changeColumnFilterValue(columnIndex, getText());
                        dataSet.ShowUI();
                    }
                    @Override
                    public void changedUpdate(DocumentEvent e) {
                    }
                });
            }
        };
        popup = new JPopupMenu() {
            {
                setBorder(new MatteBorder(0, 1, 1, 1, Color.DARK_GRAY));
            }
        };
        popup.add(textField);
        //--
        dataSet.getUi().control.getColumnModel().getColumn(columnIndex).setHeaderRenderer((table, value, isSelected, hasFocus, row, column1) -> new JLabel() {
            {
                setIcon(Utils.getIcon("/icons/Filter.png"));
                setForeground(dataSet.getUi().control.getTableHeader().getForeground());
                setBackground(dataSet.getUi().control.getTableHeader().getBackground());
                setFont(dataSet.getUi().control.getTableHeader().getFont());
                setBorder(new MatteBorder(0, 0, 1, 1, Color.DARK_GRAY));
                setText("текст : "+dataSet.getColumnFilterValue(columnIndex));
            }
        });
        //--
        dataSet.getUi().control.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent event) {
                if (event.getClickCount() == 1) {
                    int columnIndex = dataSet.getUi().control.getTableHeader().columnAtPoint(event.getPoint());
                    if (dataSet.columnsFilters.containsKey(columnIndex)) {
                        Rectangle columnRectangle = dataSet.getUi().control.getTableHeader().getHeaderRect(columnIndex);
                        Dimension d =   new Dimension(columnRectangle.width - 72, columnRectangle.height - 1);
                        popup.setPreferredSize(d);
                        popup.setMaximumSize(d);
                        popup.show(dataSet.getUi().control.getTableHeader(), columnRectangle.x + 72, 0);
                        textField.setText(dataSet.getColumnFilterValue(columnIndex).toString());
                        textField.requestFocusInWindow();
                        textField.selectAll();
                    }
                }
            }
        });
    }
}

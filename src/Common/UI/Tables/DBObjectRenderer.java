package Common.UI.Tables;
import Common.Database.DBObject;
import Common.UI.DataControl;

import javax.swing.*;
public abstract class DBObjectRenderer extends RendererCell<DBObject> {
    @Override
    public DBObject Init(JTable table, Object value, int row, int column) {
        return ((DataControl) table).getRowObject(row);
    }
}
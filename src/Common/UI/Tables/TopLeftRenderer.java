package Common.UI.Tables;
import javax.swing.*;
public class TopLeftRenderer extends RendererCell {
    @Override
    public Object Init(JTable table, Object value, int row, int column) {
        return value;
    }
    @Override
    public void Display() {
        if (value != null)
            setText(value.toString());
    }
}

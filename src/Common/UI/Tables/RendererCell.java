package Common.UI.Tables;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
public abstract class RendererCell<T> extends StyledCellLabel implements TableCellRenderer {
    public T value;
    public abstract T Init(JTable table, Object value, int row, int column); //получить значение
    public abstract void Display(); //отобразить его.
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value_in, boolean isSelected, boolean hasFocus, int row, int column) {
        setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
        value = Init(table, value_in, row, column);
        Display();
        return this;
    }
}

package Common.UI.Tables;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;
import java.util.Vector;
public class HiddenListRenderer extends RendererCell<Vector<String>> {
    @Override
    public Vector<String> Init(JTable table, Object value, int row, int column) {
        return (Vector<String>) value;
    }
    @Override
    public void Display() {
        setText(String.join(";", value));
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreePlain));
    }
}

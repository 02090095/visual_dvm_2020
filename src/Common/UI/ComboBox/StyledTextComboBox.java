package Common.UI.ComboBox;
import Common.UI.Menus.TextComboBoxMenu;

import javax.swing.*;
public class StyledTextComboBox extends JComboBox<String> {
    public StyledTextComboBox() {
        setComponentPopupMenu(new TextComboBoxMenu(this));
    }
}

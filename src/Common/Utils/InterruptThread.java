package Common.Utils;
import ProjectData.Project.db_project_info;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.concurrent.Callable;
public class InterruptThread extends Thread{
    //------------
    public static final String Eliminated = "Eliminated";
    public InterruptThread(int sleep_ms, Callable action){
        super(() -> {
            File interruptFile = new File(db_project_info.interrupt);
            try {
                while (true) {
                    Thread.sleep(sleep_ms);
                    if (interruptFile.exists()) {
                        FileUtils.writeStringToFile(new File(Eliminated + " by INTERRUPT file"), "");
                        FileUtils.forceDelete(interruptFile);
                        action.call();
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }
}

package Common.Utils.Validators;
import Common.Utils.TextLog;
public class Validator {
    protected char c;
    protected int i;
    protected String string_name;
    protected char[] chars;
    protected TextLog Log;
    public Validator(String string, String string_name_in, TextLog log_in) {
        string_name = string_name_in;
        chars = string.toCharArray();
        Log = log_in;
        reset();
    }
    protected void reset() {
    }
    protected int getStartIndex() {
        return 0;
    }
    protected boolean continueCondition() {
        return true;
    }
    protected void Body() {
    }
    protected void PerformFinish() {
    }
    public void Validate() {
        for (i = getStartIndex(); i < chars.length && continueCondition(); ++i) {
            c = chars[i];
            System.out.print(c);
            Body();
        }
        System.out.println();
        PerformFinish();
    }
}

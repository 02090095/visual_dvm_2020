package Common.Utils.Files;
import Common.Utils.Utils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
public class VDirectoryChooser extends VFileChooser_ {
    public VDirectoryChooser(String title) {
        super(title, new FileFilter() {
            @Override
            public boolean accept(File f) {
                return !Utils.ContainsCyrillic(f.getAbsolutePath());
            }
            @Override
            public String getDescription() {
                return "Все папки";
            }
        });
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
}

package Common.Utils.Files;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
public class ProjectsChooser extends VFileChooser_ {
    public ProjectsChooser(String title) {
        super(title, new FileFilter() {
            @Override
            public boolean accept(File f) {
                return
                        !Utils.ContainsCyrillic(f.getAbsolutePath()) &&
                                !f.getName().equalsIgnoreCase(db_project_info.data)
                        ;
            }
            @Override
            public String getDescription() {
                return "Все папки";
            }
        });
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }
}

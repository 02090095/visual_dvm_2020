package Common.Database;
import Common.Utils.Utils;
import com.sun.org.glassfish.gmbal.Description;
public abstract class nDBObject extends DBObject {
    String getClassNameL() {
        return getClass().getSimpleName().toLowerCase();
    }
    @Description("PRIMARY KEY, UNIQUE")
    public String id = "";
    @Override
    public Object getPK() {
        return id;
    }
    @Override
    public String getFKName() {
        return getClassNameL() + "_id";
    }
    @Override
    public Object getEmptyFK() {
        return "";
    }
    public void genName() {
        id = Utils.getDateName(getClassNameL());
    }
    //-
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        id = ((nDBObject)src).id;
    }
    public nDBObject(nDBObject src){
        this.SynchronizeFields(src);
    }
    public nDBObject(){}
}

package Common.Database;
import Common.UI.Menus_2023.StableMenuItem;
import Common.Utils.Utils;

import javax.swing.*;
public class TableFilter<D extends DBObject> {
    DataSet table;
    public JMenuItem menuItem;   //пункт меню фильтра. ( возможно потом сделать и кнопку)
    String description;
    boolean active = false; //включен ли фильтр
    public int count = 0;
    protected boolean validate(D object) {
        return true;
    }
    public boolean Validate(D object) {
        boolean valid;
        if (valid=validate(object))
            count++;
        return !active || valid;
    }
    static String getNotActiveIconPath() {
        return "/icons/NotPick.png";
    }
    static String getActiveIconPath() {
        return "/icons/Pick.png";
    }
    public TableFilter(DataSet table_in, String description_in) {
        table = table_in;
        menuItem = new StableMenuItem((description = description_in)+" (0)");
        menuItem.addActionListener(e -> {
            active = !active;
            Mark();
            table.ShowUI();
        });
        Mark();
    }
    public void Mark() {
        menuItem.setIcon(Utils.getIcon(active ? getActiveIconPath() : getNotActiveIconPath()));
    }
    public void ShowDescriptionAndCount() {
        menuItem.setText(description + " " + Utils.RBrackets(count));
    }
}

package Common.Database;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Vector;
public class DBTableColumn {
    public String Name = "";
    public ColumnType type = ColumnType.UNDEFINED;
    public Vector<String> attributes = new Vector<String>();
    public boolean Ignore = false;
    public boolean PrimaryKey = false;
    public boolean AutoIncrement = false;
    public Object default_value = null;
    public DBTableColumn(Field field) {
        ExtractAttributes(field);
        PrimaryKey = attributes.contains("PRIMARY KEY");
        AutoIncrement = attributes.contains("AUTOINCREMENT");
        Name = field.getName();
        type = (field.getType().isEnum()) ? ColumnType.STRING : ColumnType.valueOf(field.getType());
        Ignore = ((Modifier.isStatic(field.getModifiers()) ||
                type.equals(ColumnType.UNDEFINED) ||
                attributes.contains("IGNORE")
        )
        );
    }
    public String QName() {
        return "\"" + Name + "\"";
    }
    public void ExtractAttributes(Field field) {
        attributes = new Vector<String>();
        Annotation[] annotations = field.getAnnotations();
        for (Annotation a : annotations) {
            String[] data = a.toString().split("value=");
            if (data.length > 1) {
                String[] attributes_ = data[1].split("[,\")]");
                for (String attribute : attributes_) {
                    if (attribute.length() > 0)
                        attributes.add(attribute);
                }
            }
        }
    }
    @Override
    public String toString() {
        String res = QName() + " " + type.getSQLType();
        if (attributes.size() > 0)
            res += " " + String.join(" ", attributes);
        return res;
    }
}

package Common.Database;
import java.util.LinkedHashMap;
public abstract class DataSetAnchestor {
    //чтобы обмануть стирание типов во всех параметризованных полях. используется не во всех потомках.
    //ибо не все наборы данных относятся к базам.
    public LinkedHashMap<String, DBTableColumn> columns = new LinkedHashMap<>();
    //то же самое. на самом деле внешние ключи бывают только у таблиц бд
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        return new LinkedHashMap<>();
    }
}

package Common.Database;
public enum ColumnType {
    UNDEFINED,
    INT,
    LONG,
    DOUBLE,
    STRING;
    public static ColumnType valueOf(Class<?> type) {
        ColumnType res = UNDEFINED;
        try {
            res = valueOf(type.getSimpleName().toUpperCase());
        } catch (Exception ignored) {
        }
        return res;
    }
    public String getSQLType() {
        String res = "";
        switch (this) {
            case INT:
            case LONG:
                res = "INTEGER";
                break;
            case DOUBLE:
                res = "REAL";
                break;
            case STRING:
                res = "VARCHAR";
        }
        return res;
    }
}

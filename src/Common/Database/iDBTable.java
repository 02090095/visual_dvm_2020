package Common.Database;
public abstract class iDBTable<D extends iDBObject> extends DBTable<Integer, D> {
    public iDBTable(Class<D> d_in) {
        super(Integer.class, d_in);
    }
}

package Common.Database;
import Common.Utils.Utils;
import com.sun.org.glassfish.gmbal.Description;
//автоинкрементальный ключ
public class iDBObject extends DBObject {
    @Description("PRIMARY KEY,AUTOINCREMENT")
    public int id;
    @Override
    public Object getPK() {
        return id;
    }
    @Override
    public String getFKName() {
        return getClass().getSimpleName().toLowerCase() + "_id";
    }
    @Override
    public Object getEmptyFK() {
        return Utils.Nan;
    }
    //---
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        id = ((iDBObject)src).id;
    }
    public iDBObject(){}
    public iDBObject(iDBObject src){
        this.SynchronizeFields(src);
    }
}

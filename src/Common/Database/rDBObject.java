package Common.Database;
import java.util.Date;
//объект репозитория. ключ имя, и есть данные отправителя.
public class rDBObject extends nDBObject {
    public String sender_name = "";
    public String sender_address = "";
    public String description = "";
    //-
    public long date = 0;
    public long change_date;
    public Date getDate() {
        return new Date(date);
    }
    public Date getChangeDate() {
        return new Date(change_date);
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        rDBObject r = (rDBObject) src;
        sender_name = r.sender_name;
        sender_address = r.sender_address;
        description = r.description;
        date = r.date;
        change_date = r.change_date;
    }
    public rDBObject(rDBObject src) {
        this.SynchronizeFields(src);
    }
    public rDBObject() {
    }
}

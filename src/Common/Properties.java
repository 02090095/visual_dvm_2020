package Common;
import Common.UI.Menus_2023.StableMenuItem;
import Common.Utils.Utils;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
public abstract class Properties {
    public void addFlagMenuItem(JMenu menu, String fieldName) {
        JMenuItem menu_item = new StableMenuItem(getFieldDescription(fieldName),
                getFlag(fieldName) ? "/icons/Pick.png" : "/icons/NotPick.png");
        //-
        menu_item.addActionListener(e -> {
            switchFlag(fieldName);
            Update();
            menu_item.setIcon(Utils.getIcon(getFlag(fieldName) ? "/icons/Pick.png" : "/icons/NotPick.png"));
        });
        menu.add(menu_item);
    }
    public boolean getFlag(String fieldName) {
        boolean field = false;
        try {
            field = (boolean) GlobalProperties.class.getField(fieldName).get(this);
            //
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return field;
    }
    public void switchFlag(String fieldName) {
        boolean field = false;
        try {
            field = (boolean) GlobalProperties.class.getField(fieldName).get(this);
            GlobalProperties.class.getField(fieldName).set(this, !field);
            //
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void Update() {
        try {
            FileUtils.write(getFile(), Utils.jsonToPrettyFormat(Utils.gson.toJson(this)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //--
    public abstract String getFieldDescription(String fieldName);
    public abstract File getFile();
}

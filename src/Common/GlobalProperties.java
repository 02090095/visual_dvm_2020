package Common;
import com.google.gson.annotations.Expose;

import java.io.File;
import java.nio.file.Paths;
public class GlobalProperties extends Properties {
    @Expose
    public Current.Mode Mode = Current.Mode.Normal;
    //---
    @Expose
    public int SocketTimeout = 5000;
    @Expose
    public boolean OldServer = false;
    //---
    @Expose
    public String SMTPHost = "smtp.mail.ru";
    @Expose
    public int SMTPPort = 465;
    @Expose
    public int MailSocketPort = 465;
    //---
    @Expose
    public String BackupWorkspace = "_sapfor_x64_backups";
    @Expose
    public int BackupHour = 5;
    @Expose
    public int BackupMinute = 0;
    @Expose
    public boolean EmailAdminsOnStart = false;
    //---
    @Expose
    public boolean AutoUpdateSearch = true;
    // настройки визуализатора. по крайней мере, флаги.
    @Expose
    public boolean ConfirmPassesStart = true;
    @Expose
    public boolean ShowPassesDone = true;
    @Expose
    public boolean FocusPassesResult = true;
    //-
    @Expose
    public String GlobalDBName = "db7.sqlite";
    @Expose
    public String ProjectDBName = "new_project_base.sqlite";
    @Expose
    public String BugReportsDBName = "bug_reports.sqlite";
    @Expose
    public String TestsDBName = "tests.sqlite";
    //-
    @Expose
    public int ComponentsWindowWidth = 650;
    @Expose
    public int ComponentsWindowHeight = 250;
    //-
    @Expose
    public String VisualiserPath = "";
    @Expose
    public String Sapfor_FPath = "";
    @Expose
    public String Visualizer_2Path = "";
    @Expose
    public String InstructionPath = "";
    @Expose
    public String PerformanceAnalyzerPath = "";
    @Expose
    public int ComponentsBackUpsCount=10;
    @Expose
    public long SapforTaskMaxId = 0; //вероятно, временно. когда перейдем на удаленную машину.
    //-
    @Override
    public String getFieldDescription(String fieldName) {
        switch (fieldName) {
            case "ShowPassesDone":
                return "Сообщать об успешном выполнении проходов";
            case "ConfirmPassesStart":
                return "Запрашивать подтверждения начала выполнения проходов";
            case "FocusPassesResult":
                return "Переходить на результирующую вкладку проходов по их завершении";
            default:
                return "?";
        }
    }
    @Override
    public File getFile() {
        return Paths.get(System.getProperty("user.dir"),"properties").toFile();
    }
}

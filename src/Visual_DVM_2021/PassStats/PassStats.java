package Visual_DVM_2021.PassStats;
import Common.Database.DBObject;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import com.sun.org.glassfish.gmbal.Description;
public class PassStats extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public PassCode_2021 code = PassCode_2021.Undefined;
    //меняется только когда юзер кликает на пункте меню или же на кнопке.
    public int Usages = 0;
    public PassStats() {
    }
    public PassStats(Pass_2021 pass) {
        code = pass.code();
    }
    public void Inc() {
        Usages++;
    }
    public void Drop() {
        Usages = 0;
    }
    public boolean HasUsages() {
        return Usages > 0;
    }
    @Override
    public Object getPK() {
        return code;
    }
}

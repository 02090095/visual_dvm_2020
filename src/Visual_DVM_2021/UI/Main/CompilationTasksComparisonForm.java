package Visual_DVM_2021.UI.Main;
import Common.Current;
import GlobalData.Tasks.CompilationTask.CompilationTask;
public class CompilationTasksComparisonForm extends TasksComparisonForm<CompilationTask> {
    public CompilationTasksComparisonForm(ComparisonForm<CompilationTask> slave_in) {
        super(CompilationTask.class, slave_in);
    }
    @Override
    protected Current getCurrentObjectName() {
        return Current.CompilationTask;
    }
    @Override
    protected String getTextByTab() {
        switch (status) {
            case CompilationOutput:
                return object.getOutput();
            case CompilationErrors:
                return object.getErrors();
            default:
                return "";
        }
    }
    @Override
    protected TasksComparisonState[] getForbiddenStates() {
        return new TasksComparisonState[]{
                TasksComparisonState.RunOutput,
                TasksComparisonState.RunErrors,
                TasksComparisonState.Sts
        };
    }
}

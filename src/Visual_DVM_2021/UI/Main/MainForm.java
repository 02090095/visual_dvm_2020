package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Form;
import Common.UI.Windows.FormType;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.CallbackWindow;
import Common.UI.Menus_2023.MainMenuBar.MainWindow;
import Visual_DVM_2021.UI.Interface.ProjectWindow;
import Visual_DVM_2021.UI.Interface.TestingWindow;

import javax.swing.*;
import java.awt.*;
public class MainForm extends Form implements MainWindow {
    private final WelcomeForm welcomeForm = new WelcomeForm(); //заглушка когда проекта нет.
    int global_index = 0;
    //-----------------
    private ProjectForm projectForm;
    private CallbackForm callbackForm;
    private TestingForm testingForm;
    //-----------------
    private JPanel Content;
    private JTabbedPane globalTabs;
    private JPanel mainPanel;
    public MainForm() {
        mainPanel.add(UI.mainMenuBar, BorderLayout.NORTH);
        InstallWelcomePanel();
        InstallCallbackPanel();
        InstallTestingPanel();
        ShowUpdatesIcon();
        //----------------------
    }
    @Override
    protected JPanel getMainPanel() {
        return Content;
    }
    @Override
    protected FormType getFormType() {
        return FormType.Main;
    }
    @Override
    public String getIconName() {
        return "Sapfor.png";
    }
    @Override
    public String getWTitleText() {
        return "Visual Sapfor 3.0";
    }
    @Override
    public String getUTitleText() {
        return "Visual Sapfor 3.0";
    }
    @Override
    public void AfterClose() {
        Pass_2021.passes.get(PassCode_2021.CloseCurrentProject).Do();
        if (UI.getVersionsWindow() != null)
            ((VersionsForm) UI.getVersionsWindow()).SaveSplitters();
        SaveCallbackPanel();
        SaveTestingPanel();
        Global.FinishApplication();
    }
    private void InstallWelcomePanel() {
        globalTabs.insertTab("Начало работы",
                null,
                welcomeForm.content,
                "Для начала работы откройте проект с помощью правой панели инструментов или перетащите его сюда", 0
        );
    }
    private void InstallProjectPanel() {
      //  globalTabs.insertTab("Проект: " + Current.getProject().name,
      //          Utils.getIcon("/icons/Common.png"),
      //          (projectForm = new ProjectForm()).content, Current.getProject().description, 0);
        globalTabs.insertTab("", null,
                (projectForm = new ProjectForm()).content, Current.getProject().description, 0);
        globalTabs.setTabComponentAt(0,
                new TabToolBar("Проект: " + Current.getProject().name, PassCode_2021.CloseCurrentProject){
                    @Override
                    public void leftMouseAction() {
                        globalTabs.setSelectedIndex(0);
                    }
                });
    }
    private void RemoveProjectPanel() {
        globalTabs.removeTabAt(0);
        if (projectForm != null) {
            projectForm.SaveSplitters();
            projectForm = null;
        }
    }
    private void InstallCallbackPanel() {
        globalTabs.insertTab("Обратная связь",
                Utils.getIcon("/icons/Bug.png"),
                (callbackForm = new CallbackForm()).getContent(),
                "Журнал ошибок и связь с разработчиками", 1);
    }
    private void SaveCallbackPanel() {
        if (callbackForm != null) {
            callbackForm.SaveSplitters();
            callbackForm = null;
        }
    }
    private void InstallTestingPanel() {
        testingForm = new TestingForm();
        ShowTestingTab();
    }
    @Override
    public void ShowTestingTab() {
        if (globalTabs.getTabCount() < 4)
            globalTabs.insertTab("Тестирование",
                    Utils.getIcon("/icons/Session.png"),
                    testingForm.getContent(),
                    "Система тестирования", 2);
    }
    @Override
    public void HideTestingTab() {
        if (globalTabs.getTabCount() == 3)
            globalTabs.removeTabAt(2);
    }
    private void SaveTestingPanel() {
        if (testingForm != null) {
            testingForm.SaveSplitters();
            testingForm = null;
        }
    }
    private void saveGlobalTab() {
        global_index = globalTabs.getSelectedIndex();
    }
    private void restoreGlobalTab() {
        globalTabs.setSelectedIndex(global_index);
    }
    @Override
    public ProjectWindow getProjectWindow() {
        return projectForm;
    }
    @Override
    public CallbackWindow getCallbackWindow() {
        return callbackForm;
    }
    @Override
    public void ShowUpdatesIcon() {
        UI.mainMenuBar.ShowUpdatesIcon();
    }
    @Override
    public void FocusProject() {
        globalTabs.setSelectedIndex(0);
    }
    @Override
    public void FocusCallback() {
        globalTabs.setSelectedIndex(1);
    }
    @Override
    public void FocusTesting() {
        globalTabs.setSelectedIndex(2);
    }
    @Override
    public TestingWindow getTestingWindow() {
        return testingForm;
    }
    @Override
    public void Show() {
        //приходится идти на это только ПОСЛЕ создания главного окна.
        // иначе ссылка на главное окно в методах пустая.
        getCallbackWindow().ShowAll();
        if (getTestingWindow() != null)
            getTestingWindow().ShowAll();
        UI.windowsStack.push(this);
        System.out.println("New Front Window");
        super.Show();
    }
    @Override
    public void ShowProject() {
        getTestingWindow().ShowProject();
        //-
        saveGlobalTab();
        RemoveProjectPanel();
        InstallProjectPanel();
        restoreGlobalTab();
        getTestingWindow().DropCompilationTasksComparison();
        UI.mainMenuBar.ShowProject(true);
    }
    @Override
    public void ShowNoProject() {
        getTestingWindow().ShowNoProject();
        //-
        saveGlobalTab();
        RemoveProjectPanel();
        InstallWelcomePanel();
        restoreGlobalTab();
        UI.mainMenuBar.ShowProject(false);
    }
}

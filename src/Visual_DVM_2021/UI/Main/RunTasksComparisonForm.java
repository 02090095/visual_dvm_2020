package Visual_DVM_2021.UI.Main;
import Common.Current;
import GlobalData.Tasks.RunTask.RunTask;
public class RunTasksComparisonForm extends TasksComparisonForm<RunTask> {
    public RunTasksComparisonForm(ComparisonForm<RunTask> slave_in) {
        super(RunTask.class, slave_in);
    }
    @Override
    protected Current getCurrentObjectName() {
        return Current.RunTask;
    }
    @Override
    protected TasksComparisonState getFirstState() {
        return TasksComparisonState.RunOutput;
    }
    @Override
    protected TasksComparisonState[] getForbiddenStates() {
        return new TasksComparisonState[]{
                TasksComparisonState.CompilationOutput,
                TasksComparisonState.CompilationErrors
        };
    }
    @Override
    protected String getTextByTab() {
        switch (status) {
            case RunOutput:
                return object.getOutput();
            case RunErrors:
                return object.getErrors();
            case Sts:
                return object.getLocalStsText();
            default:
                return "";
        }
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.Global;
import Common.UI.Editor.Viewer;
import Common.UI.Menus_2023.ProjectMenuBar.ProjectMenuBar;
import Common.UI.Trees.TreeForm;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.UI.FilesTree;
import ProjectData.ProjectView;
import ProjectData.SapforData.Functions.UI.InlineTree;
import ProjectData.SapforData.Functions.UI.InlineTree2;
import ProjectData.SapforData.Includes.UI.IncludesTree;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.*;

import javax.swing.*;
import java.awt.*;
public class ProjectForm implements FormWithSplitters, ProjectWindow {
    private final ArraysWindow arraysForm;
    private final FunctionsWindow functionsForm;
    private final AnalysisWindow analysisForm;
    public JSplitPane SC3;
    //</editor-fold>
    public JPanel content;
    public TreeForm includesForm;
    private JTabbedPane projectTabs;
    private TreeForm filesForm;
    private Viewer projectDescription = null;
    private JScrollPane logScroll;
    private JPanel inlinePanel2;
    private JPanel inlinePanel;
    private TreeForm inlineForm;
    private TreeForm inlineForm2;
    private Viewer logText;
    private JPanel includesPanel;
    private JPanel filePanel;
    private JPanel arraysFormPanel;
    private JPanel functionsFormPanel;
    private JPanel analysisFormPanel;
    private final JScrollPane FastAccessScroll;
    private JPanel projectViewPanel;
    private JPanel projectBackground;
    ProjectMenuBar menuBar;
    //-
    @Override
    public ArraysWindow getArraysWindow() {
        return arraysForm;
    }
    @Override
    public FunctionsWindow getFunctionsWindow() {
        return functionsForm;
    }
    @Override
    public AnalysisWindow getAnalysisWindow() {
        return analysisForm;
    }
    public ProjectForm() {
        LoadSplitters();
        //-
        projectBackground.add(menuBar = new ProjectMenuBar(), BorderLayout.NORTH);
        arraysFormPanel.add((arraysForm = new ArraysForm()).getContent());
        functionsFormPanel.add((functionsForm = new FunctionsForm()).getContent());
        analysisFormPanel.add((analysisForm = new AnalysisForm()).getContent());
        //-
        FastAccessScroll = new JScrollPane(UI.fastAccessMenuBar) {
            {
                getHorizontalScrollBar().setPreferredSize(new Dimension(0, 10));
                setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
                setPreferredSize(new Dimension(-1, 45));
            }
        };
        content.add(FastAccessScroll, BorderLayout.NORTH);
        //-
        filesForm.Show();
        InstallVersionsPanel();
        ShowAllAnalyses();
        ShowProjectView();
        SwitchScreen(Global.db.settings.get(SettingName.SmallScreen).toBoolean());
        RefreshTabsNames();
        /*
        SC3.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent pce) {
                // do here
                UI.Info(pce.getNewValue().toString());
            }
        });
         */
    }
    @Override
    public void SaveSplitters() {
        FormWithSplitters.super.SaveSplitters();
        arraysForm.SaveSplitters();
        analysisForm.SaveSplitters();
        functionsForm.SaveSplitters();
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        filesForm = new TreeForm(FilesTree.class);
        projectDescription = new Viewer();
        logText = new Viewer();
        logScroll = new JScrollPane(logText);
        includesForm = new TreeForm(IncludesTree.class);
        inlineForm = new TreeForm(InlineTree.class);
        inlineForm2 = new TreeForm(InlineTree2.class);
    }

    @Override
    public void RefreshProjectFiles() {
        filesForm.Refresh();
    }
    @Override
    public void ShowSelectedDirectory() {
        System.out.println(Current.getSelectedDirectory().getAbsolutePath());
    }
    @Override
    public void ShowSelectedFile() {
        System.out.println(Current.getSelectedFile().file.getAbsolutePath());
    }
    @Override
    public void ShowNoSelectedFile() {
        System.out.println("?");
    }
    @Override
    public void ShowProjectSapforLog() {
        logText.setText(Current.getProject().Log);
        logText.setCaretPosition(0);
    }
    @Override
    public void ShowFunctions() {
        functionsForm.ShowFunctions();
        inlineForm.Show();
        inlineForm2.Show();
    }
    @Override
    public void ShowNoFunctions() {
        functionsForm.ShowNoFunctions();
        inlineForm.Clear();
        inlineForm2.Clear();
    }
    @Override
    public void ShowNoIncludes() {
        includesForm.Clear();
    }
    @Override
    public void ShowIncludes() {
        includesForm.Show();
    }
    @Override
    public void ShowFile() {
        //пока текущий файл только один.
        //filesTabs.insertTab(null, null, (Current.getFile().form = new FileForm(Current.getFile())).content,
        //        "текущий файл проекта", 0);
        //filesTabs.setTabComponentAt(0,
        //        new TabToolBar(Current.getFile().name, PassCode_2021.CloseCurrentFile));
        // projectTabs.setTitleAt(0, "Файл: " + Current.getFile().name);
        filePanel.add((Current.getFile().form = new FileForm(Current.getFile())).getContent());
        projectTabs.setTabComponentAt(0,
                new TabToolBar("Файл: " + Current.getFile().name, PassCode_2021.CloseCurrentFile) {
                    @Override
                    public void leftMouseAction() {
                        projectTabs.setSelectedIndex(0);
                    }
                });
    }
    @Override
    public void ShowNoFile() {
        //  projectTabs.setTitleAt(0, "Файл: -");
        projectTabs.setTabComponentAt(0, new TabToolBar("Файл: -"){
            @Override
            public void leftMouseAction() {
                projectTabs.setSelectedIndex(0);
            }
        });
        UI.Clear(filePanel);
        //filesTabs.removeAll();
    }
    @Override
    public void GotoFile(String fileName, int line, boolean focus) {
        if (focus) FocusFile();
        if (Current.HasFile() && Current.getFile().name.equals(fileName))
            Current.getFile().form.getEditor().gotoLine(line);
        else if (Current.getProject().db.files.containsKey(fileName)) {
            DBProjectFile targetFile = Current.getProject().db.files.get(fileName);
            targetFile.UpdateLastLine(line);
            Pass_2021.passes.get(PassCode_2021.OpenCurrentFile).Do(targetFile);
        }
    }
    @Override
    public void FocusFile() {
        projectTabs.setSelectedIndex(0);
    }
    @Override
    public void FocusFileTabs() {
        projectTabs.requestFocus();
    }
    @Override
    public void FocusDependencies() {
        FocusFile();
        menuBar.projectViewMenu.SelectView(ProjectView.Includes);
    }
    @Override
    public void FocusArrays() {
        projectTabs.setSelectedIndex(1);
    }
    @Override
    public void FocusFunctions() {
        projectTabs.setSelectedIndex(2);
        menuBar.projectViewMenu.SelectView(ProjectView.FunctionsHierarchy);
    }
    @Override
    public void FocusAnalysis() {
        projectTabs.setSelectedIndex(3);
    }
    @Override
    public void FocusHierarchy() {
        FocusFile();
        menuBar.projectViewMenu.SelectView(ProjectView.FunctionsHierarchy);
    }
    @Override
    public void FocusPoints() {
        FocusFile();
        menuBar.projectViewMenu.SelectView(ProjectView.FunctionsCallsPoints);
    }
    @Override
    public void RefreshTabsNames() {
        UI.ShowTabsNames(projectTabs, 1);
    }
    @Override
    public void RefreshProjectTreeAndMessages() {
        RefreshProjectFiles();
        DBProjectFile badFile = Current.getProject().getFirstBadFile();
        if (badFile != null) {
            Pass_2021.passes.get(PassCode_2021.OpenCurrentFile).Do(badFile);
            UI.getMainWindow().FocusProject();
            UI.getMainWindow().getProjectWindow().FocusFile();
        }
        //-
        if (Current.HasFile()) {
            Current.getFile().form.ShowMessages();
            if (badFile != null && badFile.equals(Current.getFile()))
                Current.getFile().form.ShowFirstError();
        }
    }
    @Override
    public TreeForm getFilesTreeForm() {
        return filesForm;
    }
    @Override
    public void SwitchScreen(boolean small) {
        FastAccessScroll.setVisible(!small);
        content.repaint();
        content.revalidate();
    }
    @Override
    public void ShowProjectView() {
        UI.Clear(projectViewPanel);
        ProjectView view = Current.getProjectView();
        menuBar.getProjectViewMenu().setToolTipText(view.getDescription());
        menuBar.getProjectViewMenu().setIcon(Utils.getIcon(view.getIcon()));
        switch (view) {
            case Files:
                projectViewPanel.add(filesForm.getContent());
                break;
            case Includes:
                projectViewPanel.add(includesForm.getContent());
                break;
            case FunctionsHierarchy:
                projectViewPanel.add(inlineForm2.getContent());
                break;
            case FunctionsCallsPoints:
                projectViewPanel.add(inlineForm.getContent());
                break;
        }
        projectViewPanel.revalidate();
    }
    //-
    private void InstallVersionsPanel() {
        projectTabs.insertTab("",
                Utils.getIcon("/icons/VersionsTree.png"),
                UI.getVersionsWindow().getContent(),
                "Версии", 5);
    }
    @Override
    public void FocusVersions() {
        projectTabs.setSelectedIndex(5);
    }
}
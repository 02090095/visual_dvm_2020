package Visual_DVM_2021.UI.Main;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.DescriptionInterface;

import javax.swing.*;
public class DescriptionFields implements DescriptionInterface {
    public JSplitPane SC25;
    private JPanel content;
    private JToolBar descriptionTools;
    private JToolBar descriptionAdditionTools;
    private JPanel editorPanel;
    private JPanel additionPanel;
    public DescriptionFields() {
        LoadSplitters();
        descriptionAdditionTools.add(Pass_2021.passes.get(PassCode_2021.AppendBugReportDescription).createButton());
        descriptionTools.add(Pass_2021.passes.get(PassCode_2021.SaveBugReportDescription).createButton());
    }
    //---------
    @Override
    public void setEditorScroll(JScrollPane scroll_in) {
        editorPanel.add(scroll_in);
    }
    @Override
    public void setAdditionScroll(JScrollPane scroll_in) {
        additionPanel.add(scroll_in);
    }
    @Override
    public JPanel getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        descriptionTools = new VisualiserMenuBar();
        descriptionAdditionTools = new VisualiserMenuBar();
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Current;
import Visual_DVM_2021.UI.Interface.AnalysisWindow;

import javax.swing.*;
public class AnalysisForm implements AnalysisWindow {
    private JSplitPane SCX;
    private JPanel content;
    private JLabel l_lines;
    private JLabel l_loops;
    private JLabel l_arrays;
    private JLabel l_functions;
    private JLabel l_spf_dirs;
    private JLabel l_dvm_dirs;
    private JLabel l_maxdim;
    private JPanel regionsPanel;
    public AnalysisForm() {
        LoadSplitters();
        Current.getProject().parallelRegions.mountUI(regionsPanel);
    }
    @Override
    public JPanel getContent() {
        return content;
    }
    @Override
    public void ShowProjectMaxDim() {
        l_maxdim.setText("Наибольшая размерность DVM-шаблона: " + Current.getProject().maxdim);
    }
    @Override
    public void ShowNoProjectMaxDim() {
        l_maxdim.setText("Наибольшая размерность DVM-шаблона: ?");
    }
    @Override
    public void ShowMetrics() {
        l_lines.setText("Общее количество строк кода: " + Current.getProject().LinesCount());
        l_spf_dirs.setText("Всего объявлено SPF директив: " + Current.getProject().SPFCount());
        l_dvm_dirs.setText("Всего объявлено DVM директив: " + Current.getProject().DVMCount());
    }
    @Override
    public void ShowNoMetrics() {
        l_lines.setText("Общее количество строк кода: ?");
        l_spf_dirs.setText("Всего объявлено SPF директив: ?");
        l_dvm_dirs.setText("Всего объявлено DVM директив: ?");
    }
    @Override
    public void ShowRegions() {
        Current.getProject().parallelRegions.ShowUI();
    }
    @Override
    public void ShowNoRegions() {
        Current.getProject().parallelRegions.ClearUI();
    }
    @Override
    public void ShowLoopsCount() {
        l_loops.setText("Общее количество циклов: " + Current.getProject().LoopsCount());
    }
    @Override
    public void ShowFunctionsCount() {
        l_functions.setText("Всего объявлено процедур: " + Current.getProject().FunctionsCount());
    }
    @Override
    public void ShowArraysCount() {
        l_arrays.setText("  Всего объявлено массивов: " + Current.getProject().ArraysCount());
    }
}

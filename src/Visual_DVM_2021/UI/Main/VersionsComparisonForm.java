package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.Files.DBProjectFile;
import ProjectData.Project.db_project_info;

import javax.swing.*;
import java.util.Vector;
public class VersionsComparisonForm extends ComparisonForm<db_project_info> {
    private final JComboBox<DBProjectFile> cbFile;
    protected DBProjectFile file = null;
    private VersionsComparisonForm getMaster() {
        return (VersionsComparisonForm) master;
    }
    private VersionsComparisonForm getSlave() {
        return (VersionsComparisonForm) slave;
    }
    public VersionsComparisonForm(VersionsComparisonForm slave_in) {
        super(db_project_info.class, slave_in);
        cbFile = new JComboBox<>();
        tools.add(cbFile, 3);
        cbFile.addActionListener(e -> {
            DBProjectFile File1 = null;
            DBProjectFile File2 = null;
            ClearText();
            file = (cbFile.getSelectedItem() instanceof DBProjectFile) ?
                    ((DBProjectFile) cbFile.getSelectedItem()) : null;
            if (file != null) {
                if (isMaster()) {
                    getSlave().selectSameFile(file);
                } else {
                    File1 = getMaster().file;
                    File2 = file;
                    //---
                    if ((File1 != null) && (File2 != null)) {
                        boolean ExtensionsOn = Global.db.settings.get(SettingName.ExtensionsOn).toBoolean();
                        String name1 = ExtensionsOn ? File1.file.getName() : Utils.getFileNameWithoutExtension(File1.file);
                        String name2 = ExtensionsOn ? File2.file.getName() : Utils.getFileNameWithoutExtension(File2.file);
                        System.out.println("name1=" + Utils.Brackets(name1) + "name2=" + Utils.Brackets(name2));
                        if (Global.db.settings.get(SettingName.ComparsionDiffMergeOn).toBoolean()) {
                            if (name1.equalsIgnoreCase(name2))
                                master.DoComparePass(true);
                        } else
                            master.DoShowPass(true);
                    }
                }
            }
        });
    }
    @Override
    protected Current getCurrentObjectName() {
        return Current.Version;
    }
    @Override
    protected String getText() {
        return Utils.ReadAllText(file.file);
    }
    @Override
    protected void removeObject() {
        cbFile.removeAllItems();
        file = null;
    }
    @Override
    public boolean isReady() {
        return super.isReady() && file != null;
    }
    @Override
    protected void showObject() {
        lObjectName.setText(object.name);
        lObjectName.setToolTipText(
                object.getVersionTooltip()
        );
        cbFile.removeAllItems();
        Vector<DBProjectFile> files = object.getFilesForComparsion();
        for (DBProjectFile file : files)
            cbFile.addItem(file);
    }
    public void selectSameFile(DBProjectFile file_in) {
        file = null;
        cbFile.setSelectedIndex(-1);
        for (int i = 0; i < cbFile.getItemCount(); ++i) {
            DBProjectFile dbProjectFile = cbFile.getItemAt(i);
            if (Global.db.settings.get(SettingName.ExtensionsOn).toBoolean()) {
                //если учитываем исключения, ищем полное совпадение
                if (dbProjectFile.name.equals(file_in.name)) {
                    cbFile.setSelectedIndex(i);
                    return;
                }
            } else {
                if (Utils.getNameWithoutExtension(dbProjectFile.name).equals(Utils.getNameWithoutExtension(file_in.name))) {
                    cbFile.setSelectedIndex(i);
                    return;
                }
            }
            //если drk.x
        }
    }
    public void CheckVersion(db_project_info version_in) {
        if ((object != null) && version_in.Home.equals(object.Home)) {
            RemoveObject();
        }
    }
    @Override
    protected boolean fortranWrapsOn() {
        return Global.db.settings.get(SettingName.FortranWrapsOn).toBoolean();
    }
}

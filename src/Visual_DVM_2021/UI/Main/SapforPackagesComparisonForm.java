package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.Database.DataSet;
import Common.Global;
import Common.UI.DataSetControlForm;
import Common.UI.Label.ShortLabel;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Common.UI.UI;
import Common.Utils.TextLog;
import TestingSystem.Sapfor.SapforTask.SapforTaskResult;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackage_2023;
import TestingSystem.Test.ProjectFiles_json;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
public class SapforPackagesComparisonForm {
    //-->>
    private JPanel content;
    public JPanel getContent() {
        return content;
    }
    protected JToolBar tools;
    protected JLabel lObjectName;
    private JButton bApplyObject;
    private JButton bPrevious;
    private JButton bNext;
    private JButton bCompare;
    private JButton bClose;
    private JPanel tasksPanel;
    private JLabel flagsLabel;
    //-->>
    SapforPackagesComparisonForm this_ = null; //?
    SapforPackagesComparisonForm slave = null;
    SapforPackagesComparisonForm master = null;
    //-->>
    protected SapforTasksPackage_2023 object = null;
    //-->>
    protected DataSet<String, SapforTaskResult> packageTasks;
    protected DataSetControlForm Body;
    //--->>
    public boolean isMaster() {
        return slave != null;
    }
    public boolean isSlave() {
        return master != null;
    }
    //--->>
    //неперегружаемые методы
    protected void RemoveObject() {
        object = null;
        showNoObject();
    }
    public void ApplyObject() {
        RemoveObject();
        TextLog log = new TextLog();
        if (Current.Check(log, getCurrentObjectName())) {
            object = Current.getSapforTasksPackage();
            try {
                packageTasks = isMaster() ? Global.db.getSapforPackagesMasterDataSet(object) :
                        Global.db.getSapforPackagesSlaveDataSet(object);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            showObject();
        } else
            UI.Info(log.toString());
    }
    //предполагаем что оба объекта есть и мы можем получить с них текст.
    protected void Compare() throws Exception {
        //это всегда форма мастер
        //1. Получить файл теста.Для сапфора ограничение что файл может быть только один.Возможно и для других тоже.
        //  File project_db = new File(object.Home, "project.json");
        for (String masterTaskKey : packageTasks.Data.keySet()) {
            for (String slaveTaskKey : slave.packageTasks.Data.keySet()) {
                if (masterTaskKey.equals(slaveTaskKey)) {
                    compareTasks(packageTasks.get(masterTaskKey), slave.packageTasks.get(masterTaskKey));
                }
            }
        }
    }
    protected void compareTasks(SapforTaskResult masterTask, SapforTaskResult slaveTask) throws Exception {
        /*
        //надо сравнить все сраниваемых проектов.
        ProjectFiles_json masterFiles = getTaskFiles(masterTask);
        ProjectFiles_json slaveFiles = getTaskFiles(slaveTask);
        if (masterFiles.files.size() != slaveFiles.files.size()) {
            slaveTask.match_state = MatchState.NotMatch;
            return;
        }
        for (DBProjectFile masterDBFile : masterFiles.files) {
            boolean hasSameFile = false;
            for (DBProjectFile slaveDBFile : slaveFiles.files) {
                if (masterDBFile.name.equals(slaveDBFile.name)) {
                    hasSameFile = true;
                    File masterFile = new File(masterTask.last_version, masterDBFile.name);
                    File slaveFile = new File(slaveTask.last_version, slaveDBFile.name);
                    String masterFileText = FileUtils.readFileToString(masterFile);
                    String slaveFileText = FileUtils.readFileToString(slaveFile);
                    slaveTask.match_state = masterFileText.equals(slaveFileText) ? MatchState.Match : MatchState.NotMatch;
                    break;
                }
            }
            if (!hasSameFile) {
                slaveTask.match_state = MatchState.NotMatch;
                return;
            }
        }
         */
    }
    ProjectFiles_json getTaskFiles(SapforTaskResult task) throws Exception {
        // File project = new File(task.last_version);
        //  File file_json = new File(project, "project.json");
        //  return Utils.gson.fromJson(FileUtils.readFileToString(file_json), ProjectFiles_json.class);
        return null;
    }
    public void Show() throws Exception {
    }
    //Перегружаемые методы.
    //--->>
    protected Current getCurrentObjectName() {
        return Current.SapforTasksPackage;
    }
    protected void showNoObject() {
        lObjectName.setText("?");
        lObjectName.setToolTipText("Объект не назначен.");
        flagsLabel.setText("?");
        UI.Clear(tasksPanel);
    }
    protected void showObject() {
        lObjectName.setText(object.getPK().toString() + (isMaster() ? "(эталон)" : ""));
        lObjectName.setToolTipText(object.getPK().toString());
        flagsLabel.setText(object.flags);
        //-
        packageTasks.mountUI(tasksPanel);
        packageTasks.ShowUI();
    }
    protected String getText() {
        return "";
    }
    public SapforPackagesComparisonForm(SapforPackagesComparisonForm slave_in) {
        //-
        this_ = this;
        slave = slave_in;
        bPrevious.setVisible(isMaster());
        bNext.setVisible(isMaster());
        bApplyObject.addActionListener(e -> {
            ApplyObject();
        });
        //--->>>
        if (isMaster()) {
            slave.master = this;
            bPrevious.addActionListener(e -> {
            });
            bNext.addActionListener(e -> {
            });
            bCompare.addActionListener(e -> {
                DoComparePass(isReady() && slave.isReady());
            });
        } else {
            //рабу сравнивать не положено.
            bCompare.setVisible(false);
        }
        //--->>>
        bClose.addActionListener(e -> {
            onClose();
        });
    }
    //-->>
    private void createUIComponents() {
        // TODO: place custom component creation code here
        lObjectName = new ShortLabel(40);
        tools = new VisualiserMenuBar();
    }
    //для сравнения по кнопке.
    public boolean isReady() {
        return object != null;
    }
    public void onClose() {
        RemoveObject();
    }
    public void DoComparePass(boolean startCondition) {
        Pass_2021 pass = new Pass_2021() {
            @Override
            public String getDescription() {
                return "Сравнение";
            }
            @Override
            protected boolean needsAnimation() {
                return true;
            }
            @Override
            public boolean needsConfirmations() {
                return false;
            }
            @Override
            protected boolean canStart(Object... args) throws Exception {
                return startCondition;
            }
            @Override
            protected void body() throws Exception {
                Compare();
            }
        };
        pass.Do();
    }
    public void DoShowPass(boolean startCondition) {
        Pass_2021 pass = new Pass_2021() {
            @Override
            public String getDescription() {
                return "Отображение";
            }
            @Override
            protected boolean needsAnimation() {
                return false;
            }
            @Override
            public boolean needsConfirmations() {
                return false;
            }
            @Override
            protected boolean canStart(Object... args) throws Exception {
                return startCondition;
            }
            @Override
            protected void body() throws Exception {
                Show();
            }
        };
        pass.Do();
    }
    public void CheckObject(SapforTasksPackage_2023 object_in) {
        if ((object != null) && object_in.id == (object.id)) {
            RemoveObject();
        }
    }
}

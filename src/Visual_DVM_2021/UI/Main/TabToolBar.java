package Visual_DVM_2021.UI.Main;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
public class TabToolBar extends VisualiserMenuBar {
    public TabToolBar(String titleText, PassCode_2021... passes) {
        setFloatable(false);
        setOpaque(false);
        //-
        JLabel title = new JLabel(titleText);
        title.setOpaque(false);
        add(title);
        addSeparator();
        //-
        for (PassCode_2021 code : passes)
            add(Pass_2021.passes.get(code).createTabButton());
        MouseAdapter adapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                leftMouseAction();
            }
        };
        addMouseListener(adapter);
        title.addMouseListener(adapter);
    }
    public void  leftMouseAction(){}
    @Override
    public void setSizeLimits() {
       // setPreferredSize(new Dimension(-1, 18));
    }
}

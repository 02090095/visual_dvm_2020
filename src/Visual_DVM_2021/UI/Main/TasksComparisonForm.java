package Visual_DVM_2021.UI.Main;
import Common.Database.DBObject;
import Common.Global;
import Common.UI.Menus_2023.MenuBarButton;
import GlobalData.Settings.SettingName;

import javax.swing.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Vector;
public abstract class TasksComparisonForm<T extends DBObject> extends ComparisonForm<T> {
    public TasksComparisonForm(Class<T> t_in, ComparisonForm<T> slave_in) {
        super(t_in, slave_in);
        buttons = new LinkedHashMap<>();
        int i = 3;
        status = getFirstState();
        Vector<TasksComparisonState> forbidden = new Vector(Arrays.asList(getForbiddenStates()));
        for (TasksComparisonState state : TasksComparisonState.values()) {
            if (!forbidden.contains(state)) {
                MenuBarButton button = new MenuBarButton();
                button.setIcon( "/icons/" + state.toString() + ".png");
                button.setToolTipText(state.getDescription());
                button.addActionListener(e -> {
                    ChangeState(state);
                    if (isMaster()) {
                        TasksComparisonForm slave_ = (TasksComparisonForm) slave;
                        slave_.ChangeState(state);
                        //в отличие от комбо боксов, тут события нажатия на кнопку, нет.
                        if (isReady()) {
                            if (slave.isReady()) {
                                if (Global.db.settings.get(SettingName.ComparsionDiffMergeOn).toBoolean()) {
                                    DoComparePass(true);
                                } else
                                    DoShowPass(true);
                            } else {
                                DoShowPass(true);
                            }
                        }
                    } else {
                        if (isReady()) {
                            if (master.isReady()) {
                                if (Global.db.settings.get(SettingName.ComparsionDiffMergeOn).toBoolean()) {
                                    master.DoComparePass(true);
                                } else
                                    master.DoShowPass(true);
                            } else {
                                master.DoShowPass(true);  //!!!
                            }
                        }
                    }
                });
                tools.add(button, i);
                buttons.put(state, button);
                ++i;
            }
        }

    }
    protected TasksComparisonState status = TasksComparisonState.CompilationOutput;
    protected LinkedHashMap<TasksComparisonState, JButton> buttons = null;
    protected void DropTabsSelection() {
        for (JButton button : buttons.values()){
            button.setBorderPainted(false);
            button.repaint();
            button.revalidate();
        }

    }
    public void ChangeState(TasksComparisonState status_in) {
        status = status_in;
        DropTabsSelection();
        ClearText();
        JButton button =  buttons.get(status_in);
        button.setBorderPainted(false);
        button.repaint();
        button.revalidate();
    }
    @Override
    protected void showObject() {
        super.showObject();
        buttons.get(status).doClick();
    }
    @Override
    public void onClose() {
        super.onClose();
        if (isMaster())
            DoShowPass(true);
        else
            master.DoShowPass(true);
    }
    @Override
    protected String getText() {
        return isReady()?getTextByTab():"объект не назначен";
    }
    //-----------перегружаемые
    protected abstract String getTextByTab();
    protected TasksComparisonState[] getForbiddenStates() {
        return new TasksComparisonState[]{};
    }
    protected TasksComparisonState getFirstState() {
        return TasksComparisonState.CompilationOutput;
    }
}

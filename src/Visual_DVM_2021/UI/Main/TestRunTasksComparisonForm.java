package Visual_DVM_2021.UI.Main;
import Common.Current;
import TestingSystem.Tasks.TestRunTask;
public class TestRunTasksComparisonForm extends TasksComparisonForm<TestRunTask> {
    public TestRunTasksComparisonForm(ComparisonForm<TestRunTask> slave_in) {
        super(TestRunTask.class, slave_in);
    }
    @Override
    protected Current getCurrentObjectName() {
        return Current.TestRunTask;
    }
    @Override
    protected String getTextByTab() {
        switch (status) {
            case CompilationOutput:
                return object.compilation_output;
            case CompilationErrors:
                return object.compilation_errors;
            case RunOutput:
                return object.output;
            case RunErrors:
                return object.errors;
            case Sts:
                return object.statistic;
            default:
                return "";
        }
    }
}

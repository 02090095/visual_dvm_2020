package Visual_DVM_2021.UI.Main;
public enum TasksComparisonState {
    CompilationOutput,
    CompilationErrors,

    RunOutput,
    RunErrors,
    Sts;
    public String getDescription(){
        switch (this){
            case CompilationOutput:
                return "Поток вывода компиляции";
            case CompilationErrors:
                return "Поток ошибок компиляции";
            case RunOutput:
                return "Поток вывода запуска";
            case RunErrors:
                return "Поток ошибок запуска";
            case Sts:
                return "DVM статистика запуска";
            default:
                return "?";
        }
    }
}

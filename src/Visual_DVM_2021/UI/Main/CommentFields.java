package Visual_DVM_2021.UI.Main;
import Common.UI.Menus_2023.VisualiserMenuBar;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.CommentInterface;

import javax.swing.*;
public class CommentFields implements CommentInterface {
    public JSplitPane SC26;
    private JPanel content;
    private JToolBar commentTools;
    private JToolBar commentAdditionTools;
    private JPanel editorPanel;
    private JPanel additionPanel;
    public CommentFields() {
        LoadSplitters();
        commentTools.add(Pass_2021.passes.get(PassCode_2021.SaveBugReportComment).createButton());
        commentAdditionTools.add(Pass_2021.passes.get(PassCode_2021.AppendBugReportComment).createButton());
    }
    @Override
    public void setEditorScroll(JScrollPane scroll_in) {
        editorPanel.add(scroll_in);
    }
    @Override
    public void setAdditionScroll(JScrollPane scroll_in) {
        additionPanel.add(scroll_in);
    }
    @Override
    public JPanel getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        commentTools = new VisualiserMenuBar();
        commentAdditionTools = new VisualiserMenuBar();
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Global;
import Common.UI.Windows.Dialog.Dialog;
import Common.UI.Windows.FormType;
import GlobalData.FormsParams.DBForm;
public class ProfilesForm extends Dialog<Object, ProfilesFields> {
    private DBForm info = null;
    public ProfilesForm() {
        super(ProfilesFields.class);
    }
    @Override
    public boolean NeedsScroll() {
        return false;
    }
    @Override
    public void CreateButtons() {
    }
    @Override
    public void Init(Object... params) {
        if (Global.db.sapforProfiles.size() > 0) {
            for (Object key : Global.db.sapforProfiles.Data.keySet()) {
                Global.db.sapforProfiles.ShowUI(key);
                return;
            }
        } else {
            Global.db.sapforProfiles.ShowUI();
        }
    }
    @Override
    public int getDefaultWidth() {
        return 400;
    }
    @Override
    public int getDefaultHeight() {
        return 300;
    }
    @Override
    public void CreateContent() {
        super.CreateContent();
        fields.LoadSplitters();
        try {
            LoadWindowParameters();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    @Override
    public void onClose() {
        fields.SaveSplitters();
        try {
            SaveWindowParameters();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void LoadWindowParameters() throws Exception {
        if (Global.db.forms.Data.containsKey(FormType.Profiles)) {
            info = Global.db.forms.Data.get(FormType.Profiles);
            info.Apply(this);
            return;
        }
        setSize(getDefaultWidth(), getDefaultHeight());
    }
    public void SaveWindowParameters() throws Exception {
        if (info != null) {
            info.Init(this);
            Global.db.Update(info);
        } else
            Global.db.Insert(new DBForm(FormType.Profiles, this));
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Current;
import TestingSystem.Sapfor.SapforTask.SapforTask_2023;
//упразднить.
public class SapforTasksComparisonForm extends TasksComparisonForm<SapforTask_2023> {
    public SapforTasksComparisonForm(ComparisonForm<SapforTask_2023> slave_in) {
        super(SapforTask_2023.class, slave_in);
    }
    @Override
    protected Current getCurrentObjectName() {
        return Current.SapforTask;
    }
    @Override
    protected String getTextByTab() {

        return "";
    }
    @Override
    protected TasksComparisonState[] getForbiddenStates() {
        return new TasksComparisonState[]{
                TasksComparisonState.Sts
        };
    }
}

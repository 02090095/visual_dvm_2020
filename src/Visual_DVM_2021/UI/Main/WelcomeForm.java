package Visual_DVM_2021.UI.Main;
import Common.UI.DragDrop.FileDrop;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
public class WelcomeForm {
    public JPanel content;
    private JTextArea WelcomeText;
    public WelcomeForm() {
        // end filesDropped
        new FileDrop(System.out, WelcomeText, files -> {
            if (files.length > 0) {
                Pass_2021.passes.get(PassCode_2021.OpenCurrentProject).Do(files[0]);
            }
        });
    }
}

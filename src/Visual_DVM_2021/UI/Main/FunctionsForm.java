package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphForm;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphUI;
import Visual_DVM_2021.Passes.All.SPF_GetGraphFunctionPositions;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.FunctionsWindow;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FunctionsForm implements FunctionsWindow {
    private JPanel content;
    private JButton bFitToScreen;
    private JButton bZoomIn;
    private JButton bZoomOut;
    private JButton bSaveGraphAsImage;
    private JSpinner sIterations;
    private JSpinner sResistance;
    private JSpinner sScreen;
    private JToolBar tools;
    private JTextField filterName;
    private JCheckBox ShowStandardFilter;
    private JCheckBox ShowExternalFilter;
    private JCheckBox ShowUnreachableFilter;
    private JSpinner sDepth;
    private JCheckBox ShowIn;
    private JCheckBox ShowOut;
    private JPanel functionsGraphPanel;
    private JCheckBox cbShowByCurrentFunction;
    private JLabel lCurrentFunction;
    private JToolBar filtersBar;
    private JLabel depthLabel;
    private JButton bSaveCoordinates;
    private JToolBar currentFunctionTools;
    private FunctionsGraphForm functionsGraphForm;
    @Override
    public JPanel getContent() {
        return content;
    }
    @Override
    public Pair<Integer, Integer> getFunctionsGraphPanelSizes() {
        return new Pair<>(functionsGraphPanel.getWidth(), functionsGraphPanel.getHeight());
    }
    @Override
    public FunctionsGraphForm getFunctionsGraphWindow() {
        return functionsGraphForm;
    }
    @Override
    public void ShowFunctions() {
        functionsGraphForm.Show();
    }
    @Override
    public void ShowNoFunctions() {
        functionsGraphForm.Clear();
    }
    @Override
    public void ShowCurrentFunction() {
        lCurrentFunction.setText(Current.getFunction().funcName);
    }
    @Override
    public void ShowNoCurrentFunction() {
        lCurrentFunction.setText("?");
    }
    public void ShowFunctionGraphSettings() {
        ShowStandardFilter.setSelected(SPF_GetGraphFunctionPositions.showStandardFunctions);
        ShowExternalFilter.setSelected(SPF_GetGraphFunctionPositions.showExternalFunctions);
        ShowUnreachableFilter.setSelected(SPF_GetGraphFunctionPositions.showUnreachableFunctions);
        //-
        sIterations.setValue(Current.getProject().fgIterations);
        sResistance.setValue(Current.getProject().fgResistance);
        sScreen.setValue(Current.getProject().fgScreen);
        //-
        cbShowByCurrentFunction.setSelected(SPF_GetGraphFunctionPositions.showByCurrentFunction);
        ShowIn.setSelected(SPF_GetGraphFunctionPositions.showIn);
        ShowOut.setSelected(SPF_GetGraphFunctionPositions.showOut);
        sDepth.setValue(SPF_GetGraphFunctionPositions.depth);
    }
    public FunctionsForm() {
        LoadSplitters();
        bZoomIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                functionsGraphForm.control.zoomIn();
            }
        });
        bZoomOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                functionsGraphForm.control.zoomOut();
            }
        });
        bSaveGraphAsImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.SaveGraph).Do();
            }
        });
        sIterations.setModel(new SpinnerNumberModel(Current.getProject().fgIterations,
                100, 5000, 100
        ));
        sResistance.setModel(new SpinnerNumberModel(Current.getProject().fgResistance,
                10, 5000, 50
        ));
        sScreen.setModel(new SpinnerNumberModel(Current.getProject().fgScreen,
                0.25, 2.0, 0.05
        ));
        sDepth.setModel(new SpinnerNumberModel(1,
                0, 64, 1
        ));
        UI.MakeSpinnerRapid(sIterations, e -> {
            Current.getProject().UpdatefgIterations((int) sIterations.getValue());
            FunctionsGraphUI.ffTimer.restart();
        });
        UI.MakeSpinnerRapid(sResistance, e -> {
            Current.getProject().UpdatefgResistance((int) sResistance.getValue());
            FunctionsGraphUI.ffTimer.restart();
        });
        UI.MakeSpinnerRapid(sScreen, e -> {
            Current.getProject().UpdatefgScreen((double) sScreen.getValue());
            FunctionsGraphUI.ffTimer.restart();
        });
        UI.MakeSpinnerRapid(sDepth, e -> {
                SPF_GetGraphFunctionPositions.depth = (int) sDepth.getValue();
                FunctionsGraphUI.ffTimer.restart();
        });
        //----фильтрация функций
        //<editor-fold desc="Фильтрация функций">
        FunctionsGraphUI.ffTimer.setRepeats(false);
        filterName.setText(SPF_GetGraphFunctionPositions.filterName);
        filterName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                SPF_GetGraphFunctionPositions.filterName = filterName.getText();
                FunctionsGraphUI.ffTimer.restart();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                SPF_GetGraphFunctionPositions.filterName = filterName.getText();
                FunctionsGraphUI.ffTimer.restart();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        ShowFunctionGraphSettings();
        //-
        ShowStandardFilter.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showStandardFunctions = ShowStandardFilter.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowExternalFilter.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showExternalFunctions = ShowExternalFilter.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowUnreachableFilter.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showUnreachableFunctions = ShowUnreachableFilter.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        cbShowByCurrentFunction.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showByCurrentFunction = cbShowByCurrentFunction.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowIn.addActionListener(e -> {
                SPF_GetGraphFunctionPositions.showIn = ShowIn.isSelected();
                FunctionsGraphUI.ffTimer.stop();
                Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowOut.addActionListener(e -> {
                SPF_GetGraphFunctionPositions.showOut = ShowOut.isSelected();
                FunctionsGraphUI.ffTimer.stop();
                Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        //---------------------------------------------------------
        filtersBar.add(Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).createButton(), 0);
        filtersBar.add(Pass_2021.passes.get(PassCode_2021.ApplyCurrentFunction).createButton(), 6);
        filtersBar.setPreferredSize(new Dimension(0,30));
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        functionsGraphPanel = (functionsGraphForm = new FunctionsGraphForm()).getContent();
        filterName = new StyledTextField();
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.UI.Trees.TreeForm;
import Common.Utils.Utils;
import ProjectData.SapforData.Arrays.UI.DimensionsTableForm;
import ProjectData.SapforData.Arrays.UI.RulesTree;
import Visual_DVM_2021.UI.Interface.VariantsWindow;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class VariantsForm implements VariantsWindow {
    public JSplitPane SC4;
    public JSplitPane SC5;
    private JButton BDistributed;
    private JButton bMultiplied;
    private JPanel dimensionsPanel;
    private JPanel distributionPanel;
   // private JLabel SelectedVariantsCount;
    private JLabel VisibleVariantsCount;
    private JLabel TotalVariantsCount;
    private JPanel variantsPanel;
    private JPanel content;
    private JToolBar variantsFilterTools1;
    //--
    public TreeForm distributionForm1;
    private DimensionsTableForm dimensionsForm;
    //--
    public VariantsForm() {
        LoadSplitters();
        Current.getProject().parallelVariants.mountUI(variantsPanel);
        //-
        BDistributed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Current.getProject().SwitchFilterDistributed();
                BDistributed.setIcon(Utils.getIcon(Current.getProject().f_distributed() ? "/icons/Pick.png" : "/icons/NotPick.png"));
                ShowVariantsFilter();
                ShowFilteredVariantsCount();
            }
        });
        bMultiplied.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Current.getProject().SwitchFilterMultiplied();
                bMultiplied.setIcon(Utils.getIcon(Current.getProject().f_multiplied() ? "/icons/Pick.png" : "/icons/NotPick.png"));
                ShowVariantsFilter();
                ShowFilteredVariantsCount();
            }
        });
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        distributionPanel = (distributionForm1 = new TreeForm(RulesTree.class)).getContent();
        dimensionsPanel = (dimensionsForm = new DimensionsTableForm()).getContent();
    }
    @Override
    public JPanel getContent() {
        return content;
    }
    @Override
    public void ShowProjectDistribution() {
        distributionForm1.Show();
    }
    @Override
    public void ShowNoProjectDistribution() {
        distributionForm1.Clear();
    }
    @Override
    public void ShowVariantsFilterButtons() {
        BDistributed.setIcon(Utils.getIcon(Current.getProject().f_distributed() ? "/icons/Pick.png" : "/icons/NotPick.png"));
        bMultiplied.setIcon(Utils.getIcon(Current.getProject().f_multiplied() ? "/icons/Pick.png" : "/icons/NotPick.png"));
    }
    @Override
    public void ShowVariantsFilter() {
        dimensionsForm.Show();
    }
    @Override
    public void ShowNoVariantsFilter() {
        dimensionsForm.Clear();
    }
    @Override
    public void ShowFilteredVariantsCount() {
        VisibleVariantsCount.setText(String.valueOf(Current.getProject().getFilteredVariantsCount()));
    }
    @Override
    public void ShowTotalVariantsCount() {
        TotalVariantsCount.setText(String.valueOf(Current.getProject().getTotalVariantsCount()));
    }
    @Override
    public void ShowCheckedVariantsCount() {
      //  SelectedVariantsCount.setText(Current.getProject().CheckedVariantsCounter.toString());
    }
    @Override
    public void ShowVariants() {
        Current.getProject().parallelVariants.ShowUI();
    }
    @Override
    public void RefreshVariants() {
        Current.getProject().parallelVariants.RefreshUI();
    }
    @Override
    public void ShowNoVariants() {
        Current.getProject().parallelVariants.ClearUI();
    }
    @Override
    public void ShowNoCheckedVariantsCount() {

        //SelectedVariantsCount.setText("0");
    }
    @Override
    public void ShowNoFilteredVariantsCount() {
        VisibleVariantsCount.setText("0");
    }
    @Override
    public void ShowNoTotalVariantsCount() {
        TotalVariantsCount.setText("0");
    }
}

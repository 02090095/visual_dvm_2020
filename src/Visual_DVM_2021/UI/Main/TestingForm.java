package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.Global;
import Common.UI.Menus_2023.TasksPackagesMenuBar.TasksPackagesMenuBar;
import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Compiler.CompilerType;
import GlobalData.Credentials.Credentials;
import TestingSystem.Configuration.UI.ConfigurationDBTable;
import TestingSystem.Group.Group;
import TestingSystem.MachineMaxKernels.MachineMaxKernels;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackage_2023;
import TestingSystem.TasksPackage.TasksPackageDBTable;
import TestingSystem.Test.Test;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.TestingWindow;

import javax.swing.*;
import java.util.Vector;
public class TestingForm implements FormWithSplitters, TestingWindow {
    private JPanel content;
    @Override
    public JPanel getContent() {
        return content;
    }
    public JSplitPane SC18;
    public JSplitPane SC20;
    public JSplitPane SC21;
    public JSplitPane SC35;
    public JSplitPane SC40;
    public JSplitPane SC41;
    public JSplitPane SC42;
    public JSplitPane SC43;
    public JSplitPane SC45;
    public JSplitPane SC50;
    public JSplitPane SC51;
    public JSplitPane SC52;
    public JSplitPane SC48;
    public JSplitPane SC49;
    public JSplitPane SC46;
    public JSplitPane SC53;
    public JSplitPane SC56;
    public JSplitPane SC57;
    public JSplitPane SC58;
    public JSplitPane SC59;
    public JSplitPane SC19;
    //-
    //---
    private JPanel machinesPanel;
    private JPanel usersPanel;
    private JPanel compilersPanel;
    private JTabbedPane testingTabs;
    private JTabbedPane debugTabs;
    private JPanel makefilesPanel;
    private JPanel modulesPanel;
    private JPanel runConfigurationsPanel;
    private JToolBar environmentsTools;
    private JPanel environmentsPanel;
    private JToolBar parametersTools;
    private JPanel dvmParametersPanel;
    private JPanel groupsPanel;
    private JPanel testsPanel;
    private JPanel testsRunTasksPanel;
    private JToolBar testsResultsTools;
    private JButton bChangeKernels;
    private JTextField filterName;
    private JLabel testsMatchesLabel;
    private JPanel configurationsPanel;
    private JLabel configurationsMatchesLabel;
    private JLabel packagesMatchesLabel;
    private JPanel packagesPanel;
    private JButton bCompilationFilter;
    private JPanel dvmTestingPanel;
    private JPanel projectDebugPanel;
    private JPanel runTasksPanel;
    private JPanel compilationTasksPanel;
    private JPanel sapforTestingPanel;
    private JPanel packageVersionsPanel;
    private JPanel sapforsPanel;
    private JPanel sapforConfigurationsPanel;
    private JLabel sapforConfigurationsLabel;
    private JLabel sapforConfigurationsCommandsLabel;
    private JPanel sapforConfigurationCommandsPanel;
    private JPanel sapforPackagesPanel;
    private JLabel sapforTasksPackagesLabel;
    private JLabel sapforTasksLabel;
    private JPanel groupsBackground;
    private JPanel testsRunTasksBackground;
    private JCheckBox filterFinished;
    private JButton bTest;
    private JButton bDownloadTestProject;
    private JToolBar compilationTasksTools;
    private JToolBar runTasksTools;
    //-
    public RunTasksComparisonForm singleRunTaskMaster; // одиночные запуски
    public RunTasksComparisonForm singleRunTaskSlave;
    //
    public CompilationTasksComparisonForm singleCompilationTaskMaster; // одиночная компиляция
    public CompilationTasksComparisonForm singleCompilationTaskSlave;
    //--
    //-
    private TestRunTasksComparisonForm dvmTestingRunMaster; //сравнение тестов двм системы.
    private TestRunTasksComparisonForm dvmTestingRunSlave;
    //-
    //сравнение версий полученных в результате выполнения пакета.
    public SapforPackagesComparisonForm sapforTestingMaster;
    public SapforPackagesComparisonForm sapforTestingSlave;
    //-
    public TestingForm() {
        LoadSplitters();
        //-
        singleCompilationTaskMaster = new CompilationTasksComparisonForm(singleCompilationTaskSlave = new CompilationTasksComparisonForm(null));
        singleRunTaskMaster = new RunTasksComparisonForm(singleRunTaskSlave = new RunTasksComparisonForm(null));
        dvmTestingRunMaster = new TestRunTasksComparisonForm(dvmTestingRunSlave = new TestRunTasksComparisonForm(null));
        //-
        Global.db.machines.mountUI(machinesPanel);
        Global.db.users.mountUI(usersPanel);
        Global.db.compilers.mountUI(compilersPanel);
        Global.db.remoteSapfors.mountUI(sapforsPanel);
        Global.db.makefiles.mountUI(makefilesPanel);
        //-----------------------------------------------
        Global.db.modules.mountUI(modulesPanel);
        Global.db.runConfigurations.mountUI(runConfigurationsPanel);
        Global.db.environmentValues.mountUI(environmentsPanel);
        Global.db.dvmParameters.mountUI(dvmParametersPanel);
        //-
        Global.db.compilationTasks.mountUI(compilationTasksPanel);
        Global.db.runTasks.mountUI(runTasksPanel);
        //--->>>
        RefreshTabsNames();
        //-
        SC41.setLeftComponent(singleCompilationTaskMaster.getContent());
        SC41.setRightComponent(singleCompilationTaskSlave.getContent());
        SC43.setLeftComponent(singleRunTaskMaster.getContent());
        SC43.setRightComponent(singleRunTaskSlave.getContent());
        //-
        SC50.setLeftComponent(dvmTestingRunMaster.getContent());
        SC50.setRightComponent(dvmTestingRunSlave.getContent());
        ShowNoProject();
        //-
        Global.testingServer.db.configurations.mountUI(configurationsPanel);
        Global.testingServer.db.groups.mountUI(groupsPanel);
        Global.testingServer.db.tests.mountUI(testsPanel);
        Global.testingServer.account_db.packages.mountUI(packagesPanel);
        Global.testingServer.account_db.testRunTasks.mountUI(testsRunTasksPanel);
        //   Global.testingServer.account_db.testRunTasks.setFilterUI(count -> tasksMatchesLabel.setText(String.valueOf(count)));
        //--
        Global.testingServer.db.sapforConfigurations.mountUI(sapforConfigurationsPanel);
        Global.testingServer.db.sapforConfigurationCommands.mountUI(sapforConfigurationCommandsPanel);
        Global.db.sapforTasksPackages.mountUI(sapforPackagesPanel);
        //Global.db.sapforTasks.mountUI(sapforTasksPanel);
        // Global.testingServer.account_db.sapforTasksPackages.mountUI(sapforPackagesPanel);
        // Global.testingServer.account_db.sapforTasks.mountUI(sapforTasksPanel);
        //--------------------------------------------------------------------->>>>
        /*
        filterName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                TestRunTaskInterface.filterName = filterName.getText();
                Global.testingServer.account_db.testRunTasks.ShowUI();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                TestRunTaskInterface.filterName = filterName.getText();
                Global.testingServer.account_db.testRunTasks.ShowUI();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
         */
        sapforTestingMaster = new SapforPackagesComparisonForm(sapforTestingSlave = new SapforPackagesComparisonForm(null));
        SC57.setLeftComponent(sapforTestingMaster.getContent());
        SC57.setRightComponent(sapforTestingSlave.getContent());
    }
    //-
    @Override
    public void ShowAll() {
        Global.db.machines.ShowUI();
        this.RestoreLastCredentials();
        Global.testingServer.db.configurations.ShowUI();
        Global.testingServer.db.groups.ShowUI();
        Global.testingServer.account_db.packages.ShowUI();
        Global.testingServer.db.sapforConfigurations.ShowUI();
        Global.db.sapforTasksPackages.ShowUI();
    }
    //-
    @Override
    public void ShowProjectMaxCompilationTime() {
        //   mCompilationMaxtime.setText(Current.getProject().compilation_maxtime + " сек.");
    }
    @Override
    public void ShowProjectMaxRunTime() {
        //mRunMaxtime.setText(Current.getProject().run_maxtime + " сек.");
    }
    @Override
    public void ShowCurrentTestsPackage() {
        ShowCurrentTestRunTask();
    }
    @Override
    public void DropTestRunTasksComparison() {
        dvmTestingRunMaster.RemoveObject();
        dvmTestingRunSlave.RemoveObject();
    }
    @Override
    public void DropRunTasksComparison() {
        singleRunTaskMaster.RemoveObject();
        singleRunTaskSlave.RemoveObject();
    }
    @Override
    public void DropCompilationTasksComparison() {
        singleCompilationTaskMaster.RemoveObject();
        singleCompilationTaskSlave.RemoveObject();
    }
    @Override
    public void FocusCredentials() {
        testingTabs.setSelectedIndex(0);
    }
    @Override
    public void ShowProject() {
        testingTabs.setEnabledAt(1, true);
        // testingTabs.setSelectedIndex(0);
        //-
        ShowSession();
        //-
        Global.db.compilationTasks.ShowUI(); //их видимость зависит от текущего проекта.
        Global.db.runTasks.ShowUI();
    }
    @Override
    public void ShowNoProject() {
        Global.db.compilationTasks.ClearUI();
        Global.db.runTasks.ClearUI();
        //-
        if (testingTabs.getSelectedIndex() == 1)
            testingTabs.setSelectedIndex(0);
        testingTabs.setEnabledAt(1, false);
    }
    @Override
    public void SaveSplitters() {
        FormWithSplitters.super.SaveSplitters();
        try {
            Global.db.SaveCredentials();
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
    }
    //-
    @Override
    public void ShowCurrentCompilationTask() {
        // if (singleCompilationTaskMaster.isActive()) singleCompilationTaskMaster.ShowTask();
        // else singleCompilationTaskSlave.ShowTask();
    }
    @Override
    public void ShowNoCurrentCompilationTask() {
        //  if (singleCompilationTaskMaster.isActive()) singleCompilationTaskMaster.Clear();
        // else singleCompilationTaskSlave.Clear();
    }
    @Override
    public void ShowCurrentRunTask() {
        //   if (singleRunTaskMaster.isActive()) singleRunTaskMaster.ShowTask();
        //   else singleRunTaskSlave.ShowTask();
    }
    @Override
    public void ShowNoCurrentRunTask() {
        //  if (singleRunTaskMaster.isActive()) singleRunTaskMaster.Clear();
        //  else singleRunTaskSlave.Clear();
    }
    @Override
    public void ShowCurrentTestRunTask() {
        // if (dvmTestingRunMaster.isActive()) dvmTestingRunMaster.ShowTask();
        //  else dvmTestingRunSlave.ShowTask();
    }
    @Override
    public void ShowNoTestRunTask() {
        //  dvmTestingRunMaster.Clear();
        // dvmTestingRunSlave.Clear();
    }
    @Override
    public void RefreshTabsNames() {
    }
    @Override
    public void ShowCurrentMachine() {
        String url = Current.getMachine().getURL();
        int kernelsNum = 0;
        if (Global.testingServer.db.machinesMaxKernels.containsKey(url)) {
            MachineMaxKernels kernels = Global.testingServer.db.machinesMaxKernels.get(url);
            kernelsNum = kernels.limit;
        } else kernelsNum = 4;
        //  bChangeKernels.setText("Ядра: " + kernelsNum);
    }
    @Override
    public void ShowNoCurrentMachine() {
        //bChangeKernels.setText("Ядра: ?");
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        filterName = new StyledTextField();
    }
    @Override
    public void FocusTestingSystem() {
        testingTabs.setSelectedIndex(2);
    }
    @Override
    public void ShowAutoActualizeTestsState() {
        ((TasksPackagesMenuBar) UI.menuBars.get(TasksPackageDBTable.class)).ShowAutorefresh();
    }
    @Override
    public void FocusTestingTasks() {
    }
    @Override
    public boolean isEmailTestingOn() {
        return
                ConfigurationDBTable.email;
    }
    @Override
    public void RemountTestTable() {
        UI.Clear(packagesPanel);
        UI.Clear(testsRunTasksPanel);
        Global.testingServer.account_db.packages.mountUI(packagesPanel);
        Global.testingServer.account_db.testRunTasks.mountUI(testsRunTasksPanel);
    }
    @Override
    public void setUserRights() {
        // testingTabs.removeTabAt(2);
    }
    @Override
    public void setAdminRights() {
        /*
        testingTabs.insertTab("DVM система",
                Utils.getIcon("/icons/DVM.png"),
                dvmTestingPanel,
                "DVM система", 2);

         */
    }
    @Override
    public void setDeveloperRights() {
        /*
        testingTabs.insertTab("DVM система",
                Utils.getIcon("/icons/DVM.png"),
                dvmTestingPanel,
                "DVM система", 2);
         */
    }
    @Override
    public void FocusScenarios() {
        testingTabs.setSelectedIndex(3);
    }
    //-
    @Override
    public void RestoreLastCredentials() {
        Credentials credentials = (Credentials) Current.get(Current.Credentials);
        if (credentials.machine_id != Utils.Nan) {
            if (Global.db.machines.containsKey(credentials.machine_id)) {
                Global.db.machines.ShowUI(credentials.machine_id);
                if (Global.db.users.containsKey(credentials.user_id)) {
                    Global.db.users.ShowUI(credentials.user_id);
                } else {
                    credentials.user_id = Utils.Nan;
                    Global.db.UpdateCredentials();
                }
                if (Global.db.compilers.containsKey(credentials.compiler_id)) {
                    Global.db.compilers.ShowUI(credentials.compiler_id);
                } else {
                    credentials.compiler_id = Utils.Nan;
                    Global.db.UpdateCredentials();
                }
                //-
                if (Global.db.makefiles.containsKey(credentials.makefile_id)) {
                    Global.db.makefiles.ShowUI(credentials.makefile_id);
                } else {
                    credentials.makefile_id = Utils.Nan;
                    Global.db.UpdateCredentials();
                }
                //-
                if (Global.db.runConfigurations.containsKey(credentials.runconfiguration_id)) {
                    Global.db.runConfigurations.ShowUI(credentials.runconfiguration_id);
                } else {
                    credentials.runconfiguration_id = Utils.Nan;
                    Global.db.UpdateCredentials();
                }
                if (Global.db.remoteSapfors.containsKey(credentials.remotesapfor_id)) {
                    Global.db.remoteSapfors.ShowUI(credentials.remotesapfor_id);
                } else {
                    credentials.remotesapfor_id = Utils.Nan;
                    Global.db.UpdateCredentials();
                }
            } else {
                credentials.machine_id = Utils.Nan;
                credentials.user_id = Utils.Nan;
                credentials.compiler_id = Utils.Nan;
                credentials.remotesapfor_id = Utils.Nan;
                credentials.makefile_id = Utils.Nan;
                credentials.runconfiguration_id = Utils.Nan;
                Global.db.UpdateCredentials();
            }
        }
    }
    // testingTabs.setTitleAt(4, "Cистема SAPFOR: " + Current.getRemoteSapfor().version);
    @Override
    public void ShowCredentials() {
        String res = "";
        if (Current.HasMachine()) {
            res += "@" + Current.getMachine().getURL();
            if (Current.HasUser()) {
                res = Current.getUser().login + res;
            } else res = "?" + res;
            if (Current.HasCompiler() && Current.getCompiler().type.equals(CompilerType.dvm)) {
                testingTabs.setTitleAt(3, "DVM система: " + Current.getCompiler().description);
            } else {
                testingTabs.setTitleAt(3, "DVM система: ?");
            }
        } else {
            res += "?@?";
        }
        testingTabs.setTitleAt(0, res);
    }
    @Override
    public void FocusSapforTasksPackages() {
    }
    @Override
    public void ShowCurrentSapforTasksPackage() {
        /*
        UI.Clear(packageVersionsPanel);
        if (Current.getSapforTasksPackage().root != null) {
            PackageVersionsTree tree = new PackageVersionsTree();
            packageVersionsPanel.add(new JScrollPane(tree));
            packageVersionsPanel.revalidate();
            packageVersionsPanel.repaint();
        }
         */
    }
    @Override
    public void ShowNoSapforTasksPackage() {
        UI.Clear(packageVersionsPanel);
    }
    //--->>>
    @Override
    public void ShowCheckedTestsCount() {
        int res = 0;
        for (Group group : Global.testingServer.db.groups.getCheckedItems()) {
            Vector<Test> selected_tests = new Vector<>();
            Vector<Test> group_tests = new Vector<>();
            for (Test test : Global.testingServer.db.tests.Data.values()) {
                if (test.group_id.equals(group.id)) {
                    if (test.isSelected())
                        selected_tests.add(test);
                    else
                        group_tests.add(test);
                }
            }
            //-
            if (selected_tests.isEmpty())
                res += group_tests.size();
            else
                res += selected_tests.size();
        }
        //--
        testingTabs.setTitleAt(2, "Тесты: " + res);
    }
    @Override
    public void ShowLastCompilationTask() {
        singleCompilationTaskMaster.ApplyObject();
    }
    @Override
    public void ShowLastRunTask() {
        singleRunTaskMaster.ApplyObject();
    }

    @Override
    public void RemoveSapforPackageFromComparison(SapforTasksPackage_2023 package_2023) {
        sapforTestingMaster.CheckObject(package_2023);
        sapforTestingSlave.CheckObject(package_2023);
    }
    @Override
    public void DropSapforTasksComparison() {
        sapforTestingMaster.RemoveObject();
        sapforTestingSlave.RemoveObject();
    }
}

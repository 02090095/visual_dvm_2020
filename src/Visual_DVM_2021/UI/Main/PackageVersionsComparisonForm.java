package Visual_DVM_2021.UI.Main;
import Common.Current;
public class PackageVersionsComparisonForm extends VersionsComparisonForm{
    public PackageVersionsComparisonForm(PackageVersionsComparisonForm slave_in) {
        super(slave_in);
    }
    @Override
    protected Current getCurrentObjectName() {
        return Current.PackageVersion;
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Global;
import Common.UI.Editor.Viewer;
import Common.UI.Menus_2023.FileMenuBar.FileMenuBar;
import Common.UI.Trees.TreeForm;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.UI.Editor.SPFEditor;
import ProjectData.LanguageName;
import ProjectData.Messages.Errors.MessageError;
import ProjectData.Messages.Notes.MessageNote;
import ProjectData.Messages.Warnings.MessageWarning;
import ProjectData.SapforData.Arrays.UI.FileArraysTree;
import ProjectData.SapforData.Functions.UI.FileFunctionsTree;
import ProjectData.SapforData.Loops.UI.FileLoopsTree;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.FileWindow;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.SPFEditorInterface;
import com.sun.org.glassfish.gmbal.Description;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import java.awt.*;
public class FileForm implements FileWindow, FormWithSplitters {
    //контролы в столбцах
    //https://stackoverflow.com/questions/7137786/how-can-i-put-a-control-in-the-jtableheader-of-a-jtable
    //todo _ временно
    public static int current_file_line = Utils.Nan;
    private final Viewer compilationOutput;
    private final Viewer runOutput;
    private final Viewer gcovLog;
    private final DBProjectFile file;
    //-
    public JSplitPane SC1;
    public JSplitPane SC12;
    //-
    private JPanel content;
    @Description("IGNORE")
    protected boolean events_on = true;
    private JButton bClose;
    private JToolBar Tools;
    private JPanel editorPanel;
    private JTabbedPane messagesTabs;
    private JPanel notesPanel;
    private JPanel warningsPanel;
    private JPanel errorsPanel;
    private JTabbedPane graphsTabs;
    private JPanel loopsGraphPanel;
    private JPanel functionsGraphPanel;
    private JPanel arraysGraphPanel;
    private JPanel gcov_log_panel;
    private JPanel compilation_out_panel;
    private JPanel run_out_panel;
    //---------------->
    private JPanel recommendationsPanel;
    private JPanel graphsBackground;
    private JPanel messagesBackground;
    //--------------->
    //-
    private TreeForm loopsForm; //рудиментарно. следует сделать интерфейс такой же как у таблиц
    private TreeForm functionsForm;
    private TreeForm arraysForm;
    //-
    private SPFEditor Body = null; //времянка? не очень красиво.
    FileMenuBar menuBar = null;
    //-
    public FileForm(DBProjectFile file_in) {
        LoadSplitters();
        //--
        MessageWarning.filterValue = "";
        MessageError.filterValue = "";
        MessageNote.filterValue="";
        //-
        file = file_in;
        file.father.db.notes.mountUI(notesPanel);
        file.father.db.warnings.mountUI(warningsPanel);
        file.father.db.errors.mountUI(errorsPanel);
        file.father.db.recommendations.mountUI(recommendationsPanel);
        //---------------------------------------------------------------------------
        compilation_out_panel.add(new JScrollPane(compilationOutput = new Viewer()));
        run_out_panel.add(new JScrollPane(runOutput = new Viewer()));
        gcov_log_panel.add(new JScrollPane(gcovLog = new Viewer()));
        //----------------------------------------------------------------------------
        ShowText();
        ShowMessages();
        ShowGCOVLog();
        ShowAllAnalyses();
        Body.addCaretListener(ce -> ShowCaretInfo());
        editorPanel.add(menuBar = new FileMenuBar(Body), BorderLayout.NORTH);
        ShowCaretInfo();
        Body.requestFocus();
        RefreshTabsNames();
        menuBar.sToGo.addChangeListener(e -> Body.gotoLine((Integer) menuBar.sToGo.getValue()));
        ShowProperties();
        Pass_2021.passes.get(PassCode_2021.Save).setControlsEnabled(false);
    }
    @Override
    public void ShowLanguage() {
        Body.switching_language = true;
        if (file.languageName.equals(LanguageName.fortran)) {
            switch (file.style) {
                case fixed:
                case extended:
                case none:
                    Body.setSyntaxEditingStyle("text/FortranSPF");
                    break;
                case free:
                    Body.setSyntaxEditingStyle("text/FreeFortranSPF");
                    break;
            }
        } else
            Body.setSyntaxEditingStyle(file.languageName.getStyleKey());
        Body.setCodeFoldingEnabled(true);
        Body.switching_language = false;
        menuBar.ShowLanguage();
    }
    @Override
    public void ShowType() {
        menuBar.ShowType();
    }
    @Override
    public void ShowStyle() {
        Body.switching_language = true;
        int m = file.style.getMargin();
        Body.setMarginLineEnabled(true);
        Body.setMarginLinePosition(m);
        if (m <= 0)
            Body.setMarginLineEnabled(false);
        if (file.languageName.equals(LanguageName.fortran)) {
            switch (file.style) {
                case fixed:
                case extended:
                case none:
                    Body.setSyntaxEditingStyle("text/FortranSPF");
                    break;
                case free:
                    Body.setSyntaxEditingStyle("text/FreeFortranSPF");
                    break;
            }
        }
        Body.switching_language = false;
        menuBar.ShowStyle();
    }
    @Override
    public SPFEditorInterface getEditor() {
        return Body;
    }
    @Override
    public void FocusLoops() {
        graphsTabs.setSelectedIndex(0);
    }
    @Override
    public void FocusFunctions() {
        graphsTabs.setSelectedIndex(1);
    }
    @Override
    public void FocusArrays() {
        graphsTabs.setSelectedIndex(2);
    }
    //----
    @Override
    public void ShowWarningsCount() {
        boolean flag = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        messagesTabs.setTitleAt(1, (flag ? "Предупреждения: " : "") + file.father.db.warnings.getVisibleKeys().size());
    }
    @Override
    public void ShowErrorsCount() {
        boolean flag = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        messagesTabs.setTitleAt(2, (flag ? "Ошибки: " : "") + file.father.db.errors.getVisibleKeys().size());
    }
    @Override
    public void ShowNotesCount() {
        boolean flag = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        messagesTabs.setTitleAt(0, (flag ? "Примечания: " : "") + file.father.db.notes.getVisibleKeys().size());
    }
    @Override
    public void ShowRecommendationsCount() {
        boolean flag = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        messagesTabs.setTitleAt(3, (flag ? "Рекомендации: " : "") + file.father.db.recommendations.getVisibleKeys().size());
    }
    //---
    @Override
    public void RefreshTabsNames() {
        UI.ShowTabsNames(graphsTabs);
        boolean flag = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        //--
        ShowNotesCount();
        ShowWarningsCount();
        ShowErrorsCount();
        ShowRecommendationsCount();
        //--
        messagesTabs.setTitleAt(4, (flag ? "Компиляция" : ""));
        messagesTabs.setTitleAt(5, (flag ? "Запуск" : ""));
        messagesTabs.setTitleAt(6, (flag ? "Журнал GCOV" : ""));
    }
    @Override
    public void FocusCompilationOut() {
        messagesTabs.setSelectedIndex(4);
    }
    @Override
    public void ShowText() {
        UI.Clear(editorPanel);
        editorPanel.add(new RTextScrollPane(Body = new SPFEditor(file)));
        UI.getSearchReplaceForm().setEditor(Body);
        if (UI.getSearchReplaceForm().isVisible())
            UI.getSearchReplaceForm().requestFocus();
    }
    @Override
    public void ShowMessages() {
        file.father.db.notes.ShowUI();
        file.father.db.warnings.ShowUI();
        file.father.db.errors.ShowUI();
        file.father.db.recommendations.ShowUI();
        ShowNotesCount();
        ShowWarningsCount();
        ShowErrorsCount();
        ShowRecommendationsCount();
        //-
        FocusMessagesPriority();
    }
    @Override
    public void ShowNoMessages() {
        boolean full_tiles = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        file.father.db.notes.ClearUI();
        file.father.db.warnings.ClearUI();
        file.father.db.errors.ClearUI();
        file.father.db.recommendations.ClearUI();
        messagesTabs.setTitleAt(0, (full_tiles ? "Примечания: " : "") + "-");
        messagesTabs.setTitleAt(1, (full_tiles ? "Предупреждения: " : "") + "-");
        messagesTabs.setTitleAt(2, (full_tiles ? "Ошибки: " : "") + "-");
        messagesTabs.setTitleAt(3, (full_tiles ? "Рекомендации: " : "") + "-");
    }
    @Override
    public void ShowLoops() {
        loopsForm.Show();
        Body.HighlightLoops();
    }
    @Override
    public void ShowNoLoops() {
        loopsForm.Clear();
        Body.ClearLoopsHighLights();
    }
    @Override
    public void ShowGCOVLog() {
        gcovLog.setText(file.GCOVLog);
    }
    @Override
    public void ShowGCOV() {
        Body.HighlightGCOV();
    }
    @Override
    public void ShowNoGCOVLog() {
        gcovLog.setText("");
    }
    @Override
    public void FocusGCOVLog() {
        messagesTabs.setSelectedIndex(6);
    }
    @Override
    public void ShowNoGCOV() {
        Body.ClearGOCVHighlights();
    }
    @Override
    public void ShowFunctions() {
        functionsForm.Show();
    }
    @Override
    public void ShowNoFunctions() {
        functionsForm.Clear();
    }
    @Override
    public void ShowArrays() {
        arraysForm.Show();
    }
    @Override
    public void ShowNoArrays() {
        arraysForm.Clear();
    }
    public void EventsOn() {
        events_on = true;
    }
    public void EventsOff() {
        events_on = false;
    }
    @Override
    public void ShowCaretInfo() {
        int new_current_line = Body.getCurrentLine();
        boolean line_changed = (new_current_line != current_file_line);
        current_file_line = new_current_line;
        menuBar.sToGo.setModel(new SpinnerNumberModel(current_file_line,
                1, Body.getLineCount(), 1
        ));
        //-
        menuBar.ShowLinesCount();
        menuBar.CurrentSymbolLabel.setText(String.valueOf(Body.getCurrentSymbol()));
        //если выделяем строку из графа, то тоже надо отключиьт события.
        //чтобы по ней не начался поиск.
        if (events_on && line_changed) {
            // System.out.println("Search in graphs by line...");
            if (loopsForm.getTree() != null)
                loopsForm.getTree().SelectNodeByCriteria(current_file_line);
            if (functionsForm.getTree() != null)
                functionsForm.getTree().SelectNodeByCriteria(current_file_line);
            if (arraysForm.getTree() != null)
                arraysForm.getTree().SelectNodeByCriteria(current_file_line);
            //  System.out.println("DONE");
        }
    }
    @Override
    public void FocusMessagesPriority() {
        switch (file.state) {
            case HasErrors:
                messagesTabs.setSelectedIndex(2);
                break;
            case HasWarnings:
                messagesTabs.setSelectedIndex(1);
                break;
            default:
                messagesTabs.setSelectedIndex(0);
                break;
        }
    }
    @Override
    public void ShowFirstError() {
        if (file.father.db.errors.ui_.control.getRowCount() > 0)
            file.father.db.errors.ui_.control.SelectRow(0);
    }
    @Override
    public void ShowCompilationOutput() {
        compilationOutput.setText(file.father.compilation_output);
    }
    @Override
    public void ShowNoCompilationOutput() {
        compilationOutput.setText("");
    }
    @Override
    public void ShowRunOutput() {
        runOutput.setText(file.father.run_output);
    }
    @Override
    public void ShowNoRunOutput() {
        runOutput.setText("");
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        loopsGraphPanel = (loopsForm = new TreeForm(FileLoopsTree.class)).getContent();
        functionsGraphPanel = (functionsForm = new TreeForm(FileFunctionsTree.class)).getContent();
        arraysGraphPanel = (arraysForm = new TreeForm(FileArraysTree.class)).getContent();
    }
    @Override
    public JPanel getContent() {
        return content;
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Global;
import Common.UI.Windows.Dialog.DialogFields;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;

import javax.swing.*;
import java.awt.*;
public class ProfilesFields implements DialogFields, FormWithSplitters {
    private JPanel content;
    public JSplitPane SC61;
    private JPanel profilesPanel;
    private JPanel settingsPanel;
    @Override
    public Component getContent() {
        return content;
    }
    public ProfilesFields(){
        Global.db.sapforProfiles.mountUI(profilesPanel);
        Global.db.sapforProfilesSettings.mountUI(settingsPanel);
    }
}

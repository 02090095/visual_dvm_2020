package Visual_DVM_2021.UI.Main;
import Common.UI.Trees.TreeForm;
import Common.UI.UI;
import ProjectData.Project.UI.VersionsTree;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.VariantsWindow;
import Visual_DVM_2021.UI.Interface.VersionsWindow;

import javax.swing.*;
import java.awt.*;
public class VersionsForm implements FormWithSplitters, VersionsWindow {
    private final db_project_info root;
    private JPanel content;
    //-
    public JSplitPane SC2;
    public JSplitPane SC9;
    public VersionsComparisonForm MasterComparsionForm;
    public VersionsComparisonForm SlaveComparsionForm;
    private JPanel versionsPanel;
    private JTabbedPane versionsTabs;
    private JPanel variantsFormPanel;
    private JPanel versionsBackground;
    private TreeForm versionsTreeForm;
    //-
    private VariantsWindow variantsForm = null;
    public VersionsForm(db_project_info root_in) {
        LoadSplitters();
        root = root_in;
        MasterComparsionForm = new VersionsComparisonForm(SlaveComparsionForm = new VersionsComparisonForm(null));
        SC9.setLeftComponent(MasterComparsionForm.getContent());
        SC9.setRightComponent(SlaveComparsionForm.getContent());
        versionsTreeForm.Show();
        //-
        versionsTabs.setEnabledAt(0, false);
        versionsPanel.add(UI.versionsMenuBar, BorderLayout.NORTH);
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        versionsPanel = (versionsTreeForm = new TreeForm(VersionsTree.class)).getContent();
    }
    @Override
    public VariantsWindow getVariantsWindow() {
        return variantsForm;
    }
    @Override
    public void ShowProjectVariants() {
        variantsFormPanel.add((variantsForm = new VariantsForm()).getContent());
        versionsTabs.setEnabledAt(0, true);
    }
    @Override
    public void ShowNoProjectVariants() {
        UI.Clear(variantsFormPanel);
        versionsTabs.setEnabledAt(0, false);
    }
    @Override
    public void FocusDistribution() {
        versionsTabs.setSelectedIndex(0);
    }
    @Override
    public void BlockVariants() {
        versionsTabs.setEnabledAt(0, false);
        versionsTabs.setSelectedIndex(1);
    }
    @Override
    public void UnblockVariants() {
        versionsTabs.setEnabledAt(0, true);
    }
    @Override
    public void RemoveVersionFromComparison(db_project_info version) {
        MasterComparsionForm.CheckVersion(version);
        SlaveComparsionForm.CheckVersion(version);
    }
    @Override
    public TreeForm getVersionsForm() {
        return versionsTreeForm;
    }
    @Override
    public JPanel getContent() {
        return content;
    }
    @Override
    public void SaveSplitters() {
        FormWithSplitters.super.SaveSplitters();
        if (variantsForm != null) variantsForm.SaveSplitters();
    }
}

package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.UI.TextField.StyledTextField;
import ProjectData.SapforData.Arrays.ProjectArray;
import Visual_DVM_2021.UI.Interface.ArraysWindow;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
public class ArraysForm implements ArraysWindow {
    private JPanel content;
    private JPanel arraysPanel;
    private JPanel savedArraysPanel;
    public JSplitPane SC8;
    private JTextField filterName;
    private JLabel arraysMatchesLabel;
    private JTextField filterLocationName;
    private JTextField filterLocation;
    private JTextField filterFile;
    private JTextField filterRegion;
    private JPanel savedArraysBackground;
    public ArraysForm(){
        LoadSplitters();
        Current.getProject().declaratedArrays.mountUI(arraysPanel);
        Current.getProject().db.savedArrays.mountUI(savedArraysPanel);
        //--
        filterName.setText(ProjectArray.filterName);
        filterName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterName = filterName.getText();
                ShowArrays();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterName = filterName.getText();
                ShowArrays();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterLocation.setText(ProjectArray.filterLocation);
        filterLocation.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterLocation = filterLocation.getText();
                ShowArrays();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterLocation = filterLocation.getText();
                ShowArrays();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterLocationName.setText(ProjectArray.filterLocationName);
        filterLocationName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterLocationName = filterLocationName.getText();
                ShowArrays();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterLocationName = filterLocationName.getText();
                ShowArrays();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterFile.setText(ProjectArray.filterFile);
        filterFile.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterFile = filterFile.getText();
                ShowArrays();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterFile = filterFile.getText();
                ShowArrays();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterRegion.setText(ProjectArray.filterRegion);
        filterRegion.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterRegion = filterRegion.getText();
                ShowArrays();
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterRegion = filterRegion.getText();
                ShowArrays();
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });    }

    @Override
    public JPanel getContent() {
        return content;
    }
    @Override
    public void ShowArrays() {
        Current.getProject().declaratedArrays.ShowUI();
        Current.getProject().db.savedArrays.ShowUI();
    }
    @Override
    public void ShowNoArrays() {
        Current.getProject().declaratedArrays.ClearUI();
        Current.getProject().db.savedArrays.ClearUI();
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        filterName = new StyledTextField();
        filterLocationName = new StyledTextField();
        filterLocation = new StyledTextField();
        filterFile = new StyledTextField();
        filterRegion = new StyledTextField();
    }
}

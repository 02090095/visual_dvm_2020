package Visual_DVM_2021.UI.Interface;
public interface FileWindow extends VisualizerForm{
    void ShowText();
    void ShowLanguage();
    void ShowType();
    void ShowStyle();
    default void ShowProperties() {
        ShowLanguage();
        ShowType();
        ShowStyle();
    }
    void ShowMessages();
    void ShowNoMessages();
    void ShowLoops();
    void ShowNoLoops();
    void ShowGCOV();
    void ShowNoGCOV();
    void ShowFunctions();
    void ShowNoFunctions();
    void ShowArrays();
    void ShowNoArrays();
    void ShowCaretInfo();
    void FocusMessagesPriority();
    void ShowFirstError();
    //-
    void ShowCompilationOutput();
    void ShowNoCompilationOutput();
    void ShowRunOutput();
    void ShowNoRunOutput();
    void ShowGCOVLog();
    void ShowNoGCOVLog();
    void FocusGCOVLog();
    SPFEditorInterface getEditor();
    default void ShowAllAnalyses() {
        ShowLoops();
        ShowFunctions();
        ShowArrays();
        ShowGCOV();
        ShowCompilationOutput();
        ShowRunOutput();
    }
    default void ShowNoAnalyses() {
        ShowNoLoops();
        ShowNoFunctions();
        ShowNoArrays();
        ShowNoGCOV();
        ShowNoCompilationOutput();
        ShowNoRunOutput();
    }
    void FocusLoops();
    void FocusFunctions();
    void FocusArrays();
    void RefreshTabsNames();
    void FocusCompilationOut();
    //--
    void ShowWarningsCount();
    void ShowErrorsCount();
    void ShowNotesCount();
    void ShowRecommendationsCount();
}

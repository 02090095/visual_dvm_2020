package Visual_DVM_2021.UI.Interface;
public interface FilterWindow extends VisualizerForm {
    void ShowMatchesCount(int count);
    default void ShowNoMatches() {
        ShowMatchesCount(0);
    }
}

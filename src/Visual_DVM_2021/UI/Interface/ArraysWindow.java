package Visual_DVM_2021.UI.Interface;
public interface ArraysWindow extends VisualizerForm, FormWithSplitters{
    void ShowArrays();
    void ShowNoArrays();
}

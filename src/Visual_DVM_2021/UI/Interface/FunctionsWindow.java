package Visual_DVM_2021.UI.Interface;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphForm;
import javafx.util.Pair;
public interface FunctionsWindow extends VisualizerForm, FormWithSplitters {
    void ShowFunctions();
    void ShowNoFunctions();
    void ShowCurrentFunction();
    void ShowNoCurrentFunction();
    Pair<Integer, Integer> getFunctionsGraphPanelSizes();
    FunctionsGraphForm getFunctionsGraphWindow();
}

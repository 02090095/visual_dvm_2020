package Visual_DVM_2021.UI.Interface;
import javax.swing.*;
public interface DescriptionInterface extends VisualizerForm,FormWithSplitters {
    void setEditorScroll(JScrollPane scroll_in);
    void setAdditionScroll(JScrollPane scroll_in);
}

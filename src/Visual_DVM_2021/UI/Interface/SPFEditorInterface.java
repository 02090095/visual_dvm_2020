package Visual_DVM_2021.UI.Interface;
public interface SPFEditorInterface {
    //-----------------------------------
    void ClearHighlights();
    //---------------->>
    void ClearLoopsHighLights();
    void ClearGOCVHighlights();
    //----------------->>
    void HighlightLoops();
    void HighlightGCOV();
    //------------------------------------
    void gotoLine(int num);
    int getCurrentLine();
    int getCaretPosition();
    String getText();
}

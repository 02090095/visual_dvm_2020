package Visual_DVM_2021.UI.Interface;
import javax.swing.*;
public interface VisualizerForm {
    //просто некая панель, на которой может быть все что угодно. монтируется на панель родителя,
    //при условии что она BorderLayout
    default JPanel getContent() {
        return null;
    }
}

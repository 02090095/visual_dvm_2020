package Visual_DVM_2021.UI.Interface;
public interface AnalysisWindow extends FormWithSplitters, VisualizerForm{
    void ShowProjectMaxDim();
    void ShowNoProjectMaxDim();

    void ShowMetrics();
    void ShowNoMetrics();
    void ShowRegions();
    void ShowNoRegions();
    void ShowLoopsCount();
    void ShowFunctionsCount();
    void ShowArraysCount();
}

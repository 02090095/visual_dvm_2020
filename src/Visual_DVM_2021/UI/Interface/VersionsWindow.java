package Visual_DVM_2021.UI.Interface;
import Common.UI.Trees.TreeForm;
import ProjectData.Project.db_project_info;
public interface VersionsWindow extends VisualizerForm {
    VariantsWindow getVariantsWindow();
    void ShowProjectVariants();
    void ShowNoProjectVariants();
    void FocusDistribution();
    void BlockVariants();
    void UnblockVariants();
    void RemoveVersionFromComparison(db_project_info version);
    TreeForm getVersionsForm();
}

package Visual_DVM_2021.UI.Interface;
public interface VariantsWindow extends VisualizerForm, FormWithSplitters {
    void ShowProjectDistribution();
    void ShowNoProjectDistribution();
    void ShowVariantsFilterButtons();
    void ShowVariantsFilter();
    void ShowNoVariantsFilter();
    void ShowTotalVariantsCount();
    void ShowFilteredVariantsCount();
    void ShowCheckedVariantsCount();
    void ShowNoCheckedVariantsCount();
    void ShowNoFilteredVariantsCount();
    void ShowNoTotalVariantsCount();
    void ShowVariants();
    void RefreshVariants();
    void ShowNoVariants();
}

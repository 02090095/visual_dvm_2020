package Visual_DVM_2021.UI.Interface;
import Common.UI.Trees.TreeForm;
import Common.UI.UI;
public interface ProjectWindow {
    //-
    ArraysWindow getArraysWindow();
    FunctionsWindow getFunctionsWindow();
    AnalysisWindow getAnalysisWindow();
    //-
    void RefreshProjectFiles();
    void ShowSelectedDirectory();
    void ShowSelectedFile();
    void ShowNoSelectedFile();
    void ShowProjectSapforLog();
    void ShowIncludes();
    void ShowNoIncludes();
    void ShowFunctions();
    void ShowNoFunctions();
    void RefreshProjectTreeAndMessages();
    TreeForm getFilesTreeForm();
    //-
    void SwitchScreen(boolean small);
    void ShowProjectView();
    //-
    default void ShowAllAnalyses() {
        ShowProjectSapforLog();
        ShowIncludes();
        ShowFunctions();
        getArraysWindow().ShowArrays();
        //------------------------------>>
        UI.getVersionsWindow().getVariantsWindow().ShowVariantsFilterButtons();
        UI.getVersionsWindow().getVariantsWindow().ShowProjectDistribution();
        UI.getVersionsWindow().getVariantsWindow().ShowVariantsFilter();
        UI.getVersionsWindow().getVariantsWindow().ShowTotalVariantsCount();
        UI.getVersionsWindow().getVariantsWindow().ShowFilteredVariantsCount();
        UI.getVersionsWindow().getVariantsWindow().ShowCheckedVariantsCount();
        //----------------------------->>
        getAnalysisWindow().ShowMetrics();
        getAnalysisWindow().ShowLoopsCount();
        getAnalysisWindow().ShowFunctionsCount();
        getAnalysisWindow().ShowArraysCount();
        getAnalysisWindow().ShowRegions();
        getAnalysisWindow().ShowProjectMaxDim();
    }
    default void ShowNoAnalyses() {
        ShowNoIncludes();
        ShowNoFunctions();
        getFunctionsWindow().ShowNoCurrentFunction();
        getArraysWindow().ShowNoArrays();
        UI.getVersionsWindow().getVariantsWindow().ShowNoProjectDistribution();
        UI.getVersionsWindow().getVariantsWindow().ShowNoVariants();
        UI.getVersionsWindow().getVariantsWindow().ShowNoVariantsFilter();
        UI.getVersionsWindow().getVariantsWindow().ShowNoTotalVariantsCount();
        UI.getVersionsWindow().getVariantsWindow().ShowNoFilteredVariantsCount();
        UI.getVersionsWindow().getVariantsWindow().ShowNoCheckedVariantsCount();
        getAnalysisWindow().ShowNoMetrics();
        getAnalysisWindow().ShowNoRegions();
        getAnalysisWindow().ShowLoopsCount();
        getAnalysisWindow().ShowFunctionsCount();
        getAnalysisWindow().ShowArraysCount();
        getAnalysisWindow().ShowNoProjectMaxDim();
    }
    //---
    void ShowFile();
    void ShowNoFile();
    //---
    void GotoFile(String fileName, int line, boolean focus);
    void FocusFile();
    void FocusFileTabs();
    //-
    void FocusDependencies();
    void FocusArrays();
    void FocusFunctions();
    void FocusAnalysis();
    void FocusHierarchy();
    void FocusPoints();
    //-
    void RefreshTabsNames();
    void FocusVersions();
}

package Visual_DVM_2021.UI.Interface;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackage_2023;
public interface TestingWindow extends VisualizerForm {
    void ShowAll();
    void RestoreLastCredentials();
    void ShowProjectMaxCompilationTime();
    void ShowProjectMaxRunTime();
    //-
    void ShowCurrentTestsPackage();
    void DropTestRunTasksComparison();
    void DropRunTasksComparison();
    void DropCompilationTasksComparison();
    void DropSapforTasksComparison();
    void ShowCurrentTestRunTask();
    void ShowNoTestRunTask();
    default void ShowSession() {
        ShowProjectMaxCompilationTime();
        ShowProjectMaxRunTime();
    }
    void FocusCredentials();
    void ShowProject();
    void ShowNoProject();
    //-
    void ShowCurrentCompilationTask();
    void ShowNoCurrentCompilationTask();
    void ShowCurrentRunTask();
    void ShowNoCurrentRunTask();
    //-
    void RefreshTabsNames();
    void FocusTestingSystem();
    void ShowAutoActualizeTestsState();
    void RemountTestTable();
    //-
    void FocusTestingTasks();
    boolean isEmailTestingOn();
    void setUserRights();
    void setAdminRights();
    void setDeveloperRights();
    void FocusScenarios();
    //---------------------------------------->>>
    void ShowCurrentMachine();
    void ShowNoCurrentMachine();
    //---------------------------------------->>>
    void ShowCredentials();
    void FocusSapforTasksPackages();
    void ShowCurrentSapforTasksPackage();
    void ShowNoSapforTasksPackage();
    //----------------------------------------->>>
    void ShowCheckedTestsCount();
    //--
    void ShowLastCompilationTask();
    void ShowLastRunTask();
    //-

    void RemoveSapforPackageFromComparison(SapforTasksPackage_2023 package_2023);
}

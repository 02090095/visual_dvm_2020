package Visual_DVM_2021.UI.Interface;
public interface ScenariosWindow extends FormWithSplitters, VisualizerForm{
    void ShowAll();
    void FocusSapforTasksPackages();
    void ShowCurrentSapforTasksPackage();
    void ShowNoSapforTasksPackage();
}

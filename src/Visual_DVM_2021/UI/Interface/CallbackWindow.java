package Visual_DVM_2021.UI.Interface;
public interface CallbackWindow extends VisualizerForm {
    void ShowAccount();
    void ShowBugReports();
    void ShowNoBugReports();
    void ShowSubscribers();
    void ShowNoSubscribers();
    void ShowCurrentBugReport();
    void ShowNoCurrentBugReport();
    void ShowSubscription();
    //-
    default void ShowAll() {
        ShowAccount();
        ShowSubscription();
        ShowBugReports();
        ShowSubscribers();
    }
    String getBugReportDescriptionText();
    String getBugReportDescriptionAdditionText();
    String getBugReportCommentText();
    String getBugReportCommentAdditionText();
    void ClearBugReportDescriptionAdditionText();
    void ClearBugReportCommentAdditionText();
    void FocusRecipients();
    void SwitchScreen(boolean small);
    void setUserRights();
    void setAdminRights();
}

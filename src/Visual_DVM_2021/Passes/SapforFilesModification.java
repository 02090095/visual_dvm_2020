package Visual_DVM_2021.Passes;
import Common.Current;
import Common.Global;
import GlobalData.Settings.SettingName;
public class SapforFilesModification extends SapforModification {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }
    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation();
        if (Global.getSetting(SettingName.SaveModifications).toBoolean()) {
            Current.getProject().createModification();
        }
    }
    @Override
    protected void performDone() throws Exception {
        Current.getSapfor().UpdateProjectFiles(true);
    }
}

package Visual_DVM_2021.Passes;
import Common.UI.Menus_2023.PassMenuItem;
import Common.UI.UI;
import ProjectData.Project.db_project_info;
import Repository.Component.Sapfor.TransformationPermission;

import javax.swing.*;
public class Transformation extends CurrentProjectPass {
    protected TransformationPermission getPermission() {
        return TransformationPermission.All;
    }
    //todo - возможно сделать все же интерфейс для преобразований. чтобы наконец обобщить сапфоровские и несапфоровские? хз
    @Override
    protected void performPreparation() throws Exception {
        db_project_info.ResetNewVersions();
        target.createEmptyVersion(getVersionLetter(), getVersionDescription());
    }
    protected String getVersionDescription() {
        return getDescription();
    }
    @Override
    public String getIconPath() {
        return "/icons/Transformations/" + code().toString() + ".png";
    }
    protected String getVersionLetter() {
        return "v";
    }
    @Override
    protected boolean hasStats() {
        return true;
    }
    @Override
    protected void FocusResult() {
        UI.getMainWindow().getProjectWindow().FocusVersions();
    }
    @Override
    protected void performDone() throws Exception {
        //миграция типов файлов.
        //это несапфоровские дела.тут копируем хедеры сами.
        target.migrateFilesSettings(target.last_version, true, true, false);
    }
    @Override
    protected void performFail() throws Exception {
        super.performFail();
        target.undoLastTransformation();
    }
    @Override
    public JMenuItem createMenuItem() {
        if (menuItem == null)
            menuItem = new PassMenuItem(this);
        return menuItem;
    }
}

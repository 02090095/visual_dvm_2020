package Visual_DVM_2021.Passes;
import Common.Global;
public class SapforModification extends SapforPass {
    public String addOpt1 = "";
    public String addOpt2 = "";
    @Override
    protected boolean isGoodCode() {
        return sapfor.getErrorCode() >= 0;
    }
    @Override
    protected void body() throws Exception {
        sapfor.RunModification(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.packSapforSettings(),
                target.getProjFile().getAbsolutePath(),
                "",
                addOpt1,
                addOpt2);
    }
}

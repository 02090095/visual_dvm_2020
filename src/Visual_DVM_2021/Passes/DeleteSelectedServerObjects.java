package Visual_DVM_2021.Passes;
import Common.Current;
import Common.Database.DataSet;
import Common.Database.rDBObject;
import Common.UI.UI;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;

import java.util.Vector;
public class DeleteSelectedServerObjects extends TestingSystemPass<Vector<rDBObject>> {
    Class objects_class;
    public DeleteSelectedServerObjects(Class class_in){
        objects_class=class_in;
    }
    Vector<String> names;
    @Override
    public String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        DataSet table = server.db.tables.get(objects_class);
        if (table.getCheckedCount() == 0) {
            Log.Writeln_("Не отмечено ни одного объекта.");
            return false;
        }
        Vector checkedItems = table.getCheckedItems();
        target = new Vector<>();
        names = new Vector<>();
        for (Object o : checkedItems) {
            rDBObject r = (rDBObject)o;
            if (Current.getAccount().CheckAccessRights(r.sender_address, Log)) {
                names.add(r.description);
                target.add(r);
            }
        }
        if (target.isEmpty()) {
            Log.Writeln_("Не отмечено ни одного объекта, принадлежащего текущему пользователю.");
            return false;
        }
        return UI.Warning(getDescription()+"\n" + String.join("\n", names));
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DeleteObjects, "", target));
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        passes.get(PassCode_2021.SynchronizeTests).Do();
    }
}


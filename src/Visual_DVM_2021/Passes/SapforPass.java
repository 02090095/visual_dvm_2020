package Visual_DVM_2021.Passes;
import Common.Current;
import Common.UI.UI;
public class SapforPass extends SilentSapforPass {
    //отличается наличием журнала и сообщений
    @Override
    protected void performPreparation() throws Exception {
     //   target.CleanInterruptFile();
    }
    @Override
    protected void performFinish() throws Exception {
        target.db.BeginTransaction();
        unpackMessages();
        target.db.Commit();
    }
    protected void unpackMessages() throws Exception {
        target.unpackMessagesAndLog(sapfor.getOutputMessage(), sapfor.getOutput());
    }
    @Override
    protected void showFinish() throws Exception {
        UI.getMainWindow().getProjectWindow().RefreshProjectTreeAndMessages();
        UI.getMainWindow().getProjectWindow().ShowProjectSapforLog();
    }
    @Override
    public void Interrupt() throws Exception {
      //  target.CreateInterruptFile();
       Current.getSapfor().Interrupt();
    }
}

package Visual_DVM_2021.Passes;
public enum PassCode_2021 {
    Undefined,
    //-
    SaveProfile,
    ApplyProfile,
    EditProfile,
    DeleteProfile,
    CompareSapforPackages,
    //-
    StartSapforTests,
    DeleteSapforTasksPackage,
    PerformScenario,
    AnalyseSapforPackageResults,
    OpenSapforTest,
    PerformSapforTasksPackage,
    //-
    //-
    CheckAccount,
    CheckRegistrationOnServer,
    EditAccount,
    AddSubscriber,
    EditSubscriber,
    EditSubscriberOnServer,
    DeleteSubscriber,
    DeleteSubscriberOnServer,
    //----
    SetSelectedFilesLanguage,
    SetSelectedFilesStyle,
    SetSelectedFilesType,
    DeleteSelectedFiles,
    //----
    DropFastAccess,
    DropLastProjects,
    UpdateSetting,
    //-
    OpenCurrentProject,
    CloseCurrentProject,
    DropAnalyses,
    CleanAnalyses,
    DeleteLonelyM,
    DeleteSubversions,
    DeleteDebugResults,
    ResetCurrentProject,
    CreateEmptyProject,
    DeleteVersion,
    //-
    OpenCurrentFile,
    CloseCurrentFile,
    Save,
    //-
    AddFile,
    RenameFile,
    IncludeFile,
    ExcludeFile,
    DeleteFile,
    ImportFiles,
    //-
    CreateEmptyDirectory,
    RenameDirectory,
    DeleteDirectory,
    //-
    SPF_ParseFilesWithOrder,
    SPF_GetIncludeDependencies,
    SPF_GetFileLineInfo,
    SPF_GetGraphLoops,
    SPF_GetGraphFunctions,
    SPF_GetGraphFunctionPositions,
    SPF_GetAllDeclaratedArrays,
    SPF_GetArrayDistributionOnlyAnalysis,
    SPF_GetArrayDistribution,
    //-
    SPF_CorrectCodeStylePass,
    SPF_CreateIntervalsTree,
    SPF_RemoveDvmDirectives,
    SPF_RemoveDvmDirectivesToComments,
    SPF_RemoveUnusedFunctions,
    SPF_ResolveParallelRegionConflicts,
    SPF_LoopEndDoConverterPass,
    SPF_LoopFission,
    SPF_LoopUnion,
    SPF_PrivateShrinking,
    SPF_PrivateExpansion,
    SPF_RemoveDvmIntervals,
    SPF_DuplicateFunctionChains,
    SPF_ConvertStructures,
    SPF_CreateCheckpoints,
    SPF_InitDeclsWithZero,
    //-
    SPF_LoopUnionCurrent,
    SPF_ChangeSpfIntervals,
    SPF_ExpressionSubstitution,
    SPF_InsertDvmhRegions,
    SPF_StatisticAnalyzer,
    //-
    CombineFiles,
    EraseBadSymbols,
    CopyProject,
    //-
    GenerateParallelVariants,
    PredictParallelVariants,
    SPF_PredictParallelVariant,
    CreateParallelVariants,
    SPF_CreateParallelVariant,
    SPF_InlineProceduresH,
    SPF_InlineProcedures,
    SPF_InsertIncludesPass,
    SPF_InlineProcedure,
    SPF_ModifyArrayDistribution,
    SPF_GetGCovInfo,
    SPF_GetArrayDistributionOnlyRegions,
    SPF_LoopUnrolling,
    SPF_ResolveCommonBlockConflicts,
    SPF_SharedMemoryParallelization,
    RestoreSavedArrays,
    MassSelectArrays,
    DropSavedArrays,
    //-
    SynchronizeBugReports,
    ZipFolderPass,
    UnzipFolderPass,
    AddBugReport,
    DeleteBugReport,
    DeleteBugReportFromServer,
    PublishBugReport,
    SendBugReport,
    DeleteDownloadedBugReports,
    ExtractRecipients,
    Email,
    //-
    GetComponentsActualVersions,
    PublishComponent,
    CreateComponentBackUp,
    InstallComponentFromFolder,
    ResurrectComponent,
    ResurrectComponentFromServer,
    DownloadComponent,
    ShowComponentChangesLog,
    UpdateComponent,
    BuildComponent,
    DownloadRepository,
    SaveGraph,
    ApplyBugReportSettings,
    DownloadBugReport,
    UpdateBugReportField,
    AppendBugReportField,
    GetComponentsBackupsFromServer,
    //-
    AddMachine,
    EditMachine,
    DeleteMachine,
    //-
    AddUser,
    EditUser,
    DeleteUser,
    //-
    AddCompiler,
    EditCompiler,
    DeleteCompiler,
    ShowCompilerHelp,
    //-
    AddMakefile,
    EditMakefile,
    DeleteMakefile,
    //-
    EditModule,
    //-
    AddRunConfiguration,
    EditRunConfiguration,
    DeleteRunConfiguration,
    //-
    AddEnvironmentValue,
    EditEnvironmentValue,
    DeleteEnvironmentValue,
    //-
    ShowMakefilePreview,
    //-
    InitialiseUser,
    DownloadProject,
    //-
    SelectRemoteFile,
    Compile,
    Run,
    RemoteCompilation,
    ServerRun,
    MVSRun,
    //-
    LinuxLocalCompilation,
    LinuxLocalRun,
    //-
    EditProjectCompilationMaxtime,
    EditProjectRunMaxtime,
    //-
    OpenBugReportTestProject,
    OpenBugReport,
    CloseBugReport,
    UpdateBugReportProgress,
    SaveBugReportDescription,
    SaveBugReportComment,
    AppendBugReportDescription,
    AppendBugReportComment,
    SaveBugReportRecipients,
    SaveBugReportExecutor,
    //-
    EditTest,
    //-
    EditGroup,
    PublishGroup,
    EditGroupOnServer,
    DownloadGroup,
    //-
    DownloadAllBugReportsArchives,
    ShowInstruction,
    //-
    ArchivesBackupPass,
    PublishTest,
    DownloadTest,
    EditTestOnServer,
    SynchronizeTests,
    //-
    MakeScreenShot,
    RemoteInitialiseUser,
    LocalInitaliseUser,
    RemoteSingleCommand,
    LocalSingleCommand,
    //-
    WindowsLocalCompilation,
    WindowsLocalRun,
    PickCompilerOptions,
    PickCompilerEnvironments,
    //-
    DeleteSelectedCompilationTasks,
    DeleteSelectedRunTasks,
    AddDVMParameter,
    EditDVMParameter,
    DeleteDVMParameter,
    //-
    SynchronizeTestsTasks,
    SPF_GetArrayLinks,
    Precompilation,
    GCOV,
    ConvertCorrectnessTests,
    EditMachineKernels,
    ShowCompilerVersion,
    //-
    PickCompilerEnvironmentsForTesting,
    AddDVMParameterForTesting,
    DeleteSelectedTestsRunTasks,
    //-
    RefreshDVMTests,
    PauseTesting,
    PlayTesting,
    //-
    SPF_GetMaxMinBlockDistribution,
    DeleteSelectedGroups,
    //-
    PublishConfiguration,
    EditConfiguration,
    EditConfigurationOnServer,
    //-
    StartTests,
    DeleteSelectedTests,
    DeleteSelectedConfigurations,
    SwitchTestingEmail,
    //-
    CopyConfigurations,
    CopyGroups,
    DownloadTaskTest,
    //-
    GetTestsQueueSize,
    ActualizePackages,
    AbortSelectedPackages,
    ApplyCurrentFunction,
    //->
    // GetRemoteUserHome,
    CheckRemoteWorkspace,
    PublishRemoteWorkspace,
    //->
    PrepareForModulesAssembly,
    //->
    DVMConvertProject,
    //->
    UpdateSelectedComponents,
    SPF_PrivateRemoving,
    //->
    OpenProject,
    CloseProject,
    //->
    CreateParallelVariantsCoverageForScenario,
    SaveFunctionsGraphCoordinates,
    DeleteSelectedVersions,
    CreateTestsGroupFromSelectedVersions,
    ExcludeSelectedFiles,
    IncludeSelectedFiles,
    AddSapfor,
    EditSapfor,
    DeleteSapfor,
    InstallRemoteSapfor,
    //--
    PublishSapforConfiguration,
    EditSapforConfiguration,
    EditSapforConfigurationOnServer,
    DeleteSapforConfiguration,
    //-
    PublishSapforConfigurationCommand,
    EditSapforConfigurationCommand,
    EditSapforConfigurationCommandOnServer,
    DeleteSapforConfigurationCommand,
    //->
    TestPass;
    public String getDescription() {
        switch (this) {
            case Undefined:
                return "?";
            case PerformSapforTasksPackage:
                return "Выполнить пакет задач SAPFOR";
            case CompareSapforPackages:
                return "Сравнение пакетов задач SAPFOR";
            case EditProfile:
                return "Редактировать профиль";
            case DeleteProfile:
                return "Удалить профиль";
            case ApplyProfile:
                return "Применить профиль";
            case SaveProfile:
                return "Сохранить текущие настройки SAPFOR как профиль";
            case OpenSapforTest:
                return "Открыть результирующую версию теста.";
            case AnalyseSapforPackageResults:
                return "Анализ результатов пакета задач";
            case PerformScenario:
                return "Выполнить сценарий SAPFOR (пакетный режим)";
            case StartSapforTests:
                return "Создать задачи тестирования SAPFOR";
            case SPF_SharedMemoryParallelization:
                return "Распараллеливание на общую память";
            case DeleteSapforTasksPackage:
                return "Удалить пакет задач SAPFOR";
            case ResurrectComponentFromServer:
                return "Восстановление предыдущей версии компонента с сервера";
            case ResurrectComponent:
                return "Восстановление предыдущей версии компонента с этого компьютера";
            case GetComponentsBackupsFromServer:
                return "Получить список предыдущих версий компонент с сервера";
            case MassSelectArrays:
                return "Массовый выбор массивов";
            case AddSubscriber:
                return "Добавить учётную запись";
            case EditSubscriber:
                return "Редактировать учётную запись";
            case DeleteSubscriber:
                return "Удалить учётную запись";
            case DeleteSubscriberOnServer:
                return "Удалить учётную запись на сервере";
            case EditSubscriberOnServer:
                return "Редактировать учётную запись на сервере";
            case CheckAccount:
                return "Проверка учётной записи";
            case CheckRegistrationOnServer:
                return "Проверка регистрации учетной записи на сервере";
            case EditAccount:
                return "Редактирование учётной записи";
            case DeleteSelectedFiles:
                return "Удалить отмеченные файлы проекта";
            case SetSelectedFilesLanguage:
                return "Установить язык для отмеченных файлов";
            case SetSelectedFilesStyle:
                return "Установить стиль для отмеченных файлов";
            case SetSelectedFilesType:
                return "Установить тип для отмеченных файлов";
            case DeleteDownloadedBugReports:
                return "Удалить загруженные отчёты об ошибках";
            case DeleteSapforConfigurationCommand:
                return "Удалить выбранные команды конфигурации тестирования Sapfor на сервере";
            case EditSapforConfigurationCommandOnServer:
                return "Редактировать команду конфигурации тестирования Sapfor на сервере";
            case EditSapforConfigurationCommand:
                return "Редактировать команду конфигурации тестирования Sapfor";
            case PublishSapforConfigurationCommand:
                return "Опубликовать команду конфигурации тестирования Sapfor";
            case PublishSapforConfiguration:
                return "Опубликовать конфигурацию тестирования SAPFOR";
            case EditSapforConfiguration:
                return "Редактировать конфигурацию тестирования SAPFOR";
            case EditSapforConfigurationOnServer:
                return "Редактировать конфигурацию тестирования SAPFOR на сервере";
            case DeleteSapforConfiguration:
                return "Удалить выбранные конфигурации тестирования SAPFOR на сервере";
            case InstallRemoteSapfor:
                return "Установить систему SAPFOR";
            case AddSapfor:
                return "Добавление SAPFOR";
            case EditSapfor:
                return "Редактирование SAPFOR";
            case DeleteSapfor:
                return "Удаление SAPFOR";
            case ExcludeSelectedFiles:
                return "Исключить из рассмотрения отмеченные файлы";
            case IncludeSelectedFiles:
                return "Включить в рассмотрение отмеченные файлы";
            case CreateTestsGroupFromSelectedVersions:
                return "Создать группу тестов из отмеченных версий";
            case DeleteSelectedVersions:
                return "Удаление отмеченных версий";
            case SaveFunctionsGraphCoordinates:
                return "Сохранить координаты вершин графа процедур";
            case SPF_InsertDvmhRegions:
                return "Вставка DVMH-регионов";
            case SPF_ResolveCommonBlockConflicts:
                return "Разрешение конфликтов common блоков";
            case CreateParallelVariantsCoverageForScenario:
                return "Построение минимального покрытия параллельных вариантов";
            case DownloadGroup:
                return "Загрузка группы в рабочее пространство";
            case OpenProject:
                return "Открытие проекта (пакетный режим)";
            case CloseProject:
                return "Закрытие проекта (пакетный режим)";
            case SPF_PrivateRemoving:
                return "Удаление приватных переменных";
            case UpdateSelectedComponents:
                return "Обновить отмеченные компоненты";
            case DVMConvertProject:
                return "Конвертация проекта";
            case PrepareForModulesAssembly:
                return "Подготовка к сборке модулей";
            case CheckRemoteWorkspace:
                return "Проверка удалённой рабочей папки";
            case PublishRemoteWorkspace:
                return "Публикация удалённой рабочей папки";
                /*
            case GetRemoteUserHome:
                return "Получить корневую папку серверного компонента";
                 */
            case ApplyCurrentFunction:
                return "Назначить текущую функцию по имени";
            case AbortSelectedPackages:
                return "Прерывать отмеченные пакеты задач";
            case GetTestsQueueSize:
                return "Очередь перед текущим пакетом";
            case ActualizePackages:
                return "Обновить текущий пакет задач";
            case DownloadTaskTest:
                return "Загрузить тест текущей задачи";
            case CopyGroups:
                return "Дублировать отмеченные группы";
            case CopyConfigurations:
                return "Дублировать отмеченные конфигурации";
            case SwitchTestingEmail:
                return "Настроить отправку оповещений тестирования";
            case DeleteSelectedConfigurations:
                return "Удалить отмеченные конфигурации";
            case DeleteSelectedTests:
                return "Удалить отмеченные тесты";
            case StartTests:
                return "Запуск тестов";
            case PublishConfiguration:
                return "Опубликовать конфигурацию тестирования";
            case EditConfiguration:
                return "Просмотр/Редактирование конфигурацию тестирования";
            case EditConfigurationOnServer:
                return "Редактировать конфигурацию на сервере";
            case DeleteSelectedGroups:
                return "Удалить отмеченные группы";
            case SPF_GetMaxMinBlockDistribution:
                return "Определить размерность теста по DVM директивам";
            case PauseTesting:
                return "Поставить тестирование на паузу";
            case PlayTesting:
                return "Возобновить тестирование";
            case RefreshDVMTests:
                return "Обновить DVM тесты";
            case DeleteSelectedTestsRunTasks:
                return "Удалить отмеченные тестовые задачи";
            case AddDVMParameterForTesting:
                return "Добавить параметр DVM системы для группы";
            case PickCompilerEnvironmentsForTesting:
                return "Выбрать набор переменных окружения";
            case ShowCompilerVersion:
                return "Отобразить версию текущего компилятора";
            case EditMachineKernels:
                return "Изменить число ядер для тестирования текущей машины";
            case ConvertCorrectnessTests:
                return "Конвертировать тесты из репозитория";
            case SPF_GetArrayDistributionOnlyRegions:
                return "Анализ областей распараллеливания";
            case SPF_GetGCovInfo:
                return "Показать покрытие GCOV";
            case GCOV:
                return "Обновить покрытие GCOV";
            case Precompilation:
                return "Предварительная компиляция на локальной машине";
            case SPF_GetArrayLinks:
                return "Получить связи между массивами";
            case SynchronizeTestsTasks:
                return "Синхронизация базы данных тестовых задач.";
            case EditGroupOnServer:
                return "Обновить группу на сервере";
            case SaveBugReportExecutor:
                return "Назначить исполнителя отчёта об ошибке";
            case SaveBugReportRecipients:
                return "Сохранить адресатов отчёта об ошибке";
            case AppendBugReportComment:
                return "Дополнить комментария отчёта об ошибке";
            case AppendBugReportDescription:
                return "Дополнить описание отчёта об ошибке";
            case SaveBugReportDescription:
                return "Сохранить описание отчёта об ошибке";
            case SaveBugReportComment:
                return "Сохранить комментарий отчёта об ошибке";
            case UpdateBugReportProgress:
                return "Обновить степень завершенности отчёта об ошибке";
            case OpenBugReport:
                return "Открытие отчёта об ошибке";
            case CloseBugReport:
                return "Закрытие отчёта об ошибке";
            case CopyProject:
                return "Копирование проекта";
            case AddDVMParameter:
                return "Добавление параметра DVM системы";
            case EditDVMParameter:
                return "Редактирование параметра DVM системы";
            case DeleteDVMParameter:
                return "Удаление параметра DVM системы";
            case EditGroup:
                return "Редактирование группы";
            case PublishGroup:
                return "Опубликовать группу";
            case DeleteSelectedCompilationTasks:
                return "Удалить отмеченные задачи на компиляцию";
            case DeleteSelectedRunTasks:
                return "Удалить отмеченные задачи на запуск";
            case PickCompilerEnvironments:
                return "Выбор переменных окружения DVM компилятора";
            case PickCompilerOptions:
                return "Выбор опций DVM компилятора";
            case WindowsLocalCompilation:
                return "Компиляция на локальной машине (Windows)";
            case WindowsLocalRun:
                return "Запуск на локальной машине (Windows)";
            case LocalSingleCommand:
                return "Одиночная команда на локальной машине";
            case RemoteSingleCommand:
                return "Одиночная команда на удаленной машине";
            case LocalInitaliseUser:
                return "Инициализация пользователя на локальной машине";
            case RemoteInitialiseUser:
                return "Инициализация пользователя на удаленной машине";
            case MakeScreenShot:
                return "Сделать скриншот";
            case SynchronizeTests:
                return "Синхронизация Базы данных групп тестов";
            case DownloadTest:
                return "Загрузить тест";
            case PublishTest:
                return "Опубликовать тест";
            case EditTestOnServer:
                return "Редактировать тест на сервере";
            case ArchivesBackupPass:
                return "Бэкап архивов журнала ошибок";
            case ShowInstruction:
                return "Показать инструкцию";
            case DownloadAllBugReportsArchives:
                return "Скачать все архивы";
            case CleanAnalyses:
                return "Очистка анализов";
            case EditTest:
                return "Редактирование теста";
            case DropFastAccess:
                return "Сброс панели быстрого доступа";
            case DropLastProjects:
                return "Сброс списка последних открытых проектов";
            case UpdateSetting:
                return "Обновить настройку";
            case OpenCurrentProject:
                return "Открытие проекта";
            case CloseCurrentProject:
                return "Закрытие проекта";
            case DropAnalyses:
                return "Сброс актуальности анализов";
            case DeleteLonelyM:
                return "Удаление бесхозных копий";
            case DeleteSubversions:
                return "Удаление всех подверсий текущего проекта";
            case DeleteDebugResults:
                return "Очистка результатов компиляции и запуска";
            case ResetCurrentProject:
                return "Полная очистка проекта";
            case CreateEmptyProject:
                return "Создание проекта";
            case DeleteVersion:
                return "Удаление версии";
            case OpenCurrentFile:
                return "Открытие файла";
            case CloseCurrentFile:
                return "Закрытие файла";
            case Save:
                return "Сохранение файла";
            case AddFile:
                return "Добавление файла";
            case RenameFile:
                return "Переименование файла";
            case IncludeFile:
                return "Включение в рассмотрение файла";
            case ExcludeFile:
                return "Исключение из рассмотрения файла";
            case DeleteFile:
                return "Удаление файла";
            case ImportFiles:
                return "Добавление внешних файлов";
            case CreateEmptyDirectory:
                return "Создание пустой папки";
            case RenameDirectory:
                return "Переименование папки";
            case DeleteDirectory:
                return "Удаление папки";
            case SPF_ParseFilesWithOrder:
                return "Синтаксический анализ";
            case SPF_GetFileLineInfo:
                return "Метрика кода";
            case SPF_GetIncludeDependencies:
                return "Поиск зависимостей по включению";
            case SPF_GetGraphLoops:
                return "Граф циклов";
            case SPF_GetGraphFunctions:
                return "Граф процедур";
            case SPF_GetGraphFunctionPositions:
                return "Координаты графа процедур";
            case SPF_LoopUnrolling:
                return "Разворачивание циклов";
            case SPF_GetAllDeclaratedArrays:
                return "Объявления массивов";
            case SPF_GetArrayDistributionOnlyAnalysis:
                return "Анализ кода";
            case SPF_GetArrayDistribution:
                return "Распределение данных";
            case SPF_CorrectCodeStylePass:
                return "Коррекция стиля кода";
            case SPF_CreateIntervalsTree:
                return "Построение полной системы интервалов";
            case SPF_RemoveDvmDirectives:
                return "Очистка DVM директив";
            case SPF_RemoveDvmDirectivesToComments:
                return "Комментирование DVM директив";
            case SPF_ResolveParallelRegionConflicts:
                return "Разрешение конфликтов областей";
            case SPF_LoopEndDoConverterPass:
                return "Преобразование циклов в DO-ENDDO";
            case SPF_LoopFission:
                return "Разделение циклов";
            case SPF_LoopUnion:
                return "Слияние циклов";
            case SPF_PrivateShrinking:
                return "Сужение приватных переменных";
            case SPF_PrivateExpansion:
                return "Расширение приватных переменных";
            case SPF_RemoveDvmIntervals:
                return "Очистка DVM интервалов";
            case SPF_DuplicateFunctionChains:
                return "Дублирование цепочек вызовов";
            case SPF_InitDeclsWithZero:
                return "Дополнение инициализации переменных";
            case SPF_RemoveUnusedFunctions:
                return "Удаление неиспользуемых процедур";
            case SPF_CreateCheckpoints:
                return "Построение контрольных точек";
            case SPF_ConvertStructures:
                return "Конвертация производных типов данных";
            case CombineFiles:
                return "Слияние файлов";
            case EraseBadSymbols:
                return "Очистка некорректных символов";
            case GenerateParallelVariants:
                return "Генерация параллельных вариантов";
            case PredictParallelVariants:
                return "Оценка выбранных параллельных вариантов";
            case SPF_PredictParallelVariant:
                return "Оценка параллельного варианта";
            case CreateParallelVariants:
                return "Построение параллельных вариантов";
            case SPF_CreateParallelVariant:
                return "Построение параллельного варианта";
            case SPF_InlineProceduresH:
                return "Иерархическая подстановка процедур";
            case SPF_InlineProcedures:
                return "Точечная подстановка процедур";
            case SPF_InsertIncludesPass:
                return "Подстановка заголовочных файлов";
            case SPF_LoopUnionCurrent:
                return "Объединение текущего цикла";
            case SPF_ExpressionSubstitution:
                return "Подстановка выражений";
            case SPF_ChangeSpfIntervals:
                return "Изменение области распараллеливания";
            case SPF_InlineProcedure:
                return "Выборочная подстановка процедуры";
            case SPF_ModifyArrayDistribution:
                return "Изменение распределения данных";
            case SPF_StatisticAnalyzer:
                return "Распаковка DVM статистики";
            case RestoreSavedArrays:
                return "Восстановление сохраненного распределения массивов";
            case DropSavedArrays:
                return "Сброс сохраненных массивов";
            case SynchronizeBugReports:
                return "Синхронизация журнала ошибок";
            case ZipFolderPass:
                return "Архивация папки";
            case UnzipFolderPass:
                return "Распаковка папки";
            case AddBugReport:
                return "Создание отчёта об ошибке";
            case DeleteBugReport:
                return "Удаление отчёта об ошибке";
            case DeleteBugReportFromServer:
                return "Удаление отчёта об ошибке с сервера";
            case PublishBugReport:
                return "Публикация отчёта об ошибке";
            case SendBugReport:
                return "Отправка отчёта об ошибке на сервер";
            case ExtractRecipients:
                return "Извлечение адресатов";
            case Email:
                return "Отправка по email";
            case GetComponentsActualVersions:
                return "Получение актуальных версий компонент";
            case PublishComponent:
                return "Публикация компонента";
            case CreateComponentBackUp:
                return "Создание копии предыдущей версии компонента";
            case InstallComponentFromFolder:
                return "Установка компонента из целевой папки";
            case DownloadComponent:
                return "Скачивание компонента с сервера";
            case ShowComponentChangesLog:
                return "Отображение журнала изменений компонента";
            case UpdateComponent:
                return "Обновление компонента";
            case BuildComponent:
                return "Сборка компонента";
            case DownloadRepository:
                return "Загрузка репозитория";
            case SaveGraph:
                return "Сделать скриншот графа функций";
            case ApplyBugReportSettings:
                return "Применение настроек отчёта об ошибке";
            case DownloadBugReport:
                return "Загрузка тестового проекта с сервера";
            case UpdateBugReportField:
                return "Обновление поля отчёта об ошибке";
            case AppendBugReportField:
                return "Дополнение поля отчёта об ошибке";
            //-----
            case AddMachine:
                return "Добавление машины";
            case EditMachine:
                return "Редактирование машины";
            case DeleteMachine:
                return "Удаление машины";
            //-----
            case AddUser:
                return "Добавление пользователя";
            case EditUser:
                return "Редактирование пользователя";
            case DeleteUser:
                return "Удаление пользователя";
            //-----
            case AddCompiler:
                return "Добавление компилятора";
            case EditCompiler:
                return "Редактирование компилятора";
            case DeleteCompiler:
                return "Удаление компилятора";
            //-
            case AddMakefile:
                return "Добавление мейкфайла";
            case EditMakefile:
                return "Редактирование мейкфайла";
            case DeleteMakefile:
                return "Удаление мейкфайла";
            case ShowCompilerHelp:
                return "Показать справочную информацию компилятора";
            //-
            case EditModule:
                return "Редактирование модуля";
            //-
            case AddRunConfiguration:
                return "Добавление конфигурации запуска";
            case EditRunConfiguration:
                return "Редактирование конфигурации запуска";
            case DeleteRunConfiguration:
                return "Удаление конфигурации запуска";
            //-
            case DeleteEnvironmentValue:
                return "Удалить переменную окружения";
            case EditEnvironmentValue:
                return "Изменить переменную окружения";
            case AddEnvironmentValue:
                return "Добавить переменную окружения";
            //-
            case ShowMakefilePreview:
                return "Предпросмотр мейкфайла";
            case DownloadProject:
                return "Скачивание проекта";
            case InitialiseUser:
                return "Инициализация пользователя";
            case TestPass:
                return "Отладка";
            case Run:
                return "Запуск";
            case Compile:
                return "Компиляция";
            case SelectRemoteFile:
                return "Выбор удалённого файла/папки";
            case MVSRun:
                return "Запуск на MVS кластере";
            case ServerRun:
                return "Запуск на удалённом сервере";
            case RemoteCompilation:
                return "Компиляция на удалённой машине";
            //----------------------------------------------------
            case LinuxLocalCompilation:
                return "Компиляция на локальной машине (Linux)";
            case LinuxLocalRun:
                return "Запуск на локальной машине (Linux)";
            //-
            case EditProjectCompilationMaxtime:
                return "Изменить максимальное допустимое время компиляции";
            case EditProjectRunMaxtime:
                return "Изменить максимальное допустимое время запуска";
            //-
            case OpenBugReportTestProject:
                return "Открыть тестовый проект";
            //-
            default:
                return toString();
        }
    }
    public String getTestingCommand() {
        String p = "-passN";
        String name = "?";
        switch (this) {
            case SPF_CorrectCodeStylePass:
                name = "CORRECT_CODE_STYLE";
                break;
            //--
            case SPF_RemoveDvmDirectives:
                name = "REMOVE_DVM_DIRS";
                break;
            case SPF_RemoveDvmDirectivesToComments:
                name = "REMOVE_DVM_DIRS_TO_COMMENTS";
                break;
            case SPF_CreateCheckpoints:
                name = "CREATE_CHEKPOINTS";
                break;
            case SPF_CreateIntervalsTree:
                name = "INSERT_INTER_TREE";
                break;
            case SPF_RemoveDvmIntervals:
                name = "REMOVE_DVM_INTERVALS";
                break;
            case SPF_LoopEndDoConverterPass:
                name = "CONVERT_TO_ENDDO";
                break;
            case SPF_LoopUnion:
                name = "LOOPS_COMBINER";
                break;
            case SPF_LoopFission:
                name = "LOOPS_SPLITTER";
                break;
            //--
            case SPF_PrivateShrinking:
                name = "PRIVATE_ARRAYS_SHRINKING";
                break;
            case SPF_PrivateExpansion:
                name = "PRIVATE_ARRAYS_EXPANSION";
                break;
            case SPF_PrivateRemoving:
                name = "PRIVATE_REMOVING";
                break;
            //-
            case SPF_ResolveParallelRegionConflicts:
                name = "RESOLVE_PAR_REGIONS";
                break;
            case SPF_RemoveUnusedFunctions:
                name = "REMOVE_UNUSED_FUNCTIONS";
                break;
            case SPF_DuplicateFunctionChains:
                name = "DUPLICATE_FUNCTIONS";
                break;
            //--
            case SPF_InitDeclsWithZero:
                name = "SET_TO_ALL_DECL_INIT_ZERO";
                break;
            case SPF_ConvertStructures:
                name = "CONVERT_STRUCTURES_TO_SIMPLE";
                break;
            //--
            case SPF_SharedMemoryParallelization:
                name = "INSERT_PARALLEL_DIRS_NODIST";
                break;
            case SPF_InsertDvmhRegions:
                name = "INSERT_REGIONS";
                break;
            case SPF_ExpressionSubstitution:
                name = "SUBST_EXPR_RD_AND_UNPARSE";
                break;
            case SPF_ResolveCommonBlockConflicts:
                name = "FIX_COMMON_BLOCKS";
                break;
        }
        return p + " " + name;
    }
}


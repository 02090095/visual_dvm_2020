package Visual_DVM_2021.Passes;
import Common.Current;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.Variants.ParallelVariant;
import Repository.Component.Sapfor.Sapfor;
public abstract class VariantsMassPass extends CurrentProjectPass {
    public abstract PassCode_2021 getSubPassCode();
    int count;
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    public void Interrupt() throws Exception {
        Current.getSapfor().Interrupt();
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        count =0;
        if (super.canStart(args)) {
            count = target.parallelVariants.getCheckedCount();
            if (count== 0) {
                Log.Writeln_("Не выбрано ни одного параллельного варианта");
                return false;
            } else if (!passes.get(PassCode_2021.SPF_GetArrayDistribution).isDone()) {
                Log.Writeln_("Варианты неактуальны. Сначала перестройте распределение данных.");
                return false;
            }
            return true;
        }
        return false;
    }
    protected void body() throws Exception {
        ShowProgress(count, 0, true);
        int i = 0;
        for (ParallelVariant p : target.parallelVariants.Data.values())
            if (p.isSelected()) {
                if (!passes.get(getSubPassCode()).Do(p)) break;
                ShowProgress(count, ++i, true);
            }
    }
    @Override
    protected void performFinish() throws Exception {
        //распаковку соо делаем только 1 раз. после всей массы вариантов.
        Sapfor sapfor = Current.getSapfor();
        target.unpackMessagesAndLog(sapfor.getOutputMessage(), sapfor.getOutput());
    }
    @Override
    protected void showFinish() throws Exception {
        UI.getVersionsWindow().getVariantsWindow().RefreshVariants();
        UI.getMainWindow().getProjectWindow().RefreshProjectFiles();
        UI.getMainWindow().getProjectWindow().ShowProjectSapforLog();
        //-
        DBProjectFile badFile = target.getFirstBadFile();
        if (badFile != null) {
            passes.get(PassCode_2021.OpenCurrentFile).Do(badFile);
            UI.getMainWindow().FocusProject();
            UI.getMainWindow().getProjectWindow().FocusFile();
        }
        //-
        if (Current.HasFile())
            Current.getFile().form.ShowMessages();
    }
}

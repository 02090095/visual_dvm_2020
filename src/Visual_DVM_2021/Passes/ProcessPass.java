package Visual_DVM_2021.Passes;
import Common.Global;
import Common.Utils.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Vector;
public abstract class ProcessPass<T> extends Pass_2021<T> {
    public Process process = null;
    public int exit_code = Utils.Nan;
    public LinkedHashMap<String, String> envs = new LinkedHashMap<>();
    public Vector<String> output = new Vector<>();
    protected String process_path = "";
    public void CreateProcess(String exec_path_in, File workspace, String ... command) throws Exception {
        output.clear();
        exit_code = Utils.Nan;
        process_path = exec_path_in;
        ProcessBuilder procBuilder = new ProcessBuilder(process_path);
        //-
        if (workspace!=null)
            procBuilder.directory(workspace);
        //-
        procBuilder.redirectErrorStream(true);
        for (String name : envs.keySet())
            procBuilder.environment().put(name, envs.get(name));
        envs.clear();
        // запуск программы
        process = procBuilder.start();
    }
    public void CreateProcess(String exec_path_in) throws Exception {
        CreateProcess(exec_path_in, null);
    }
    //ожидать завершения читая весь его поток в процессе выполнения.
    public void WaitForProcess() throws Exception {
        exit_code = process.waitFor();
        System.out.println("завершено с кодом " + exit_code);
        process = null;
        if (exit_code != 0)
            throw new PassException("Процесс завершился с кодом " + exit_code);
    }
    public void ReadAllOutput() {
        try {
            InputStream stdout = process.getInputStream();
            InputStreamReader isrStdout = new InputStreamReader(stdout);
            BufferedReader brStdout = new BufferedReader(isrStdout);
            String line;
            while ((line = brStdout.readLine()) != null) {
                System.out.println(line);
                output.add(line);
            }
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
    }
    public String ReadLine() throws Exception {
        InputStream stdout = process.getInputStream();
        InputStreamReader isrStdout = new InputStreamReader(stdout);
        BufferedReader brStdout = new BufferedReader(isrStdout);
        return brStdout.readLine();
    }
    public void PerformScript(String script_text) throws Exception {
        PerformScript(Utils.CreateTempFile("script", Global.isWindows ? "bat" : "", script_text), true);
    }
    public void PerformScript(File script, boolean wait) throws Exception {
        if (!script.setExecutable(true)) throw new PassException("Не удалось создать исполняемый файл для скрипта");
        CreateProcess(script.getAbsolutePath());
        if (wait) {
            ReadAllOutput();
            WaitForProcess();
        }
    }
    @Override
    protected boolean needsAnimation() {
        return true;
    }
}

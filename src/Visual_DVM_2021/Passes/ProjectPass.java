package Visual_DVM_2021.Passes;
import Common.Current;
import ProjectData.Project.db_project_info;
public class ProjectPass extends Pass_2021<db_project_info>{
    protected db_project_info getProject(){
       return Current.getProject();
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (target = getProject()) != null;
    }
}

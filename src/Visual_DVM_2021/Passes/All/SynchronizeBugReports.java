package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
import org.apache.commons.io.FileUtils;

import java.io.File;
public class SynchronizeBugReports extends ComponentsRepositoryPass<Object> {
    File new_db_file;
    @Override
    public String getIconPath() {
        return "/icons/ComponentsActual.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return Current.getAccount().CheckRegistered(Log);
    }
    @Override
    protected void showPreparation() throws Exception {
        server.db.bugReports.SaveLastSelections();
        server.db.bugReports.ClearUI();
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.ReceiveBugReportsDatabase));
        response.Unpack(new_db_file = Utils.getTempFileName("bdb"));
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        server.db.Disconnect();
        Utils.forceDeleteWithCheck(server.db.getFile());
        FileUtils.moveFile(new_db_file, server.db.getFile());
        server.db.Connect();
        server.db.prepareTablesStatements();
        server.db.Synchronize();
    }
    @Override
    protected void showDone() throws Exception {
        if (UI.HasNewMainWindow()) {
            UI.getMainWindow().getCallbackWindow().ShowBugReports();
            Global.componentsServer.db.bugReports.RestoreLastSelections();
        }
    }
}

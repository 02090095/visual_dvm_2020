package Visual_DVM_2021.Passes.All;
import Common.Current;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.LanguageStyle;
import Visual_DVM_2021.Passes.FilesMassPass;
public class SetSelectedFilesStyle extends FilesMassPass<LanguageStyle> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            target = (LanguageStyle) args[0];
            return true;
        }
        return false;
    }
    @Override
    protected void operation(DBProjectFile file) {

        file.UpdateStyle(target);
        if (Current.HasFile() && file.file.equals(Current.getFile().file))
            file.form.ShowStyle();
    }
}

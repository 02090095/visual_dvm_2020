package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.RemoteSapfor.RemoteSapfor;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditSapfor extends EditObjectPass<RemoteSapfor> {
    public EditSapfor() {
        super(RemoteSapfor.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

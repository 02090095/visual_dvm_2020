package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.EmailMessage;
import Repository.Subscribes.Subscriber;
import Repository.Subscribes.UI.SubscriberForm;

import javax.swing.*;
import java.util.Vector;
public class EditAccount extends Email {
    public String name;
    public String email;
    String password;
    SubscriberForm f = new SubscriberForm(){
        {
            fields.cbRole.setEnabled(false);
        }
    };
    public static int getRandomIntegerBetweenRange(int min, int max) {
        return (int) ((Math.random() * ((max - min) + 1)) + min);
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        Subscriber res = new Subscriber(); // объект для заполнения полей.не более.
        if (f.ShowDialog("Регистрация", res)) {
            if (!Utils.validateEmail(res.address, Log)) {
                return false;
            }
            name = res.name;
            email = res.address;
            Vector<String> rec = new Vector<>();
            rec.add(email);
            password = String.valueOf(getRandomIntegerBetweenRange(1111, 9999));
            return super.canStart(new EmailMessage("Код подтверждения визуализатора для: " + Utils.Brackets(name), password, rec));
        }
        return false;
    }
    @Override
    protected boolean validate() {
        String attempt = null;
        do {
            attempt = JOptionPane.showInputDialog(null,
                    new String[]{"Введите код активации, полученный по почте"},
                    "Подтверждение адреса почты",
                    JOptionPane.INFORMATION_MESSAGE);
            if (attempt != null) {
                System.out.println("Введенный код: " + Utils.Brackets(attempt));
                if (attempt.equals(password)) {
                    UI.Info("Почта успешно подтверждена!");
                    return true;
                } else {
                    UI.Error("Неверный код активации.\nПовторите попытку.");
                }
            } else {
                UI.Info("Подтверждение почты отменено");
                return false;
            }
        } while (true);
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        Current.getAccount().name = name;
        Current.getAccount().email = email;
        Global.db.Update(Current.getAccount());
        //это не регистрация. только заполнение почты в своей бд и ее подтверждение на реальность.
    }
}

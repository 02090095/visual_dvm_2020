package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import Repository.Subscribes.Subscriber;
import Visual_DVM_2021.Passes.DeleteObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class DeleteSubscriber extends DeleteObjectPass<Subscriber> {
    public DeleteSubscriber() {
        super(Subscriber.class);
    }
    @Override
    protected Database getDb() {
        return Global.componentsServer.db;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        passes.get(PassCode_2021.DeleteSubscriberOnServer).Do(target);
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommand;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class PublishSapforConfigurationCommand extends TestingSystemPass<SapforConfigurationCommand> {
    @Override
    public String getIconPath() {
        return "/icons/RedAdd.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().CheckRegistered(Log)&& Current.Check(Log, Current.SapforConfiguration)) {
            target = new SapforConfigurationCommand();
            target.genName();
            target.sender_name = Current.getAccount().name;
            target.sender_address = Current.getAccount().email;
            target.sapforconfiguration_id=Current.getSapforConfiguration().id;
            return server.db.sapforConfigurationCommands.ShowAddObjectDialog(target);
        }
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.PublishObject, "", target));
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        passes.get(PassCode_2021.SynchronizeTests).Do();
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        server.db.sapforConfigurationCommands.ui_.Show(target.getPK());
    }
}

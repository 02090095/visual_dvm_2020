package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.SapforProfile.SapforProfile;
import Visual_DVM_2021.Passes.AddObjectPass;

import java.util.Date;
public class SaveProfile extends AddObjectPass<SapforProfile> {
    public SaveProfile() {
        super(SapforProfile.class);
    }
    @Override
    public String getIconPath() {
        return "/icons/Save.png";
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        //1. проверить нет ли уже такой последовательности настроек.
        SapforProfile profile = Global.db.checkProfileForCurrentSettings();
        if (profile != null) {
            Log.Writeln_("Текущий набор настроек уже сохранён как профиль " + Utils.Brackets(profile.description));
            return false;
        }
        return super.canStart(args);
    }
    @Override
    protected void body() throws Exception {
        target.creationDate = new Date().getTime();
        super.body();
        Global.db.insertProfileSettings(target);
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.TasksPackage.TasksPackage;
import TestingSystem.TasksPackage.TasksPackageState;
import TestingSystem.TestingServer;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
import javafx.util.Pair;

import java.util.Vector;
public class ActualizePackages extends TestingSystemPass<Vector<TasksPackage>> {
    boolean needsSynchronize;
    @Override
    protected void ServerAction() throws Exception {
        Vector<TasksPackage> actual_packages = new Vector<>();
        for (TasksPackage p : target) {
            TasksPackageState oldState = p.state;
            Command(new ServerExchangeUnit_2021(ServerCode.GetAccountObjectCopyByPK, Current.getAccount().email, new Pair<>(TasksPackage.class, p.id)));
            TasksPackage actual_package = (TasksPackage) response.object;
            actual_packages.add(actual_package);
            //-
            p.SynchronizeFields(actual_package);
            if (!oldState.equals(p.state) && p.state.equals(TasksPackageState.Done))
                needsSynchronize=true; //состояние изменилось, и на Done значит нужно скачивать задачи.
            server.account_db.Update(p);
        }
    }
    @Override
    protected boolean needsAnimation() {
        return false;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        needsSynchronize=false;
        target = server.account_db.getActivePackages();
        return !target.isEmpty();
    }
    @Override
    protected void performCanNotStart() throws Exception {
        TestingServer.TimerOff();
    }
    @Override
    protected void showCanNotStart() throws Exception {
        UI.getMainWindow().getTestingWindow().ShowAutoActualizeTestsState();
    }
    @Override
    protected void performPreparation() throws Exception {
        server.account_db.SaveLastSelections();
    }
    @Override
    protected void showPreparation() throws Exception {
        server.account_db.packages.ClearUI();
    }
    @Override
    protected void showDone() throws Exception {
        server.account_db.packages.ShowUI();
        if (!TestingServer.checkTasks) UI.getMainWindow().getTestingWindow().ShowAutoActualizeTestsState();
        server.account_db.RestoreLastSelections();
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        if (needsSynchronize) {
            passes.get(PassCode_2021.SynchronizeTestsTasks).Do();
        }
    }
}

package Visual_DVM_2021.Passes.All;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Repository.Subscribes.Subscriber;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class AddSubscriber extends ComponentsRepositoryPass<Subscriber> {
    @Override
    public String getIconPath() {
        return "/icons/RedAdd.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Subscriber();
        return fillObjectFields();
    }
    protected boolean fillObjectFields() throws Exception {
        return server.db.subscribers.ShowAddObjectDialog(target);
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.PublishObject, "", target));
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        passes.get(PassCode_2021.SynchronizeBugReports).Do();
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        server.db.subscribers.ui_.Show(target.getPK());
    }
}

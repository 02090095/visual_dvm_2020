package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import GlobalData.Machine.MachineType;
import GlobalData.RemoteSapfor.RemoteSapfor;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddSapfor extends AddObjectPass<RemoteSapfor> {
    public AddSapfor() { super(RemoteSapfor.class); }
    @Override
    protected Database getDb() {
        return Global.db;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Machine)){
            if (Current.getMachine().type.equals(MachineType.Server)){
                target = d.newInstance();
                return fillObjectFields();
            }else Log.Writeln_("Тестирование SAPFOR поддерживается только на удаленном одиночном сервере.");
        }
        return false;
    }
}

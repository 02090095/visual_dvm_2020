package Visual_DVM_2021.Passes.All;
import Common.Utils.Utils;
import GlobalData.Tasks.TaskState;
import ProjectData.Project.db_project_info;
import TestingSystem.Sapfor.SapforTask.SapforTask_2023;
import TestingSystem.Sapfor.SapforVersion_json;
import TestingSystem.Sapfor.ScenarioResults_json;
import TestingSystem.Sapfor.Scenario_json;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.PassException;
import Visual_DVM_2021.Passes.Pass_2021;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;
public class PerformSapforTasksPackage extends Pass_2021<Scenario_json> {
    File packageWorkspace;
    File sapfor_drv;
    File scenarioFile;
    //---
    File parentTask;
    File task;
    //---
    Process process = null;
    int exit_code = Utils.Nan;
    //----
    File outputFile = null;
    File errorsFile = null;
    //--
    Vector<String> outputLines;
    Vector<String> errorsLines;
    //--
    ScenarioResults_json results;
    //-
    SapforTask_2023 taskResult = null;
    //--
    @Override
    protected boolean canStart(Object... args) throws Exception {
        //--
        packageWorkspace = new File((String)args[0]);
        //---
        scenarioFile = new File(packageWorkspace, "scenario.txt");
        sapfor_drv = new File(packageWorkspace, "SAPFOR_F.exe");
        String packed = FileUtils.readFileToString(scenarioFile, Charset.defaultCharset());
        target = Utils.gson.fromJson(packed, Scenario_json.class);
        //---
        results = new ScenarioResults_json();
        return true;
    }
    @Override
    protected void body() throws Exception {
        for (String test : target.tests) {
            //--
            parentTask = new File(packageWorkspace, test);
            //--
            taskResult = new SapforTask_2023();
            taskResult.test_description = test;
            taskResult.sapfortaskspackage_2023_id = Integer.parseInt(packageWorkspace.getName());
            taskResult.versions.add(new SapforVersion_json(parentTask.getAbsolutePath(), "исходная"));
            //--
            for (PassCode_2021 code : target.codes) {
                if (parse()) {
                    if (code.equals(PassCode_2021.CreateParallelVariants))
                        variants();
                    else if (!transformation(code))
                        break;
                } else
                    break;
            }
            //---
            results.tasks.add(taskResult);
        }
    }
    protected boolean performSapforScript(String name, File workspace, String flags, String command, String outName, String errName) throws Exception {
        process = null;
        exit_code = Utils.Nan;
        //---
        File data_workspace = new File(workspace, db_project_info.data);
        Utils.CheckDirectory(data_workspace);
        outputFile = new File(data_workspace, outName);
        errorsFile = new File(data_workspace, errName);
        Utils.delete_with_check(outputFile);
        Utils.delete_with_check(errorsFile);
        //---
        File file = new File(data_workspace, name + ".bat");
        FileUtils.write(file,
                Utils.DQuotes(sapfor_drv.getAbsolutePath())
                        + (flags.isEmpty() ? "" : (" " + flags))
                        + " -noLogo"
                        + " " + command +
                        " 1>" +
                        Utils.DQuotes(outputFile.getAbsolutePath()) +
                        " 2>" +
                        Utils.DQuotes(errorsFile.getAbsolutePath()),
                Charset.defaultCharset());
        if (!file.setExecutable(true))
            throw new PassException("Не удалось сделать файл скрипта " + name + " исполняемым!");
        //--
        ProcessBuilder procBuilder = new ProcessBuilder(file.getAbsolutePath());
        procBuilder.directory(workspace);
        process = procBuilder.start();
        exit_code = process.waitFor();
        if (exit_code != 0)
            throw new PassException("Процесс завершился с кодом " + exit_code);
        process = null;
        //---
        outputLines = new Vector<>(FileUtils.readLines(outputFile));
        errorsLines = new Vector<>(FileUtils.readLines(errorsFile));
        return checkLines(outputLines) && checkLines(errorsLines);
    }
    protected boolean checkLines(Vector<String> lines) {
        for (String line : lines) {
            if (line.toLowerCase().contains("internal error")) {
                return false;
            }
            if (line.toLowerCase().contains("exception")) {
                return false;
            }
            if (line.contains("[ERROR]")) {
                return false;
            }
        }
        return true;
    }
    protected boolean parse() throws Exception {
        if (performSapforScript("parse", parentTask, target.flags,
                "-parse *.f *.for *.fdv *.f90 *.f77",
                "parse_out.txt", "parse_err.txt")
                && (new File(parentTask, "dvm.proj")).exists()) {
            return true;
        } else {
            taskResult.state = TaskState.DoneWithErrors;
            return false;
        }
    }
    protected boolean transformation(PassCode_2021 code) throws Exception {
        task = new File(parentTask, "v1");
        Utils.CheckAndCleanDirectory(task); //папка для преобразования.
        if (performSapforScript("transformation", parentTask, target.flags,
                code.getTestingCommand() + " -F " + Utils.DQuotes(task.getAbsolutePath()),
                "out.txt",
                "err.txt"
        )) {
            taskResult.state = TaskState.Done;
            taskResult.versions.add(new SapforVersion_json(task.getAbsolutePath(), code.getDescription()));
            parentTask = task;
            return true;
        }
        Utils.delete_with_check(task);
        taskResult.state = TaskState.DoneWithErrors;
        return false;
    }
    protected boolean variants() throws Exception {
        if (performSapforScript("create_variants", parentTask, target.flags, " -t 13 -allVars",
                "out.txt",
                "err.txt"
        )) {
            //найти папки с вариантами.
            File[] files_ = parentTask.listFiles((dir, name) -> dir.isDirectory() && Utils.isParallelVersionName(name));
            if ((files_ != null) && (files_.length > 0)) {
                Vector<File> files = new Vector<>(Arrays.asList(files_));
                files.sort(Comparator.comparingInt(o -> Integer.parseInt(o.getName().substring(1))));
                taskResult.state = TaskState.Done;
                for (File file : files)
                    taskResult.variants.add(new SapforVersion_json(file.getAbsolutePath(), PassCode_2021.SPF_CreateParallelVariant.getDescription()));
                return true;
            }
        }
        taskResult.state = TaskState.DoneWithErrors;
        return false;
    }
}

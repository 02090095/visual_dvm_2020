package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
public class CloseCurrentFile extends Pass_2021<DBProjectFile> {
    @Override
    public String getIconPath() {
        return "/icons/Close.png";
    }
    @Override
    public Icon getTabIcon() {
        return Utils.getIcon("/icons/Close_18.png");
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (target = Current.getFile()) != null;
    }
    @Override
    protected void body() throws Exception {
        //действия по закрытию. сохранение и т д.
        target.form.SaveSplitters();
        target.UpdateLastLine(target.form.getEditor().getCurrentLine());
        passes.get(PassCode_2021.Save).Do();
        UI.getSearchReplaceForm().ClearMarkers();
        target.form = null;
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.File, null);
        Current.set(Current.FileGraphElement, null);
        //-
        Current.set(Current.Notes, null);
        Current.set(Current.Warnings, null);
        Current.set(Current.Errors, null);
        //-
    }
    @Override
    protected void showDone() throws Exception {
        //отобразить отсутствие файла.
        UI.getMainWindow().getProjectWindow().ShowNoFile();
    }
}

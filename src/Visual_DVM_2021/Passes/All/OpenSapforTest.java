package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.LanguageName;
import ProjectData.Messages.Errors.MessageError;
import ProjectData.Project.db_project_info;
import TestingSystem.Sapfor.SapforTask.SapforTaskResult;
import TestingSystem.Sapfor.SapforTask.SapforTask_2023;
import TestingSystem.Sapfor.SapforVersion_json;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Paths;
import java.util.Vector;

import static java.lang.Character.isDigit;
public class OpenSapforTest extends Pass_2021<SapforTaskResult> {
    String last_version = null;
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        last_version = null;
        Current targetName = (Current) args[0];
        if (Current.Check(Log, targetName)) {
            target = (SapforTaskResult) Current.get(targetName);
            return true;
        }
        return false;
    }
    @Override
    public String getIconPath() {
        return "/icons/OpenProject.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    public MessageError unpackMessage(String line_in) throws Exception {
        MessageError res = new MessageError();
        res.file = "";
        res.line = Utils.Nan;
        res.value = "";
        String line = line_in.substring(9);
        System.out.println(line);
        int i = 0;
        int s = 0;
        String lexeme = "";
        //#1020: red43.fdv: line 988]: Active DVM directives are not supported (turn on DVM-directive support option)
        for (char c : line.toCharArray()) {
            //  System.out.print("<s=" + s + ">");
            //  System.out.println(c);
            switch (s) {
                case 0:
                    //поиск groups_s
                    if (c == '#') {
                        s = 1;
                        lexeme = "";
                    } else return null;
                    break;
                case 1:
                    //group_s
                    if (isDigit(c)) {
                        res.group_s += c;
                        lexeme += c;
                    } else if (c == ':') {
                        s = 2;
                        res.group = Integer.parseInt(lexeme);
                    } else return null;
                    break;
                case 2:
                    //поиск filename
                    if (c == ' ') {
                        s = 3;
                    } else return null;
                    break;
                case 3:
                    //filename
                    if (c == ':') {
                        s = 4;
                    } else {
                        res.file += c;
                    }
                    break;
                case 4:
                    //поиск line
                    if (c == ' ') {
                        s = 5;
                        lexeme = "";
                    } else return null;
                    break;
                case 5:
                    //line
                    if (c == ' ') {
                        if (!lexeme.equals("line"))
                            return null;
                        else {
                            s = 6;
                            lexeme = "";
                        }
                    } else {
                        lexeme += c;
                    }
                    break;
                case 6:
                    //line number
                    if (isDigit(c)) {
                        lexeme += c;
                    } else if (c == ']') {
                        res.line = Integer.parseInt(lexeme);
                        s = 7;
                    } else return null;
                    break;
                case 7:
                    //Поиск value
                    if (c == ':') {
                        s = 8;
                    } else return null;
                    break;
                case 8:
                    if (c == ' ') {
                        s = 9;
                    } else return null;
                    break;
                case 9:
                    //value
                    res.value += c;
                    break;
            }
            ;
            ++i;
        }
        //--
        if (s != 9)
            return null;
        //--
        return res;
    }
    public void readMessagesFromFileDump(File file, Vector<MessageError> messages) throws Exception {
        Vector<String> lines = new Vector<>(FileUtils.readLines(file));
//ERROR - [#1020: red43.fdv: line 988]: Active DVM directives are not supported (turn on DVM-directive support option)
        if (!lines.isEmpty()) {
            for (int i = lines.size() - 1; i >= 0; --i) {
                String line = lines.get(i);
                if (line.startsWith("ERROR - ")) {
                    // System.out.println(i + ":" + Utils.Brackets(line));
                    MessageError message = unpackMessage(line);
                    if (message != null)
                        messages.add(message);
                    //--
                } else break;
            }
        }
    }
    protected void checkVersion(SapforVersion_json version, boolean isTransformation) throws Exception {
        ShowMessage2(version.description + " — " + version.version);
        db_project_info project = new db_project_info();
        project.Home = new File(version.version);
        project.name = project.Home.getName();
        project.description = version.description;
        project.languageName = LanguageName.fortran;
        project.creationDate = Utils.getDateNumber();
        //--
        Vector<MessageError> messages = new Vector<>();
        //--
        if (isTransformation) {
            File p_out = Paths.get(project.Home.getAbsolutePath(), db_project_info.data, "parse_out.txt").toFile();
            File out = Paths.get(project.Home.getAbsolutePath(), db_project_info.data, "out.txt").toFile();
            //--
            if (p_out.exists()) {
                project.Log += (FileUtils.readFileToString(p_out));
                readMessagesFromFileDump(p_out, messages);
            }
            if (out.exists()) {
                project.Log += "\n" + FileUtils.readFileToString(out);
                readMessagesFromFileDump(out, messages);
            }
            //--

        }
        project.CreateVisualiserData();
        //---
        if (isTransformation && !messages.isEmpty()) {
            project.Open();
            project.db.BeginTransaction();
            System.out.println("messages size=" + messages.size());
            for (MessageError m : messages) {
                if (project.db.files.containsKey(m.file)) {
                    DBProjectFile file = project.db.files.Data.get(m.file);
                    file.CreateAndAddNewMessage(1, m.value, m.line, m.group);
                    //update file
                    project.db.Update(file);
                }
            }
            project.db.Commit();
            project.db.Disconnect();
        }
    }
    @Override
    protected void body() throws Exception {
        File resultsFile = Paths.get(target.sapforTasksPackage.workspace, target.task.test_description, db_project_info.data, "results.txt").toFile();
        String resultsText = FileUtils.readFileToString(resultsFile);
        SapforTask_2023 savedTask = Utils.gson.fromJson(resultsText, SapforTask_2023.class);
        //--->>
        if (target.task.versions_tree_built == 0) {
            ShowMessage1("Построение дерева версий..");
            for (SapforVersion_json version : savedTask.versions)
                checkVersion(version, true);
            for (SapforVersion_json version : savedTask.variants)
                checkVersion(version, false);
            target.task.versions_tree_built = 1;
            Global.db.Update(target.task);
        }
        if (!savedTask.variants.isEmpty()) {
            last_version = savedTask.variants.lastElement().version;
        } else if (!savedTask.versions.isEmpty())
            last_version = savedTask.versions.lastElement().version;
    }
    @Override
    protected boolean validate() {
        return last_version != null;
    }
    @Override
    protected void performDone() throws Exception {
        passes.get(PassCode_2021.OpenCurrentProject).Do(new File(last_version));
    }
}

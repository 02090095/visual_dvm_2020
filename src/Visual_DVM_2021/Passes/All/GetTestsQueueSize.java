package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class GetTestsQueueSize extends TestingSystemPass<Long> {
    @Override
    public String getIconPath() {
        return "/icons/Help.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return Current.Check(Log, Current.TasksPackage);
    }
    @Override
    protected void ServerAction() throws Exception {
        target = 0L;
        Command(new ServerExchangeUnit_2021(ServerCode.GetQueueSize, "", Current.getTasksPackage().StartDate));
        target = (long) response.object;
    }
    @Override
    protected void showDone() throws Exception {
        UI.Info("Задач в очереди: "+target);
    }
}

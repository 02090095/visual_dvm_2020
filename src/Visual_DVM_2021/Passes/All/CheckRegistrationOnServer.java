package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Repository.Subscribes.Subscriber;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
public class CheckRegistrationOnServer extends ComponentsRepositoryPass<Subscriber> {
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.CheckSubscriberRole, "", Current.getAccount()));
        target = (Subscriber) response.object;
        Current.getAccount().role = target.role;
    }
}

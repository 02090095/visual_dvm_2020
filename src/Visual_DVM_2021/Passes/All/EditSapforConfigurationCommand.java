package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommand;
import Visual_DVM_2021.Passes.EditObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class EditSapforConfigurationCommand  extends EditObjectPass<SapforConfigurationCommand> {
    public EditSapforConfigurationCommand() {
        super(SapforConfigurationCommand.class);
    }
    @Override
    public String getIconPath() {
        return "/icons/Edit.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.SapforConfiguration)) {
            return getTable().ShowEditObjectDialog(target = (SapforConfigurationCommand) Current.get(Current.SapforConfigurationCommand));
        }
        return false;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //отправка.
        passes.get(PassCode_2021.EditSapforConfigurationCommandOnServer).Do(target);
    }
    @Override
    protected Database getDb() {
        return Global.testingServer.db;
    }
}


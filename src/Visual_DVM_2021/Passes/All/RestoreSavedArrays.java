package Visual_DVM_2021.Passes.All;
import Visual_DVM_2021.Passes.SapforModification;

import java.util.Vector;
public class RestoreSavedArrays extends SapforModification {
    @Override
    public boolean needsConfirmations() {
        return false;
    }
    @Override
    protected String getSapforPassName() {
        return "SPF_SetDistributionFlagToArrays";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args) && !target.db.savedArrays.Data.isEmpty()) {
            Vector<String> keys = new Vector<>();
            Vector<String> states = new Vector<>();
            for (String s : target.db.savedArrays.Data.keySet()) {
                keys.add(s);
                states.add(String.valueOf(target.db.savedArrays.Data.get(s).State.ordinal()));
            }
            addOpt1 = String.join("|", keys);
            addOpt2 = String.join("|", states);
            return true;
        }
        return false;
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;

import java.io.File;
import java.util.Vector;
public class GetComponentsBackupsFromServer extends ComponentsRepositoryPass<Vector<File>> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Vector<>();
        return (Current.Check(Log, Current.Component));
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.GetComponentsBackups, Current.getComponent().getComponentType().toString()));
        target = (Vector<File>) response.object;
    }
    @Override
    protected boolean validate() {
        if (target.isEmpty()) {
            Log.Writeln_("Не найдено резервных копий на сервере для компонента " + Current.getComponent().getComponentType().getDescription());
            return false;
        }
        return true;
    }
}

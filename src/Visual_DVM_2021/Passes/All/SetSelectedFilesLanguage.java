package Visual_DVM_2021.Passes.All;
import Common.Current;
import ProjectData.Files.DBProjectFile;
import ProjectData.LanguageName;
import Visual_DVM_2021.Passes.FilesMassPass;
public class SetSelectedFilesLanguage extends FilesMassPass<LanguageName> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            target = (LanguageName) args[0];
            return true;
        }
        return false;
    }
    @Override
    protected void operation(DBProjectFile file) {
        file.UpdateLanguage(target);
        if (Current.HasFile() && file.file.equals(Current.getFile().file))
            file.form.ShowLanguage();
    }
}

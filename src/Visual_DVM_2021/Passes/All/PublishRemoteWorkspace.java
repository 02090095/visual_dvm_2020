package Visual_DVM_2021.Passes.All;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Repository.SubscriberWorkspace.SubscriberWorkspace;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
public class PublishRemoteWorkspace extends ComponentsRepositoryPass<SubscriberWorkspace> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (SubscriberWorkspace) args[0];
        return true;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.PublishObject, "",target));
    }
}

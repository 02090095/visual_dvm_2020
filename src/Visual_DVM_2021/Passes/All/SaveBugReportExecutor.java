package Visual_DVM_2021.Passes.All;
import Common.Current;
public class SaveBugReportExecutor extends UpdateBugReportField {
    @Override
    public String getIconPath() {
        return "/icons/Apply.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (Current.Check(Log, Current.Subscriber)) &&
                super.canStart("executor", Current.getSubscriber().name,
                        "executor_address", Current.getSubscriber().address
                );
    }
}

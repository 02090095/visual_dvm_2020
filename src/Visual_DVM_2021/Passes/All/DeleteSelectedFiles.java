package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class DeleteSelectedFiles extends Pass_2021 {
    @Override
    public String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getProject().db.files.getCheckedCount() == 0) {
            Log.Writeln_("Не отмечено ни одного файла.");
            return false;
        }
        return UI.Warning("Удалить " + Current.getProject().db.files.getCheckedCount() + " файлов.");
    }
    @Override
    protected void performPreparation() throws Exception {
        boolean hasCurrent = false;
        boolean hasSelected = false;
        if (Current.HasFile()) {
            for (DBProjectFile file : Current.getProject().db.files.getCheckedItems()) {
                if (Current.getFile().file.equals(file.file))
                    hasCurrent = true;
                if (Current.getSelectedFile().file.equals(file.file))
                    hasSelected = true;
            }
        }
        if (hasCurrent)
            passes.get(PassCode_2021.CloseCurrentFile).Do();
        if (hasSelected) {
            Current.set(Current.SelectedFile, null);
            Current.set(Current.ProjectNode, null);
        }
    }
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    protected void body() throws Exception {
        for (DBProjectFile file : Current.getProject().db.files.getCheckedItems()) {
            ShowMessage1(file.name);
            UI.getMainWindow().getProjectWindow().getFilesTreeForm().getTree().RemoveNode(file.node);
            Current.getProject().db.Delete(file);
            Utils.forceDeleteWithCheck(file.file);
        }
    }
}

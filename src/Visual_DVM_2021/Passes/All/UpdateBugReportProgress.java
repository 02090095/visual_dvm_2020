package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.Windows.Dialog.PercentsForm;
public class UpdateBugReportProgress extends UpdateBugReportField {
    @Override
    public String getIconPath() {
        return "/icons/Progress.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (!Current.Check(Log, Current.BugReport))
            return false;
        PercentsForm f = new PercentsForm();
        if (f.ShowDialog("Завершённость работы над ошибкой", Current.getBugReport().percentage))
            return super.canStart("percentage", f.Result);
        return false;
    }
    @Override
    protected boolean canUpdate() {
        return Current.getAccount().ExtendedCheckAccessRights(target, Log);
    }
}

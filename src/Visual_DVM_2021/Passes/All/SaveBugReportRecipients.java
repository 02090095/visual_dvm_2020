package Visual_DVM_2021.Passes.All;
import Repository.BugReport.BugReportInterface;
public class SaveBugReportRecipients extends UpdateBugReportField {
    @Override
    public String getIconPath() {
        return "/icons/Save.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("targets", BugReportInterface.getPackedTargets());
    }
}

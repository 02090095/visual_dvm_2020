package Visual_DVM_2021.Passes.All;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Repository.Subscribes.Subscriber;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class DeleteSubscriberOnServer extends ComponentsRepositoryPass<Subscriber> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (Subscriber) args[0];
        return true;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DeleteObject, "", target));
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        passes.get(PassCode_2021.SynchronizeBugReports).Do();
    }
}

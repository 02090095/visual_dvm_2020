package Visual_DVM_2021.Passes.All;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;

import java.util.Vector;
public class DeleteSubversions extends CurrentProjectPass {
    @Override
    public String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    protected void body() throws Exception {
        Vector<db_project_info> targets = new Vector<>(target.versions.values());
        for (db_project_info m : targets) {
            passes.get(PassCode_2021.DeleteVersion).Do(m);
        }
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
public class AppendBugReportComment extends AppendBugReportField {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("comment",
                UI.getMainWindow().getCallbackWindow().getBugReportCommentAdditionText());
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        UI.getMainWindow().getCallbackWindow().ClearBugReportCommentAdditionText();
    }
    @Override
    protected boolean canUpdate() {
        return Current.getAccount().ExtendedCheckAccessRights(target, Log);
    }
}

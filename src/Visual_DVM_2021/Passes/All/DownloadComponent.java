package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Utils.Utils;
import Repository.Component.Component;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
public class DownloadComponent extends ComponentsRepositoryPass<Component> {
    @Override
    protected void ServerAction() throws Exception {
        target = Current.getComponent();
        String packed = target.getComponentType()+"\n"+target.getFileName();
        Command(new ServerExchangeUnit_2021(ServerCode.ReceiveComponent, packed));
        Utils.unpackFile((byte[]) response.object, target.getNewFile());
    }
}

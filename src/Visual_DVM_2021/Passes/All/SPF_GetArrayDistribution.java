package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.UI.UI;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforAnalysis;
public class SPF_GetArrayDistribution extends SPF_GetArrayDistributionOnlyAnalysis {
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        SapforAnalysis code_analysis = (SapforAnalysis) passes.get(PassCode_2021.SPF_GetArrayDistributionOnlyAnalysis);
        if (!code_analysis.isDone()) {
            code_analysis.setDone();
            code_analysis.MarkAsDone();
        }
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        UI.getVersionsWindow().UnblockVariants();
        UI.getVersionsWindow().getVariantsWindow().ShowVariants();
    }
    @Override
    protected void FocusResult() {
        super.FocusResult();
        UI.getMainWindow().getProjectWindow().FocusVersions();
        UI.getVersionsWindow().FocusDistribution();
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Global.db.settings.get(SettingName.MPI_PROGRAM).toBoolean()) {
            UI.Info("Включена настройка SAPFOR 'MPI программа'." +
                    "\nПостроение распределения данных невозможно.");
            passes.get(PassCode_2021.SPF_SharedMemoryParallelization).Do();
            return false;
        }
        return super.canStart(args);
    }
}

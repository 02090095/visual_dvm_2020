package Visual_DVM_2021.Passes.All;
import Common.Current;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileType;
import Visual_DVM_2021.Passes.FilesMassPass;
public class SetSelectedFilesType extends FilesMassPass<FileType> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            target = (FileType) args[0];
            return true;
        }
        return false;
    }
    @Override
    protected void operation(DBProjectFile file) {
        file.UpdateType(target);
        if (Current.HasFile() && file.file.equals(Current.getFile().file))
            file.form.ShowType();
    }
}

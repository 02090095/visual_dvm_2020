package Visual_DVM_2021.Passes.All;
import Common.Current;
import Visual_DVM_2021.Passes.SapforFilesModification;
public class SPF_ChangeSpfIntervals extends SapforFilesModification {
    //  SPF_ChangeSpfIntervals (addOpt1_c -> file, addOpt2_c-> int lines, '|' as delimiter)
    int start;
    int end;
    int mod;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args) && Current.Check(Log, Current.File)) {
            start = (int) args[0];
            end = (int) args[1];
            mod = (int) args[2];
            addOpt1 = Current.getFile().name;
            addOpt2 = start + "|" + end + "|" + mod;
            return true;
        }
        return false;
    }
}

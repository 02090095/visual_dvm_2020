package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileType;
import Repository.Component.Sapfor.TransformationPermission;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Transformation;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Vector;
public class PrepareForModulesAssembly extends Transformation {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetIncludeDependencies;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        switch (Global.transformationPermission) {
            case All:
                return super.canStart(args) && target.CheckSameStyle(Log);
            case VariantsOnly:
                if (getPermission().equals(TransformationPermission.VariantsOnly)) {
                    return super.canStart(args) && target.CheckSameStyle(Log);
                } else {
                    Log.Writeln_("Разрешено только построение параллельных вариантов!");
                    return false;
                }
            case None:
                Log.Writeln_("Нет разрешения на выполнение преобразований");
                return false;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        File total = Paths.get(target.last_version.Home.getAbsolutePath(), "total.for").toFile();
        while (total.exists()) {
            total = Paths.get(target.last_version.Home.getAbsolutePath(), Utils.getDateName("total") + ".for").toFile();
            Thread.sleep(1000);
        }
        //--создать
        Vector<String> total_lines = new Vector<>();
        for (String name : target.files_order) {
            total_lines.add("      include " + Utils.Quotes(Utils.toU(name)));
        }
        FileUtils.writeLines(total, total_lines, false);
        //-------------------------------
        //скопировать все файлы
        for (String name : target.db.files.Data.keySet()) {
            Files.copy(
                    target.db.files.Data.get(name).file.toPath(),
                    Paths.get(target.last_version.Home.getAbsolutePath(), name)
            );
        }
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //--->>>
        target.last_version.Open();
        target.last_version.db.BeginTransaction();
        for (String name : target.files_order){
            DBProjectFile file = target.last_version.db.files.get(name);
            file.fileType= FileType.header;
            target.last_version.db.Update(file);
        }
        target.last_version.db.Commit();
        target.last_version.Close();
    }
}

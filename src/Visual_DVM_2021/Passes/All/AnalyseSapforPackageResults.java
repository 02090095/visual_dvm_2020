package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import TestingSystem.Sapfor.SapforTask.SapforTask_2023;
import TestingSystem.Sapfor.ScenarioResults_json;
import Visual_DVM_2021.Passes.Pass_2021;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.charset.Charset;
public class AnalyseSapforPackageResults extends Pass_2021<ScenarioResults_json> {
    File packageWorkspace;
    File scenarioFile;
    //--
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    //--
    @Override
    protected boolean canStart(Object... args) throws Exception {
        //--
        packageWorkspace = new File((String) args[0]);
        scenarioFile = new File(packageWorkspace, "results.txt");
        //--
        String packed = FileUtils.readFileToString(scenarioFile, Charset.defaultCharset());
        target = Utils.gson.fromJson(packed, ScenarioResults_json.class);
        //---
        return true;
    }
    @Override
    protected void body() throws Exception {
        for (SapforTask_2023 task: target.tasks) {
            task.id=Global.db.IncSapforMaxTaskId();
            Global.db.Insert(task);
        }
    }
}

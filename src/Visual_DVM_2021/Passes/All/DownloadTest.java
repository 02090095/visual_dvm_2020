package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestInterface;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class DownloadTest extends TestingSystemPass<Test> {
    @Override
    public String getIconPath() {
        return "/icons/DownloadBugReport.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().CheckRegistered(Log) && Current.Check(Log, Current.Test)) {
            target = Current.getTest();
            return true;
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentProject).Do();
        Current.set(Current.Root, null); //чтобы гарантированно не существовало корня.
        Utils.forceDeleteWithCheck(TestInterface.getArchive(target));
        Utils.forceDeleteWithCheck(TestInterface.getHomePath(target));
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DownloadTest, target.id));
        response.Unpack(TestInterface.getArchive(target));
    }
    @Override
    protected boolean validate() {
        return TestInterface.getArchive(target).exists();
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        if (passes.get(PassCode_2021.UnzipFolderPass).Do(
                TestInterface.getArchive(target).getAbsolutePath(),
                Global.visualiser.getWorkspace().getAbsolutePath(), false
        ))
            if (UI.Question("Тестовый проект успешно загружен под именем\n" +
                    Utils.Brackets(TestInterface.getHomePath(target).getName()) +
                    "\nОткрыть его"))
                passes.get(PassCode_2021.OpenCurrentProject).Do(TestInterface.getHomePath(target));
    }
}



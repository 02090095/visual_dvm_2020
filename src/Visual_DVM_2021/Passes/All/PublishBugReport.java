package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportInterface;
import Repository.BugReport.BugReportState;
import Repository.EmailMessage;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
import java.util.Date;
public class PublishBugReport extends Pass_2021<BugReport> {
    @Override
    public String getIconPath() {
        return "/icons/Publish.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) {
        if (Current.Check(Log, Current.BugReport)) {
            target = Current.getBugReport();
            if (!target.state.equals(BugReportState.draft)) {
                Log.Writeln_("Отчёт об ошибке " + target.id + " уже опубликован!");
                return false;
            }
            if (target.executor.isEmpty()) {
                UI.getMainWindow().getCallbackWindow().FocusRecipients();
                if (!UI.Question("Для отчёта об ошибке не назначен исполнитель.\nВсе равно опубликовать его"))
                    return false;
            }
            if (!Current.getAccount().CheckAccessRights(target.sender_address, Log) && (BugReportInterface.CheckDraft(target, Log))) {
                return false;
            }
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        target.targets = BugReportInterface.getPackedTargets();
        target.change_date = new Date().getTime();
        Global.componentsServer.db.Update(target);
        target.state = BugReportState.active;
    }
    @Override
    protected void performDone() throws Exception {
        //2 - Отправка на сервер, с активным состоянием?
        passes.get(PassCode_2021.SendBugReport).Do();
        Global.componentsServer.db.Update(target);
        //3- рассылка
        EmailMessage message = new EmailMessage("Обнаружена ошибка " + Utils.Brackets(target.id),
                BugReportInterface.getNewMailText(target),
                BugReportInterface.getRecipients(target, true));
        if (!target.project_version.isEmpty()) {
            message.addAttachement(BugReportInterface.getArchiveFile(target));
            //со скринами будет небольшой трабл. потому что теретически возможна ситуация,
            //что проект черновика бага уже закрыт.
            if (Current.HasProject()) {
                for (File screen : Current.getProject().getScreenShots())
                    message.addAttachement(screen);
            }
        }
        passes.get(PassCode_2021.Email).Do(message);
    }
    @Override
    protected void showDone() throws Exception {
        Global.componentsServer.db.bugReports.RefreshUI();
        UI.getMainWindow().getCallbackWindow().ShowCurrentBugReport();
    }
}

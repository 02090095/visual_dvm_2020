package Visual_DVM_2021.Passes.All;
import Common.Database.DataSet;
import TestingSystem.Sapfor.SapforTask.SapforTaskResult;
import Visual_DVM_2021.Passes.Pass_2021;
public class CompareSapforPackages extends Pass_2021 {
    DataSet<String, SapforTaskResult> masterTasks;
    DataSet<String, SapforTaskResult> slaveTasks;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        masterTasks = (DataSet<String, SapforTaskResult>) args[0];
        slaveTasks = (DataSet<String, SapforTaskResult>) args[1];
        return true;
    }
}

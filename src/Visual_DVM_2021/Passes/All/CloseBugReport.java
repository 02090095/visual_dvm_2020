package Visual_DVM_2021.Passes.All;
import Repository.BugReport.BugReportState;
public class CloseBugReport extends UpdateBugReportField {
    @Override
    public String getIconPath() {
        return "/icons/CloseBugReport.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("state", BugReportState.closed, "percentage", 100);
    }
}

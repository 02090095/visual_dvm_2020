package Visual_DVM_2021.Passes.All;
import Common.Current;
import TestingSystem.Tasks.TestRunTask;
import TestingSystem.Test.Test;
public class DownloadTaskTest extends DownloadTest {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = null;
        if (Current.getAccount().CheckRegistered(Log) && Current.Check(Log, Current.TestRunTask)) {
            TestRunTask task = Current.getTestRunTask();
            //-- квазиобъект, нам от него нужно только имя.
            target = new Test();
            target.id = task.test_id;
            return true;
        }
        return false;
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Repository.Subscribes.Subscriber;
import Visual_DVM_2021.Passes.EditObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class EditSubscriber extends EditObjectPass<Subscriber> {
    public EditSubscriber() {
        super(Subscriber.class);
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Subscriber)){
            target = Current.getSubscriber();
            return getTable().ShowEditObjectDialog(target);
        }
        return false;
    }
    @Override
    protected Database getDb() {
        return Global.componentsServer.db;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //отправка.
        passes.get(PassCode_2021.EditSubscriberOnServer).Do(target);
    }
}

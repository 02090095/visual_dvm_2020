package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.Windows.Dialog.Dialog;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Compiler.CompilerType;
import GlobalData.CompilerEnvironment.CompilerEnvironment;
import GlobalData.CompilerEnvironment.UI.CompilerEnvironmentsFields;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.util.Vector;
public class PickCompilerEnvironmentsForTesting extends Pass_2021<String> {
    Compiler compiler = null;
    //-
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (args[0] == null) {
            Log.Writeln_("Отсутствует текущий компилятор. Выбор переменных окружения недоступен.");
            return false;
        }
        target = "";
        compiler = Current.getCompiler();
        if (!compiler.type.equals(CompilerType.dvm)) {
            Log.Writeln_("Выбор переменных окружения возможен только для DVM компилятора,");
            return false;
        }
        if (!(compiler.helpLoaded || passes.get(PassCode_2021.ShowCompilerHelp).Do(compiler, false)))
            return false;
        Dialog<String, CompilerEnvironmentsFields> dialog =
                new Dialog<String, CompilerEnvironmentsFields>(CompilerEnvironmentsFields.class) {
                    @Override
                    public void InitFields() {
                        compiler.environments.mountUI((JPanel) fields.getContent());
                        compiler.environments.ShowUI();
                    }
                    @Override
                    public boolean NeedsScroll() {
                        return false;
                    }
                    @Override
                    public void validateFields() {
                        for (CompilerEnvironment compilerEnv : compiler.environments.Data.values()) {
                            if (compilerEnv.isSelected() && compilerEnv.value.isEmpty())
                                Log.Writeln_("Отмеченная переменная " + compilerEnv.name + " предполагает непустое значение.");
                        }
                    }
                };
        return dialog.ShowDialog("Назначение переменных окружения");
    }
    @Override
    protected void body() throws Exception {
        Vector<String> envs = new Vector<>();
        for (CompilerEnvironment compilerEnv : compiler.environments.Data.values()) {
            if (compilerEnv.isSelected())
                envs.add(compilerEnv.name + "=" + Utils.DQuotes(compilerEnv.value));
        }
        target = String.join(" ", envs);
    }
}

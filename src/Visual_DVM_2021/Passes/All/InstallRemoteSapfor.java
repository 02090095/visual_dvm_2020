package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Machine.MachineType;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.RemoteSapfor.RemoteSapfor;
import GlobalData.SVN.SVN;
import ProjectData.LanguageName;
import Visual_DVM_2021.Passes.SSH.CurrentConnectionPass;
public class InstallRemoteSapfor extends CurrentConnectionPass<RemoteSapfor> {
    boolean result;
    String version_text;
    @Override
    public String getIconPath() {
        return "/icons/DownloadAll.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) {
        result = false;
        version_text = "";
        target = null;
        if (super.canStart(args)) {
            if (Current.getMachine().type.equals(MachineType.Server)) {
                return true;
            } else
                Log.Writeln_("Установка системы SAPFOR разрешена только на одиночном сервере.");
        }
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        RemoteFile repo = new RemoteFile(user.workspace, "repo");
        tryMKDir(repo);
        ShowMessage1("Синхронизация ветви DVM...");
        ShellCommand(
                "cd " + Utils.DQuotes(repo.full_name),
                "svn checkout " + SVN.REPOSITORY_AUTHENTICATION + " " + SVN.DVM_REPOSITORY + "\n"
        );
        ShowMessage1("Синхронизация ветви SAPFOR...");
        ShellCommand(
                "cd " + Utils.DQuotes(repo.full_name),
                "svn checkout " + SVN.REPOSITORY_AUTHENTICATION + " " + SVN.SAPFOR_REPOSITORY + "\n"
        );
        RemoteFile repoSapforHome = new RemoteFile(repo.full_name + "/sapfor/experts/Sapfor_2017/_bin", true);
        ShowMessage1("Сборка SAPFOR...");
        ShellCommand(
                "cd " + Utils.DQuotes(repoSapforHome.full_name),
                "cmake ../",
                "make -j 4"
        );
        result = Exists(repoSapforHome.full_name, "Sapfor_F");
        if (result) {
            //создать папку. Для того чтобы скопировать из репозитория.
            RemoteFile sapforHome = new RemoteFile(user.workspace, Utils.getDateName("sapfor"));
            tryMKDir(sapforHome);
            RemoteFile repoSapforBin = new RemoteFile(repoSapforHome, "Sapfor_F");
            RemoteFile sapforBin = new RemoteFile(sapforHome, "Sapfor_F");
            copy(repoSapforBin, sapforBin);
            target = new RemoteSapfor();
            target.description = "";
            target.home_path = sapforHome.full_name;
            target.call_command = sapforBin.full_name;
            target.languageName = LanguageName.fortran;
            target.machine_id = Current.getMachine().id;
            ParseVersion();
        }
    }
    public void ParseVersion() throws Exception {
        String raw = ShellCommand(target.getVersionCommand());
        String[] data = raw.split(" ");
        if (data.length >= 4)
            target.version = data[3].replace(",", "");
    }
    @Override
    protected boolean validate() {
        return result;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        Global.db.Insert(target);
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        Global.db.remoteSapfors.ShowUI(target.getPK());
    }
}

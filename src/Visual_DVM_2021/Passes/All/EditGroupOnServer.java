package Visual_DVM_2021.Passes.All;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Group.Group;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class EditGroupOnServer extends TestingSystemPass<Group> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (Group) args[0];
        return true;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.EditObject, "", target));
    }
}

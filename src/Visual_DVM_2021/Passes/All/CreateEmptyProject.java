package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.UI.Windows.Dialog.Text.FileNameForm;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.PassException;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
import java.nio.file.Paths;
public class CreateEmptyProject extends Pass_2021<File> {
    String project_name;
    FileNameForm ff = new FileNameForm();
    @Override
    public String getIconPath() {
        return "/icons/CreateProject.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        project_name = "";
        target = null;
        if (ff.ShowDialog("Укажите имя создаваемого проекта", "NewProject")) {
            project_name = ff.Result;
            target = Paths.get(Global.visualiser.getWorkspace().getAbsolutePath(), project_name).toFile();
            if (target.exists())
                Log.Writeln("Файл\n" + Utils.Brackets(target.getAbsolutePath()) + "\nуже существует");
            return Log.isEmpty();
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        File data = new File(target, db_project_info.data);
        if (!(target.mkdir()&&data.mkdir()))
            throw new PassException("Не удалось создать проект.");
    }
    @Override
    protected void performDone() throws Exception {
        passes.get(PassCode_2021.OpenCurrentProject).Do(target);
    }
}

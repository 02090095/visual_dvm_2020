package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Dialog.PercentsForm;
import Common.UI.Windows.Dialog.SliderNumberForm;
import Common.UI.Windows.Dialog.SpinnerNumberForm;
import Common.UI.Windows.Dialog.Text.TextFieldDialog;
import Common.Utils.Files.VDirectoryChooser;
import Common.Utils.Files.VFileChooser;
import Common.Utils.Utils;
import GlobalData.Settings.DBSetting;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.io.File;
public class UpdateSetting extends Pass_2021<DBSetting> {

    String NewValue;
    VDirectoryChooser directoryChooser = new VDirectoryChooser("");
    VFileChooser fileChooser = new VFileChooser("", "exe");
    boolean silent;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = Global.db.settings.get(args[0]);
        NewValue = target.Value;
        silent = false;
        if (args.length == 1) {
            //интерфейсный режим. получение по клику на пункт меню.
            switch (target.settingType) {
                case SapforFlag:
                    NewValue = target.toBoolean() ? "0" : "1";
                    break;
                case PercentField:
                    PercentsForm f = new PercentsForm();
                    if (f.ShowDialog(target.Name.getDescription(), target.toInt32()))
                        NewValue = String.valueOf(f.Result);
                    break;
                case StringField:
                    switch (target.Name) {
                        case Workspace:
                            directoryChooser.setTitle(target.Name.getDescription());
                            directoryChooser.SetCurrentDirectory(
                                    target.Value.isEmpty() ? Global.ProjectsDirectory : new File(target.Value));
                            File dir = directoryChooser.ShowDialog();
                            if (dir != null)
                                NewValue = dir.getAbsolutePath();
                            break;
                        case LocalMakePathWindows:
                            fileChooser.setTitle(target.Name.getDescription());
                            File file = fileChooser.ShowDialog();
                            if (file != null)
                                NewValue = file.getAbsolutePath();
                            break;
                        default:
                            TextFieldDialog ff = new TextFieldDialog();
                            if (ff.ShowDialog(target.Name.getDescription(), target.Value)) {
                                NewValue = ff.Result;
                                if (NewValue.length() == 0)
                                    NewValue = " ";
                            }
                            break;
                    }
                    break;
                case IntField:
                    int min = 0;
                    int max = 0;
                    switch (target.Name) {
                        case LastOpenedProjectsCount:
                            SpinnerNumberForm f_ = new SpinnerNumberForm() {
                                @Override
                                public void InitFields() {
                                    fields.setModel(new SpinnerNumberModel(
                                            target.toInt32(),
                                            1,
                                            50,
                                            1));
                                }
                            };
                            if (f_.ShowDialog(target.Name.getDescription()))
                                NewValue = String.valueOf(f_.Result);
                            break;
                        case FastAccessPassesCount:
                            min = 5;
                            max = 15;
                            SliderNumberForm fff = new SliderNumberForm();
                            if (fff.ShowDialog(target.Name.getDescription(), target.toInt32(), min, max))
                                NewValue = String.valueOf(fff.Result);
                            break;
                        case Kernels:
                            min = 1;
                            max = Utils.getMaxKernels();
                            SliderNumberForm ffff = new SliderNumberForm();
                            if (ffff.ShowDialog(target.Name.getDescription(), target.toInt32(), min, max))
                                NewValue = String.valueOf(ffff.Result);
                            break;
                    }

                    break;
            }
        } else {
            NewValue = args[1].toString();
            silent = true;
        }
        //программный, тихий режим.}
        //--
        System.out.println(target.Value + "/" + NewValue);
        return !NewValue.equals(target.Value);
    }
    @Override
    protected void body() throws Exception {
        target.Value = NewValue;
        Global.db.Update(target);
    }
    @Override
    protected void showDone() throws Exception {
        if (target.Visible) {
            target.Mark();
            switch (target.Name) {
                case GCOVLimit:
                    if (Current.HasFile())
                        Current.getFile().form.ShowGCOV();
                    break;
                case FastAccessPassesCount:
                    if (Current.HasProject())
                        UI.fastAccessMenuBar.Refresh();
                    break;
                case DarkThemeOn:
                    UI.refreshTheme();
                    break;
                case ShowFullArraysDeclarations:
                    if (Current.HasProject())
                        Current.getProject().declaratedArrays.ShowUI();
                    break;
                case ShowFullTabsNames:
                    UI.getMainWindow().getTestingWindow().RefreshTabsNames();
                    if (Current.HasProject())
                        UI.getMainWindow().getProjectWindow().RefreshTabsNames();
                    if (Current.HasFile())
                        Current.getFile().form.RefreshTabsNames();
                    break;
                case MPI_PROGRAM:
                    Pass_2021.passes.get(PassCode_2021.CleanAnalyses).Do();
                    break;
                case SmallScreen:
                    boolean small = target.toBoolean();
                    if (Current.HasProject())
                        UI.getMainWindow().getProjectWindow().SwitchScreen(small);
                    UI.getMainWindow().getCallbackWindow().SwitchScreen(small);
                    break;
                case Precompilation:
                    Current.getSapfor().ResetAllAnalyses();
                    break;
            }
        }
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Global;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SilentSapforPass;
public class SPF_GetMaxMinBlockDistribution extends SilentSapforPass {
    @Override
    public boolean needsConfirmations() {
        return false;
    }
    @Override
    protected boolean needsAnimation() {
        return false;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)){
            SPF_ParseFilesWithOrder.silent = true;
            return (passes.get(PassCode_2021.SPF_ParseFilesWithOrder).isDone() || passes.get(PassCode_2021.SPF_ParseFilesWithOrder).Do());
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        sapfor.RunAnalysis(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.packSapforSettings(),
                target.getProjFile().getAbsolutePath());
    }

    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        SPF_ParseFilesWithOrder.silent = false;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        target.testMaxDim = 0;
        String packed = sapfor.getResult();
        String [] data = packed.split(" ");
        if (data.length>0)
            target.testMaxDim = Integer.parseInt(data[data.length-1]);
    }
}

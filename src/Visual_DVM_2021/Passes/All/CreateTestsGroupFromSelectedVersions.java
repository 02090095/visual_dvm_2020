package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestType;
import TestingSystem.TestingServer;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;
public class CreateTestsGroupFromSelectedVersions extends Pass_2021<Vector<db_project_info>> {
    Group group;
    Test test;
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    public String getIconPath() {
        return "/icons/DownloadAll.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (!Current.getAccount().CheckRegistered(Log))
            return false;
        if (!Global.versions_multiselection) {
            Log.Writeln_("Нажмите правую клавишу мыши, и перейдите в режим выбора версий.");
            return false;
        }
        target = new Vector<>();
        group = null;
        test = null;
        Current.getRoot().getSelectedVersions(target);
        if (target.size() == 0) {
            Log.Writeln_("Не отмечено ни одной версии.");
            return false;
        }
        if (Current.getProject().hasSubdirectories()) {
            Log.Writeln_("Запрещено добавлять тестовые проекты, содержащие подпапки!");
            return false;
        }
        String cp_info = "";
        if (Current.HasProject()) {
            for (db_project_info version : target) {
                if (version.Home.getAbsolutePath().equals(Current.getProject().Home.getAbsolutePath())) {
                    cp_info = "Текущий проект будет закрыт.";
                    break;
                }
            }
        }
        return UI.Question("Будет сформирована группа из " + target.size() + " тестов.\n" + cp_info +
                "\nПродолжить");
    }
    @Override
    protected void performPreparation() throws Exception {
        if (TestingServer.checkTasks)
            TestingServer.TimerOff();
            passes.get(PassCode_2021.CloseCurrentProject).Do();
    }
    @Override
    protected void showPreparation() throws Exception {
        UI.getMainWindow().getTestingWindow().ShowAutoActualizeTestsState();
    }
    @Override
    protected void body() throws Exception {
        PublishGroup publishGroup = new PublishGroup() {
            @Override
            public String getDescription() {
                return "Добавление группы";
            }
            @Override
            protected boolean fillObjectFields() throws Exception {
                //тут имя надо генерить, потому что может совпасть.
                target.description = Utils.getDateName(Current.getRoot().name);
                target.type = TestType.Default;
                return true;
            }
            @Override
            protected boolean needsAnimation() {
                return false;
            }
        };
        ShowMessage1("Публикация группы ");
        if (publishGroup.Do()) {
            group = publishGroup.target;
            for (db_project_info vizTestProject : target) {
                //на случай если версия в текущем сеансе еще не открывалась.
                vizTestProject.Open();
                vizTestProject.Close();
                ShowMessage1("Публикация теста " + vizTestProject.getTitle());
                PublishTest pass = new PublishTest() {
                    @Override
                    protected boolean setProject() {
                        project = vizTestProject;
                        switch (project.languageName) {
                            case fortran:
                                project.testMaxDim = Current.getSapfor().getTextMaxDim(null, project);
                                break;
                            default:
                                project.testMaxDim = 0;
                                break;
                        }
                        return true;
                    }
                    @Override
                    protected boolean needsAnimation() {
                        return false;
                    }
                    @Override
                    protected boolean fillObjectFields() throws Exception {
                        return true;
                    }
                    @Override
                    public String getDescription() {
                        return "Добавление версии " + vizTestProject.name;
                    }
                    @Override
                    protected void performFinish() throws Exception {
                        disconnect();
                    }
                    @Override
                    protected void showDone() throws Exception {
                    }
                };
                if (pass.Do())
                    test = pass.target;
            }
            if (passes.get(PassCode_2021.SynchronizeTests).Do()) {
                if (group != null) Global.testingServer.db.groups.ui_.Show(group.getPK());
                if (test != null) Global.testingServer.db.tests.ui_.Show(test.getPK());
            }
        }
    }
    @Override
    protected void FocusResult() {
        UI.getMainWindow().FocusTesting();
        UI.getMainWindow().getTestingWindow().FocusTestingSystem();
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import TestingSystem.Test.Test;
import Visual_DVM_2021.Passes.EditObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class EditTest extends EditObjectPass<Test> {
    public EditTest() {
        super(Test.class);
    }
    @Override
    protected Database getDb() {
        return Global.testingServer.db;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Test)){
            target = Current.getTest();
            if (!Current.getAccount().CheckAccessRights(target.sender_address, Log)){
                return false;
            }
            return getTable().ShowEditObjectDialog(target);
        }
        return false;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //отправка.
        passes.get(PassCode_2021.EditTestOnServer).Do(target);
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
import org.apache.commons.io.FileUtils;

import java.io.File;
public class SynchronizeTests extends TestingSystemPass<Object> {
    File new_db_file;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return Current.getAccount().CheckRegistered(Log);
    }
    @Override
    public String getIconPath() {
        return "/icons/ComponentsActual.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected void showPreparation() throws Exception {
        server.db.SaveLastSelections();
        server.db.configurations.ClearUI();
        server.db.groups.ClearUI();
        server.db.sapforConfigurations.ClearUI();
      //  server.db.sapforTasksPackages.ClearUI();
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.ReceiveTestsDatabase));
        response.Unpack(new_db_file = Utils.getTempFileName("tdb"));
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        server.db.Disconnect();
        Utils.forceDeleteWithCheck(server.db.getFile());
        FileUtils.moveFile(new_db_file, server.db.getFile());
        server.db.Connect();
        server.db.prepareTablesStatements();
        server.db.Synchronize();
        //убрать пересоздание таблиц. ибо сносится связь с интерфейсом окна.
    }
    @Override
    protected void showDone() throws Exception {
        if (UI.HasNewMainWindow()) {
            server.db.configurations.ShowUI();
            server.db.groups.ShowUI();
            server.db.sapforConfigurations.ShowUI();
            server.db.RestoreLastSelections();
            UI.getMainWindow().getTestingWindow().ShowCheckedTestsCount();
            //для отображения числа ядер.
            if (Current.HasMachine())
                UI.getMainWindow().getTestingWindow().ShowCurrentMachine();
            else
                UI.getMainWindow().getTestingWindow().ShowNoCurrentMachine();

        }
    }
}

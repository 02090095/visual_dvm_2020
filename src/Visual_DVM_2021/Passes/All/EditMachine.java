package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Machine.Machine;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditMachine extends EditObjectPass<Machine> {
    public EditMachine() {
        super(Machine.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

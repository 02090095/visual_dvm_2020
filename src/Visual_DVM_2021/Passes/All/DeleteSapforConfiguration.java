package Visual_DVM_2021.Passes.All;
import Common.Current;
import TestingSystem.Sapfor.SapforConfiguration.SapforConfiguration;
import Visual_DVM_2021.Passes.DeleteSelectedServerObjects;

import java.util.Vector;
public class DeleteSapforConfiguration extends DeleteSelectedServerObjects {
    public DeleteSapforConfiguration() {
        super(SapforConfiguration.class);
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.SapforConfiguration)){
            target= new Vector<>();
            target.add(Current.getSapforConfiguration());
            return true;
        }
        return false;
    }
}

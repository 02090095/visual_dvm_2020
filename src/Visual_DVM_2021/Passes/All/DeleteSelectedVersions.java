package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;
public class DeleteSelectedVersions extends Pass_2021<Vector<db_project_info>> {
    boolean has_current_project;
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    public String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        has_current_project = false;
        target = new Vector<>();
        //------------------------
        if (!Global.versions_multiselection) {
            Log.Writeln_("Нажмите правую клавишу мыши, и перейдите в режим выбора версий.");
            return false;
        }
        if (!Current.Check(Log, Current.Root)) {
            return false;
        }
        Vector<db_project_info> allVersions = new Vector<>();
        Current.getRoot().getSelectedVersions(allVersions);
        if (allVersions.size() == 0) {
            Log.Writeln_("Не отмечено ни одной версии.");
            return false;
        }
        int q = Current.getRoot().getSelectedVersionsForDeletion(target);
        if (Current.HasProject()) {
            for (db_project_info version : target) {
                if (Current.getProject().Home.getAbsolutePath().startsWith(version.Home.getAbsolutePath())) {
                    has_current_project = true;
                    break;
                }
            }
        }
        return UI.Warning("Удалить " + q + " версий.");
    }
    @Override
    protected void body() throws Exception {
        for (db_project_info version : target)
            passes.get(PassCode_2021.DeleteVersion).Do(version);
    }
}

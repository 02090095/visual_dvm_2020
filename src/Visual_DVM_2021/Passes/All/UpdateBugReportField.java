package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportInterface;
import Repository.BugReport.BugReportState;
import Repository.EmailMessage;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
import Visual_DVM_2021.Passes.PassCode_2021;
import javafx.util.Pair;

import java.io.Serializable;
import java.util.Date;
import java.util.Vector;
public class UpdateBugReportField extends ComponentsRepositoryPass<BugReport> {
    Vector<String> fieldNames = new Vector<>();
    Vector<Serializable> fieldValues = new Vector<>();
    String old_description = "";
    String old_comment = "";
    protected boolean canUpdate() {
        return Current.getAccount().CheckAccessRights(target.sender_address, Log);
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (!Current.Check(Log, Current.BugReport))
            return false;
        old_description = "";
        old_comment = "";
        //--
        target = Current.getBugReport();
        fieldNames.clear();
        fieldValues.clear();
        for (int i = 0; i < args.length; ++i) {
            if (i % 2 == 0) {
                fieldNames.add((String) args[i]);
            } else {
                fieldValues.add((Serializable) args[i]);
            }
        }
        if (fieldNames.size() != fieldValues.size()) {
            Log.Writeln("Неверный формат аргументов");
            return false;
        }
        if (target.state.equals(BugReportState.draft)) {
            //если черновик, сразу меняем и не заморачиваемся, если нужно.
            //тут проход все равно сугубо интерфейсный.
            for (int i = 0; i < fieldNames.size(); ++i)
                BugReport.class.getField(fieldNames.get(i)).set(target, fieldValues.get(i));
            target.change_date = new Date().getTime();
            server.db.Update(target);
            server.db.bugReports.RefreshUI();
            UI.getMainWindow().getCallbackWindow().ShowCurrentBugReport();
        } else
            return canUpdate();
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        //1. прежде чем дополнять поле(комментарий или описание) следует скачать с сервера
        //последнюю версию этого баг репорта.
        //все это должно быть в рамках одной транзакции с сервером!
        Command(new ServerExchangeUnit_2021(ServerCode.GetObjectCopyByPK, "", new Pair<>(BugReport.class, target.id)));
        target.SynchronizeFields((BugReport) response.object);
        for (int i = 0; i < fieldNames.size(); ++i) {
            String fieldName = fieldNames.get(i);
            switch (fieldName) {
                case "comment":
                    old_comment = (String) BugReport.class.getField(fieldName).get(target);
                    break;
                case "description":
                    old_description = (String) BugReport.class.getField(fieldName).get(target);
                    break;
            }
            BugReport.class.getField(fieldNames.get(i)).set(target, fieldValues.get(i));
        }
        target.change_date = new Date().getTime();
        //3. отправляем на сервер
        Command(new ServerExchangeUnit_2021(ServerCode.UpdateBugReport, "", target));
        server.db.Update(target);
    }
    @Override
    protected void showFinish() throws Exception {
        Global.componentsServer.db.bugReports.RefreshUI();
        UI.getMainWindow().getCallbackWindow().ShowCurrentBugReport();
    }
    @Override
    protected void performDone() throws Exception {
        String message_header = BugReportInterface.getMailTitlePrefix(target);

        /*
        String message_text = "DUMMY TEXT: ";
        Random random = new Random();
        for (int k = 0; k < random.nextInt(200); ++k)
            message_text += random.nextDouble();

         */
        String message_text = String.join("\n",
                target.description,
                target.comment);
        if (fieldNames.size() >= 1) {
            switch (fieldNames.get(0)) {
                case "sender_address":
                case "sender_name":
                case "targets":
                case "percentage":
                    return;
                case "description":
                    message_header += "описание изменено";
                    message_text = Utils.compareTexts(old_description, target.description);
                    break;
                case "comment":
                    message_header += "комментарий изменён";
                    message_text = Utils.compareTexts(old_comment, target.comment);
                    break;
                case "executor":
                    message_header += target.executor + " назначается исполнителем";
                    break;
                case "state":
                    message_header += "состояние изменилось на " + Utils.Brackets(target.state.getDescription());
                    break;
            }
            passes.get(PassCode_2021.Email).Do(
                    new EmailMessage(message_header,
                            message_text,
                            BugReportInterface.getRecipients(target, true))
            );
        }
    }
}

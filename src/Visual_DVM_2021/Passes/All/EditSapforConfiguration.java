package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import TestingSystem.Sapfor.SapforConfiguration.SapforConfiguration;
import Visual_DVM_2021.Passes.EditObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class EditSapforConfiguration extends EditObjectPass<SapforConfiguration> {
    public EditSapforConfiguration() {
        super(SapforConfiguration.class);
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.SapforConfiguration)) {
            return getTable().ShowEditObjectDialog(target = Current.getSapforConfiguration());
        }
        return false;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //отправка.
        passes.get(PassCode_2021.EditSapforConfigurationOnServer).Do(target);
    }
    @Override
    protected Database getDb() {
        return Global.testingServer.db;
    }
}

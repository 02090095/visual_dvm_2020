package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Text.ComboTextDialog;
import Common.Utils.Utils;
import ProjectData.SapforData.Functions.FuncInfo;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Comparator;
import java.util.Vector;
public class ApplyCurrentFunction extends Pass_2021 {
    @Override
    public String getIconPath() {
        return "/icons/Apply.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (passes.get(PassCode_2021.SPF_GetGraphFunctions).isDone()) {
            ComboTextDialog ff = new ComboTextDialog();
            Vector<String> names = new Vector<>(Current.getProject().allFunctions.keySet());
            names.sort(Comparator.naturalOrder());
            if (ff.ShowDialog("Выберите имя текущей процедуры", names)) {
                String func_name = ff.Result;
                if (Current.getProject().allFunctions.containsKey(func_name)) {
                    FuncInfo fi = Current.getProject().allFunctions.get(func_name);
                    Current.set(Current.Function, fi);
                    return true;
                } else {
                    Log.Writeln_("Проект не содержит процедуры с именем " + Utils.Brackets(func_name));
                }
            }
        }
        return false;
    }
    @Override
    protected void showDone() throws Exception {
        UI.getMainWindow().getProjectWindow().getFunctionsWindow().ShowCurrentFunction();
        if (SPF_GetGraphFunctionPositions.showByCurrentFunction) {
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        }
    }
}

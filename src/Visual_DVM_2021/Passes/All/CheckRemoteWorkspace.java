package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Repository.SubscriberWorkspace.SubscriberWorkspace;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
public class CheckRemoteWorkspace extends ComponentsRepositoryPass<SubscriberWorkspace> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target=null;
        return Current.getAccount().CheckRegistered(Log);
    }
    @Override
    protected void ServerAction() throws Exception {
        String email = Current.getAccount().email;
        String machineURL = Current.getMachine().getURL();
        String login = Current.getUser().login;
        Command(new ServerExchangeUnit_2021(ServerCode.CheckURLRegistered,
                email+"\n"+machineURL+"\n"+login));
        target = (SubscriberWorkspace) response.object;
    }
}

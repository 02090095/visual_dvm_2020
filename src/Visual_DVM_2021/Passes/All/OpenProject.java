package Visual_DVM_2021.Passes.All;
import Common.Current;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class OpenProject extends Pass_2021<db_project_info> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (db_project_info) args[0];
        return true;
    }
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseProject).Do();
    }
    @Override
    protected void body() throws Exception {
        target.CreateVersionsTree();
        target.Open();
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.Project, target);
        /*
        пока непонятно надо
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                SettingName.FREE_FORM, target.style.equals(LanguageStyle.free) ? "1" : "0");
         */
    }
}
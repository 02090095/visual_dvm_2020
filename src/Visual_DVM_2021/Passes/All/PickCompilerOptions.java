package Visual_DVM_2021.Passes.All;
import Common.UI.Windows.Dialog.Dialog;
import GlobalData.Compiler.Compiler;
import GlobalData.Compiler.CompilerType;
import GlobalData.CompilerOption.CompilerOption;
import GlobalData.CompilerOption.UI.CompilerOptionsFields;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.util.Vector;
public class PickCompilerOptions extends Pass_2021<String> {
    Compiler compiler = null;
    //-
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (args[0] == null) {
            Log.Writeln_("Отсутствует текущий компилятор. Выбор флагов недоступен.");
            return false;
        }
        compiler = (Compiler) args[0];
        if (!compiler.type.equals(CompilerType.dvm)) {
            Log.Writeln_("Выбор опций возможен только для DVM компилятора,");
            return false;
        }
        if (!(compiler.helpLoaded || passes.get(PassCode_2021.ShowCompilerHelp).Do(compiler, false)))
            return false;
        Dialog<String, CompilerOptionsFields> dialog = new Dialog<String, CompilerOptionsFields>(CompilerOptionsFields.class) {
            @Override
            public void InitFields() {
                compiler.options.mountUI((JPanel) fields.getContent());
                compiler.options.ShowUI();
            }
            @Override
            public boolean NeedsScroll() {
                return false;
            }
            @Override
            public void validateFields() {
                for (CompilerOption option : compiler.options.Data.values()) {
                    if (option.isSelected())
                        if (option.hasParameter() && (option.parameterValue.isEmpty()))
                            Log.Writeln_("Отмеченная опция " + option.name + " предполагает непустое значение.");
                }
            }
        };
        return dialog.ShowDialog("Назначение опций компилятора");
    }
    @Override
    protected void body() throws Exception {
        Vector<String> res = new Vector<>();
        for (CompilerOption option : compiler.options.Data.values()) {
            if (option.isSelected())
                res.add(option.toString());
        }
        target = String.join(" ", res);
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import GlobalData.Makefile.UI.MakefilePreviewForm;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class ShowMakefilePreview extends Pass_2021<db_project_info> {
    @Override
    public String getIconPath() {
        return "/icons/ShowPassword.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }
    @Override
    protected boolean canStart(Object... args) {
        if (Current.Check(Log, Current.Project)) {
            target = Current.getProject();
            return Current.Check(Log, Current.Makefile);
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        new MakefilePreviewForm().ShowDialog("Предпросмотр мейкфайла для текущего проекта",
                Current.getMakefile().Generate(target));
    }
}

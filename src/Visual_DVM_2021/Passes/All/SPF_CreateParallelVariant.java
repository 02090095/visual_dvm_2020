package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import ProjectData.SapforData.Variants.ParallelVariant;
import Repository.Component.Sapfor.TransformationPermission;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforTransformation;
public class SPF_CreateParallelVariant extends SapforTransformation {
    ParallelVariant variant = null;
    @Override
    protected TransformationPermission getPermission() {
        return TransformationPermission.VariantsOnly;
    }
    @Override
    protected boolean hasStats() {
        return false;
    }
    @Override
    public boolean needsConfirmations() {
        return false;
    }
    @Override
    protected boolean needsAnimation() {
        return false;
    }
    @Override
    protected boolean resetAnalysesAfter() {
        return false;
    }
    @Override
    protected String getVersionLetter() {
        return "p";
    }
    @Override
    protected String getVersionDescription() {
        return variant.getDescription();
    }
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetArrayDistribution;
    }
    @Override
    protected void performPreparation() throws Exception {
        target.createEmptyVersion(getVersionLetter(), getVersionDescription());
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            if (args.length > 0) {
                variant = (ParallelVariant) args[0];
            } else {
                target.filter_distributed = 1;
                target.filter_multiplied = 1;
                target.gen_variants_vectors();
                if (target.parallelVariants.size() == 1) {
                    variant = target.parallelVariants.getFirstRecord();
                } else return false;
            }
            Options = variant.getVector();
            return true;
        }
        return false;
    }
    @Override
    protected void performDone() throws Exception {
        Global.transformationPermission = TransformationPermission.VariantsOnly;
        target.migrateFilesSettings(target.last_version, true, false, true);
        if (Current.hasUI())
            variant.UpdateStats(sapfor.getPredictorStats(), target.last_version.name);
    }
    @Override
    protected void performFinish() throws Exception {
        //в пакетном режиме вариант всегда один. поэтому, сообщения надо распаковать сразу.
        if (Current.mode.equals(Current.Mode.Package))
            super.performFinish();
    }
    @Override
    protected void showFinish() throws Exception {
    }
    @Override
    protected void FocusResult() {
    }
}

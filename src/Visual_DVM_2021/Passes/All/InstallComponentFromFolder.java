package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.GlobalProperties;
import Common.Utils.Files.VFileChooser;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.CurrentComponentPass;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
public class InstallComponentFromFolder extends CurrentComponentPass {
    File file;
    @Override
    public String getIconPath() {
        return "/icons/LastOpened.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)){
            VFileChooser fileChooser = target.getFileChooser();
            String propertyName = target.getComponentType()+"Path";
            System.out.println(propertyName);
            String lastDirectory = (String) GlobalProperties.class.getField(propertyName).get(Global.properties);
            System.out.println("last directory = "+ Utils.Brackets(lastDirectory));
            if (!lastDirectory.isEmpty()){
                //настройка выставлена не впервые, устанавливаем ее как папку
                fileChooser.SetCurrentDirectory(lastDirectory);
            }
            boolean res = (file = fileChooser.ShowDialog()) != null;
            File newDirectory = fileChooser.getCurrentDirectory();
            System.out.println("new directory = "+newDirectory.getAbsolutePath());
            GlobalProperties.class.getField(propertyName).set(Global.properties, newDirectory.getAbsolutePath());
            Global.properties.Update();
            return res;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        Files.copy(file.toPath(), target.getNewFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        target.Update();
    }
    @Override
    protected void performDone() throws Exception {
        target.InitialVersionCheck();
        if (target.CanBeUpdated())
            target.CheckIfNeedsUpdateOrPublish();
    }
    @Override
    protected void showDone() throws Exception {
        Global.RefreshUpdatesStatus();
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.Windows.Dialog.Text.ReadOnlyMultilineTextForm;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.MachineType;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class ShowCompilerHelp extends Pass_2021<String> {
    Pass_2021<String> subpass;
    Compiler compiler;
    boolean needsShow = true;
    //-
    @Override
    public String getIconPath() {
        return "/icons/Help.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = null;
        if (args.length == 0) {
            if (Current.Check(Log, Current.Compiler)) {
                compiler = Current.getCompiler();
                needsShow = true;
                return true;
            }
        } else {
            compiler = (Compiler) args[0];
            needsShow = (boolean) args[1];
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        subpass = null;
        compiler.ResetHelp();
        if (Current.getMachine().type.equals(MachineType.Local)) {
            subpass = passes.get(PassCode_2021.LocalSingleCommand);
        } else {
            subpass = passes.get(PassCode_2021.RemoteSingleCommand);
        }
        if (subpass != null) {
            if (compiler.helpLoaded = subpass.Do(compiler.getHelpCommand())) {
                target = subpass.target;
                compiler.helpText = target;
                compiler.ParseHelp();
            }
        }
    }
    @Override
    protected boolean validate() {
        return (subpass != null) && (target != null);
    }
    @Override
    protected void showDone() throws Exception {
        if (needsShow) {
            ReadOnlyMultilineTextForm ff = new ReadOnlyMultilineTextForm();
            ff.ShowDialog("Справка", target);
        }
    }
}

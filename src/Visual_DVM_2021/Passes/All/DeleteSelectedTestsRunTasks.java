package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Tasks.TestRunTask;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;

import java.util.Vector;
public class DeleteSelectedTestsRunTasks extends TestingSystemPass<Vector<TestRunTask>> {
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    public String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Vector<>();
        //------------------------
        if (Current.getAccount().CheckRegistered(Log)) {
            for (TestRunTask task : Global.testingServer.account_db.testRunTasks.getCheckedItems()) {
                if (!task.state.isActive())
                    target.add(task);
            }
            if (target.isEmpty()) {
                Log.Writeln_("не отмечено неактивных задач.");
                return false;
            }
            return true;
        }
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DeleteAccountObjects, Current.getAccount().email, target));
    }
    @Override
    protected void performDone() throws Exception {
        passes.get(PassCode_2021.SynchronizeTestsTasks).Do();
    }
}

package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Common.Utils.Index;
import ProjectData.SapforData.Regions.ParallelRegion;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforAnalysis;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
public class SPF_GetArrayDistributionOnlyAnalysis extends SapforAnalysis {
    @Override
    protected Vector<PassCode_2021> getForbiddenPhases() {
        return new Vector<>(Collections.singletonList(PassCode_2021.SPF_GetArrayDistributionOnlyRegions));
    }
    @Override
    public String phase() {
        return "CREATE_DISTR_DIRS";
    }
    @Override
    protected boolean isAtomic() {
        return false;
    }
    @Override
    protected void unpack(String packed) throws Exception {
        String[] splited = packed.split("#");
        Index idx = new Index();
        int n = Integer.parseInt(splited[idx.Inc()]);
        for (int i = 0; i < n; ++i) {
            ParallelRegion p = new ParallelRegion(splited, idx);
            target.parallelRegions.put(p.regionId, p);
        }
        //отсортировать по имени шаблоны
        target.templates.sort(Comparator.comparing(o -> o.name));
        target.GenRules();
    }
    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation(); //удаление интеррупта.
        //как то вывести эти методы в проект. чтобы и при очистке юзать.
        //иначе анархия.
        target.parallelRegions.clear();
        target.templates.clear();
        target.parallelVariants.clear();
    }
    @Override
    protected void showPreparation() {
        UI.getVersionsWindow().getVariantsWindow().ShowNoProjectDistribution();
        UI.getVersionsWindow().getVariantsWindow().ShowNoVariantsFilter();
        UI.getVersionsWindow().getVariantsWindow().ShowNoVariants();
        UI.getMainWindow().getProjectWindow().getAnalysisWindow().ShowNoRegions();
        UI.getMainWindow().getProjectWindow().getAnalysisWindow().ShowNoProjectMaxDim();
    }
    @Override
    protected void showDone() throws Exception {
        UI.getVersionsWindow().getVariantsWindow().ShowProjectDistribution();
        UI.getVersionsWindow().getVariantsWindow().ShowVariantsFilter();
        UI.getVersionsWindow().getVariantsWindow().ShowTotalVariantsCount();
        UI.getVersionsWindow().getVariantsWindow().ShowFilteredVariantsCount();
        UI.getVersionsWindow().getVariantsWindow().ShowCheckedVariantsCount();
        UI.getMainWindow().getProjectWindow().getAnalysisWindow().ShowRegions();
        UI.getMainWindow().getProjectWindow().getAnalysisWindow().ShowProjectMaxDim();
        super.showDone();
    }
    @Override
    protected void FocusResult() {
        UI.getMainWindow().FocusProject();
        UI.getMainWindow().getProjectWindow().FocusAnalysis();
    }
}
package Visual_DVM_2021.Passes.All;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.Supervisor.Remote.MVSRunSupervisor;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.SSH.TaskConnectionPass;
public class MVSRun extends TaskConnectionPass {
    public MVSRun() {
        super(MVSRunSupervisor.class);
    }
    @Override
    protected boolean canStart(Object... args) {
        supervisor.Init((RunTask) args[0], this, (db_project_info) args[1]);
        return true;
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.TasksPackage.TasksPackage;
import TestingSystem.TasksPackageToKill.TasksPackageToKill;
import Visual_DVM_2021.Passes.TestingSystemPass;

import java.util.Vector;
public class AbortSelectedPackages extends TestingSystemPass<Vector<TasksPackage>> {
    Vector<TasksPackageToKill> packagesToKill;
    @Override
    public String getIconPath() {
        return "/icons/Ban.PNG";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = server.account_db.packages.getCheckedItems();
        packagesToKill = new Vector<>();
        for (TasksPackage tasksPackage : target) {
            switch (tasksPackage.state) {
                case Done:
                case Aborted:
                    break;
                default:
                    TasksPackageToKill tasksPackageToKill = new TasksPackageToKill();
                    tasksPackageToKill.packageName = tasksPackage.id;
                    packagesToKill.add(tasksPackageToKill);
                    break;
            }
        }
        if (packagesToKill.isEmpty()) {
            Log.Writeln_("Не отмечено ни одного активного пакета для удаления");
        }
        return true;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.PublishAccountObjects, Current.getAccount().email, packagesToKill));
    }
}

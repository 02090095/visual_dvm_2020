package Visual_DVM_2021.Passes.All;
import Visual_DVM_2021.Passes.CurrentComponentPass;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
public class CreateComponentBackUp extends CurrentComponentPass {
    @Override
    protected void body() throws Exception {
        //form.ShowMessage2("копирование предыдущей версии...");
        if (target.getFile().exists())
            Files.copy(target.getFile().toPath(), target.getBackUpFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
    }
}

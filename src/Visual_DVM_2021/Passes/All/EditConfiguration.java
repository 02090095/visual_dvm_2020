package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import TestingSystem.Configuration.Configuration;
import Visual_DVM_2021.Passes.EditObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class EditConfiguration extends EditObjectPass<Configuration> {
    public EditConfiguration() {
        super(Configuration.class);
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Configuration)) {
            return getTable().ShowEditObjectDialog(target= Current.getConfiguration());
        }
        return false;
    }
    @Override
    protected Database getDb() {
        return Global.testingServer.db;
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //отправка.
        passes.get(PassCode_2021.EditConfigurationOnServer).Do(target);
    }
}

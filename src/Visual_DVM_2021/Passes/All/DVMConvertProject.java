package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.Menus_2023.PassMenuItem;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.Files.DBProjectFile;
import ProjectData.Project.db_project_info;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
import Visual_DVM_2021.Passes.PassCode_2021;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Vector;
public class DVMConvertProject extends ComponentsRepositoryPass<db_project_info> {
    File workspace = null;
    File archive = null;
    Vector<DBProjectFile> programsToConvert = null;
    Vector<String> programsNames = null;
    String output = null;
    File versionArchive = null;
    File version = null;
    String badFiles = "";
    @Override
    protected boolean canStart(Object... args) throws Exception {
        programsToConvert = null;
        programsNames = null;
        output = "";
        workspace = null;
        archive = null;
        versionArchive = null;
        version = null;
        badFiles = "";
        //--
        if (Current.Check(Log, Current.Project)) {
            target = Current.getProject();
            programsToConvert = target.getPrograms().get(target.languageName);
            programsNames = new Vector<>();
            if (programsToConvert.size() > 100) {
                Log.Writeln_("Количество активных файлов кода на языке " + target.languageName + " в проекте превышает 100.");
                return false;
            }
            for (DBProjectFile program : programsToConvert) {
                if (Utils.getFileSizeMegaBytes(program.file) > 1) {
                    Log.Writeln_("Размер файла кода " + Utils.Brackets(program.name) + " превышает 1 Мб.");
                    return false;
                } else programsNames.add(Utils.toU(program.name));
            }
            workspace = Utils.getTempFileName("convertation");
            FileUtils.forceMkdir(workspace);
            File cleanCopy = Paths.get(workspace.getAbsolutePath(), target.name).toFile();
            FileUtils.forceMkdir(cleanCopy);
            archive = Paths.get(workspace.getAbsolutePath(), target.name + ".zip").toFile();
            target.Clone(cleanCopy, false);
            if (!passes.get(PassCode_2021.ZipFolderPass).Do(cleanCopy.getAbsolutePath(), archive.getAbsolutePath())) {
                Log.Writeln_("Не удалось создать архив проекта!");
                return false;
            }
            ;
            if (Utils.getFileSizeMegaBytes(archive) > 500) {
                Log.Writeln_("Размер архива проекта превышает 500 МБ");
                return false;
            }
            return true;
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation();
        db_project_info.ResetNewVersions();
    }
    //-
    @Override
    public String getIconPath() {
        return "/icons/Transformations/" + code().toString() + ".png";
    }
    @Override
    protected boolean hasStats() {
        return true;
    }
    @Override
    protected int getTimeout() {
        return 0;
    }
    @Override
    protected void ServerAction() throws Exception {
        ServerExchangeUnit_2021 unit = new ServerExchangeUnit_2021(ServerCode.DVMConvertProject);
        unit.object = Utils.packFile(archive);
        Vector<String> unit_args = new Vector<>();
        unit_args.add(target.name);
        unit_args.add(target.languageName.toString());
        unit_args.add(Global.db.settings.get(SettingName.DVMConvertationOptions).toString());
        unit_args.addAll(programsNames);
        unit.arg = String.join("\n", unit_args);
        Command(unit);
        output = response.arg;
        versionArchive = new File(workspace, target.name + "_result.zip");
        response.Unpack(versionArchive);
        File result = new File(workspace, "result");
        if (passes.get(PassCode_2021.UnzipFolderPass).Do(versionArchive.getAbsolutePath(), result.getAbsolutePath())) {
            if (target.last_modification == null) {
                target.last_modification = new db_project_info(target,
                        "m",
                        "копия от " + new Date(),
                        "");
                target.last_modification.CloneParent();
            }
            File resultProject = new File(result, target.name);
            String versionName = target.GenerateVersionName("v");
            version = new File(target.Home, versionName);
            FileUtils.moveDirectory(resultProject, version);
            if (!version.exists()) Log.Writeln_("Не удалось перенести результат");
        } else Log.Writeln_("Не удалось распаковать результат");
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        String[] data = output.split("\\|");
        if (data.length > 1) {
            badFiles = "Не удалось сконвертировать " + data[0].split("\n").length + " файлов:\n" + data[0];
            target.updateCompilationOut(data[1]);
        } else
            target.updateCompilationOut(output);
    }
    @Override
    protected void showFinish() throws Exception {
        UI.getMainWindow().getProjectWindow().RefreshProjectTreeAndMessages();
        if (Current.HasFile()) {
            Current.getFile().form.ShowCompilationOutput();
            if (!output.isEmpty())
                Current.getFile().form.FocusCompilationOut();
        }
        if (!badFiles.isEmpty())
            UI.Error(badFiles);
    }
    @Override
    protected void performDone() throws Exception {
        target.joinExistingVersion(version, getDescription());
        target.migrateFilesSettings(target.last_version, false, false, false);
    }
    @Override
    public JMenuItem createMenuItem() {
        if (menuItem == null)
            menuItem = new PassMenuItem(this);
        return menuItem;
    }
}

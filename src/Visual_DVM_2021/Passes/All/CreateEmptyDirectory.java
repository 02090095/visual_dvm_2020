package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Text.FileNameForm;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.ChangeFilePass;
import Visual_DVM_2021.Passes.PassException;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.nio.file.Paths;
public class CreateEmptyDirectory extends ChangeFilePass {
    @Override
    protected boolean canStart(Object... args) {
        resetArgs();
        if ((ff = new FileNameForm()).ShowDialog("Введите имя создаваемой папки")) {
            fileName = ff.Result;
            //->
            parent_node = Current.getProjectCurrentParentNode();
            target_dir = (File) parent_node.getUserObject();
            //->
            dst = Paths.get(target_dir.getAbsolutePath(), fileName).toFile();
            if (dst.exists()) {
                Log.Writeln("Файл с именем " + Utils.Brackets(fileName) + " уже существует");
                return false;
            }
            if (fileName.equalsIgnoreCase(db_project_info.data)) {
                Log.Writeln(Utils.Brackets(db_project_info.data) + " является зарезервированным именем!");
                return false;
            }
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        if (!dst.mkdir()) throw new PassException("Не удалось создать папку.");
        UI.getMainWindow().getProjectWindow().getFilesTreeForm().getTree().AddNode(parent_node, dst_node = new DefaultMutableTreeNode(dst));
    }
}

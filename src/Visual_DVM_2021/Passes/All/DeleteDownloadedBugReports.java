package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
import java.util.Arrays;
import java.util.Vector;
public class DeleteDownloadedBugReports extends Pass_2021<Vector<File>> {
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    public boolean needsConfirmations() {
        return true;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = null;
        File workspace = new File(Global.db.settings.get(SettingName.Workspace).toString());
        File[] files = workspace.listFiles(pathname -> pathname.isDirectory() && pathname.getName().toLowerCase().startsWith("bugreport_"));
        if (files != null) {
            target = new Vector<>(Arrays.asList(files));
            return UI.Warning("Будет удалено " + target.size() + " скачанных отчётов об ошибках." +
                    (Current.HasProject() ? "(Текущий проект будет закрыт)" : "")
            );
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        if (Current.HasProject())
            passes.get(PassCode_2021.CloseCurrentProject).Do();
    }
    @Override
    protected void body() throws Exception {
        for (File file : target) {
            ShowMessage1(file.getAbsolutePath());
            Utils.forceDeleteWithCheck(file);
        }
    }
}

package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.VariantsMassPass;
public class CreateParallelVariants extends VariantsMassPass {
    @Override
    public String getIconPath() {
        return "/icons/Create.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    public PassCode_2021 getSubPassCode() {
        return PassCode_2021.SPF_CreateParallelVariant;
    }
    @Override
    protected void FocusResult() {
        UI.getMainWindow().getProjectWindow().FocusVersions();
    }
    @Override
    protected void performPreparation() throws Exception {
        db_project_info.ResetNewVersions();
        super.performPreparation();
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;
public class ExcludeSelectedFiles extends Pass_2021<Vector<DBProjectFile>> {
    @Override
    public String getIconPath() {
        return "/icons/Exclude.png";
    }
    @Override
    protected boolean canStart(Object... args) {
        if (!Global.files_multiselection) {
            Log.Writeln_("Нажмите правую клавишу мыши, и перейдите в режим выбора файлов.");
            return false;
        }

        target = new Vector<>();
        for (DBProjectFile file: Current.getProject().db.files.Data.values())
            if (file.isSelected()) target.add(file);
        if (target.isEmpty()){
            Log.Writeln_("Не отмечено ни одного файла.");
            return false;
        }
        return true;
    }
    @Override
    protected void body() throws Exception {
        for (DBProjectFile file : target)
            passes.get(PassCode_2021.ExcludeFile).Do(file);
    }
}

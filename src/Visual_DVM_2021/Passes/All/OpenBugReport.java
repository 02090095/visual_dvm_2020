package Visual_DVM_2021.Passes.All;
import Repository.BugReport.BugReportState;
public class OpenBugReport extends UpdateBugReportField {
    @Override
    public String getIconPath() {
        return "/icons/OpenBugReport.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("state", BugReportState.active, "percentage", 0);
    }
}

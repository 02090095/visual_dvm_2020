package Visual_DVM_2021.Passes.All;
import Common.Utils.Files.VFileChooser;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
import java.util.Arrays;
import java.util.Vector;
public class ImportFiles extends Pass_2021<Vector<File>> {
    VFileChooser fileChooser =
            new VFileChooser("Выберите файлы для добавления");
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Vector<>();
        if (args.length > 0)
            Arrays.stream(args).map(arg -> (File) arg).filter(File::isFile).forEach(file -> target.add(file));
        else
            target = fileChooser.ShowMultiDialog();
        return !target.isEmpty();
    }
    @Override
    protected void body() throws Exception {
        for (File file : target)
            passes.get(PassCode_2021.AddFile).Do(file);
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import GlobalData.User.UserState;
import Repository.SubscriberWorkspace.SubscriberWorkspace;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.PassException;
import Visual_DVM_2021.Passes.Pass_2021;
public class InitialiseUser extends Pass_2021 {
    @Override
    public String getIconPath() {
        return "/icons/InitializeUser.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return Current.Check(Log, Current.Machine, Current.User);
    }
    @Override
    protected void body() throws Exception {
        switch (Current.getMachine().type) {
            case Local:
                passes.get(PassCode_2021.LocalInitaliseUser).Do();
                break;
            case Server:
            case MVS_cluster:
                if (passes.get(PassCode_2021.CheckRemoteWorkspace).Do()) {
                    SubscriberWorkspace workspace = (SubscriberWorkspace) passes.get(PassCode_2021.CheckRemoteWorkspace).target;
                    if (workspace == null) {
                        if (passes.get(PassCode_2021.RemoteInitialiseUser).Do()){
                            workspace = new SubscriberWorkspace();
                            workspace.email=Current.getAccount().email;
                            workspace.URL=Current.getMachine().getURL();
                            workspace.login=Current.getUser().login;
                            workspace.path=(String) passes.get(PassCode_2021.RemoteInitialiseUser).target;
                            //---
                            if (passes.get(PassCode_2021.PublishRemoteWorkspace).Do(workspace)) {
                                Current.getUser().workspace = workspace.path;
                                Current.getUser().state = UserState.ready_to_work;
                                Global.db.Update(Current.getUser());
                            }
                        }
                    }
                    else {
                        //рега была. просто заносим то что там пользователю
                        //todo дополнительно проверять существование папки этого пространства.
                        //даже с учетом БД. может быть вероятность что пространство кто то удалил.
                        Current.getUser().workspace = workspace.path;
                        Current.getUser().state = UserState.ready_to_work;
                        Global.db.Update(Current.getUser());
                    }
                }
                break;
            default:
                throw new PassException("Неопределенный тип пользователя");
        }
    }
    @Override
    protected void showFinish() throws Exception {
        Global.db.users.RefreshUI();
    }
}

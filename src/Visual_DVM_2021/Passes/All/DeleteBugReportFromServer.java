package Visual_DVM_2021.Passes.All;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportState;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
public class DeleteBugReportFromServer extends ComponentsRepositoryPass<BugReport> {
    @Override
    protected boolean canStart(Object... args) {
        target = (BugReport) args[0];
        return !target.state.equals(BugReportState.draft);
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DeleteObject,"", target));
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Visual_DVM_2021.Passes.PassState;
import Common.UI.Menus_2023.PassControl;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SilentSapforPass;
public class SPF_GetGCovInfo extends SilentSapforPass {
    @Override
    public String getIconPath() {
        return "/icons/NotPick.png";
    }
    protected String getDoneIconPath() {
        return "/icons/Pick.png";
    }
    @Override
    protected boolean hasStats() {
        return true;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (UI.Question("Все анализы будут сброшены.Продолжить")) {
            SPF_ParseFilesWithOrder.silent = true;
            return super.canStart(args) && passes.get(PassCode_2021.GCOV).Do()&&passes.get(PassCode_2021.SPF_ParseFilesWithOrder).Do();
        }
        return false;
    }
    @Override
    protected void performFinish() throws Exception {
        SPF_ParseFilesWithOrder.silent = false;
        super.performFinish();
    }
    @Override
    protected void performPreparation() throws Exception {
        sapfor.cd(target.Home);
        target.CleanInterruptFile();
    }
    @Override
    protected void body() throws Exception {
        sapfor.RunAnalysis(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.packSapforSettings(),
                target.getProjFile().getAbsolutePath());
    }
    @Override
    protected void performDone() throws Exception {
        if (!sapfor.getResult().isEmpty())
            unpack(sapfor.getResult());
    }
    protected void unpack(String packed) throws Exception {
        System.out.println("PACKED GCOV INFO ="+Utils.Brackets(packed));
        String[] byFiles = packed.split("@");
        for (int z = 0; z < byFiles.length; ++z) {
            DBProjectFile cur = target.db.files.get((byFiles[z++]).replace("/", "\\"));
            String[] inFile = byFiles[z].split(" ");
            for (int k = 0; k < (inFile.length / 2) * 2; k += 2) {
                int line = Integer.parseInt(inFile[k]);
                long execution = Long.parseLong(inFile[k + 1]);
                if (line > 0) {
                    int v_line = line - 1;
                    if (!cur.gcov_info.line_info.containsKey(v_line)) {
                        cur.gcov_info.add_line(v_line,
                                (execution >= 0) ? execution : 0);
                    }
                }
            }
            //завешение настроек.
            cur.gcov_info.setup();
        }
    }
    @Override
    public void Reset() {
        if (state == PassState.Done) {
            state = PassState.Inactive;
            for (PassControl control: controls)
                control.setIcon(getIconPath());
        }
    }
    @Override
    protected void showDone() throws Exception {
        if (Current.HasFile())
            Current.getFile().form.ShowGCOV();
        for (PassControl control: controls)
            control.setIcon(getDoneIconPath());
    }
    @Override
    protected void FocusResult() {
        if (Current.HasFile())
            Current.getFile().form.FocusGCOVLog();
    }
}

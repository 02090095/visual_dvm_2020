package Visual_DVM_2021.Passes.All;
import Common.Current;
import GlobalData.Settings.SettingName;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.TSetting.TSetting;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class SwitchTestingEmail extends TestingSystemPass<TSetting> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().CheckRegistered(Log)) {
            target = server.account_db.settings.get(SettingName.Email);
            target.value =  ((boolean) (args[0]))?1:0;
            return true;
        }
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.EditAccountObject, Current.getAccount().email, target));
    }
}

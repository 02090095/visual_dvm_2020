package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.RunConfiguration.RunConfiguration;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditRunConfiguration extends EditObjectPass<RunConfiguration> {
    public EditRunConfiguration() {
        super(RunConfiguration.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Utils.Utils;
import Repository.BugReport.BugReportInterface;
import Repository.BugReport.BugReportState;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
public class SendBugReport extends ComponentsRepositoryPass {
    @Override
    protected void ServerAction() throws Exception {
        if (!Current.getBugReport().project_version.isEmpty()) {
            //отправить архив.
            Command(new ServerExchangeUnit_2021(ServerCode.SendBugReport,
                    Current.getBugReport().id,
                    Utils.packFile(BugReportInterface.getArchiveFile(Current.getBugReport()))
            ));
        }
        // синхрон бд
        Command(new ServerExchangeUnit_2021(ServerCode.PublishObject, "", Current.getBugReport()));
    }
    @Override
    protected void performFail() throws Exception {
        Current.getBugReport().state = BugReportState.draft;
    }
}

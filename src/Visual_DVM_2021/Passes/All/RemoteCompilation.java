package Visual_DVM_2021.Passes.All;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.Supervisor.Remote.RemoteCompilationSupervisor;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.SSH.TaskConnectionPass;
public class RemoteCompilation extends TaskConnectionPass {
    public RemoteCompilation() {
        super(RemoteCompilationSupervisor.class);
    }
    @Override
    protected boolean canStart(Object... args) {
        supervisor.Init((CompilationTask) args[0], this, (db_project_info) args[1]);
        return true;
    }
}

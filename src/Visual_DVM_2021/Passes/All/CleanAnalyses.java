package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Visual_DVM_2021.Passes.CurrentProjectPass;
public class CleanAnalyses extends CurrentProjectPass {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args) && (Current.getSapfor() != null);
    }
    @Override
    protected void body() throws Exception {
        Current.getSapfor().ResetAllAnalyses();
        target.CleanAnalyses();
        target.CreateParserOptionsDirs();
        target.ClearGCOV();
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        UI.getMainWindow().getProjectWindow().RefreshProjectFiles();
        UI.getMainWindow().getProjectWindow().ShowNoAnalyses();
        UI.getMainWindow().getProjectWindow().ShowProjectSapforLog();
        if (Current.HasFile()) {
            Current.getFile().form.ShowNoMessages();
            Current.getFile().form.ShowNoAnalyses();
            Current.getFile().form.ShowNoGCOVLog();
        }
    }
}
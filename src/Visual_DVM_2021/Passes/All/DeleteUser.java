package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.User.User;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteUser extends DeleteObjectPass<User> {
    public DeleteUser() {
        super(User.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

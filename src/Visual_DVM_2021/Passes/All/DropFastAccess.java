package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.UI.UI;
import Visual_DVM_2021.Passes.Pass_2021;
public class DropFastAccess extends Pass_2021 {
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected void body() throws Exception {
        for (Pass_2021 pass : Pass_2021.FAPasses) {
            pass.stats.Drop();
            Global.db.Update(pass.stats);
        }
    }
    @Override
    protected void showDone() throws Exception {
       UI.fastAccessMenuBar.Drop();
    }
}

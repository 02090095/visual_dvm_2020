package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.SessionMaxtimeDialog;
import Visual_DVM_2021.Passes.CurrentProjectPass;
public class EditProjectRunMaxtime extends CurrentProjectPass {
    @Override
    public String getIconPath() {
        return "/icons/Maxtime.png";
    }
    SessionMaxtimeDialog f;
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args) &&
                (f = new SessionMaxtimeDialog()).ShowDialog("максимальное время запуска", Current.getProject().run_maxtime);
    }
    @Override
    protected void body() throws Exception {
        target.UpdateRunMaxtime(f.Result);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getMainWindow().getTestingWindow().ShowProjectMaxRunTime();
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackage_2023;
import Visual_DVM_2021.Passes.DeleteObjectPass;

import java.io.File;
public class DeleteSapforTasksPackage extends DeleteObjectPass<SapforTasksPackage_2023> {
    public DeleteSapforTasksPackage() {
        super(SapforTasksPackage_2023.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
    @Override
    protected void performPreparation() throws Exception {
        if (Current.hasUI()) {
            UI.getMainWindow().getTestingWindow().RemoveSapforPackageFromComparison(target);
        }
    }
    @Override
    protected void body() throws Exception {
        super.body();
        Utils.delete_with_check(new File(target.workspace));
    }
}

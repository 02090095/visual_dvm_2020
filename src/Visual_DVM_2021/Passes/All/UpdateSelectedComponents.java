package Visual_DVM_2021.Passes.All;
import Common.Global;
import Repository.Component.Component;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;
public class UpdateSelectedComponents extends Pass_2021<Vector<Component>> {
    @Override
    public String getIconPath() {
        return "/icons/Update.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    public boolean needsConfirmations() {
        return true;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Vector<>();
        //------------------------
        if (Global.Components.getCheckedCount() == 0) {
            Log.Writeln_("Не отмечено ни одного компонента!");
            return false;
        }
        target = Global.Components.getCheckedItems();
        return true;
    }
    @Override
    public String getStartDescription() {
        Vector<String> question = new Vector<>();
        question.add("Обновить компоненты");
        for (Component component : Global.Components.getCheckedItems()) {
            question.add(component.getComponentType().getDescription());
        }
        return String.join("\n", question);
    }
    @Override
    protected void body() throws Exception {
        for (Component component : target) {
            Global.Components.ui_.Select(component.getPK());
            passes.get(PassCode_2021.UpdateComponent).Do();
        }
    }
}

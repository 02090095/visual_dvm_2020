package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.EnvironmentValue.EnvironmentValue;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditEnvironmentValue extends EditObjectPass<EnvironmentValue> {
    public EditEnvironmentValue() {
        super(EnvironmentValue.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

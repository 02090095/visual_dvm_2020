package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.SapforProfile.SapforProfile;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditProfile extends EditObjectPass<SapforProfile> {
    public EditProfile() {
        super(SapforProfile.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

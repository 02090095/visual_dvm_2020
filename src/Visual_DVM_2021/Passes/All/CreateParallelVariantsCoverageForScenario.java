package Visual_DVM_2021.Passes.All;
import Common.Current;
import ProjectData.SapforData.Variants.ParallelVariant;
import Repository.Component.Sapfor.Sapfor;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class CreateParallelVariantsCoverageForScenario extends CurrentProjectPass {
    @Override
    public void Interrupt() throws Exception {
        Current.getSapfor().Interrupt();
    }
    @Override
    protected void performPreparation() throws Exception {
    }
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetArrayDistribution;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            target.gen_variants_coverage();
            if (target.parallelVariants.size() == 0) {
                Log.Writeln_("Покрытие вариантов пусто");
                return false;
            } else if (!passes.get(PassCode_2021.SPF_GetArrayDistribution).isDone()) {
                Log.Writeln_("Варианты неактуальны. Сначала перестройте распределение данных.");
                return false;
            }
            return true;
        }
        return false;
    }
    protected void body() throws Exception {
        for (ParallelVariant p : target.parallelVariants.Data.values())
            if (!passes.get(PassCode_2021.SPF_CreateParallelVariant).Do(p)) break;
    }
    @Override
    protected void performFinish() throws Exception {
        //распаковку соо делаем только 1 раз. после всей массы вариантов.
        Sapfor sapfor = Current.getSapfor();
        target.unpackMessagesAndLog(sapfor.getOutputMessage(), sapfor.getOutput());
    }
}

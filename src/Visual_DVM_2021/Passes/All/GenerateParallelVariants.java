package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;

import java.math.BigInteger;
public class GenerateParallelVariants extends CurrentProjectPass {
    boolean all_variants;
    @Override
    public String getIconPath() {
        return "/icons/ParallelVariants.png";
    }
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetArrayDistribution;
    }
    @Override
    protected void FocusBeforeStart() {
        UI.getMainWindow().getProjectWindow().FocusVersions();
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            all_variants = (boolean) args[0];
            BigInteger amount = all_variants ? target.getFilteredVariantsCount() : BigInteger.valueOf(target.getVariantsCoverageCount());
            if (amount.compareTo(BigInteger.valueOf(1600)) > 0) {
                Log.Writeln_("Количество вариантов " + amount + "\nпревышает 1600. Отображение запрещено.");
                return false;
            }
            return UI.Question("Будет отображено " + amount + " вариантов. Хотите продолжить");
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        target.parallelVariants.clear();
    }
    @Override
    protected void showPreparation() throws Exception {
        UI.getVersionsWindow().getVariantsWindow().ShowNoVariants();
    }
    @Override
    protected void body() throws Exception {
        if (all_variants)
            target.gen_variants_vectors();
        else
            target.gen_variants_coverage();
    }
    @Override
    protected void showDone() throws Exception {
        UI.getVersionsWindow().getVariantsWindow().ShowVariants();
    }
}

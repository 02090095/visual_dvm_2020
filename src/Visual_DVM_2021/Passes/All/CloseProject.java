package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
public class CloseProject extends Pass_2021<db_project_info> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (target = Current.getProject()) != null;
    }
    @Override
    protected void body() throws Exception {
        target.Close();
    }
    @Override
    protected void performDone() throws Exception {
        Current.getSapfor().ResetAllAnalyses();
        Current.getSapfor().cd(new File(Global.Home));
        Current.set(Current.Project, null);
        //-
    }
}
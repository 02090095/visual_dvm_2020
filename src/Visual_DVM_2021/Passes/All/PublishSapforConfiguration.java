package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Sapfor.SapforConfiguration.SapforConfiguration;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class PublishSapforConfiguration extends TestingSystemPass<SapforConfiguration> {
    @Override
    public String getIconPath() {
        return "/icons/RedAdd.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().CheckRegistered(Log)) {
            target = new SapforConfiguration();
            target.genName();
            target.sender_name = Current.getAccount().name;
            target.sender_address = Current.getAccount().email;
            return server.db.sapforConfigurations.ShowAddObjectDialog(target);
        }
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.PublishObject, "", target));
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        passes.get(PassCode_2021.SynchronizeTests).Do();
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        server.db.sapforConfigurations.ui_.Show(target.getPK());
    }
}
package Visual_DVM_2021.Passes.All;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class RefreshDVMTests extends TestingSystemPass<Object>{
    @Override
    public String getIconPath() {
        return "/icons/DownloadAll.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.RefreshDVMTests));
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        passes.get(PassCode_2021.SynchronizeTests).Do();
    }

    //todo будет реализован когда наладится запуск сапфора на сервере тестирования.
    //todo тогда все процедуры с репозиторием и созданием групп и тестов, и определением сапфором их размерности будут на стороне сервера.
}

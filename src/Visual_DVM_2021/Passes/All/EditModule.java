package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Module.Module;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditModule extends EditObjectPass<Module> {
    public EditModule() {
        super(Module.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

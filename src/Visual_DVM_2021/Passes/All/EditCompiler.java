package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Compiler.Compiler;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditCompiler extends EditObjectPass<Compiler> {
    public EditCompiler() {
        super(Compiler.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

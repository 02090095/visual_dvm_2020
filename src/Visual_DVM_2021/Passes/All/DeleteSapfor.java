package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.RemoteSapfor.RemoteSapfor;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteSapfor extends DeleteObjectPass<RemoteSapfor>{
    public DeleteSapfor() {
        super(RemoteSapfor.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.DBObject;
import Common.Database.DataSet;
import Common.Database.rDBObject;
import Common.UI.UI;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;

import java.util.Vector;
public class CopyServerObjects extends TestingSystemPass<Vector<DBObject>> {
    @Override
    protected int getTimeout() {
        return 400000;
    }
    Class objects_class;
    Vector<String> names;
    public CopyServerObjects(Class class_in) {
        objects_class = class_in;
    }
    @Override
    public String getIconPath() {
        return "/icons/Editor/Copy.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().CheckRegistered(Log)) {
            DataSet table = server.db.tables.get(objects_class);
            if (table.getCheckedCount() == 0) {
                Log.Writeln_("Не отмечено ни одного объекта.");
                return false;
            }
            Vector checkedItems = table.getCheckedItems();
            target = new Vector<>();
            names = new Vector<>();
            for (Object o : checkedItems) {
                rDBObject r = (rDBObject) o;
                names.add(r.description);
                target.add(r);
            }
            return UI.Warning(getDescription() + "\n" + String.join("\n", names));
        } else return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.CopyObjects, Current.getAccount().name + "\n" + Current.getAccount().email, target));
    }
    @Override
    protected void performFinish() throws Exception {
        super.performFinish();
        passes.get(PassCode_2021.SynchronizeTests).Do();
    }
}

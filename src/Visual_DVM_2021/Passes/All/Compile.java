package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.TaskState;
import GlobalData.User.UserState;
import ProjectData.LanguageName;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.PassException;
import Visual_DVM_2021.Passes.Pass_2021;
public class Compile extends Pass_2021<db_project_info> {
    Pass_2021 subpass = null;
    CompilationTask compilationTask = null;
    @Override
    protected PassCode_2021 necessary() {
        return Current.getProject().languageName.equals(LanguageName.fortran) ? PassCode_2021.SPF_ParseFilesWithOrder : null;
    }
    @Override
    public String getIconPath() {
        return "/icons/Start.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) {
        if (Current.Check(Log, Current.Project, Current.Machine, Current.User, Current.Makefile)) {
            target = Current.getProject();
            subpass = null;
            compilationTask = null;
            if (Current.getUser().state != UserState.ready_to_work)
                Log.Writeln_("Пользователь " + Utils.Brackets(Current.getUser().login) +
                        " не проинициализирован\nПерейдите на вкладку 'Настройки компиляции и запуска',\n" +
                        " и выполните команду 'Инициализация пользователя'\n");
            Current.getMakefile().Validate(Log);
            return Log.isEmpty();
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        compilationTask = new CompilationTask();
        compilationTask.machine_id = Current.getMachine().id;
        compilationTask.user_id = Current.getUser().id;
        compilationTask.makefile_id = Current.getMakefile().id;
        compilationTask.project_path = target.Home.getAbsolutePath();
        compilationTask.project_description = target.description;
        //------------------------------------------
        compilationTask.CompleteSummary(target.compilation_maxtime);
        compilationTask.state = TaskState.Inactive;
        Global.db.Insert(compilationTask);
        Utils.forceDeleteWithCheck(compilationTask.getLocalWorkspace());
    }
    @Override
    protected void showPreparation() throws Exception {
        Global.db.compilationTasks.ShowUI(compilationTask.getPK());
    }
    @Override
    protected void body() throws Exception {

        switch (Current.getMachine().type) {
            case Local:
                if (Global.isWindows) {
                    subpass = passes.get(PassCode_2021.WindowsLocalCompilation);
                } else
                    subpass = passes.get(PassCode_2021.LinuxLocalCompilation);
                break;
            case Undefined:
                throw new PassException("Компиляция не реализована для типа машины " + Utils.DQuotes(Current.getMachine().type));
            default:
                subpass = passes.get(PassCode_2021.RemoteCompilation);
                break;
        }
        subpass.Do(Current.getCompilationTask(), Current.getProject());
    }
    @Override
    protected boolean validate() {
        return (subpass != null) && subpass.isDone();
    }
    @Override
    protected void showFinish() throws Exception {
        Global.db.compilationTasks.ShowUI(compilationTask.getPK());
        UI.getMainWindow().getTestingWindow().ShowLastCompilationTask();
    }
    /*
    @Override
    protected void showCanNotStart() throws Exception {
        UI.getNewMainWindow().getTestingWindow().FocusCredentials();
    }
    */
}

package Visual_DVM_2021.Passes.All;
import Common.Database.DBObject;
import Common.Database.Database;
import Common.Global;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.Machine;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddCompiler extends AddObjectPass<Compiler> {
    public AddCompiler() {
        super(Compiler.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
    @Override
    public Class<? extends DBObject> getOwner() {
        return Machine.class;
    }
}

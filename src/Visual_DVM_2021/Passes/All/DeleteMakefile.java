package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Makefile.Makefile;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteMakefile extends DeleteObjectPass<Makefile> {
    public DeleteMakefile() {
        super(Makefile.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Module.Module;
import ProjectData.Files.DBProjectFile;
import ProjectData.LanguageName;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.LinkedHashMap;
public class GCOV extends Precompilation {
    protected File getBinary() {
        return new File(workspace, getBinaryName());
    }
    protected String getBinaryName() {
        return Global.isWindows ? "0.exe" : "0";
    }
    @Override
    public boolean needsConfirmations() {
        return false;
    }
    @Override
    public String getIconPath() {
        return "/icons/GCOV.PNG";
    }
    @Override
    protected boolean hasStats() {
        return true;
    }
    @Override
    protected String getLinkFlags() {
        return "-O2 -g -fprofile-arcs -ftest-coverage";
    }
    @Override
    protected String getFortranFlags() {
        return "-O2 -g -fprofile-arcs -ftest-coverage";
    }
    @Override
    protected void prepareForParse() throws Exception {
        //-- тут анализы чистить не надо.
    }
    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation();
        target.ClearGCOV();
    }
    @Override
    protected void showPreparation() throws Exception {
        //super.showPreparation(); тут не надо отображать пустые анализы.
        if (Current.HasFile()) {
            Current.getFile().form.ShowNoRunOutput();
            Current.getFile().form.ShowNoGCOVLog();
        }
    }
    @Override
    protected String genMakefileText(LinkedHashMap<LanguageName, Module> modules) throws Exception {
        return makefile.Generate(target, false, modules);
    }
    @Override
    protected void body() throws Exception {
        ShowMessage1("Сборка для GCOV");
        super.body();
        super.performFinish(); //тут только обработка вывода.
        if (getBinary().exists()) {
            ShowMessage1("Запуск для GCOV");
            name_to_kill = "0.exe";
            StartProcess(Global.isWindows?"0.exe":"./0", target.run_maxtime);
            target.updateRunOut(output);
        } else {
            Log.Writeln_("Не удалось собрать проект.");
            return;
        }
        ShowMessage1("Получение тестовых покрытий для файлов..");
        for (DBProjectFile file : target.getPrograms().get(LanguageName.fortran)) {
            ShowMessage2(file.name);
            String uname = file.last_assembly_name.substring(0, file.last_assembly_name.length() - 2);
            String gcno = uname + ".gcno";
            File gcov = Paths.get(workspace.getAbsolutePath(), file.file.getName() + ".gcov").toFile();
            Utils.forceDeleteWithCheck(gcov);
            name_to_kill = "gcov.exe";
            StartProcess("gcov -b " +
                    Utils.DQuotes(
                            Paths.get(workspace.getAbsolutePath(), uname))
                    + "  -o " +
                    Utils.DQuotes(
                            Paths.get(workspace.getAbsolutePath(), gcno)
                    ), 40);
            if (gcov.exists()) {
                file.GCOVLog = output;
                target.db.Update(file);
                File targetGcov = Paths.get(
                        target.getGCOVDirectory().getAbsolutePath(),
                        (Global.isWindows?file.name:file.getUnixName()) + ".gcov").toFile();
                Files.copy(gcov.toPath(), targetGcov.toPath(), StandardCopyOption.REPLACE_EXISTING);
            }
        }
    }
    @Override
    protected boolean validate() {
        return Log.isEmpty();
    }
    @Override
    protected void performFinish() throws Exception {
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        if (Current.HasFile()) {
            Current.getFile().form.ShowRunOutput();
            Current.getFile().form.ShowGCOVLog();
        }
    }
    @Override
    protected void showFail() throws Exception {
        super.showFail();
        if (Current.HasFile())
            Current.getFile().form.FocusCompilationOut();
    }
    @Override
    protected void performDone() throws Exception {
       // target.updateGCOV_status(1);
    }
    @Override
    protected void FocusResult() {
        if (Current.HasFile())
            Current.getFile().form.FocusGCOVLog();
    }
    /*
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)){
            if (passes.get(PassCode_2021.SPF_GetArrayDistribution).isDone()){
                Log.Writeln_("Не разрешено при выполненном анализе кода/построенном распределении данных.Сбросьте актуальность анализов.");
                return false;
            }
            else return true;
        };
        return false;
    }

     */
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import GlobalData.SapforProfile.SapforProfile;
import GlobalData.SapforProfileSetting.SapforProfileSetting;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;
public class ApplyProfile extends Pass_2021<SapforProfile> {
    @Override
    public String getIconPath() {
        return "/icons/Apply.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    public boolean needsConfirmations() {
        return true;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.SapforProfile)) {
            target = (SapforProfile) Current.get(Current.SapforProfile);
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        Vector<SapforProfileSetting> settings = new Vector<>();
        for (SapforProfileSetting sapforProfileSetting : Global.db.sapforProfilesSettings.Data.values())
            if (sapforProfileSetting.sapforprofile_id == target.id)
                settings.add(sapforProfileSetting);
        //--
        for (SapforProfileSetting setting : settings)
            if (Global.db.settings.containsKey(setting.name))
                passes.get(PassCode_2021.UpdateSetting).Do(setting.name, setting.value);
        //--
    }
}

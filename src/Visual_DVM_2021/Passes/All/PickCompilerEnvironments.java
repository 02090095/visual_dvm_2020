package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.Windows.Dialog.Dialog;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Compiler.CompilerType;
import GlobalData.CompilerEnvironment.CompilerEnvironment;
import GlobalData.CompilerEnvironment.UI.CompilerEnvironmentsFields;
import GlobalData.EnvironmentValue.EnvironmentValue;
import GlobalData.RunConfiguration.RunConfiguration;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.util.LinkedHashMap;
public class PickCompilerEnvironments extends Pass_2021<String> {
    Compiler compiler = null;
    @Override
    public String getIconPath() {
        return "/icons/Menu/Regions.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    //-
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.RunConfiguration)) {
            RunConfiguration configuration = Current.getRunConfiguration();
            if (configuration.compiler_id == Utils.Nan) {
                Log.Writeln_("Отсутвует DVM компилятор, связанный с текущей конфигурацией запуска.\n" +
                        "Если конфигурация содержит вызов DVM компилятора, но была создана на версии 801 и ниже,\n" +
                        "войдите в окно её редактирования,нажмите ОК, а затем повторите попытку.");
                return false;
            }
            //-
            compiler = configuration.getCompiler();
            if (!compiler.type.equals(CompilerType.dvm)) {
                Log.Writeln_("Выбор переменных окружения возможен только для DVM компилятора,");
                return false;
            }
            //-
            if (!(compiler.helpLoaded || passes.get(PassCode_2021.ShowCompilerHelp).Do(compiler, false)))
                return false;
            //-
            Dialog<String, CompilerEnvironmentsFields> dialog =
                    new Dialog<String, CompilerEnvironmentsFields>(CompilerEnvironmentsFields.class) {
                        @Override
                        public void InitFields() {
                            compiler.environments.mountUI((JPanel) fields.getContent());
                            compiler.environments.ShowUI();
                        }
                        @Override
                        public boolean NeedsScroll() {
                            return false;
                        }
                        @Override
                        public void validateFields() {
                            for (CompilerEnvironment compilerEnv : compiler.environments.Data.values()) {
                                if (compilerEnv.isSelected() && compilerEnv.value.isEmpty())
                                    Log.Writeln_("Отмеченная переменная " + compilerEnv.name + " предполагает непустое значение.");
                            }
                        }
                    };
            return dialog.ShowDialog("Назначение переменных окружения конфигурации запуска");
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        LinkedHashMap<String, String> envValues = Current.getRunConfiguration().getEnvMap();
        for (CompilerEnvironment compilerEnv : compiler.environments.Data.values()) {
            if (compilerEnv.isSelected()) {
                EnvironmentValue confEnv;
                if (!envValues.containsKey(compilerEnv.name)) {
                    confEnv = new EnvironmentValue();
                    confEnv.machine_id = Current.getRunConfiguration().machine_id;
                    confEnv.run_configuration_id = Current.getRunConfiguration().id;
                    confEnv.name = compilerEnv.name;
                    confEnv.value = compilerEnv.value;
                    Global.db.Insert(confEnv);
                } else {
                    confEnv = Global.db.environmentValues.getEnvByName(compilerEnv.name);
                    if (confEnv != null) {
                        confEnv.name = compilerEnv.name;
                        confEnv.value = compilerEnv.value;
                        Global.db.Update(confEnv);
                    }
                }
            }
        }
    }
    @Override
    protected void showDone() throws Exception {
        Global.db.environmentValues.ShowUI();
    }
}

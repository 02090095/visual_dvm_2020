package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportInterface;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
import java.nio.file.Paths;
public class OpenBugReportTestProject extends Pass_2021<BugReport> {
    File root = null;
    File project = null;
    @Override
    public String getIconPath() {
        return "/icons/DownloadBugReport.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.BugReport) &&
                (BugReportInterface.CheckNotDraft(target=Current.getBugReport(),Log))) {

            if (!target.project_version.isEmpty()) {
                root = Paths.get(Global.visualiser.getWorkspace().getAbsolutePath(),
                        target.id).toFile();
                project = Paths.get(root.getAbsolutePath(),
                        Global.isWindows ? Utils.toW(target.project_version)
                                : Utils.toU(target.project_version)).toFile();
                return true;
            }else {
                Log.Writeln_("Отчёт об ошибке не содержит прикреплённого проекта!");
                return false;
            }
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentProject).Do();
        Current.set(Current.Root, null); //чтобы гарантированно не существовало корня.
    }
    @Override
    protected void body() throws Exception {
        //попытка скачки. проверка существования архива уже там.
        passes.get(PassCode_2021.DownloadBugReport).Do(target);
        if (BugReportInterface.getArchiveFile(target).exists()) {
            passes.get(PassCode_2021.UnzipFolderPass).Do(
                    BugReportInterface.getArchiveFile(target).getAbsolutePath(),
                    root.getAbsolutePath()
            );
        }
    }
    @Override
    protected boolean validate() {
        if (!project.exists()) {
            Log.Writeln_("Тестовый проект\n" + project.getAbsolutePath() +
                    "\nне был скачан.");
            return false;
        }
        return true;
    }
    @Override
    protected void performDone() throws Exception {
        if (passes.get(PassCode_2021.OpenCurrentProject).Do(project))
            passes.get(PassCode_2021.ApplyBugReportSettings).Do();
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Global;
import GlobalData.DBLastProject.DBLastProject;
import Visual_DVM_2021.Passes.Pass_2021;
public class DropLastProjects extends Pass_2021 {
    @Override
    protected void body() throws Exception {
        Global.db.DeleteAll(DBLastProject.class);
    }
}

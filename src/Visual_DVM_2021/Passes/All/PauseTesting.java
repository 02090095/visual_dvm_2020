package Visual_DVM_2021.Passes.All;
import Common.Current;
import GlobalData.Settings.SettingName;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.TSetting.TSetting;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class PauseTesting extends TestingSystemPass<TSetting> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return Current.getAccount().CheckRegistered(Log);
    }
    @Override
    public String getIconPath() {
        return "/icons/Pause.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected void ServerAction() throws Exception {
        target =  new TSetting(SettingName.Pause, true);
        Command(new ServerExchangeUnit_2021(ServerCode.EditAccountObject, Current.getAccount().email, target));
    }
    @Override
    protected void showFinish() throws Exception {
        passes.get(PassCode_2021.SynchronizeTestsTasks).Do();
    }
}

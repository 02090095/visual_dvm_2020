package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.SVN.SVN;
import Visual_DVM_2021.Passes.ProcessPass;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Paths;
public class DownloadRepository extends ProcessPass {

    File dvmHome;
    File sapforHome;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        dvmHome = Paths.get(Global.RepoDirectory.getAbsolutePath(),
                "dvm").toFile();
        sapforHome = Paths.get(Global.RepoDirectory.getAbsolutePath(),
                "sapfor").toFile();
        return true;
    }
    private void synchronize(String src, File dst) throws Exception {
        File loadedFile = Paths.get(dst.getAbsolutePath(), SVN.LOADED).toFile();
        if (loadedFile.exists()) {
            PerformScript("cd " +
                    dst.getAbsolutePath() +
                    "\nsvn update " + SVN.REPOSITORY_AUTHENTICATION + "\n");
        } else {
            Utils.CleanDirectory(dst);
            PerformScript("cd Repo\nsvn checkout " + SVN.REPOSITORY_AUTHENTICATION + " " + src + "\n"); //export
            FileUtils.write(loadedFile, "+");
        }
    }
    @Override
    protected void body() throws Exception {
        ShowProgress(2, 0, true);
        synchronize(SVN.DVM_REPOSITORY, dvmHome);
        ShowProgress(2, 1, true);
        synchronize(SVN.SAPFOR_REPOSITORY, sapforHome);
        ShowProgress(2, 2, true);
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommand;
import Visual_DVM_2021.Passes.DeleteSelectedServerObjects;

import java.util.Vector;
public class DeleteSapforConfigurationCommand extends DeleteSelectedServerObjects {
    public DeleteSapforConfigurationCommand() {
        super(SapforConfigurationCommand.class);
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.SapforConfigurationCommand)){
            target= new Vector<>();
            target.add(Current.getSapforConfigurationCommand());
            return true;
        }
        return false;
    }
}

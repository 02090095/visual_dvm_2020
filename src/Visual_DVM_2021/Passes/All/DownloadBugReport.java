package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportInterface;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;
public class DownloadBugReport extends ComponentsRepositoryPass<BugReport> {
    @Override
    protected boolean canStart(Object... args) {
        target = (BugReport) args[0];


        return !BugReportInterface.getArchiveFile(target).exists();
    }
    @Override
    protected void performPreparation() throws Exception {
        Utils.CheckDirectory(Global.BugReportsDirectory);
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.ReceiveBugReport, target.id));
        Utils.unpackFile((byte[]) response.object, BugReportInterface.getArchiveFile(target));
    }
    @Override
    protected boolean validate() {
        return BugReportInterface.getArchiveFile(target).exists();
    }
}

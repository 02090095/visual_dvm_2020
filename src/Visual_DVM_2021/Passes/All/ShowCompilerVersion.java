package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.Windows.Dialog.Text.ReadOnlyMultilineTextForm;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.MachineType;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class ShowCompilerVersion extends Pass_2021<String> {
    Pass_2021<String> subpass;
    boolean needsShow = true;
    Compiler compiler;
    //-
    @Override
    public String getIconPath() {
        return "/icons/versions/Version.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = null;
        if (args.length == 0) {
            if (Current.Check(Log, Current.Compiler)) {
                compiler = Current.getCompiler();
                needsShow = true;
                return true;
            }
        } else if (args.length == 2) {
            compiler = (Compiler) args[0];
            needsShow = (boolean) args[1];
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        subpass = null;
        compiler.ResetVersion();
        if (Current.getMachine().type.equals(MachineType.Local)) {
            subpass = passes.get(PassCode_2021.LocalSingleCommand);
        } else {
            subpass = passes.get(PassCode_2021.RemoteSingleCommand);
        }
        if (subpass != null) {
            if (compiler.versionLoaded = subpass.Do(compiler.getVersionCommand())) {
                target = subpass.target;
                compiler.versionText = target;
                compiler.ParseVersion();
            }
        }
    }
    @Override
    protected boolean validate() {
        return (subpass != null) && (target != null);
    }
    @Override
    protected void showDone() throws Exception {
        if (needsShow) {
            Global.db.compilers.RefreshUI();
            ReadOnlyMultilineTextForm ff = new ReadOnlyMultilineTextForm();
            ff.ShowDialog("Версия", target);
        }
    }
}

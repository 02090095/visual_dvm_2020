package Visual_DVM_2021.Passes.All;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommand;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class EditSapforConfigurationCommandOnServer extends TestingSystemPass<SapforConfigurationCommand> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (SapforConfigurationCommand) args[0];
        return true;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.EditObject, "", target));
    }
}
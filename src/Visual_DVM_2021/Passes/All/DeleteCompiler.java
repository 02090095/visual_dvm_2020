package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Compiler.Compiler;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteCompiler extends DeleteObjectPass<Compiler> {
    public DeleteCompiler() {
        super(Compiler.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

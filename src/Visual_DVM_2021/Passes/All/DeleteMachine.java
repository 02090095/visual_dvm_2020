package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Machine.Machine;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteMachine extends DeleteObjectPass<Machine> {
    public DeleteMachine() {
        super(Machine.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
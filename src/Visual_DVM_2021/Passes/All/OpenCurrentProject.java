package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.DebugPrintLevel;
import Common.UI.UI;
import Common.Utils.Files.VDirectoryChooser;
import Common.Utils.Utils;
import GlobalData.DBLastProject.DBLastProject;
import GlobalData.Settings.SettingName;
import ProjectData.Files.LanguageStyle;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
public class OpenCurrentProject extends Pass_2021<db_project_info> {
    Mode mode = Mode.Undefined;
    File dir = null;
    boolean root_changes;
    db_project_info new_root = null;
    VDirectoryChooser directoryChooser = new VDirectoryChooser("Выбор домашней папки проекта");
    @Override
    public String getIconPath() {
        return "/icons/OpenProject.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    void restoreBrowserPath() {
        String last_dir_home =
                Global.db.settings.get(SettingName.ProjectsSearchDirectory).Value;
        if (!last_dir_home.isEmpty())
            directoryChooser.SetCurrentDirectory(last_dir_home);
    }
    boolean needsOpen() {
        return !Current.HasProject() || !Current.getProject().Home.equals(dir);
    }
    //-----------------
    @Override
    protected boolean canStart(Object... args) throws Exception {
        mode = Mode.Directory;
        dir = null;
        target = null;
        if (args.length == 0) {
            restoreBrowserPath();
            dir = directoryChooser.ShowDialog();
        } else {
            Object arg = args[0];
            if (arg instanceof File) {
                dir = (File) arg;
            } else if (arg instanceof db_project_info) {
                mode = Mode.ProjectInfo;
                target = (db_project_info) arg;
                dir = target.Home;
                Global.Log.Print(DebugPrintLevel.Project, "готовая версия " + Utils.Brackets(dir.getAbsolutePath()));
                return needsOpen();
            }
        }
        if ((dir != null) && needsOpen()) {
            Global.Log.Print(DebugPrintLevel.Project, Utils.Brackets(dir.toString()));
            if (!dir.isDirectory()) {
                Log.Writeln_(Utils.Brackets(dir) + "\nне является папкой!");
                return false;
            }
            if (dir.getName().equals(db_project_info.data)) {
                Log.Writeln_(Utils.Brackets(dir) + "\nявляется служебной папкой визуализатора!");
                return false;
            }
            return Utils.validateProjectFolder(dir, Log) && Utils.containsSource(dir, true);
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentProject).Do();
    }
    @Override
    protected void body() throws Exception {
        root_changes = true;
        switch (mode) {
            case Directory:
                if (Current.HasRoot()) {
                    db_project_info root = Current.getRoot();
                    db_project_info project = root.find_version_r(dir);
                    if (project != null) {
                        Global.Log.Print("версия найдена в текущем корне");
                        //версия уже существует. и выстраивать дерево второй раз не нужно.
                        //как и отображать дерево.
                        target = project;
                        target.Open();
                        root_changes = false;
                        return;
                    }
                }
                Global.Log.Print(DebugPrintLevel.Project, "построение дерева версий");
                target = new db_project_info(dir);
                new_root = target.CreateVersionsTree();
                target.Open();
                break;
            case ProjectInfo:
                //если нам суют версию. значит уже априори корень не сменится.
                root_changes = false;
                target.Open();
                break;
        }
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.Project, target);
        if (root_changes)
            Current.set(Current.Root, new_root);
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                SettingName.ProjectsSearchDirectory, dir.getParent()
        );
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                SettingName.FREE_FORM, target.style.equals(LanguageStyle.free) ? "1" : "0")
        ;
        DBLastProject lastProject;
        if (Global.db.lastProjects.containsKey(target.Home.getAbsolutePath())) {
            lastProject = Global.db.lastProjects.get(target.Home.getAbsolutePath());
            lastProject.RefreshOpenTime();
            Global.db.Update(lastProject);
        } else {
            lastProject = new DBLastProject(target);
            Global.db.Insert(lastProject);
        }
        target.setInfo(lastProject);
    }
    @Override
    protected void showDone() throws Exception {
        if (root_changes)
            UI.CreateVersionsWindow();
        UI.getVersionsWindow().ShowProjectVariants();
        UI.getVersionsWindow().BlockVariants();
        //-
        UI.getMainWindow().ShowProject();
        //криво. но при тихом режиме открытие файлов все равно не понадобится.
        passes.get(PassCode_2021.OpenCurrentFile).Do(target.getLastOpenedFile());
    }
    @Override
    protected void FocusResult() {
        UI.getMainWindow().FocusProject();
    }
    enum Mode {
        Undefined,
        Directory,
        ProjectInfo
    }
}

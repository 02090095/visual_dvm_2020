package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Dialog.SliderNumberForm;
import GlobalData.Machine.Machine;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.MachineMaxKernels.MachineMaxKernels;
import Visual_DVM_2021.Passes.TestingSystemPass;
public class EditMachineKernels extends TestingSystemPass<MachineMaxKernels> {
    @Override
    public String getIconPath() {
        return "/icons/Machine.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Machine)) {
            Machine machine = Current.getMachine();
            String url = machine.getURL();
            if (server.db.machinesMaxKernels.containsKey(url)) target = server.db.machinesMaxKernels.get(url);
            else {
                target = new MachineMaxKernels(url);
                server.db.Insert(target);
            }
            SliderNumberForm fff = new SliderNumberForm();
            if (fff.ShowDialog(url, target.limit, 1, 16)) {
                target.limit = fff.Result;
                return true;
            }
        }
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.EditObject, "", target));
        Global.testingServer.db.Update(target);
    }
    @Override
    protected void showDone() throws Exception {
        if (Current.HasMachine())
            UI.getMainWindow().getTestingWindow().ShowCurrentMachine();
        else
            UI.getMainWindow().getTestingWindow().ShowNoCurrentMachine();
    }
}

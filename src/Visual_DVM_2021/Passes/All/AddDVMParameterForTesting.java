package Visual_DVM_2021.Passes.All;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import GlobalData.DVMParameter.DVMParameter;
import GlobalData.DVMParameter.UI.DVMParameterFields;
import Visual_DVM_2021.Passes.Pass_2021;
public class AddDVMParameterForTesting extends Pass_2021<String> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        DBObjectDialog<DVMParameter, DVMParameterFields> dialog =
            new DBObjectDialog<DVMParameter, DVMParameterFields>(DVMParameterFields.class) {
                @Override
                public void fillFields() {
                }
                @Override
                public void validateFields() {
                    String name = (String) fields.cbName.getSelectedItem();
                    String value = fields.tfValue.getText();
                    if (name.isEmpty())
                        Log.Writeln("Имя параметра DVM системы не может быть пустым.");
                    if (Utils.isLinuxSystemCommand(name))
                        Log.Writeln(Utils.DQuotes(name) + " является системной командой Linux");
                    if (Utils.isLinuxSystemCommand(value))
                        Log.Writeln(Utils.DQuotes(value) + " является системной командой Linux");
                }
                @Override
                public void ProcessResult() {
                    target = fields.cbName.getSelectedItem() +"="+fields.tfValue.getText();
                }
                @Override
                public int getDefaultHeight() {
                    return 200;
                }
            };
        return dialog.ShowDialog("Добавление параметра DVM системы", new DVMParameter());
    }
}

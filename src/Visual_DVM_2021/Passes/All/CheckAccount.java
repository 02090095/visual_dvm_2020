package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class CheckAccount extends Pass_2021 {
    @Override
    public String getIconPath() {
        return "/icons/Registry.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected void body() throws Exception {
        if (Current.getAccount().email.equals("?")) {
            if (Pass_2021.passes.get(PassCode_2021.EditAccount).Do())
                Pass_2021.passes.get(PassCode_2021.CheckRegistrationOnServer).Do();
        } else
            Pass_2021.passes.get(PassCode_2021.CheckRegistrationOnServer).Do();
    }
    @Override
    protected void showDone() throws Exception {
        if (UI.getMainWindow() != null) {
            UI.getMainWindow().getCallbackWindow().ShowAccount();
            if (Current.HasBugReport())
                UI.getMainWindow().getCallbackWindow().ShowCurrentBugReport();
        }
        if (Current.getAccount().isAdmin())
            Pass_2021.passes.get(PassCode_2021.PublishComponent).setControlsVisible(true);
    }
}

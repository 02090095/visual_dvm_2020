package Visual_DVM_2021.Passes.All;
import Common.Global;
import Repository.Component.Component;
import Repository.Component.ComponentType;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.ComponentsRepositoryPass;

import java.util.LinkedHashMap;
import java.util.Vector;
public class GetComponentsActualVersions extends ComponentsRepositoryPass {
    @Override
    public String getIconPath() {
        return "/icons/Components.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected void ServerAction() throws Exception {
        Vector<String> versions = new Vector<>();
        for (Component component : Global.Components.Data.values())
            if (component.isVisible())
                versions.add(component.getComponentType().toString());
        Command(new ServerExchangeUnit_2021(ServerCode.GetComponentsVersions, String.join("\n", versions)));
        LinkedHashMap<ComponentType, String> response_actual_versions = (LinkedHashMap<ComponentType, String>) response.object;
        for (ComponentType componentType : response_actual_versions.keySet()) {
            Global.Components.get(componentType).unpackActualVersion(response_actual_versions.get(componentType));
        }
        //-- получить актуальные версии с сервера.
        Command(new ServerExchangeUnit_2021(ServerCode.GetComponentsMinimalVersions, String.join("\n", versions)));
        LinkedHashMap<ComponentType, String> response_minimal_versions = (LinkedHashMap<ComponentType, String>) response.object;
        for (ComponentType componentType : response_minimal_versions.keySet()) {
            Global.Components.get(componentType).unpackMinimalVersion(response_minimal_versions.get(componentType));
        }
        for (Component component : Global.Components.Data.values()) {
            if (component.CanBeUpdated())
                component.CheckIfNeedsUpdateOrPublish();
        }
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.io.File;
public class CloseCurrentProject extends Pass_2021<db_project_info> {
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    public String getIconPath() {
        return "/icons/Close.png";
    }
    @Override
    public Icon getTabIcon() {
        return Utils.getIcon("/icons/Close_18.png");
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (target = Current.getProject()) != null;
    }
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentFile).Do();
    }
    @Override
    protected void body() throws Exception {
        target.Close();
        UI.HideSearchForm();
    }
    @Override
    protected void performDone() throws Exception {
        Global.files_multiselection = false;
        //--
        Current.getSapfor().ResetAllAnalyses();
        Current.getSapfor().cd(new File(Global.Home));
        Current.set(Current.Project, null);
        Current.set(Current.File, null);
        Current.set(Current.Function, null);
        Current.set(Current.SelectedFunction,null);
        Current.set(Current.ProjectNode, null);
        Current.set(Current.SelectedFile, null);
        Current.set(Current.SelectedDirectory, null);
        //-мб перестраховка. мб и нет.
        Current.set(Current.ParallelVariant, null);
        Current.set(Current.Dimensions, null);
        Current.set(Current.Array, null);
        Current.set(Current.DBArray, null);
        //-
    }
    @Override
    protected void showDone() throws Exception {
        if (UI.HasNewMainWindow()) {
            UI.getMainWindow().ShowNoProject();
            UI.getVersionsWindow().ShowNoProjectVariants();
            UI.getVersionsWindow().BlockVariants();
        }
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.SapforProfile.SapforProfile;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteProfile extends DeleteObjectPass<SapforProfile> {
    public DeleteProfile() {
        super(SapforProfile.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

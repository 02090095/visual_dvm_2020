package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.GlobalProperties;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.LanguageName;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Group.Group;
import TestingSystem.Sapfor.SapforConfiguration.SapforConfiguration;
import TestingSystem.Sapfor.SapforConfiguration.SapforConfigurationInterface;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommand;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackage_2023;
import TestingSystem.Sapfor.Scenario_json;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestInterface;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.TestingSystemPass;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Vector;
public class StartSapforTests extends Pass_2021<Vector<SapforTasksPackage_2023>> {
    protected int allTasksCount = 0;
    //--
    protected LinkedHashMap<String, Vector<String>> groupsTests = null;
    //--
    protected Vector<String> testsNames = null; //все тесты что участвуют здесь.
    protected Vector<LanguageName> groupsLanguages = null;
    protected File sapfor = null;
    //---
    @Override
    public String getIconPath() {
        return "/icons/Start.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    //--
    protected boolean checkTestName(String testId) {
        Test test = Global.testingServer.db.tests.get(testId);
        //-
        String name = test.description.toLowerCase();
        if (testsNames.contains(name)) {
            Log.Writeln_("В пакет не могут входить тесты с одинаковыми именами :" + test.description.toLowerCase());
            return false;
        }
        testsNames.add(name);
        return true;
    }
    protected boolean getGroupTests(String groupId) {
        Vector<String> groupTests = new Vector<>();
        Vector<String> selectedGroupTests = new Vector<>();
        //---
        for (Test test : Global.testingServer.db.tests.Data.values()) {
            if (test.group_id.equals(groupId)) {
                groupTests.add(test.id);
                if (test.isSelected())
                    selectedGroupTests.add(test.id);
            }
        }
        if (!groupTests.isEmpty() && !selectedGroupTests.isEmpty())
            groupTests = selectedGroupTests;
        //---
        if (groupTests.isEmpty()) {
            Log.Writeln_("Пустая группа тестов: " + groupId);
            return false;
        }
        //--
        for (String testId : groupTests) {
            if (!checkTestName(testId))
                return false;
        }
        //--
        groupsTests.put(groupId, groupTests);
        //--
        return true;
    }
    public boolean getConfigurationCommands(SapforConfiguration sapforConfiguration) {
        //1. получить список всех команд.
        Vector<PassCode_2021> codes = new Vector<>();
        int v = 0;
        for (SapforConfigurationCommand command : Global.testingServer.db.sapforConfigurationCommands.Data.values()) {
            if (command.sapforconfiguration_id.equals(sapforConfiguration.id)) {
                codes.add(command.passCode);
                if (command.passCode.equals(PassCode_2021.CreateParallelVariants))
                    v++;
            }
        }
        //--
        if (codes.size() == 0) {
            Log.Writeln_("Пустая конфигурация:" + sapforConfiguration.id);
            return false;
        }
        //--
        if (v > 2) {
            Log.Writeln_("Неверная конфигурация:" + sapforConfiguration.id + ": построение параллельных вариантов возможно единожды.");
            return false;
        }
        //--
        if ((v == 1) && codes.size() > 1) {
            if (!codes.lastElement().equals(PassCode_2021.CreateParallelVariants)) {
                Log.Writeln_("Неверная конфигурация:" + sapforConfiguration.id + ": построение параллельных вариантов может быть только завершающей командой!");
                return false;
            }
        }
        return true;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Vector<>();
        //--
        allTasksCount = 0;
        //--
        groupsTests = new LinkedHashMap<>();
        //--->>
        testsNames = new Vector<>();
        groupsLanguages = new Vector<>();
        //проверка стартовых условий.
        //--->>
        if (Global.testingServer.db.sapforConfigurations.getCheckedCount() == 0) {
            Log.Writeln_("Не отмечено ни одной конфигурации SAPFOR.");
            return false;
        }
        //-
        for (SapforConfiguration configuration : Global.testingServer.db.sapforConfigurations.getCheckedItems()) {
            if (!getConfigurationCommands(configuration))
                return false;
        }
        //--
        if (Global.testingServer.db.groups.getCheckedCount() == 0) {
            Log.Writeln_("Не отмечено ни одной группы тестов");
            return false;
        }
        //--
        for (Group group : Global.testingServer.db.groups.getCheckedItems()) {
            //---
            if (!groupsLanguages.contains(group.language))
                groupsLanguages.add(group.language);
            //-
            if (groupsLanguages.get(0) != LanguageName.fortran) {
                Log.Writeln_("Поддерживается пакетный режим только для языка Fortran!");
                return false;
            }
            if (groupsLanguages.size() > 1) {
                Log.Writeln_("Запуск тестов на разных языках в рамках одного пакета запрещен!");
                return false;
            }
            //---
            if (!getGroupTests(group.id))
                return false;
        }
        //--
        for (int i = 0; i < Global.testingServer.db.sapforConfigurations.getCheckedCount(); ++i) {
            for (Vector<String> tests : groupsTests.values())
                allTasksCount += tests.size();
        }
        //--
        return (UI.Question("Будет запущено:\n"
                + allTasksCount + " задач\n" +
                "Продолжить")) && new TestingSystemPass() {
            @Override
            public String getDescription() {
                return "Синхронизация тестов";
            }
            @Override
            protected boolean needsAnimation() {
                return true;
            }
            @Override
            protected void ServerAction() throws Exception {
                for (String groupId : groupsTests.keySet()) {
                    for (String testId : groupsTests.get(groupId)) {
                        Test test = Global.testingServer.db.tests.get(testId);
                        ShowMessage1("загрузка " + test.description);
                        Command(new ServerExchangeUnit_2021(ServerCode.DownloadTest, test.id));
                        response.Unpack(TestInterface.getArchive(test));
                    }
                }
            }
        }.Do();
    }
    @Override
    protected void body() throws Exception {
        //---
        ShowMessage1("Распаковка тестов...");
        //сразу в задачи распаковывать нельзя, потому что копируются под ид,а не дескриптион.
        for (String groupId : groupsTests.keySet()) {
            for (String testId : groupsTests.get(groupId)) {
                Test test = Global.testingServer.db.tests.get(testId);
                ShowMessage2(test.description);
                File testDst = new File(Global.TestsDirectory, test.id);
                Utils.delete_with_check(testDst);
                passes.get(PassCode_2021.UnzipFolderPass).Do(
                        TestInterface.getArchive(test).getAbsolutePath(),
                        Global.TestsDirectory.getAbsolutePath(), false
                );
            }
        }
        ShowMessage1("Создание рабочих пространств...");
        for (SapforConfiguration configuration : Global.testingServer.db.sapforConfigurations.getCheckedItems()) {
            SapforTasksPackage_2023 sapforTasksPackage_2023 = new SapforTasksPackage_2023();
            //--
            sapforTasksPackage_2023.StartDate = new Date().getTime();
            sapforTasksPackage_2023.sapfor_version = String.valueOf(Current.getSapfor().version);
            sapforTasksPackage_2023.tasksCount = allTasksCount;
            sapforTasksPackage_2023.sapforconfiguration_id = configuration.id;
            sapforTasksPackage_2023.flags = SapforConfigurationInterface.getFlags(configuration);
            sapforTasksPackage_2023.passesNames = SapforConfigurationInterface.getTransformationsNames(configuration);
            //--
            sapforTasksPackage_2023.STATIC_SHADOW_ANALYSIS = configuration.STATIC_SHADOW_ANALYSIS;//"Оптимизация теневых обменов"; -sh
            sapforTasksPackage_2023.FREE_FORM = configuration.FREE_FORM;
            sapforTasksPackage_2023.MAX_SHADOW_WIDTH = configuration.MAX_SHADOW_WIDTH; // "Максимальный размер теневых граней"; (%) -shwidth значение поля
            sapforTasksPackage_2023.KEEP_SPF_DIRECTIVES = configuration.KEEP_SPF_DIRECTIVES; //"Сохранять SPF директивы при построении параллельных вариантов"; -keepSPF
            sapforTasksPackage_2023.KEEP_DVM_DIRECTIVES = configuration.KEEP_DVM_DIRECTIVES;// "Учитывать DVM директивы"; -keepDVM
            //--
            Global.db.Insert(sapforTasksPackage_2023);
            sapforTasksPackage_2023.workspace = Paths.get(
                    Global.db.settings.get(SettingName.Workspace).Value,
                    String.valueOf(sapforTasksPackage_2023.id)
            ).toFile().getAbsolutePath();
            //---
            Utils.CheckAndCleanDirectory(new File(sapforTasksPackage_2023.workspace));
            Utils.CheckAndCleanDirectory(new File(sapforTasksPackage_2023.workspace, "state"));
            sapfor = new File(sapforTasksPackage_2023.workspace, "SAPFOR_F.exe"); //развилка на линукс
            ShowMessage2("Копирование SAPFOR");
            FileUtils.copyFile(Current.getSapfor().getFile(), sapfor);
            ShowMessage2("Копирование визуализатора");
            File visualiser = new File(sapforTasksPackage_2023.workspace, "VisualSapfor.jar");
            FileUtils.copyFile(Global.visualiser.getFile(), visualiser);
            GlobalProperties properties = new GlobalProperties();
            properties.Mode = Current.Mode.Package;
            File propertiesFile = new File(sapforTasksPackage_2023.workspace, "properties");
            FileUtils.write(propertiesFile, Utils.jsonToPrettyFormat(Utils.gson.toJson(properties)));
            ShowMessage2("");
            //-
            File scenarioFile = new File(sapforTasksPackage_2023.workspace, "scenario.txt");
            Scenario_json scenario_json = new Scenario_json();
            scenario_json.tests.addAll(testsNames);
            //создать папки для сценариев.
            Vector<PassCode_2021> codes = SapforConfigurationInterface.getPassCodes(configuration);
            Vector<String> testsNames = new Vector<>();
            //---
            for (Vector<String> testIds : groupsTests.values()) {
                for (String testId : testIds) {
                    Test test = Global.testingServer.db.tests.get(testId);
                    ShowMessage2(test.description);
                    //--
                    File taskWorkspace = new File(sapforTasksPackage_2023.workspace, test.description);
                    Utils.CheckAndCleanDirectory(taskWorkspace);
                    //--
                    Utils.copyDirectory(new File(Global.TestsDirectory, test.id), taskWorkspace);
                    testsNames.add(test.description);
                }
            }
            //---
            scenario_json.flags = SapforConfigurationInterface.getFlags(configuration);
            scenario_json.codes.addAll(codes);
            //---
            FileUtils.write(scenarioFile, Utils.jsonToPrettyFormat(Utils.gson.toJson(scenario_json)));
            //---
            sapforTasksPackage_2023.testsNames = String.join(";", testsNames);
            Global.db.Update(sapforTasksPackage_2023);
            target.add(sapforTasksPackage_2023);
        }
        //--
    }
    @Override
    protected void showFinish() throws Exception {
        Global.db.sapforTasksPackages.ShowUI(target.lastElement());
    }
    @Override
    protected void performDone() throws Exception {
        //тут должен быть планировщик и очередь пакетов. отдельной нитью.
        for (SapforTasksPackage_2023 sapforTasksPackage_2023 : target) {
            File workspaceFile = new File(sapforTasksPackage_2023.workspace);
            Utils.startScript(workspaceFile, workspaceFile, "start", "java -jar VisualSapfor.jar");
        }
    }
    @Override
    protected void showDone() throws Exception {
        Global.db.sapforTasksPackages.ShowUI();
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import ProjectData.SapforData.Functions.FuncCoordinates;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphForm;
import Visual_DVM_2021.Passes.Pass_2021;
public class SaveFunctionsGraphCoordinates extends Pass_2021 {
    FunctionsGraphForm graphForm = null;
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    public String getIconPath() {
        return "/icons/ScreenShot.png";
    }
    @Override
    protected boolean canStart(Object... args) {
        graphForm = UI.getMainWindow().getProjectWindow().getFunctionsWindow().getFunctionsGraphWindow();
        if (!graphForm.isShown())
            Log.Writeln_("Сначала отобразите граф");
        if (Current.getProject().functionsGraph.isEmpty())
            Log.Writeln_("Граф процедур пуст");
        return Log.isEmpty();
    }
    @Override
    protected void body() throws Exception {
        for (String funcName : Current.getProject().functionsGraph.vertexCoordinates.keySet()) {
            FuncCoordinates coords = null;
            if (Current.getProject().db.funcCoordinates.containsKey(funcName)) {
                coords = Current.getProject().db.funcCoordinates.get(funcName);
                coords.name=funcName;
                coords.X = Current.getProject().functionsGraph.vertexCoordinates.get(funcName).getKey();
                coords.Y = Current.getProject().functionsGraph.vertexCoordinates.get(funcName).getValue();
                Current.getProject().db.Update(coords);
            } else {
                coords = new FuncCoordinates();
                coords.name=funcName;
                coords.X = Current.getProject().functionsGraph.vertexCoordinates.get(funcName).getKey();
                coords.Y = Current.getProject().functionsGraph.vertexCoordinates.get(funcName).getValue();
                Current.getProject().db.Insert(coords);
            }
        }
    }
}

package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.RunConfiguration.RunConfiguration;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteRunConfiguration extends DeleteObjectPass<RunConfiguration> {
    public DeleteRunConfiguration() {
        super(RunConfiguration.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

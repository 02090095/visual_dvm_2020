package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.EnvironmentValue.EnvironmentValue;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddEnvironmentValue extends AddObjectPass<EnvironmentValue> {
    public AddEnvironmentValue() {
        super(EnvironmentValue.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

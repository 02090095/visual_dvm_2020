package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.TestingServer;
import Visual_DVM_2021.Passes.TestingSystemPass;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Vector;
public class SynchronizeTestsTasks extends TestingSystemPass<Vector<Integer>> {
    File new_db_file;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return Current.getAccount().CheckRegistered(Log);
    }
    @Override
    protected boolean needsAnimation() {
        return false;
    }
    @Override
    public String getIconPath() {
        return "/icons/ComponentsActual.png";
    }
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    protected void performPreparation() throws Exception {
        server.account_db.Disconnect();
    }
    @Override
    protected void showPreparation() throws Exception {
        server.account_db.packages.ClearUI();
       // server.account_db.sapforTasksPackages.ClearUI();
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.ReceiveTestsTasksDatabase, Current.getAccount().email));
        response.Unpack(new_db_file = Utils.getTempFileName("tdb"));
        if (new_db_file.exists()) {
            Utils.forceDeleteWithCheck(server.account_db.getFile());
            FileUtils.moveFile(new_db_file, server.account_db.getFile());
            server.account_db.setFile(Current.getAccount().email);
        }
        server.account_db.Connect();
        server.account_db.CreateAllTables();
        server.account_db.prepareTablesStatements();
        server.account_db.Synchronize();
        UI.getMainWindow().getTestingWindow().RemountTestTable();
    }
    @Override
    protected void performDone() throws Exception {
        server.account_db.CheckKeysActuality();
    }
    @Override
    protected boolean validate() {
        return (response.object != null);
    }
    @Override
    protected void showDone() throws Exception {
        server.account_db.packages.ShowUI();
       // server.account_db.sapforTasksPackages.ShowUI();
        if (!TestingServer.checkTasks) UI.getMainWindow().getTestingWindow().ShowAutoActualizeTestsState();
    }
}


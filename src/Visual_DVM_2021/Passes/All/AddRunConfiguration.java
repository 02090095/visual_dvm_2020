package Visual_DVM_2021.Passes.All;
import Common.Database.DBObject;
import Common.Database.Database;
import Common.Global;
import GlobalData.Machine.Machine;
import GlobalData.RunConfiguration.RunConfiguration;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddRunConfiguration extends AddObjectPass<RunConfiguration> {
    public AddRunConfiguration() {
        super(RunConfiguration.class);
    }
    @Override
    public Class<? extends DBObject> getOwner() {
        return Machine.class;
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}

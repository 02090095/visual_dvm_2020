package Visual_DVM_2021.Passes;
import Common.Global;
import Common.UI.Menus_2023.PassMenuItem;
import Common.UI.UI;
import ProjectData.Project.db_project_info;
import Repository.Component.Sapfor.TransformationPermission;

import javax.swing.*;
public class SapforTransformation extends SapforPass {
    protected String Options = ""; //дополнительный аргумент для преобразований.
    protected TransformationPermission getPermission() {
        return TransformationPermission.All;
    }
    @Override
    public String getIconPath() {
        return "/icons/Transformations/" + code().toString() + ".png";
    }
    protected String getVersionLetter() {
        return "v";
    }
    protected String getVersionDescription() {
        return getDescription();
    }
    //-
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }
    protected boolean resetAnalysesAfter() {
        return true;
    }
    @Override
    protected boolean isGoodCode() {
        return sapfor.getErrorCode() >= 0;
    }
    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation();
        db_project_info.ResetNewVersions();
        target.createEmptyVersion(getVersionLetter(), getVersionDescription());
        //уже добавлена. отображена и выбрана!
    }

    @Override
    protected void body() throws Exception {
        sapfor.RunTransformation(
                code().toString(),
                -Global.messagesServer.getPort(),
                Global.packSapforSettings(),
                target.getProjFile().getAbsolutePath(),
                target.last_version.Home.getAbsolutePath(),
                Options
        );
    }
    @Override
    protected void performDone() throws Exception {
        //миграция типов файлов.
        target.migrateFilesSettings(target.last_version, true, false, true);
        if (resetAnalysesAfter())
            sapfor.ResetAllAnalyses();
    }
    @Override
    protected boolean hasStats() {
        return true;
    }
    @Override
    protected void FocusResult() {
        UI.getMainWindow().getProjectWindow().FocusVersions();
    }
    @Override
    protected void performFail() throws Exception {
        super.performFail();
        target.undoLastTransformation();
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        switch (Global.transformationPermission) {
            case All:
                return super.canStart(args);
            case VariantsOnly:
                if (getPermission().equals(TransformationPermission.VariantsOnly)) {
                    return super.canStart(args);
                } else {
                    Log.Writeln_("Разрешено только построение параллельных вариантов!");
                    return false;
                }
            case None:
                Log.Writeln_("Нет разрешения на выполнение преобразований");
                return false;
        }
        return false;
    }
    @Override
    public JMenuItem createMenuItem() {
        if (menuItem == null)
            menuItem = new PassMenuItem(this);
        return menuItem;
    }
}

package Visual_DVM_2021.Passes;
import Common.Global;
import TestingSystem.TestingServer;
public abstract class TestingSystemPass<T> extends RepositoryPass<TestingServer, T> {
    public TestingSystemPass() {
        super(Global.testingServer);
    }
    @Override
    protected int getTimeout() {
        return 120000;
    }
}

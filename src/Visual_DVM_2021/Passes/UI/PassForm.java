package Visual_DVM_2021.Passes.UI;
import Common.Global;
import Common.UI.Windows.Dialog.Dialog;
import Visual_DVM_2021.Passes.Pass_2021;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
public class PassForm extends Dialog<Pass_2021, PassFields> {
    public PassForm(Pass_2021 pass_in) {
        super(PassFields.class);
        Result = pass_in;
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                Result.animation_sem.release();
            }
        });
    }
    public boolean ShowDialog() {
        return super.ShowDialog("");
    }
    @Override
    public int getDefaultWidth() {
        return 600;
    }
    @Override
    public int getDefaultHeight() {
        return 250;
    }
    //кнопок нет
    @Override
    public void CreateButtons() {
    }
    @Override
    protected void onCancel() {
        try {
            Result.Interrupt();
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
    }
    @Override
    public String getTitleText() {
        return Result.getDescription();
    }
}

package Visual_DVM_2021.Passes;
import Common.Current;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;
public abstract class FilesMassPass<T> extends Pass_2021<T> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getProject().db.files.getCheckedCount() == 0) {
            Log.Writeln_("Не отмечено ни одного файла.");
            return false;
        }
        return true;
    }
    /*
    @Override
    protected boolean needsAnimation() {
        return true;
    }
     */
    @Override
    protected void body() throws Exception {
        Current.getProject().db.BeginTransaction();
        for (DBProjectFile file : Current.getProject().db.files.getCheckedItems()) {
            ShowMessage1(file.name);
            operation(file);
        }
    }
    @Override
    protected void performFinish() throws Exception {
        Current.getProject().db.Commit();
    }
    protected abstract void operation(DBProjectFile file);
    @Override
    protected void showFinish() throws Exception {
        UI.getMainWindow().getProjectWindow().RefreshProjectFiles();
    }
}

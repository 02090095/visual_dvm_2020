package Visual_DVM_2021.Passes;
import java.util.Comparator;
class SortPassesByStats implements Comparator<Pass_2021> {
    public int compare(Pass_2021 p1, Pass_2021 p2) {
        return p2.stats.Usages - p1.stats.Usages;
    }
}

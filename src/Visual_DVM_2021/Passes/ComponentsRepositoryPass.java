package Visual_DVM_2021.Passes;
import Common.Global;
import Repository.Server.ComponentsServer;
public abstract class ComponentsRepositoryPass<T> extends RepositoryPass<ComponentsServer, T> {
    public ComponentsRepositoryPass() {
        super(Global.componentsServer);
    }
}

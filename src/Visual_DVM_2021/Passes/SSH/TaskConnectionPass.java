package Visual_DVM_2021.Passes.SSH;
import Common.Current;
import Common.Global;
import GlobalData.Tasks.Supervisor.Remote.RemoteTaskSupervisor;
public abstract class TaskConnectionPass<S extends RemoteTaskSupervisor> extends ConnectionPass {
    public S supervisor; //инициализация идет в конструкторе потомка.
    public TaskConnectionPass(Class<S> s_class) {
        try {
            supervisor = s_class.newInstance();
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
    }
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    public void Connect() throws Exception {
        machine = supervisor.task.getMachine();
        user = supervisor.task.getUser();
        super.Connect();
    }
    @Override
    protected void ServerAction() throws Exception {
        supervisor.PerformTask();
    }
    @Override
    protected void performFinish() throws Exception {
        supervisor.UpdateTask();
        super.performFinish(); //disconnect
    }
    @Override
    public void Interrupt() throws Exception {
        Current.getProject().CreateInterruptFile();
    }
}

#include "CompilationSupervisor.h"     
#include "RunTask.h"   
#pragma once     
class RunSupervisor: public Supervisor<RunTask> {   
public:    
	RunSupervisor(CompilationSupervisor * compilationSupervisor){  
		this->init("runTasks", 8);   
		//проверить отмененные задачи.  
		for (long i=0; i< this->length; ++i){  
			RunTask * task = this->get(i);  
			CompilationTask * parent = compilationSupervisor->getTaskById( task->getTestCompilationTaskId());  
			task->setState((parent->getState()==Done)?Waiting:Canceled); 
			task->setParent(parent); 
			printf("id=%ld; parent_id = %ld; state=%s\n",
			task->getId(),
			task->getParent()->getId(),
			task->printState().getCharArray());  
		}	  
	}
	virtual String getStatePrefix(){
		return String("Running");
	}
	/*
	virtual void Finalize(){
		this->state = Archivation;
		saveState();
		printf("Archivation started\n");
		Utils::ZipFolder(String("./"),String("archive.zip"));
		printf("Archivation ended\n");
	}
	*/
};
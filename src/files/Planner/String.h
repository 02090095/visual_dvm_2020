#include <stdio.h>     
#include <stdlib.h>     
#include <string.h>    
#include <ctype.h> 
#pragma once     
class String {    
	friend String operator+(const String& a, const String& b);   
	long length;     
	char* body;     
public:      
	String() {     
		length = 0;     
		body = new char[1];     
		body[0] = '\0';     
	}     
     
	String(const char* s) {     
		length = strlen(s);     
		body = new char[length + 1];     
		for (long i = 0; i < length; ++i)     
			body[i] = s[i];	     
		body[length]='\0';     
	}    
	String(const char* s, char ps) {     
		length = strlen(s);     
		body = new char[length + 1];     
		for (long i = 0; i < length; ++i){     
			body[i] = (s[i]==ps)? '\n': s[i];	     
		}    
		body[length]='\0';     
	}     
	~String() {     
		if (body != NULL) {     
			delete[] body;			     
		}     
	}      
	void println() const{     
		printf("[%s]\n", body);     
	}     
	void addChar(char c) {     
		char* buf = new char[length + 2];     
		for (long i = 0; i < length; ++i) {     
			buf[i] = body[i];     
		}     
		buf[length] = c;     
     
		length++;     
		//--     
		buf[length] = '\0';     
		delete[] body;     
		body = buf;     
		buf = NULL;     
	}    
	char * getCharArray() const{    
		return body;    
	} 
	 
	 
	String (int s){   
		length = 0;     
		body = new char[1];     
		body[0] = '\0';   
		if (s>=0){   
			int s_ = s;	   
			int size = 1;	   
			while (s_>=10){   
				s_ = s_/10;   
				size++;   
			}	   
			length = size;   
			body = new char [size+1];   
			sprintf(body, "%d", s);   
		}				   
	}     
	String (long s){   
		length = 0;     
		body = new char[1];     
		body[0] = '\0';   
		if (s>=0){   
			long s_ = s;	   
			long size = 1;	   
			while (s_>=10){   
				s_ = s_/10;   
				size++;   
			}	   
			length = size;   
			body = new char [size+1];   
			sprintf(body, "%ld", s);   
		}				   
	}   
 
	const String& operator=(const String& s){ 
		if (body != NULL)     
			delete[] body; 
		length = s.length; 
		body = new char[length+1]; 
		for (long i=0; i<length; ++i) 
			body[i]=s.body[i];		 
		body[length]='\0';     
		return *this; 
	} 
	 
	static String DQuotes(const String& s){ 
		String res;		 
		res.length = s.length+2; 
		res.body=new char[res.length+1]; 
		res.body[0] = '"'; 
		res.body[res.length-1] = '"'; 
		res.body[res.length] = '\0';	 
		for (long i=0; i< s.length; ++i) 
			res.body[i+1]=s.body[i]; 
		return res; 
	} 
	 
	String Replace(char f, char t){ 
		String res; 
		res.length = length; 
		res.body = new char[length]; 
		res.body[length]='\0';		 
		for (long i = 0; i < length; ++i)  
			res.body[i]= (body[i]==f)? t: body[i];		 
		return res; 
	} 
	String Remove(char f){
		String res;
		for (long i=0; i<length; ++i){
			if (body[i]!=f)
				res.addChar(body[i]);
		}
		return res;
	}
	bool isEmpty() const{ 
		return length==0; 
	} 
	bool contains(const String& s) const{ 
		bool res = false; 
		bool search_on=false; 
		if (s.isEmpty()) return true; 
		if (s.length>length) return false; 
		long k=0; 
		long start=-1; 
		for (long i=0; i<length; ++i){ 
			if (search_on){ 
				if (k<s.length){ 
					if (body[i]==s.body[k]){ 
						k++; 
					}else { 
						//обрыв поиска. 
						search_on =false; 
						k=0; 
						start=-1; 
					} 
				} 
				else { 
					//printf("starts with %ld", start); 
					return true; 
				} 
			}else { 
				if (body[i]== s.body[0]){ 
					k=1; 
					start=i; 
					search_on=true; 
					//printf("search started %d\n", start); 
				} 
			} 
		} 
		if (search_on) { 
			//printf("starts with %ld\n", start); 
			res=true; 
		} 
		return res; 
	} 
	String toUpper(){
		String res = String(this->getCharArray());
		for (long i=0; i<length; ++i)
			res.body[i]= toupper(body[i]);
		res.println();
		return res;
	}
	long getLength(){ 
		return length; 
	} 
	bool operator==(const String&s) const { 
		if (length!=s.length) return false; 
		for (long i=0; i<length; ++i){ 
			if (body[i]!=s.body[i]) return false; 
		} 
		return true; 
	} 
/* регистр. 
 #include <stdio.h> 
#include <ctype.h> 
  
int main(void) { 
    FILE * f; 
    int c; 
  
    if ( ! ( f = fopen("file.txt", "r") ) ) 
        return -1; 
  
    while ( ( c = fgetc(f) ) != EOF ) 
        putchar( isupper(c) ? tolower(c) : toupper(c) ); 
  
    return ( fclose(f) ); 
}  
 */ 
	 
};   
   
String operator+(const String& a, const String& b){   
	String res = String();   
	res.length =a.length+b.length;   
	res.body = new char[res.length+1];   
	for (long i=0; i<a.length; ++i)   
		res.body[i]=a.body[i];   
	for (long i=0; i<b.length; ++i)   
		res.body[i+a.length]=b.body[i];   
	res.body[res.length]='\0'; 
	return res;	   
}
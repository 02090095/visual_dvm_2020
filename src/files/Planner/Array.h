#include <stdio.h>  
#include <stdlib.h>  
#include <string.h> 
#pragma once 
template <class T>
class Array {
protected:
	long length;  
	T** elements;
public:
	Array(){
		length=0;
		elements=NULL;
	}
	virtual ~Array(){
		if (elements !=NULL){
			for (long i=0; i<length; ++i)
				delete elements[i];			
			delete [] elements;
		}
	}
	void add(T * new_line) { 
		T ** buf = new T*[length + 1]; 
		for (long i = 0; i < length; ++i) { 
			buf[i] = elements[i]; 
		} 
		buf[length] = new_line;  
		length++; 
		delete[] elements; 
		elements = buf; 
		buf = NULL; 
	}
	long getLength(){
		return length;
	}
	T * get(long i){
		return elements[i];	
	}
	T** getElements(){
		return elements;
	}
};
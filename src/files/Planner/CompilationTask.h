#include "Task.h"    
#pragma once    
class CompilationTask: public Task {    
	String test_id; 
	String makefile_text;	    
public:   	  
	void setTestId(String * test_id_in){    
		test_id = String(test_id_in->getCharArray());	    
	}    
	void setMakefileText(String * makefile_text_in){    
		makefile_text = String(makefile_text_in->getCharArray(), '|');    
	}    
	virtual void print(){	    
		printf("id=%ld; maxtime=%d; test_id=%s\n", id, maxtime, 
		test_id.getCharArray());    
		printf("makefile_text=%s\n", makefile_text.getCharArray());    
	}    
	CompilationTask(Text * lines, int offset):Task(lines,offset) {   
		setTestId(lines->get(offset+2));		      
		setMakefileText(lines->get(offset+3));	  
		setState(Waiting);     
		kernels=1;
	}  
	 
	virtual void prepareWorkspace(){ 
		String makeFilePath = String(id)+"/Makefile"; 
		File makeFileFile = File(makeFilePath, this->makefile_text); 
		String tests = userWorkspace+"/projects"; 
		String testPath= tests+"/"+test_id; 
		String copyCommand = "cp -r " + String::DQuotes(testPath + "/.") + " "+ String::DQuotes(workspace); 
		system(copyCommand.getCharArray()); 
	}   
	virtual String getLaunchScriptText(){		 
		String modules = userWorkspace+"/modules"; 
		String starterCall  = modules+"/starter";	 
		String launcherCall = modules+"/launcher"; 
		return String::DQuotes(starterCall)+" "+ 
			String::DQuotes(launcherCall)+" "+ 
			String(maxtime)+" "+ 
			String::DQuotes("")+" "+ 
			"make -j -f Makefile"; 
	} 
	virtual void analyseResults(){
		String binary = workspace+"/0"; 
		state = Utils::Exists(binary)? Done:DoneWithErrors; 
	}
};
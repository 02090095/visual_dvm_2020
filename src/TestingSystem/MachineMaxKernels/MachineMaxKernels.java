package TestingSystem.MachineMaxKernels;
import Common.Database.DBObject;
import TestingSystem.MachineKernels.MachineKernels;
import com.sun.org.glassfish.gmbal.Description;
public class MachineMaxKernels extends MachineKernels {
    @Description("DEFAULT 4")
    public int limit = 4;
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        MachineMaxKernels mk = (MachineMaxKernels) src;
        limit = mk.limit;
    }
    public MachineMaxKernels(MachineMaxKernels src) {
        this.SynchronizeFields(src);
    }
    public MachineMaxKernels() {
    }
    public MachineMaxKernels(String URL_in) {
        super(URL_in);
    }
}

package TestingSystem.MachineMaxKernels;
import Common.Database.DBTable;
public class MachineMaxKernelsDBTable extends DBTable<String, MachineMaxKernels> {
    public MachineMaxKernelsDBTable() {
        super(String.class, MachineMaxKernels.class);
    }
}

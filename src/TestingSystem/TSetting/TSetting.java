package TestingSystem.TSetting;
import Common.Database.DBObject;
import GlobalData.Settings.SettingName;
import com.sun.org.glassfish.gmbal.Description;
public class TSetting extends DBObject {
    @Description("PRIMARY KEY,UNIQUE")
    public SettingName Name;
    public long value;
    @Override
    public Object getPK() {
        return Name;
    }
    public TSetting(SettingName name_in, long value_in) {
        Name = name_in;
        value = value_in;
    }
    public TSetting(SettingName name_in, boolean value_in) {
        this(name_in, value_in ? 1 : 0);
    }
    public TSetting() {
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        TSetting t = (TSetting) src;
        Name = t.Name;
        value = t.value;
    }
    public TSetting(TSetting src) {
        this.SynchronizeFields(src);
    }
    public boolean toBoolean(){
        return value==1;
    }
}

package TestingSystem.TSetting;
import Common.Database.DBTable;
public class TSettingsDBTable extends DBTable<String, TSetting> {
    public TSettingsDBTable() {
        super(String.class, TSetting.class);
    }
}

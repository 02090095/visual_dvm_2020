package TestingSystem.Group;
import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.Menus_2023.GroupsMenuBar.GroupsMenuBar;
import Common.UI.Menus_2023.VisualiserMenu;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import ProjectData.LanguageName;
import TestingSystem.Group.UI.GroupFields;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestType;

import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Vector;
//-
public class GroupsDBTable extends DBTable<String, Group> {
    public Vector<TableFilter<Group>> typeFilters;
    public Vector<TableFilter<Group>> languageFilters;
    //------------------------------------------------>>>
    public GroupsDBTable() {
        super(String.class, Group.class);
        if (Current.hasUI()) {
            //--
            typeFilters = new Vector<>();
            languageFilters = new Vector<>();
            //--
            for (TestType type : TestType.values()) {
                typeFilters.add(
                        new TableFilter<Group>(this, type.getDescription()) {
                            @Override
                            protected boolean validate(Group object) {
                                return object.type.equals(type);
                            }
                        });
            }
            //--
            for (LanguageName languageName : LanguageName.values()) {
                languageFilters.add(new TableFilter<Group>(this, languageName.getDescription()) {
                    @Override
                    protected boolean validate(Group object) {
                        return object.language.equals(languageName);
                    }
                });
            }
        }
    }
    @Override
    public void mountUI(JPanel content_in) {
        super.mountUI(content_in);
        //---
        GroupsMenuBar menuBar = (GroupsMenuBar) UI.menuBars.get(getClass());
        menuBar.DropFilters();
        //----
        menuBar.addFilters(
                new VisualiserMenu("Тип", "/icons/Filter.png", true) {
                    {
                        for (TableFilter filter : typeFilters)
                            add(filter.menuItem);
                    }
                },
                new VisualiserMenu("Язык", "/icons/Filter.png", true) {
                    {
                        for (TableFilter filter : languageFilters)
                            add(filter.menuItem);
                    }
                }
        );
    }
    public void ResetFiltersCount() {
        for (TableFilter filter : typeFilters)
            filter.count = 0;
        for (TableFilter filter : languageFilters)
            filter.count = 0;
    }
    public void ShowFiltersCount() {
        for (TableFilter filter : typeFilters)
            filter.ShowDescriptionAndCount();
        for (TableFilter filter : languageFilters)
            filter.ShowDescriptionAndCount();
    }
    public boolean applyFilters(Group object) {
        for (TableFilter filter : typeFilters)
            if (!filter.Validate(object)) return false;
        for (TableFilter filter : languageFilters)
            if (!filter.Validate(object)) return false;
        return true;
    }
    @Override
    public void ShowUI() {
        ResetFiltersCount();
        super.ShowUI();
        ShowFiltersCount();
    }
    @Override
    public void ShowUI(Object key) {
        ResetFiltersCount();
        super.ShowUI(key);
        ShowFiltersCount();
    }
    @Override
    public String getSingleDescription() {
        return "группа тестов";
    }
    @Override
    public String getPluralDescription() {
        return "группы";
    }
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(Test.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                //columns.get(0).setVisible(false);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "имя",
                "автор",
                "тип",
                "язык"
        };
    }
    @Override
    public Object getFieldAt(Group object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.description;
            case 3:
                return object.sender_name;
            case 4:
                return object.type.getDescription();
            case 5:
                return object.language.getDescription();
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.Group;
    }
    @Override
    public DBObjectDialog<Group, GroupFields> getDialog() {
        return new DBObjectDialog<Group, GroupFields>(GroupFields.class) {
            @Override
            public int getDefaultHeight() {
                return 250;
            }
            @Override
            public int getDefaultWidth() {
                return 400;
            }
            @Override
            public void validateFields() {
            }
            @Override
            public void fillFields() {
                fields.tfName.setText(Result.description);
                UI.TrySelect(fields.cbType, Result.type);
                UI.TrySelect(fields.cbLanguage, Result.language);
            }
            @Override
            public void ProcessResult() {
                Result.description = fields.tfName.getText();
                Result.type = (TestType) fields.cbType.getSelectedItem();
                Result.language = (LanguageName) fields.cbLanguage.getSelectedItem();
            }
        };
    }
}

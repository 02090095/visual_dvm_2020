package TestingSystem.Group.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import ProjectData.LanguageName;
import TestingSystem.Test.TestType;

import javax.swing.*;
import java.awt.*;
public class GroupFields implements DialogFields {
    public JPanel content;
    public JTextField tfName;
    public JComboBox<TestType> cbType;
    public JComboBox<LanguageName> cbLanguage;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName = new StyledTextField();
        //-
        cbType = new JComboBox<>();
        cbType.addItem(TestType.Default);
        cbType.addItem(TestType.Performance);
        cbType.addItem(TestType.Correctness);
        cbType.addItem(TestType.SAPFOR);
        //-
        cbLanguage = new JComboBox<>();
        cbLanguage.addItem(LanguageName.fortran);
        cbLanguage.addItem(LanguageName.c);
    }
}

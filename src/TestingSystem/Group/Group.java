package TestingSystem.Group;
import Common.Current;
import Common.Database.DBObject;
import Common.Database.rDBObject;
import Common.UI.UI;
import ProjectData.LanguageName;
import TestingSystem.Test.TestType;
import com.sun.org.glassfish.gmbal.Description;

import java.util.LinkedHashMap;
public class Group extends rDBObject {
    @Override
    public boolean isVisible() {
        return GroupInterface.isVisible(this);
    }
    @Description("DEFAULT 'Default'")
    public TestType type = TestType.Default;
    @Description("DEFAULT 'fortran'")
    public LanguageName language = LanguageName.fortran;
    @Description("IGNORE")
    public LinkedHashMap<String, byte[]> testsFiles = new LinkedHashMap<>(); //транспорт.
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        Group g = (Group) src;
        type = g.type;
        language = g.language;
    }
    public Group(Group group) {
        this.SynchronizeFields(group);
    }
    public Group() {
    }
    @Override
    public void select(boolean flag) {
        super.select(flag);
        if (Current.hasUI())
            UI.getMainWindow().getTestingWindow().ShowCheckedTestsCount();
    }
}

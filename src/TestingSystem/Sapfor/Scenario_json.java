package TestingSystem.Sapfor;
import Visual_DVM_2021.Passes.PassCode_2021;
import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.Vector;
public class Scenario_json {
    @Expose
    public String flags;
    @Expose
    public List<PassCode_2021> codes = new Vector<>();
    @Expose
    public List<String> tests = new Vector<>();
}

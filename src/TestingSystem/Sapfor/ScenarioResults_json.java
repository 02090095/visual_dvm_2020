package TestingSystem.Sapfor;
import TestingSystem.Sapfor.SapforTask.SapforTask_2023;
import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.Vector;
public class ScenarioResults_json {
    @Expose
    public List<SapforTask_2023> tasks = new Vector<>();
}

package TestingSystem.Sapfor.SapforTask;
import Common.Database.DBObject;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackage_2023;
public class SapforTaskResult extends DBObject {
    public SapforTasksPackage_2023 sapforTasksPackage;
    public SapforTask_2023 task;
    public MatchState match_state = MatchState.Unknown;
    @Override
    public Object getPK() {
        return task.test_description;
    }
    public SapforTaskResult(SapforTasksPackage_2023 package_in, SapforTask_2023 task_in) {
        sapforTasksPackage = package_in;
        task = task_in;
    }
}

package TestingSystem.Sapfor.SapforTask;
import Common.Database.DBObject;
import Common.Utils.Utils;
import GlobalData.Tasks.TaskState;
import TestingSystem.Sapfor.SapforVersion_json;
import com.google.gson.annotations.Expose;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Vector;
public class SapforTask_2023 extends DBObject {
    //------------------------------------>>
    @Description("PRIMARY KEY, UNIQUE")
    @Expose
    public long id = Utils.Nan;
    @Description("DEFAULT '-1'")
    @Expose
    public int sapfortaskspackage_2023_id = Utils.Nan;
    //------------------------------------->>
    @Description("DEFAULT ''")
    @Expose
    public String test_description = "";
    //-------------------------------------->>
    @Description("IGNORE")
    @Expose
    public Vector<SapforVersion_json> versions = new Vector<>();
    @Description("IGNORE")
    @Expose
    public Vector<SapforVersion_json> variants = new Vector<>();
    //-------------------------------------->>
    @Description("DEFAULT 'Inactive'")
    @Expose
    public TaskState state = TaskState.Inactive;
    @Description("DEFAULT '0'")
    @Expose
    public int versions_tree_built = 0;
    //-----------
    public SapforTask_2023() {
    }
    public SapforTask_2023(SapforTask_2023 src) {
        this.SynchronizeFields(src);
    }
    @Override
    public Object getPK() {
        return id;
    }
    @Override
    public void SynchronizeFields(DBObject object) {
        super.SynchronizeFields(object);
        SapforTask_2023 t = (SapforTask_2023) object;
        id = t.id;
        sapfortaskspackage_2023_id = t.sapfortaskspackage_2023_id;
        test_description = t.test_description;
        state = t.state;
    }
}

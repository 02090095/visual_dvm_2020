package TestingSystem.Sapfor.SapforTask;
import Common.Current;
import Common.UI.StatusEnum;
import Common.UI.Themes.VisualiserFonts;

import java.awt.*;
public enum MatchState implements StatusEnum {
    Unknown,
    Match,
    NotMatch;
    public String getDescription() {
        switch (this) {
            case Unknown:
                return "неизвестно";
            case Match:
                return "да";
            case NotMatch:
                return "нет";
            default:
                return "?";
        }
    }
    @Override
    public Font getFont() {
        switch (this) {
            case Unknown:
                return Current.getTheme().Fonts.get(VisualiserFonts.UnknownState);
            case Match:
                return Current.getTheme().Fonts.get(VisualiserFonts.GoodState);
            case NotMatch:
                return Current.getTheme().Fonts.get(VisualiserFonts.BadState);
            default:
                return StatusEnum.super.getFont();
        }
    }
}

package TestingSystem.Sapfor.SapforTask;
import Common.Current;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import static Common.UI.Tables.TableRenderers.RendererStatusEnum;
public class SapforTasksDBTable extends DBTable<Long, SapforTask_2023> {
    public SapforTasksDBTable() {
        super(Long.class, SapforTask_2023.class);
    }
    @Override
    public String getSingleDescription() {
        return "задача";
    }
    @Override
    public String getPluralDescription() {
        return "задачи";
    }
    @Override
    public Current CurrentName() {
        return Current.SapforTask;
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            protected void AdditionalInitColumns() {
                columns.get(2).setRenderer(RendererStatusEnum);
            }
            @Override
            public void MouseAction2() throws Exception {
                Pass_2021.passes.get(PassCode_2021.OpenSapforTest).Do();
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Тест",
                "Статус"
        };
    }
    @Override
    public Object getFieldAt(SapforTask_2023 object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.test_description;
            case 2:
                return object.state;
            default:
                return null;
        }
    }
}

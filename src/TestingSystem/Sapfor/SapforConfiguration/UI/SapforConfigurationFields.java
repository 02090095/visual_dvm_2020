package TestingSystem.Sapfor.SapforConfiguration.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class SapforConfigurationFields implements DialogFields {
    private JPanel content;
    public JTextField tfName;
    public JSpinner sMaxtime;
    public JSpinner sTransformationMaxtime;
    public JCheckBox cbFREE_FORM;
    public JSlider sMAX_SHADOW_WIDTH;
    public JCheckBox cbSTATIC_SHADOW_ANALYSIS;
    public JCheckBox cbKEEP_SPF_DIRECTIVES;
    public JCheckBox cbKEEP_DVM_DIRECTIVES;
    //--
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName = new StyledTextField();
    }
    public SapforConfigurationFields(){
        sMaxtime.setModel(new SpinnerNumberModel(40,
                5, 3600, 1
        ));
    }
}

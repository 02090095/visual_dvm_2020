package TestingSystem.Sapfor.SapforConfiguration;
import Common.Database.rDBObject;
public class SapforConfiguration extends rDBObject {
    //настройки.
    public int maxtime = 300; //лимит времени преобразования.
    public int FREE_FORM = 0; //"Свободный выходной стиль"; -f90
    public int STATIC_SHADOW_ANALYSIS=0;//"Оптимизация теневых обменов"; -sh
    public int MAX_SHADOW_WIDTH=50; // "Максимальный размер теневых граней"; (%) -shwidth значение поля
    public int KEEP_SPF_DIRECTIVES=0; //"Сохранять SPF директивы при построении параллельных вариантов"; -keepSPF
    public int KEEP_DVM_DIRECTIVES = 0;// "Учитывать DVM директивы"; -keepDVM
}

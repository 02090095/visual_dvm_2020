package TestingSystem.Sapfor.SapforConfiguration;
import Common.Current;
import Common.Database.*;
import Common.Global;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import TestingSystem.Sapfor.SapforConfiguration.UI.SapforConfigurationFields;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommand;

import java.util.LinkedHashMap;
public class SapforConfigurationDBTable extends DBTable<String, SapforConfiguration> {
    public SapforConfigurationDBTable() {
        super(String.class, SapforConfiguration.class);
    }
    @Override
    public Current CurrentName() {
        return Current.SapforConfiguration;
    }
    @Override
    public String getSingleDescription() {
        return "конфигурация тестирования SAPFOR";
    }
    @Override
    public String getPluralDescription() {
        return "конфигурации";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this){
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                Global.db.sapforTasksPackages.ShowUI();
            }
            @Override
            public void ShowNoCurrentObject() throws Exception {
                super.ShowNoCurrentObject();
                Global.db.sapforTasksPackages.ClearUI();
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "имя",
                "автор",
                "флаги",
                "maxtime"
        };
    }
    @Override
    public Object getFieldAt(SapforConfiguration object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.description;
            case 3:
                return object.sender_name;
            case 4:
                return SapforConfigurationInterface.getFlags(object);
            case 5:
                return object.maxtime;
            default:
                return null;
        }
    }
    //--
    @Override
    public DBObjectDialog<SapforConfiguration, SapforConfigurationFields> getDialog() {
        return new DBObjectDialog<SapforConfiguration, SapforConfigurationFields>(SapforConfigurationFields.class) {
            @Override
            public int getDefaultHeight() {
                return 415;
            }
            @Override
            public int getDefaultWidth() {
                return 600;
            }
            @Override
            public void validateFields() {
            }
            @Override
            public void fillFields() {
                fields.tfName.setText(Result.description);
                fields.cbFREE_FORM.setSelected(Result.FREE_FORM != 0);
                fields.cbKEEP_DVM_DIRECTIVES.setSelected(Result.KEEP_DVM_DIRECTIVES != 0);
                fields.cbKEEP_SPF_DIRECTIVES.setSelected(Result.KEEP_SPF_DIRECTIVES != 0);
                fields.cbSTATIC_SHADOW_ANALYSIS.setSelected(Result.STATIC_SHADOW_ANALYSIS != 0);
                fields.sMAX_SHADOW_WIDTH.setValue(Result.MAX_SHADOW_WIDTH);
                fields.sMaxtime.setValue(Result.maxtime);
            }
            @Override
            public void ProcessResult() {
                Result.description = fields.tfName.getText();
                Result.maxtime = (int) fields.sMaxtime.getValue();
                // Result.transformations = ((StyledStringList) (fields.transformationsList)).pack();
                Result.FREE_FORM = Utils.fromBoolean(fields.cbFREE_FORM.isSelected());
                Result.KEEP_DVM_DIRECTIVES = Utils.fromBoolean(fields.cbKEEP_DVM_DIRECTIVES.isSelected());
                Result.KEEP_SPF_DIRECTIVES = Utils.fromBoolean(fields.cbKEEP_SPF_DIRECTIVES.isSelected());
                Result.STATIC_SHADOW_ANALYSIS = Utils.fromBoolean(fields.cbSTATIC_SHADOW_ANALYSIS.isSelected());
                Result.MAX_SHADOW_WIDTH = fields.sMAX_SHADOW_WIDTH.getValue();
            }
            @Override
            public void SetReadonly() {
                fields.tfName.setEnabled(false);
                fields.sMaxtime.setEnabled(false);
                fields.sTransformationMaxtime.setEnabled(false);
            }
        };
    }
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(SapforConfigurationCommand.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }

}

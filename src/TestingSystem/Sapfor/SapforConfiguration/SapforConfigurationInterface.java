package TestingSystem.Sapfor.SapforConfiguration;
import Common.Global;
import Common.Utils.TextLog;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommand;
import Visual_DVM_2021.Passes.PassCode_2021;

import java.util.Vector;
public class SapforConfigurationInterface {
    public static String getFlags(SapforConfiguration object) {
        Vector<String> res = new Vector<>();
        if (object.FREE_FORM > 0)
            res.add("-f90");
        if (object.STATIC_SHADOW_ANALYSIS > 0)
            res.add("-sh");
        if (object.MAX_SHADOW_WIDTH > 0)
            res.add("-shwidth " + object.MAX_SHADOW_WIDTH);
        if (object.KEEP_DVM_DIRECTIVES > 0)
            res.add("-keepDVM");
        if (object.KEEP_SPF_DIRECTIVES > 0)
            res.add("-keepSPF");
        return String.join(" ", res);
    }
    public static Vector<PassCode_2021> getPassCodes(SapforConfiguration object) {
        Vector<PassCode_2021> res = new Vector<>();
        for (SapforConfigurationCommand command : Global.testingServer.db.sapforConfigurationCommands.Data.values()) {
            if (command.sapforconfiguration_id.equals(object.id)) {
                res.add(command.passCode);
            }
        }
        return res;
    }
    public static String getTransformationsNames(SapforConfiguration object) {
        Vector<String> res = new Vector<>();
        for (SapforConfigurationCommand command : Global.testingServer.db.sapforConfigurationCommands.Data.values()) {
            if (command.sapforconfiguration_id.equals(object.id)) {
                res.add(command.passCode.getDescription());
            }
        }
        return String.join(";", res);
    }
    //todo вывести.
    public static boolean validateCommands(SapforConfiguration sapforConfiguration, TextLog log) {
        //1. получить список всех команд.
        Vector<SapforConfigurationCommand> commands = new Vector<>();
        int count = 0;
        for (SapforConfigurationCommand command : Global.testingServer.db.sapforConfigurationCommands.Data.values()) {
            if (command.sapforconfiguration_id.equals(sapforConfiguration.id)) {
                commands.add(command);
                if (command.passCode.equals(PassCode_2021.CreateParallelVariants))
                    count++;
            }
        }
        if ((count == 0) || commands.isEmpty())
            return true;
        //--
        if (count > 2) {
            log.Writeln_("Неверная конфигурация:" + sapforConfiguration.id + ": построение параллельных вариантов возможно единожды.");
            return false;
        }
        //--
        if (count == 1) {
            if (commands.size() == 1) return true;
            if (!commands.lastElement().passCode.equals(PassCode_2021.CreateParallelVariants)) {
                log.Writeln_("Неверная конфигурация:" + sapforConfiguration.id + ": построение параллельных вариантов может быть только завершающей командой!");
                return false;
            }
            ;
        }
        return true;
    }
}

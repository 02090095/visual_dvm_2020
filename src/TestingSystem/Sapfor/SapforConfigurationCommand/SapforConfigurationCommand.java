package TestingSystem.Sapfor.SapforConfigurationCommand;
import Common.Database.rDBObject;
import Visual_DVM_2021.Passes.PassCode_2021;
import com.sun.org.glassfish.gmbal.Description;
public class SapforConfigurationCommand extends rDBObject {
    @Description("DEFAULT ''")
    public String sapforconfiguration_id = "";
    public PassCode_2021 passCode = PassCode_2021.SPF_RemoveDvmDirectives;
    @Override
    public boolean isVisible() {
        return SapforConfigurationCommandInterface.isVisible(this);
    }
}

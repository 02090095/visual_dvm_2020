package TestingSystem.Sapfor.SapforConfigurationCommand;
import Common.Current;
public class SapforConfigurationCommandInterface {
    public static boolean isVisible(SapforConfigurationCommand object) {
        return Current.HasSapforConfiguration() && (Current.getSapforConfiguration().id.equals(object.sapforconfiguration_id));
    }
}

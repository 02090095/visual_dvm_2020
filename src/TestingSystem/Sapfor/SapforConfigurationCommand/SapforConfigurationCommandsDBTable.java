package TestingSystem.Sapfor.SapforConfigurationCommand;
import Common.Current;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import TestingSystem.Sapfor.SapforConfigurationCommand.UI.SapforConfigurationCommandFields;
import Visual_DVM_2021.Passes.PassCode_2021;
public class SapforConfigurationCommandsDBTable extends DBTable<String, SapforConfigurationCommand> {
    public SapforConfigurationCommandsDBTable() {
        super(String.class, SapforConfigurationCommand.class);
    }
    @Override
    public String getSingleDescription() {
        return "команда";
    }
    @Override
    public String getPluralDescription() {
        return "команды";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Проход"
        };
    }
    @Override
    public Object getFieldAt(SapforConfigurationCommand object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.passCode.getDescription();
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.SapforConfigurationCommand;
    }
    @Override
    public DBObjectDialog<SapforConfigurationCommand, SapforConfigurationCommandFields> getDialog() {
        return new DBObjectDialog<SapforConfigurationCommand, SapforConfigurationCommandFields>(SapforConfigurationCommandFields.class) {
            @Override
            public int getDefaultHeight() {
                return 250;
            }
            @Override
            public void fillFields() {
                UI.TrySelect(fields.cbPassCode, Result.passCode);
            }
            @Override
            public void ProcessResult() {
                Result.passCode = (PassCode_2021) fields.cbPassCode.getSelectedItem();
                Result.sapforconfiguration_id = Current.getSapforConfiguration().id;
            }
            @Override
            public void validateFields() {
                /*
                Vector<ScenarioCommand> commands = new Vector<>(Current.getSapforConfiguration().getCommands().values());
                if (!commands.isEmpty()&&commands.get(commands.size()-1).passCode.equals(PassCode_2021.CreateParallelVariantsCoverageForScenario)){
                    Log.Writeln_("После построения покрытия вариантов запрещено добавлять другие команды в сценарий.");
                }
                 */
            }
        };
    }
}

package TestingSystem.Sapfor.SapforConfigurationCommand.UI;
import Common.Current;
import Common.UI.Tables.StyledCellLabel;
import Common.UI.Windows.Dialog.DialogFields;
import Repository.Component.Sapfor.Sapfor;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
import java.awt.*;
public class SapforConfigurationCommandFields implements DialogFields {
    private JPanel content;
    public JComboBox<PassCode_2021> cbPassCode;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        cbPassCode = new JComboBox<>();
        cbPassCode.setRenderer((list, value, index, isSelected, cellHasFocus) -> {
            JLabel res = new StyledCellLabel();
            res.setText(value.getDescription());
            res.setBackground(isSelected ?
                    Current.getTheme().selection_background : Current.getTheme().background
            );
            return res;
        });
        //-
        for (PassCode_2021 code : Sapfor.getScenariosCodes())
            cbPassCode.addItem(code);
    }
}

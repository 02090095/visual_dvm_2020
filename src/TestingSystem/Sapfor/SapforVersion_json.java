package TestingSystem.Sapfor;
import com.google.gson.annotations.Expose;
public class SapforVersion_json {
    @Expose
    public String version = "";
    @Expose
    public String description = "";
    public SapforVersion_json(String version_in, String description_in) {
        version = version_in;
        description = description_in;
    }
}

package TestingSystem.Sapfor.SapforTasksPackage;
import Common.Database.DBObject;
import Common.Database.iDBObject;
import TestingSystem.Sapfor.SapforConfiguration.SapforConfiguration;
import TestingSystem.TasksPackage.TasksPackageState;
import TestingSystem.Test.Test;
import Visual_DVM_2021.Passes.PassCode_2021;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Vector;
public class SapforTasksPackage_2023 extends iDBObject {
    public String summary = "";
    //----
    public int FREE_FORM = 0; //"Свободный выходной стиль"; -f90
    public int STATIC_SHADOW_ANALYSIS = 0;//"Оптимизация теневых обменов"; -sh
    public int MAX_SHADOW_WIDTH = 50; // "Максимальный размер теневых граней"; (%) -shwidth значение поля
    public int STATIC_PRIVATE_ANALYSIS = 0; //"Статический анализ приватностей" -priv
    public int KEEP_SPF_DIRECTIVES = 0; //"Сохранять SPF директивы при построении параллельных вариантов"; -keepSPF
    public int KEEP_DVM_DIRECTIVES = 0;// "Учитывать DVM директивы"; -keepDVM
    //---
    @Description("DEFAULT ''")
    public String flags = "";
    @Description("DEFAULT ''")
    public String passesNames = ""; //имена преобразований через ;
    @Description("DEFAULT ''")
    public String testsNames = "";//имена тестов через ;
    //---
    public String sapfor_version = "?";
    //---
    public String workspace = "";
    //---
    public int tasksCount = 0;
    @Description("DEFAULT 0")
    public int versions_tree_built = 0;
    //---
    public double Time; //время выполнения.
    public long StartDate = 0; //дата начала выполнения
    public long ChangeDate = 0;//дата окончания выполнения
    @Description("DEFAULT ''")
    public String sapforconfiguration_id = "";
    //-
    public TasksPackageState state = TasksPackageState.Queued;
    //--
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        SapforTasksPackage_2023 tasksPackage = (SapforTasksPackage_2023) src;
        summary = tasksPackage.summary;
        sapfor_version = tasksPackage.sapfor_version;
        tasksCount = tasksPackage.tasksCount;
        Time = tasksPackage.Time;
        StartDate = tasksPackage.StartDate;
        ChangeDate = tasksPackage.ChangeDate;
        state = tasksPackage.state;
        workspace = tasksPackage.workspace;
        //---
        flags = tasksPackage.flags;
        //---
        FREE_FORM = tasksPackage.FREE_FORM;
        STATIC_SHADOW_ANALYSIS = tasksPackage.FREE_FORM;
        MAX_SHADOW_WIDTH = tasksPackage.MAX_SHADOW_WIDTH;
        STATIC_PRIVATE_ANALYSIS = tasksPackage.STATIC_PRIVATE_ANALYSIS;
        KEEP_SPF_DIRECTIVES = tasksPackage.KEEP_SPF_DIRECTIVES;
        KEEP_DVM_DIRECTIVES = tasksPackage.KEEP_DVM_DIRECTIVES;
    }
    public SapforTasksPackage_2023(SapforTasksPackage_2023 src) {
        this.SynchronizeFields(src);
    }
    public SapforTasksPackage_2023() {
    }
    //----
    @Description("IGNORE")
    public Vector<Test> tests = null;
    @Description("IGNORE")
    public Vector<PassCode_2021> codes = null;
    public SapforTasksPackage_2023(SapforConfiguration configuration_in, Vector<Test> tests_in) {
    }
}


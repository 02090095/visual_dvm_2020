package TestingSystem.Sapfor.SapforTasksPackage;
import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import TestingSystem.Sapfor.SapforTask.SapforTask_2023;

import java.util.Date;
import java.util.LinkedHashMap;

import static Common.UI.Tables.TableRenderers.RendererDate;
import static Common.UI.Tables.TableRenderers.RendererStatusEnum;
public class SapforTasksPackagesDBTable extends iDBTable<SapforTasksPackage_2023> {
    public SapforTasksPackagesDBTable() {
        super(SapforTasksPackage_2023.class);
    }
    @Override
    public Current CurrentName() {
        return Current.SapforTasksPackage;
    }
    @Override
    public String getSingleDescription() {
        return "пакеты задач Sapfor";
    }
    @Override
    public String getPluralDescription() {
        return "пакеты задач";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            protected void AdditionalInitColumns() {
                columns.get(7).setRenderer(RendererDate);
                columns.get(8).setRenderer(RendererDate);
                columns.get(9).setRenderer(RendererStatusEnum);
            }
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                // UI.getNewMainWindow().getTestingWindow().DropTestRunTasksComparison();
            }
            @Override
            public void ShowNoCurrentObject() throws Exception {
                super.ShowNoCurrentObject();
                // UI.getNewMainWindow().getTestingWindow().DropTestRunTasksComparison();
            }
            @Override
            public boolean hasCheckBox() {
                return true;
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "SAPFOR",
                "Флаги",
                "Проходы",
                "Тесты",
                "Задач",
                "Начало",
                "Изменено",
                "Статус"
        };
    }
    @Override
    public Object getFieldAt(SapforTasksPackage_2023 object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.sapfor_version;
            case 3:
                return object.flags;
            case 4:
                return object.passesNames;
            case 5:
                return object.testsNames;
            case 6:
                return object.tasksCount;
            case 7:
                return new Date(object.StartDate);
            case 8:
                return new Date(object.ChangeDate);
            case 9:
                return object.state;
            default:
                return null;
        }
    }

    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(SapforTask_2023.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }
}

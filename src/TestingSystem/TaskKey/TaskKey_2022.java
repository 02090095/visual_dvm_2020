package TestingSystem.TaskKey;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;
public class TaskKey_2022 extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public long task_id = 0;
    public String package_id = "";
    @Override
    public Object getPK() {
        return task_id;
    }
    public TaskKey_2022(TaskKey_2022 src) {
        this.SynchronizeFields(src);
    }
    public TaskKey_2022(long task_id_in, String package_id_in) {
        task_id = task_id_in;
        package_id = package_id_in;
    }
    public TaskKey_2022() {
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        TaskKey_2022 key = (TaskKey_2022) src;
        task_id = key.task_id;
        package_id = key.package_id;
    }
}

package TestingSystem.TaskKey;
import Common.Database.DBTable;
public class TaskKeysDBTable extends DBTable<Long, TaskKey_2022> {
    public TaskKeysDBTable() {
        super(Long.class, TaskKey_2022.class);
    }
}

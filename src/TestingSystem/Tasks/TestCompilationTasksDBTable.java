package TestingSystem.Tasks;
import Common.Current;
import Common.Database.DBTable;
public class TestCompilationTasksDBTable extends DBTable<Integer, TestCompilationTask> {
    public TestCompilationTasksDBTable() {
        super(Integer.class, TestCompilationTask.class);
    }
    @Override
    public String getSingleDescription() {
        return "задачи на компиляцию тестов";
    }
    @Override
    public Current CurrentName() {
        return Current.TestCompilationTask;
    }
}

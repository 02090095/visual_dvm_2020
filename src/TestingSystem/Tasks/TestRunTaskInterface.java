package TestingSystem.Tasks;
import Common.Current;
import Common.Global;
import Common.Utils.StringTemplate;
import GlobalData.Tasks.TaskState;
import javafx.util.Pair;

import java.util.List;
public class TestRunTaskInterface {
    public static String filterName = "";
    public static boolean isVisible(TestRunTask object) {
        return
                Current.HasTasksPackage() &&
                        object.taskspackage_id.equals(Current.getTasksPackage().id) &&
                        object.test_id.contains(filterName) &&
                        Global.testingServer.account_db.testRunTasks.applyFilters(object);
    }
    public static String getEnvironments(TestRunTask object) {
        return object.environments.replace("\n", ";");
    }
    public static String getUsrPar(TestRunTask object) {
        return object.usr_par.replace("\n", ";");
    }
    //--
    public static boolean isCrushedLine(String line) {
        return line.contains("RTS err")
                || line.contains("RTS stack trace")
                || line.contains("RTS fatal err")
                || line.contains("SIGEGV")
                || line.contains("There are not enough slots available in the system to satisfy the")
                || line.contains("forrtl: severe")
                || line.contains("invalid pointer")
                || line.contains("forrtl: error");
    }
    public static boolean isCrushed(List<String> output_lines, List<String> errors_lines) {
        return output_lines.stream().anyMatch(TestRunTaskInterface::isCrushedLine) || errors_lines.stream().anyMatch(TestRunTaskInterface::isCrushedLine);
    }
    public static Pair<TaskState, Integer> analyzeCorrectness(List<String> lines) {
        int complete = 0;
        int errors = 0;
        int total = 0;
        for (String s : lines) {
            String line = s.toUpperCase();
            if (line.contains("COMPLETE")) {
                complete++;
                total++;
            } else if (line.contains("ERROR")) {
                errors++;
                total++;
            }
        }
        return new Pair<>(
                (errors > 0) ? TaskState.DoneWithErrors : ((complete > 0) ? TaskState.Done : TaskState.WrongTestFormat),
                (int) ((((double) complete) / total) * 100)
        );
    }
    public static Pair<TaskState, Integer> analyzePerformance(List<String> lines) {
        StringTemplate stringTemplate = new StringTemplate("Verification =", "");
        for (String line : lines) {
            String param = stringTemplate.check_and_get_param(line);
            if (param != null) {
                switch (param) {
                    case "SUCCESSFUL":
                        return new Pair<>(TaskState.Done, 100);
                    case "UNSUCCESSFUL":
                        return new Pair<>(TaskState.DoneWithErrors, 0);
                    default:
                        break;
                }
            }
        }
        return new Pair<>(TaskState.WrongTestFormat, 0);
    }
    public static double parseCleanTime(String output) {
        double res = 0.0;
        StringTemplate template = new StringTemplate("Time in seconds =", "");
        String p = template.check_and_get_param(output);
        try {
            if (p != null) res = Double.parseDouble(p);
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
        return res;
    }
}

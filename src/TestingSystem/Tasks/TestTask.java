package TestingSystem.Tasks;
import Common.Database.DBObject;
import Common.Utils.Utils;
import GlobalData.Tasks.TaskState;
import TestingSystem.Configuration.Configuration;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestType;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Date;
import java.util.Vector;
//тут все поля должны быть текстовыми. никаких ссылок по ид. мало ли, группу удалят
public class TestTask extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public long id = Utils.Nan;
    @Description("DEFAULT ''")
    public String taskspackage_id = "";
    @Description("DEFAULT ''")
    public String group_id = "";
    @Description("DEFAULT ''")
    public String group_description = ""; //видимое имя группы для юзера
    @Description("DEFAULT ''")
    public String test_id = ""; //ключ - будет генерироваться автоматически.
    @Description("DEFAULT ''")
    public String test_description = ""; //видимое имя теста для юзера
    @Description("DEFAULT ''")
    public String flags = "";
    @Description("DEFAULT 'Inactive'")
    public TaskState state = TaskState.Inactive;
    @Description("DEFAULT ''")
    public String PID = "";
    @Description("DEFAULT 40")
    public int maxtime = 40;
    @Description("DEFAULT ''")
    public String remote_workspace = ""; //вывести. память экономим.
    @Description("DEFAULT ''")
    public String binary_name = ""; //вывести. имя генерим по ид задачи и матрице.
    @Description("DEFAULT 'Default'")
    public TestType test_type = TestType.Default;
    //результаты-------------------------------
    public double Time; //время выполнения.
    public long StartDate = 0; //дата начала выполнения
    public long ChangeDate = 0;//дата изменения
    @Description("DEFAULT ''")
    public String output = "";
    @Description("DEFAULT ''")
    public String errors = "";
    //------------------------------------------------------
    @Override
    public Object getPK() {
        return id;
    }
    public Date getChangeDate() {
        return new Date(ChangeDate);
    }
    //--->>>
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        TestTask t = (TestTask) src;
        id = t.id;
        taskspackage_id = t.taskspackage_id;
        group_id = t.group_id;
        group_description = t.group_description;
        test_id = t.test_id;
        test_description = t.test_description;
        flags = t.flags;
        state = t.state;
        PID = t.PID;
        maxtime = t.maxtime;
        remote_workspace = t.remote_workspace;
        binary_name = t.binary_name;
        test_type = t.test_type;
        Time = t.Time;
        StartDate = t.StartDate;
        ChangeDate = t.ChangeDate;
        output = t.output;
        errors = t.errors;
    }
    public TestTask(TestTask src) {
        this.SynchronizeFields(src);
    }
    public TestTask() {
    }
    public TestTask(Configuration configuration,
                    Group group, Test test, String flags_in) {
        group_id = group.id;
        test_id = test.id;
        group_description = group.description;
        test_description = test.description;
        test_type = group.type;
        flags = flags_in;
    }

    public Vector<String> pack (int kernels){
        return null;
    }
}

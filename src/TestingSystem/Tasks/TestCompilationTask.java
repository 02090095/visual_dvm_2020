package TestingSystem.Tasks;
import Common.Database.DBObject;
import TestingSystem.Configuration.Configuration;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Vector;
//-
public class TestCompilationTask extends TestTask {
    @Description("DEFAULT ''")
    public String makefile_text = "";
    @Description("DEFAULT ''")
    public String test_home = ""; //место где лежит код теста.
    @Description("IGNORE")
    public Vector<TestRunTask> runTasks = null;
    @Override
    public Vector<String> pack(int kernels_in) {
        Vector<String> res = new Vector<>();
        res.add(String.valueOf(id)); //1
        res.add(String.valueOf(maxtime)); //2
        res.add(test_id); //3
        res.add(makefile_text.replace("\n", "|")); //4
        //игнор аргумента. ядро всегда одно.
        return res;
    }
    public TestCompilationTask() {
    }
    public TestCompilationTask(Configuration configuration, Group group, Test test, String flags_in) {
        super(configuration, group, test, flags_in);
        flags = flags_in;
        maxtime = configuration.c_maxtime;
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        TestCompilationTask ct = (TestCompilationTask) src;
        makefile_text = ct.makefile_text;
        test_home = ct.test_home;
        if (ct.runTasks == null) this.runTasks = null;
        else {
            this.runTasks = new Vector<>();
            for (TestRunTask runTask : ct.runTasks) {
                this.runTasks.add(new TestRunTask(runTask));
            }
        }
    }
    public TestCompilationTask(TestCompilationTask src) {
        this.SynchronizeFields(src);
    }
}

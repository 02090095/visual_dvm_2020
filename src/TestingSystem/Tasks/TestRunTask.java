package TestingSystem.Tasks;
import Common.Database.DBObject;
import Common.Utils.Utils;
import GlobalData.Tasks.TaskState;
import ProjectData.LanguageName;
import TestingSystem.Configuration.Configuration;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Vector;
public class TestRunTask extends TestTask {
    //не факт что тут нужно переводить на полный интерфейс. достаточно убрать фильтрацию
    @Override
    public boolean isVisible() {
        return TestRunTaskInterface.isVisible(this);
    }
    //--
    public long testcompilationtask_id = Utils.Nan;
    public String matrix = "";
    @Description("DEFAULT ''")
    public String args = "";
    public double CleanTime = 0.0;
    @Description("DEFAULT 0")
    public int progress = 0;
    public LanguageName language = LanguageName.fortran;
    public int cube = 1;
    public int min_dim = 1;
    public int max_dim = 1;
    public String environments = "";
    public String usr_par = "";
    public int compilation_maxtime = 40;
    public String compilation_output = "";
    public String compilation_errors = "";
    public TaskState compilation_state = TaskState.Waiting;
    public double compilation_time = 0.0;
    public String statistic = "";
    public String jsonStatistic = "";
    public TestRunTask(Configuration configuration,
                       Group group, Test test,
                       String matrix_in, String flags_in,
                       String environments_in,
                       String par_in) {
        super(configuration, group, test, flags_in);
        //--------------------------
        //инфа о компиляции.
        language = group.language;
        compilation_maxtime = configuration.c_maxtime;
        compilation_output = "";
        compilation_errors = "";
        compilation_state = TaskState.Waiting;
        //инфа о запуске
        cube = configuration.cube;
        min_dim = configuration.max_dim_proc_count;
        max_dim = configuration.max_dim_proc_count;
        maxtime = configuration.r_maxtime;
        environments = environments_in;
        usr_par = par_in;
        args = test.args;
        //---------
        matrix = matrix_in;
    }
    public TestRunTask() {
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        TestRunTask rt = (TestRunTask) src;
        testcompilationtask_id = rt.testcompilationtask_id;
        matrix = rt.matrix;
        CleanTime = rt.CleanTime;
        progress = rt.progress;
        language = rt.language;
        cube = rt.cube;
        min_dim = rt.min_dim;
        max_dim = rt.max_dim;
        maxtime = rt.maxtime;
        environments = rt.environments;
        usr_par = rt.usr_par;
        compilation_maxtime = rt.compilation_maxtime;
        compilation_output = rt.compilation_output;
        compilation_errors = rt.compilation_errors;
        compilation_state = rt.compilation_state;
        compilation_time = rt.compilation_time;
        statistic = rt.statistic;
        args = rt.args;
        jsonStatistic = rt.jsonStatistic;
    }
    public TestRunTask(TestRunTask src) {
        this.SynchronizeFields(src);
    }
    //-
    @Override
    public Vector<String> pack(int kernels_in) {
        Vector<String> res = new Vector<>();
        res.add(String.valueOf(id)); //1
        res.add(String.valueOf(maxtime)); //2
        res.add(String.valueOf(testcompilationtask_id)); //3
        res.add(matrix); //4
        res.add(environments); //5
        res.add(usr_par.replace("\n", "|")); //6
        res.add(args); //7
        res.add(String.valueOf(kernels_in)); //8
        return res;
    }
}

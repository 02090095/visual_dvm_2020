package TestingSystem.Tasks;
import Common.Current;
import Common.Database.DBTable;
import Common.Database.TableFilter;
import Common.UI.DataSetControlForm;
import Common.UI.Menus_2023.TestRunTasksMenuBar.TestRunTasksMenuBar;
import Common.UI.Menus_2023.VisualiserMenu;
import Common.UI.UI;
import GlobalData.Tasks.TaskState;

import javax.swing.*;
import java.util.Comparator;
import java.util.Vector;

import static Common.UI.Tables.TableRenderers.RendererProgress;
import static Common.UI.Tables.TableRenderers.RendererStatusEnum;
public class TestRunTasksDBTable extends DBTable<Long, TestRunTask> {
    public Vector<TableFilter<TestRunTask>> compilationFilters;
    public Vector<TableFilter<TestRunTask>> runFilters;
    public TestRunTasksDBTable() {
        super(Long.class, TestRunTask.class);
        //--
        if (Current.hasUI()) {
            compilationFilters = new Vector<>();
            runFilters = new Vector<>();
            //--
            for (TaskState state : TaskState.values()) {
                if (state.isVisible()) {
                    compilationFilters.add(new TableFilter<TestRunTask>(this, state.getDescription()) {
                        @Override
                        protected boolean validate(TestRunTask object) {
                            return object.compilation_state.equals(state);
                        }
                    });
                }
            }
            //--
            for (TaskState state : TaskState.values()) {
                if (state.isVisible()) {
                    runFilters.add(new TableFilter<TestRunTask>(this, state.getDescription()) {
                        @Override
                        protected boolean validate(TestRunTask object) {
                            return object.state.equals(state);
                        }
                    });
                }
            }
        }
    }
    public void ResetFiltersCount() {
        for (TableFilter<TestRunTask> filter : compilationFilters)
            filter.count = 0;
        for (TableFilter<TestRunTask> filter : runFilters)
            filter.count = 0;
    }
    public void ShowFiltersCount() {
        for (TableFilter<TestRunTask> filter : compilationFilters) {
            filter.ShowDescriptionAndCount();
        }
        for (TableFilter<TestRunTask> filter : runFilters) {
            filter.ShowDescriptionAndCount();
        }
    }
    public boolean applyFilters(TestRunTask object) {
        for (TableFilter<TestRunTask> filter : compilationFilters)
            if (!filter.Validate(object)) return false;
        for (TableFilter<TestRunTask> filter : runFilters)
            if (!filter.Validate(object)) return false;
        return true;
    }
    @Override
    public String getSingleDescription() {
        return "задача";
    }
    @Override
    public String getPluralDescription() {
        return "задачи";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(2).setVisible(false);
                columns.get(6).setRenderer(RendererStatusEnum);
                columns.get(7).setRenderer(RendererStatusEnum);
                columns.get(14).setRenderer(RendererProgress);
            }
        };
    }
    @Override
    public void mountUI(JPanel content_in) {
        super.mountUI(content_in);
        //-
        TestRunTasksMenuBar menuBar = (TestRunTasksMenuBar) UI.menuBars.get(getClass());
        menuBar.DropFilters();
        //----
        menuBar.addFilters(
                new VisualiserMenu("Компиляция", "/icons/Filter.png", true) {
                    {
                        for (TableFilter filter : compilationFilters)
                            add(filter.menuItem);
                    }
                },
                new VisualiserMenu("Запуск", "/icons/Filter.png", true) {
                    {
                        for (TableFilter filter : runFilters)
                            add(filter.menuItem);
                    }
                }
        );
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Группа",
                "Тест",
                "Язык",
                "Флаги",
                "Сборка",
                "Запуск",
                "Время компиляции(с)",
                "Матрица",
                "Окружение",
                "usr.par",
                "Время выполнения (с)",
                "Чистое время (с)",
                "Прогресс",
        };
    }
    @Override
    public Object getFieldAt(TestRunTask object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.group_description;
            case 3:
                return object.test_description;
            case 4:
                return object.language;
            case 5:
                return object.flags;
            case 6:
                return object.compilation_state;
            case 7:
                return object.state;
            case 8:
                return object.compilation_time;
            case 9:
                return object.matrix;
            case 10:
                return TestRunTaskInterface.getEnvironments(object);
            case 11:
                return TestRunTaskInterface.getUsrPar(object);
            case 12:
                return object.Time;
            case 13:
                return object.CleanTime;
            case 14:
                return object.progress;
            default:
                return null;
        }
    }
    @Override
    public Comparator<TestRunTask> getComparator() {
        return (o1, o2) -> -o1.getChangeDate().compareTo(o2.getChangeDate());
    }
    @Override
    public Current CurrentName() {
        return Current.TestRunTask;
    }
    @Override
    public void ShowUI() {
        ResetFiltersCount();
        super.ShowUI();
        ShowFiltersCount();
    }
    @Override
    public void ShowUI(Object key) {
        ResetFiltersCount();
        super.ShowUI(key);
        ShowFiltersCount();
    }
}

package TestingSystem.MachineKernels;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;
public class MachineKernels extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String URL = "";
    @Override
    public Object getPK() {
        return URL;
    }
    public MachineKernels() {
    }
    public MachineKernels(String URL_in) {
        URL = URL_in;
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        MachineKernels mk = (MachineKernels) src;
        URL = mk.URL;
    }
    public MachineKernels(MachineKernels src) {
        this.SynchronizeFields(src);
    }
}

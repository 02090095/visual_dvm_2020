package TestingSystem.TasksPackage;
import Common.Current;
import Common.UI.StatusEnum;
import Common.UI.Themes.VisualiserFonts;

import java.awt.*;
public enum TasksPackageState implements StatusEnum {
    Queued,
    TestsSynchronize, //оставить.
    PackageWorkspaceCreation,
    PackageStart,
    //
    CompilationWorkspacesCreation,
    CompilationPreparation,
    CompilationExecution,
    //-
    RunningWorkspacesCreation,
    RunningPreparation,
    RunningExecution,
    //--
    RunningEnd, //скачка архива
    Cleaning, //todo удаление папки пакета на удаленной машине. пока отладки ради не делать.
    //---------------------------------------
    Analysis,
    Done,
    Aborted
    ;
    @Override
    public Font getFont() {
        switch (this) {
            case TestsSynchronize:
            case Analysis:
                return Current.getTheme().Fonts.get(VisualiserFonts.BlueState);
            case CompilationExecution:
            case RunningExecution:
                return Current.getTheme().Fonts.get(VisualiserFonts.ProgressState);
            case Done:
                return Current.getTheme().Fonts.get(VisualiserFonts.GoodState);
            default:
                return StatusEnum.super.getFont();
        }
    }
    //-
    public String getDescription() {
        switch (this) {
            case Aborted:
                return "Прерван";
            case Queued:
                return "в очереди";
            case TestsSynchronize:
                return "синхронизация тестов";
            case PackageWorkspaceCreation:
                return "создание рабочей папки пакета";
            case PackageStart:
                return "старт пакета";
            case CompilationWorkspacesCreation:
                return "создание рабочих папок компиляции";
            case CompilationPreparation:
                return "подготовка к компиляции";
            case CompilationExecution:
                return "компиляция";
            case RunningWorkspacesCreation:
                return "создание рабочих папок для запуска";
            case RunningPreparation:
                return "подготовка к запуску";
            case RunningExecution:
                return "запуск";
            case RunningEnd:
                return "загрузка результатов";
            case Analysis:
                return "анализ результатов";
            case Cleaning:
                return "очистка";
            case Done:
                return "завершен";
            default:
                return StatusEnum.super.getDescription();
        }
    }
}

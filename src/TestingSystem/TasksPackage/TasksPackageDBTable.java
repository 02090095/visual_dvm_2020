package TestingSystem.TasksPackage;
import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.Utils.Utils;
import TestingSystem.Tasks.TestRunTask;
import TestingSystem.TestingServer;

import javax.swing.*;
import java.util.Date;
import java.util.LinkedHashMap;

import static Common.UI.Tables.TableRenderers.RendererDate;
import static Common.UI.Tables.TableRenderers.RendererStatusEnum;
public class TasksPackageDBTable extends DBTable<String, TasksPackage> {

    public TasksPackageDBTable() {
        super(String.class, TasksPackage.class);
    }
    @Override
    public Current CurrentName() {
        return Current.TasksPackage;
    }
    @Override
    public String getSingleDescription() {
        return "пакет задач";
    }
    @Override
    public String getPluralDescription() {
        return "пакеты задач";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(6).setRenderer(RendererDate);
                columns.get(7).setRenderer(RendererDate);
                columns.get(8).setRenderer(RendererStatusEnum);
            }
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                UI.getMainWindow().getTestingWindow().DropTestRunTasksComparison();
            }
            @Override
            public void ShowNoCurrentObject() throws Exception {
                super.ShowNoCurrentObject();
                UI.getMainWindow().getTestingWindow().DropTestRunTasksComparison();
            }

        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Машина",
                "Пользователь",
                "DVM",
                "Задач",
                "Начало",
                "Изменено",
                "Статус"
        };
    }
    @Override
    public Object getFieldAt(TasksPackage object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.machine_address + ":" + object.machine_port;
            case 3:
                return object.user_name;
            case 4:
                return object.dvm_version;
            case 5:
                return object.runTasksCount;
            case 6:
                return new Date(object.StartDate);
            case 7:
                return new Date(object.ChangeDate);
            case 8:
                return object.state;
            default:
                return null;
        }
    }
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(TestRunTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }
}

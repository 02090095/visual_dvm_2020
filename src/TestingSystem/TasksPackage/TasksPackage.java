package TestingSystem.TasksPackage;
import Common.Database.DBObject;
import Common.Database.nDBObject;
import GlobalData.Machine.MachineType;
import TestingSystem.Tasks.TestCompilationTask;

import java.util.LinkedHashMap;
import java.util.Vector;
public class TasksPackage extends nDBObject {
    public String pid=""; //сишная часть.
    public String summary = "";
    //---
    public String dvm_version = "?";
    public String sapfor_version = "?";
    public String dvm_drv = "";
    //---
    public String machine_name = "";
    public String machine_address = "";
    public int machine_port = 22;
    public MachineType machine_type;
    public String user_name = "";
    public String user_password;
    public String user_workspace;
    //---
    public int compilationTasksCount = 0;
    public int runTasksCount = 0;
    public int needsEmail = 0;
    //---
    public double Time; //время выполнения.
    public long StartDate = 0; //дата начала выполнения
    public long ChangeDate = 0;//дата окончания выполнения
    //-
    public TasksPackageState state = TasksPackageState.Queued;
    //--
    //нужно только для публикации задач.
    public LinkedHashMap<String, LinkedHashMap<String, Vector<TestCompilationTask>>> sorted_tasks = new LinkedHashMap<>();
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        TasksPackage tasksPackage = (TasksPackage) src;
        pid = tasksPackage.pid;
        summary = tasksPackage.summary;
        dvm_drv = tasksPackage.dvm_drv;
        dvm_version = tasksPackage.dvm_version;
        sapfor_version = tasksPackage.sapfor_version;
        machine_name = tasksPackage.machine_name;
        machine_address = tasksPackage.machine_address;
        machine_port = tasksPackage.machine_port;
        machine_type = tasksPackage.machine_type;
        user_name = tasksPackage.user_name;
        user_workspace = tasksPackage.user_workspace;
        user_password = tasksPackage.user_password;
        compilationTasksCount = tasksPackage.compilationTasksCount;
        runTasksCount = tasksPackage.runTasksCount;
        needsEmail = tasksPackage.needsEmail;
        Time = tasksPackage.Time;
        StartDate = tasksPackage.StartDate;
        ChangeDate = tasksPackage.ChangeDate;
        sorted_tasks = new LinkedHashMap<>();
        state = tasksPackage.state;
        //-
        for (String group_id : tasksPackage.sorted_tasks.keySet()) {
            LinkedHashMap<String, Vector<TestCompilationTask>> src_groupTasks = tasksPackage.sorted_tasks.get(group_id);
            LinkedHashMap<String, Vector<TestCompilationTask>> dst_groupTasks = new LinkedHashMap<>();
            for (String test_id : src_groupTasks.keySet()) {
                Vector<TestCompilationTask> src_testTasks = src_groupTasks.get(test_id);
                Vector<TestCompilationTask> dst_testTasks = new Vector<>();
                for (TestCompilationTask src_testCompilationTask : src_testTasks)
                    dst_testTasks.add(new TestCompilationTask(src_testCompilationTask));
                dst_groupTasks.put(test_id, dst_testTasks);
            }
            sorted_tasks.put(group_id, dst_groupTasks);
        }
    }
    public TasksPackage(TasksPackage src) {
        this.SynchronizeFields(src);
    }
    public TasksPackage() {
    }
}

package TestingSystem;
import Common.Database.SQLITE.SQLiteDatabase;
import GlobalData.Settings.SettingName;
import TestingSystem.Configuration.UI.ConfigurationDBTable;
import TestingSystem.Group.GroupsDBTable;
import TestingSystem.MachineMaxKernels.MachineMaxKernelsDBTable;
import TestingSystem.Sapfor.SapforConfiguration.SapforConfigurationDBTable;
import TestingSystem.Sapfor.SapforConfigurationCommand.SapforConfigurationCommandsDBTable;
import TestingSystem.TSetting.TSetting;
import TestingSystem.TSetting.TSettingsDBTable;
import TestingSystem.Test.TestDBTable;

import java.nio.file.Paths;
public class TestsDatabase extends SQLiteDatabase {
    public ConfigurationDBTable configurations;
    public TestDBTable tests;
    public GroupsDBTable groups;
    public MachineMaxKernelsDBTable machinesMaxKernels;
    public TSettingsDBTable settings;
    //--
    public SapforConfigurationDBTable sapforConfigurations;
    public SapforConfigurationCommandsDBTable sapforConfigurationCommands;
 //
    //--
    public TestsDatabase() {
        super(Paths.get(System.getProperty("user.dir"), "Data", TasksDatabase.tests_db_name + ".sqlite").toFile());
    }
    @Override
    protected void initAllTables() throws Exception {
        addTable(configurations = new ConfigurationDBTable());
        addTable(groups = new GroupsDBTable());
        addTable(tests = new TestDBTable());
        addTable(machinesMaxKernels = new MachineMaxKernelsDBTable());
        addTable(settings = new TSettingsDBTable());
        //-
        addTable(sapforConfigurations = new SapforConfigurationDBTable());
        addTable(sapforConfigurationCommands = new SapforConfigurationCommandsDBTable());
    }
    @Override
    public void Init() throws Exception {
        if (!settings.containsKey(SettingName.TaskMaxId))
            Insert(new TSetting(SettingName.TaskMaxId, 63128));
        if (!settings.containsKey(SettingName.SapforTaskMaxId))
            Insert(new TSetting(SettingName.SapforTaskMaxId, 0));
    }
    public long IncMaxTaskId() throws Exception {
        TSetting setting = settings.get(SettingName.TaskMaxId);
        long res = setting.value;
        setting.value++;
        Update(setting);
        return res;
    }

    public long IncSapforMaxTaskId() throws Exception {
        TSetting setting = settings.get(SettingName.SapforTaskMaxId);
        long res = setting.value;
        setting.value++;
        Update(setting);
        return res;
    }
}

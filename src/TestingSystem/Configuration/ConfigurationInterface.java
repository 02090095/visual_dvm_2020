package TestingSystem.Configuration;
import Common.Utils.Utils;
import GlobalData.RunConfiguration.RunConfiguration;

import java.util.Vector;
public class ConfigurationInterface {
    public static Vector<String> getFlags(TestingSystem.Configuration.Configuration object) {
        return Utils.unpackStrings(object.flags);
    }
    public static Vector<String> getEnvironments(TestingSystem.Configuration.Configuration object) {
        return Utils.unpackStrings(object.environments);
    }
    public static Vector<String> getParams(TestingSystem.Configuration.Configuration object) {
        return Utils.unpackStrings(object.usr_par);
    }
    public static Vector<String> getMatrixes(TestingSystem.Configuration.Configuration object, int testDim) {
        Vector<Vector<Integer>> res_ = new Vector<>();
        Vector<String> res = new Vector<>();
        if ((object.max_proc_count==0) || (object.min_dim_proc_count == 0 && object.max_dim_proc_count == 0)) {
            res.add("");
        } else {
            if (testDim > 0) {
                Vector<String> min_border = new Vector<>();
                Vector<String> max_border = new Vector<>();
                for (int i = 1; i <= testDim; ++i) {
                    min_border.add(String.valueOf(object.min_dim_proc_count));
                    max_border.add(String.valueOf(object.max_dim_proc_count));
                }
                Vector<Integer> from = RunConfiguration.getBounds(String.join(" ", min_border));
                Vector<Integer> to = RunConfiguration.getBounds(String.join(" ", max_border));
                if (from.size() != to.size()) {
                    System.out.println("Верхняя и нижняя границы матриц конфигурации имеют разные размерности");
                    return res;
                }
                if (from.size() != testDim) {
                    System.out.println("Границы матриц не совпадают с размерностью конфигурации");
                    return res;
                }
                //1 стадия. заполнение.
                for (int j = from.get(0); j <= to.get(0); ++j) {
                    Vector<Integer> m = new Vector<>();
                    res_.add(m);
                    m.add(j);
                }
                //---
                if (testDim > 1) RunConfiguration.gen_rec(from, to, res_, 1, testDim, object.cube == 1);
                for (Vector<Integer> m : res_) {
                    Vector<String> ms = new Vector<>();
                    int proc = 1;
                    for (int i : m) {
                        ms.add(String.valueOf(i));
                        proc *= i;
                    }
                    if (proc <= object.max_proc_count)
                        res.add(String.join(" ", ms));
                }
            } else res.add("");
        }
        return res;
    }
    public static String getParamsText(Configuration object) {
        Vector<String> params = getParams(object);
        if ((params.size() == 1) && params.get(0).isEmpty()) return "";
        return String.join("\n", params);
    }
    public static String getSummary(Configuration configuration) {
        return configuration.description;
    }
}

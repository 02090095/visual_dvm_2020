package TestingSystem.Configuration.UI;
import Common.Current;
import Common.Database.DBObject;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Tables.TableRenderers;
import Common.UI.VisualiserStringList;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import TestingSystem.Configuration.Configuration;
import TestingSystem.Configuration.ConfigurationInterface;
public class ConfigurationDBTable extends DBTable<String, Configuration> {
    public static boolean email = false;
    public ConfigurationDBTable() {
        super(String.class, Configuration.class);
    }
    @Override
    public Current CurrentName() {
        return Current.Configuration;
    }
    @Override
    public String getSingleDescription() {
        return "конфигурация тестирования";
    }
    @Override
    public String getPluralDescription() {
        return "конфигурации";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                //    columns.get(0).setVisible(false);
                columns.get(4).setRenderer(TableRenderers.RendererMultiline);
                columns.get(5).setRenderer(TableRenderers.RendererMultiline);
                columns.get(12).setRenderer(TableRenderers.RendererMultiline);
            }

        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "имя",
                "автор",
                "флаги",
                "окружение",
                "c_time",
                "куб",
                "max",
                "min dim",
                "max dim",
                "r_time",
                "usr.par"
        };
    }
    @Override
    public Object getFieldAt(Configuration object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.description;
            case 3:
                return object.sender_name;
            case 4:
                return Utils.unpackStrings(object.flags, true);
            case 5:
                return Utils.unpackStrings(object.environments, true);
            case 6:
                return object.c_maxtime;
            case 7:
                return object.cube;
            case 8:
                return object.max_proc_count;
            case 9:
                return object.min_dim_proc_count;
            case 10:
                return object.max_dim_proc_count;
            case 11:
                return object.r_maxtime;
            case 12:
                return Utils.unpackStrings(object.usr_par, true);
            default:
                return null;
        }
    }
    @Override
    public DBObjectDialog<Configuration, ConfigurationFields> getDialog() {
        return new DBObjectDialog<Configuration, ConfigurationFields>(ConfigurationFields.class) {
            @Override
            public int getDefaultHeight() {
                return 480;
            }
            @Override
            public int getDefaultWidth() {
                return 1000;
            }
            @Override
            public void validateFields() {
                int min = (int) fields.sMinDimProc.getValue();
                int max = (int) fields.sMaxDimProc.getValue();
                if (max < min)
                    Log.Writeln_("Некорректный диапазон размерностей: максимум меньше минимума");
                if ((min == 0) && (max != 0) || (min != 0) && (max == 0))
                    Log.Writeln_("Некорректный диапазон размерностей. " +
                            "'0' допускается только одновременно на обеих границах,\n" +
                            "и подразумевает единственный запуск без решётки");
            }
            @Override
            public void fillFields() {
                fields.tfName.setText(Result.description);
                //------->>>>
                ((VisualiserStringList) (fields.flagsList)).fill(ConfigurationInterface.getFlags(Result));
                ((VisualiserStringList) (fields.environmentsList)).fill(ConfigurationInterface.getEnvironments(Result));
                ((VisualiserStringList) (fields.parList)).fill(ConfigurationInterface.getParams(Result));
                //------->>>>
                fields.sCompilationMaxtime.setValue(Result.c_maxtime);
                //-
                fields.sMinDimProc.setValue(Result.min_dim_proc_count);
                fields.sMaxDimProc.setValue(Result.max_dim_proc_count);
                fields.cbCube.setSelected(Result.cube == 1);
                fields.sRunMaxtime.setValue(Result.r_maxtime);
                //-
                fields.sMaxProc.setValue(Result.max_proc_count);
            }
            @Override
            public void ProcessResult() {
                Result.description = fields.tfName.getText();
                Result.c_maxtime = (int) fields.sCompilationMaxtime.getValue();
                Result.min_dim_proc_count = (int) fields.sMinDimProc.getValue();
                Result.max_dim_proc_count = (int) fields.sMaxDimProc.getValue();
                Result.cube = fields.cbCube.isSelected() ? 1 : 0;
                Result.max_proc_count = (int) fields.sMaxProc.getValue();
                Result.r_maxtime = (int) fields.sRunMaxtime.getValue();
                Result.flags = ((VisualiserStringList) (fields.flagsList)).pack();
                Result.environments = ((VisualiserStringList) (fields.environmentsList)).pack();
                Result.usr_par = ((VisualiserStringList) (fields.parList)).pack();
            }
            @Override
            public void SetReadonly() {
                fields.tfName.setEnabled(false);
                fields.sCompilationMaxtime.setEnabled(false);
                fields.sRunMaxtime.setEnabled(false);
                fields.sMinDimProc.setEnabled(false);
                fields.sMaxDimProc.setEnabled(false);
                fields.cbCube.setEnabled(false);
                fields.sMaxProc.setEnabled(false);
                fields.bAddFlags.setEnabled(false);
                fields.bDeleteFlags.setEnabled(false);
                fields.bAddEnvironments.setEnabled(false);
                fields.bDeleteEnvironment.setEnabled(false);
                fields.bAddPar.setEnabled(false);
                fields.bDeletePar.setEnabled(false);
            }
        };
    }
    @Override
    public boolean ShowEditObjectDialog(DBObject object) {
        return (Current.getAccount().CheckAccessRights(((Configuration) object).sender_address, null)) ? super.ShowEditObjectDialog(object) : ViewObject(object);
    }
}

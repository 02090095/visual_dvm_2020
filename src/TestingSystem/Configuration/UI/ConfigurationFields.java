package TestingSystem.Configuration.UI;
import Common.Current;
import Common.UI.VisualiserStringList;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class ConfigurationFields implements DialogFields {
    public JTextField tfName;
    public JSpinner sMinDimProc;
    public JSpinner sMaxDimProc;
    public JSpinner sCompilationMaxtime;
    public JSpinner sRunMaxtime;
    public JCheckBox cbCube;
    public JSpinner sMaxProc;
    public JList<String> flagsList;
    public JList<String> environmentsList;
    public JList<String> parList;
    //-
    private JPanel content;
    private JSplitPane SC1;
    private JSplitPane SC2;
    private JToolBar flagsTools;
    public JButton bAddFlags;
    public JButton bDeleteFlags;
    private JSplitPane SC3;
    private JToolBar parTools;
    public JButton bAddPar;
    public JButton bDeletePar;
    private JToolBar environmentsTools;
    public JButton bAddEnvironments;
    public JButton bDeleteEnvironment;

    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName = new StyledTextField();
        //-
        flagsList = new VisualiserStringList();
        environmentsList = new VisualiserStringList();
        parList = new VisualiserStringList();
    }
    public ConfigurationFields(){
        sMinDimProc.setModel(new SpinnerNumberModel(1, 0, 128, 1));
        sMaxDimProc.setModel(new SpinnerNumberModel(1, 0, 128, 1));
        sMaxProc.setModel(new SpinnerNumberModel(0, 0, 128, 1));
        sCompilationMaxtime.setModel(new SpinnerNumberModel(40,
                5, 3600, 1
        ));
        sRunMaxtime.setModel(new SpinnerNumberModel(40,
                5, 3600, 1
        ));
        bAddFlags.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021 pass = Pass_2021.passes.get(PassCode_2021.PickCompilerOptions);
                if (pass.Do(Current.getCompiler()))
                    ((VisualiserStringList) flagsList).addElement((String) pass.target);
            }
        });
        bDeleteFlags.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((VisualiserStringList) flagsList).removeElement(flagsList.getSelectedValue());
            }
        });
        bAddEnvironments.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021 pass = Pass_2021.passes.get(PassCode_2021.PickCompilerEnvironmentsForTesting);
                if (pass.Do(Current.getCompiler()))
                    ((VisualiserStringList) environmentsList).addElement((String) pass.target);
            }
        });
        bDeleteEnvironment.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((VisualiserStringList) environmentsList).removeElement(environmentsList.getSelectedValue());
            }
        });
        bDeletePar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ((VisualiserStringList) parList).removeElement(parList.getSelectedValue());
            }
        });
        bAddPar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021 pass = Pass_2021.passes.get(PassCode_2021.AddDVMParameterForTesting);
                if (pass.Do()) {
                    ((VisualiserStringList) parList).addElement((String) pass.target);
                }
            }
        });
    }

}

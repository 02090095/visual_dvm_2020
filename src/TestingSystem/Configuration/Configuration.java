package TestingSystem.Configuration;
import Common.Database.DBObject;
import Common.Database.rDBObject;
public class Configuration extends rDBObject {
    //компиляция.
    public String flags = "\n";
    public int c_maxtime = 40;
    //матрица
    public int cube = 1;
    public int max_proc_count = 4;
    public int min_dim_proc_count = 1;
    public int max_dim_proc_count = 4;
    //запуск
    public String environments="\n";
    public String usr_par = "";
    public int r_maxtime = 300;
    //-
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        Configuration c = (Configuration) src;
        flags = c.flags;
        c_maxtime=c.c_maxtime;
        cube= c.cube;
        max_proc_count=c.max_proc_count;
        min_dim_proc_count=c.min_dim_proc_count;
        max_dim_proc_count=c.max_dim_proc_count;
        environments=c.environments;
        usr_par=c.usr_par;
        r_maxtime=c.r_maxtime;
    }
    public Configuration(Configuration src){
        this.SynchronizeFields(src);
    }
    public Configuration(){}
}

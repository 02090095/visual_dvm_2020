package TestingSystem.Test;
public enum TestType {
    Default,
    Correctness,
    Performance,
    SAPFOR,
    ;
    public String getDescription(){
        switch (this){
            case Correctness:
                return "Корректность";
            case Performance:
                return "Производительность";
            case Default:
                return "Без типа";
            case SAPFOR:
                return "SAPFOR";
            default:
                return "?";
        }
    }
}

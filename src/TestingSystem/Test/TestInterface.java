package TestingSystem.Test;
import Common.Current;
import Common.Global;

import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
public class TestInterface {
    public static String filterName = "";
    public static String filterSenderName = "";
    public static boolean filterMyOnly = false;
    public static boolean isVisible(Test object) {
        return Current.HasGroup() && (Current.getGroup().id.equals(object.group_id))
                &&
                object.id.toUpperCase().contains(filterName.toUpperCase()) &&
                object.sender_name.toUpperCase().contains(filterSenderName.toUpperCase()) &&
                (!filterMyOnly || object.sender_address.equalsIgnoreCase(Current.getAccount().email));
    }
    //-
    public static Date getDate(Test object) {
        return new Date(object.date);
    }
    public static void CopyFields(Test src, Test dst) {
        dst.dim = src.dim;
        dst.description = src.description;
        dst.args = src.args;
    }
    public static File getArchive(Test object) {
        return Paths.get(System.getProperty("user.dir"), "Tests", object.id + ".zip").toFile();
    }
    public static File getServerPath(Test object) {
        return Paths.get(System.getProperty("user.dir"), "Tests", object.id).toFile();
    }
    public static File getHomePath(Test object) {
        return Paths.get(Global.visualiser.getWorkspace().getAbsolutePath(), object.id).toFile();
    }
    public static String getSummary(Test test) {
        return test.description;
    }
    //--
}

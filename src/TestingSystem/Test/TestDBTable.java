package TestingSystem.Test;
import Common.Current;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import TestingSystem.Test.UI.TestFields;
public class TestDBTable extends DBTable<String, Test> {
    public TestDBTable() {
        super(String.class, Test.class);
    }
    @Override
    public String getSingleDescription() {
        return "тест DVM";
    }
    @Override
    public String getPluralDescription() {
        return "тесты";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            protected void AdditionalInitColumns() {
                //columns.get(0).setVisible(false);
            }
            @Override
            public boolean hasCheckBox() {
                return true;
            }

        };
    }
    @Override
    public Object getFieldAt(Test object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.description;
            // case 3:
            //     return object.project_description;
            case 3:
                return object.dim;
            default:
                return null;
        }
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "имя", "размерность"};
    }
    @Override
    public Current CurrentName() {
        return Current.Test;
    }
    @Override
    public DBObjectDialog<Test, TestFields> getDialog() {
        return new DBObjectDialog<Test, TestFields>(TestFields.class) {
            @Override
            public int getDefaultHeight() {
                return 200;
            }
            @Override
            public int getDefaultWidth() {
                return 400;
            }
            @Override
            public void validateFields() {
                if (!edit) {
                    if (!Current.getGroup().language.equals(Current.getProject().languageName))
                        Log.Writeln_("В текущую группу могут войти только тесты на языке " + Current.getGroup().language);
                }
            }
            @Override
            public void fillFields() {
                fields.tfName.setText(Result.description);
                fields.sDim.setValue(Result.dim);
                // fields.tfProjectDescription.setText(Result.project_description);
            }
            @Override
            public void ProcessResult() {
                Result.description = fields.tfName.getText();
                Result.dim = (int) fields.sDim.getValue();
                //  Result.project_description = fields.tfProjectDescription.getText();
                if (!edit) {
                    Result.genName();
                    Result.sender_name = Current.getAccount().name;
                    Result.sender_address = Current.getAccount().email;
                }
            }
        };
    }
}
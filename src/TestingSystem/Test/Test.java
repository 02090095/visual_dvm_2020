package TestingSystem.Test;
import Common.Current;
import Common.Database.DBObject;
import Common.Database.rDBObject;
import Common.UI.UI;
import com.sun.org.glassfish.gmbal.Description;
public class Test extends rDBObject {
    @Override
    public boolean isVisible() {
        return TestInterface.isVisible(this);
    }
    @Description("DEFAULT 1")
    public int dim = 1; //размерность теста. для удобства пусть будет и внешним полем.
    @Description("DEFAULT ''")
    public String args = ""; //аргументы командной строки. на всякий случай поле зарезервирую. пусть будут.
    @Description("DEFAULT ''")
    public String group_id = "";
    @Description("DEFAULT ''")
    public String project_description = "";
    @Description("IGNORE")
    public byte[] project_archive_bytes = null;
    @Description("DEFAULT ''")
    public String files_json = "";
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        Test t = (Test) src;
        dim = t.dim;
        args = t.args;
        group_id = t.group_id;
    }
    public Test(Test test) {
        this.SynchronizeFields(test);
    }
    public Test() {
    }
    @Override
    public void select(boolean flag) {
        super.select(flag);
        if (Current.hasUI())
            UI.getMainWindow().getTestingWindow().ShowCheckedTestsCount();
    }
}

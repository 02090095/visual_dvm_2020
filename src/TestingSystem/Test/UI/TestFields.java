package TestingSystem.Test.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class TestFields implements DialogFields {
    public JTextField tfName;
    private JPanel content;
    public JSpinner sDim;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName = new StyledTextField();
    }
    public TestFields(){
        sDim.setModel(new SpinnerNumberModel(1, 0, 16,1));
    }
}

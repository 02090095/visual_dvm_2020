package TestingSystem.Test;
import ProjectData.Files.DBProjectFile;
import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.Vector;
public class ProjectFiles_json {
    @Expose
    public List<DBProjectFile> files = new Vector<>();
}

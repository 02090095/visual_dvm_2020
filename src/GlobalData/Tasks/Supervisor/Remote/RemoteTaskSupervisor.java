package GlobalData.Tasks.Supervisor.Remote;
import Common.Utils.Utils;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.Tasks.Supervisor.TaskSupervisor;
import GlobalData.Tasks.Task;
import GlobalData.Tasks.TaskState;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.SSH.ConnectionPass;
public abstract class RemoteTaskSupervisor<T extends Task> extends TaskSupervisor<T, ConnectionPass> {
    protected RemoteFile getRemoteProjectsPath() {
        return new RemoteFile(pass.user.getRemoteProjectsPath(), true);
    }
    protected RemoteFile getRemoteProject() {
        return new RemoteFile(getRemoteProjectsPath().full_name, project.getUniqKey(), true);
    }
    protected RemoteFile getBinary() {
        return new RemoteFile(getRemoteProject().full_name, "0");
    }
    protected RemoteFile getRemoteTime() {
        return new RemoteFile(getRemoteProject().full_name, db_project_info.time_file);
    }
    protected RemoteFile getRemoteOutput() {
        return new RemoteFile(getRemoteProject().full_name, db_project_info.out_file);
    }
    protected RemoteFile getRemoteErrors() {
        return new RemoteFile(getRemoteProject().full_name, db_project_info.err_file);
    }
    public RemoteFile getDONE_file() {
        return new RemoteFile(getRemoteProject().full_name, db_project_info.DONE);
    }
    public RemoteFile getTIMEOUT_file() {
        return new RemoteFile(getRemoteProject().full_name, db_project_info.TIMEOUT);
    }
    @Override
    protected void PrepareWorkspace() throws Exception {
        super.PrepareWorkspace(); //локальная подготовка
        pass.tryRM(getDONE_file());
        pass.tryRM(getTIMEOUT_file());
        pass.tryRM(getRemoteOutput());
        pass.tryRM(getRemoteErrors());
        pass.tryRM(getRemoteTime());
    }
    @Override
    protected void CheckTask() throws Exception {
        if (pass.Exists(getRemoteProject().full_name, db_project_info.DONE))
            task.state = TaskState.Finished;
        else if (pass.Exists(getRemoteProject().full_name, db_project_info.TIMEOUT))
            task.state = TaskState.AbortedByTimeout;
    }
    @Override
    protected void AchieveResults() throws Exception {
        pass.tryGetSingleFile(getRemoteOutput(), task.getOutputFile(), 10240);
        pass.tryGetSingleFile(getRemoteErrors(), task.getErrorsFile(), 10240);
    }
    @Override
    protected void AbortTask() throws Exception {
        pass.ShellCommand("kill -2 " + task.PID);
    }
    @Override
    protected void CalculatePerformanceTime() throws Exception {
        if (pass.tryGetSingleFile(getRemoteTime(), task.getTimeFile(), 0))
            task.RefreshTime();
    }
    protected String getStartCommand() {
        String res =
                String.join(" ",
                        Utils.DQuotes(pass.getStarter()),
                        Utils.DQuotes(pass.getLauncher()),
                        String.valueOf(task.maxtime),
                        Utils.DQuotes(getCoupDeGrace()),
                        task.getFullCommand()
                );
        System.out.println(res);
        return res;
    }
}

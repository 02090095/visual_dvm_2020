package GlobalData.Tasks.Supervisor.Local.Linux;
import Common.Current;
import GlobalData.Tasks.RunTask.RunTask;

import java.util.Map;
public class LinuxLocalRunSupervisor extends LinuxLocalTaskSupervisor<RunTask> {
    @Override
    protected void PrepareWorkspace() throws Exception {
        super.PrepareWorkspace();
        PrepareRunTaskWorkspace(task);
    }
    @Override
    protected void AchieveResults() throws Exception {
        super.AchieveResults();
        AchieveRunTaskResults(task);
    }
    @Override
    protected Map<String, String> getEnvs() {
        return Current.getRunConfiguration().getEnvMap();
    }
    @Override
    protected String getScriptText() {
        return "ulimit -s unlimited\n" + super.getScriptText();
    }
    @Override
    protected void ValidateTaskResults() throws Exception {
        task.AnalyzeResultsTexts(project);
    }
    protected String getCoupDeGrace() {
        return "killall -SIGKILL " + task.getCompilationTask().binary_name;
    }
}

package GlobalData.Tasks.Supervisor.Local.Windows;
import Common.Current;
import GlobalData.Tasks.RunTask.RunTask;

import java.util.Map;
public class WindowsLocalRunSupervisor extends WindowsLocalTaskSupervisor<RunTask> {
    @Override
    protected void ValidateTaskResults() throws Exception {
        task.AnalyzeResultsTexts(project);
    }
    @Override
    protected String getScriptText() {
        return task.getFullCommand() + " 1>out.txt 2>err.txt";
    }
    //общая часть для запуска.
    @Override
    protected void PrepareWorkspace() throws Exception {
        super.PrepareWorkspace();
        PrepareRunTaskWorkspace(task);
    }
    @Override
    protected void AchieveResults() throws Exception {
        super.AchieveResults();
        AchieveRunTaskResults(task);
    }
    @Override
    protected Map<String, String> getEnvs() {
        return Current.getRunConfiguration().getEnvMap();
    }
    void kill_mpi() throws Exception {
        System.out.println("KILLING MPI");
    }
    @Override
    protected void AbortTask() throws Exception {
        super.AbortTask(); //убить группу.
        kill_mpi();
    }
    @Override
    protected String getKillCommand() {
        return "taskkill /FI \"IMAGENAME eq " + task.getCompilationTask().binary_name + "\" /F /T";
    }
    /*
    @Override
    protected void CoupDeGraceTask() throws Exception {
        kill_mpi();
    }
     */
}

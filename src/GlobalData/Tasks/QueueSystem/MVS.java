package GlobalData.Tasks.QueueSystem;
import GlobalData.Tasks.TaskState;
public class MVS extends QueueSystem {
    public MVS() {
        cancel_command = "mcancel";
        check_command = "mqtest";
        refuse = "runmvsd: Error on queue task:Sorry, you achieved max task number. Try once more later.";
        commands = new QueueCommand[]{
                new QueueCommand(TaskState.Queued, "task", "queued successfully"),
                new QueueCommand(TaskState.Running, "task", "is in the run"),
                new QueueCommand(TaskState.Running, "task", "started successfully"),
                new QueueCommand(TaskState.Finished, "task", "complete"),
                new QueueCommand(TaskState.NoSuchTask, "", "no such task")
        };
    }
}

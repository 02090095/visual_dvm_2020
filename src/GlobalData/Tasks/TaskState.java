package GlobalData.Tasks;
import Common.Current;
import Common.UI.StatusEnum;
import Common.UI.Themes.VisualiserFonts;

import java.awt.*;
public enum TaskState implements StatusEnum {
    Inactive,
    //--
    Waiting,
    WorkspaceCreated,
    WorkspaceReady,
    Running,
    Canceled,
    //--- определение без скачки результатов!
    ResultsDownloaded,
    //-----------------------
    Finished,
    //--- результирующие
    Done,
    DoneWithErrors,
    AbortedByTimeout,
    AbortedByUser,
    Crushed,
    WrongTestFormat,
    InternalError,
    //--- сугубо кластерные.
    Queued,
    NoSuchTask,
    FailedToQueue,
    AbortingByUser; //только для одиночного запуска
    public boolean isVisible() {
        switch (this) {
            case Queued:
            case FailedToQueue:
            case NoSuchTask:
            case AbortingByUser:
                return false;
            default:
                return true;
        }
    }
    public String getDescription() {
        switch (this) {
            case Waiting:
                return "ожидание";
            case WorkspaceCreated:
                return "папка создана";
            case WorkspaceReady:
                return "готова к выполнению";
            case Running:
                return "выполняется";
            case Finished:
                return "ожидание анализа результатов";
            case Inactive:
                return "неактивно";
            case AbortedByTimeout:
                return "таймаут";
            case AbortedByUser:
                return "прервана";
            case Done:
                return "успешно";
            case DoneWithErrors:
                return "с ошибками";
            case Crushed:
                return "падение";
            case Canceled:
                return "отменена";
            case InternalError:
                return "внутренняя ошибка";
            case WrongTestFormat:
                return "неверный формат";
            case FailedToQueue:
                return "не удалось поставить в очередь";
            case NoSuchTask:
                return "не существует";
            case Queued:
                return "в очереди";
            case AbortingByUser:
                return "прерывание...";
            case ResultsDownloaded:
                return "результаты загружены";
            //--------------------------------------->>>
            default:
                return StatusEnum.super.getDescription();
        }
    }
    @Override
    public Font getFont() {
        switch (this) {
            case FailedToQueue:
            case NoSuchTask:
            case AbortedByUser:
            case AbortedByTimeout:
            case DoneWithErrors:
            case WrongTestFormat:
            case InternalError:
            case Canceled:
                return Current.getTheme().Fonts.get(VisualiserFonts.BadState);
            case Queued:
            case Running:
                return Current.getTheme().Fonts.get(VisualiserFonts.ProgressState);
            case Done:
                return Current.getTheme().Fonts.get(VisualiserFonts.GoodState);
            case Crushed:
                return Current.getTheme().Fonts.get(VisualiserFonts.Fatal);
            case Finished:
                return Current.getTheme().Fonts.get(VisualiserFonts.BlueState);
            case WorkspaceReady:
                return Current.getTheme().Fonts.get(VisualiserFonts.ReadyState);
            default:
                return StatusEnum.super.getFont();
        }
    }
    //
    public boolean isActive() {
        switch (this) {
            case Waiting:
            case Running:
            case Queued:
            case Finished:
            case AbortingByUser:
            case WorkspaceReady:
            case WorkspaceCreated:
                return true;
            default:
                return false;
        }
    }
    public boolean isFinished() {
        return this == TaskState.Finished;
    }
    public boolean isComplete() {
        switch (this) {
            case Done:
            case DoneWithErrors:
            case AbortedByTimeout:
            case AbortedByUser:
            case WrongTestFormat:
            case Crushed:
            case InternalError:
            case Canceled:
                return true;
            default:
                return false;
        }
    }
}

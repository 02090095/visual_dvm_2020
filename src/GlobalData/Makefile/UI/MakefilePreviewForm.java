package GlobalData.Makefile.UI;
import Common.UI.Editor.Viewer;
import Common.UI.Windows.Dialog.Text.TextDialog;
public class MakefilePreviewForm extends TextDialog<Viewer> {
    public MakefilePreviewForm() {
        super(Viewer.class);
    }
    @Override
    public void InitFields() {
        fields.setSyntaxEditingStyle("text/makefile");
        fields.setWhitespaceVisible(true);
        fields.setEditable(false);
    }
    @Override
    public void setText(String text_in) {
        fields.setText(text_in);
    }
    @Override
    public void CreateButtons() {
        //кнопок нет.
    }
}

package GlobalData.RunConfiguration.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class RunConfigurationFields implements DialogFields {
    public JPanel content;
    public JTabbedPane taskTypeTabs;
    public JTextField tfArgs;
    public JComboBox cbLaunchOptions;
    public JComboBox cbLauncherCall;
    public JButton bHelp;
    //---------------------------------------->>
    public JSpinner sMaxDim;
    public JPanel minMatrixPanel;
    public JPanel maxMatrixPanel;
    public JCheckBox cbCube;
    //---------------------------------------->>
    public MatrixBar minMatrixBar;
    public MatrixBar maxMatrixBar;
    //---------------------------------------->>
    public RunConfigurationFields() {
        sMaxDim.addChangeListener(e -> {
            minMatrixBar.ShowTillDim((Integer) sMaxDim.getValue());
            maxMatrixBar.ShowTillDim((Integer) sMaxDim.getValue());
            content.revalidate();
        });
    }
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfArgs = new StyledTextField();
    }
}

package GlobalData.SapforProfileSetting;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
public class SapforProfileSettingsDBTable extends iDBTable<SapforProfileSetting> {
    public SapforProfileSettingsDBTable() {
        super(SapforProfileSetting.class);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this){
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
            }
        };
    }
    @Override
    public Current CurrentName() {
        return Current.SapforProfileSetting;
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "имя",
                "значение"
        };
    }
    @Override
    public Object getFieldAt(SapforProfileSetting object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.name.getDescription();
            case 2:
                return object.value;
            default:
                return null;
        }
    }
}

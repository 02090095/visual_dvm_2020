package GlobalData.SapforProfileSetting;
import Common.Current;
import Common.Database.iDBObject;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import com.sun.org.glassfish.gmbal.Description;
public class SapforProfileSetting extends iDBObject {
    @Description("DEFAULT 'Undefined'")
    public SettingName name = SettingName.Undefined;
    @Description("DEFAULT ''")
    public String value = "";
    @Description("DEFAULT -1")
    public int sapforprofile_id = Utils.Nan;
    @Override
    public boolean isVisible() {
        return Current.HasSapforProfile() && Current.getSapforProfile().id == sapforprofile_id;
    }
}

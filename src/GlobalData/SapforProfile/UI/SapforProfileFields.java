package GlobalData.SapforProfile.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class SapforProfileFields implements DialogFields {
    private JPanel content;
    public JTextField tfDescription;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfDescription = new StyledTextField();
    }
}

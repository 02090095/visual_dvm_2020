package GlobalData.SapforProfile;
import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.SapforProfile.UI.SapforProfileFields;
import GlobalData.SapforProfileSetting.SapforProfileSetting;

import java.util.Date;
import java.util.LinkedHashMap;
public class SapforProfilesDBTable extends iDBTable<SapforProfile> {
    public SapforProfilesDBTable() {
        super(SapforProfile.class);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Описание",
                "Дата создания"
        };
    }
    @Override
    public Object getFieldAt(SapforProfile object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.description;
            case 2:
                return new Date(object.creationDate);
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.SapforProfile;
    }
    @Override
    public DBObjectDialog<SapforProfile, SapforProfileFields> getDialog() {
        return new DBObjectDialog<SapforProfile, SapforProfileFields>(SapforProfileFields.class) {
            @Override
            public int getDefaultHeight() {
                return 250;
            }
            @Override
            public void fillFields() {
                fields.tfDescription.setText(edit ? Result.description : "По умолчанию");
            }
            @Override
            public void ProcessResult() {
                Result.description = fields.tfDescription.getText();
            }
        };
    }
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(SapforProfileSetting.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }
}

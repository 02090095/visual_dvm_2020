package GlobalData.FormsParams;
import Common.Database.DBTable;
public class MainFormParamsDBTable extends DBTable<Integer, DBMainFormParams> {
    public MainFormParamsDBTable() {
        super(Integer.class, DBMainFormParams.class);
    }
    @Override
    public String getSingleDescription() {
        return "параметры главного окна";
    }
}

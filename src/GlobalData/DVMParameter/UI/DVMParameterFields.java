package GlobalData.DVMParameter.UI;
import Common.UI.ComboBox.StyledTextComboBox;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class DVMParameterFields implements DialogFields {
    public JTextField tfValue;
    public JComboBox<String> cbName;
    private JPanel content;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        cbName = new StyledTextComboBox();
        tfValue = new StyledTextField();
    }
}

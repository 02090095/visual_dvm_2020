package GlobalData;
import Common.Current;
import Common.Database.DBObject;
import Common.Database.DataSet;
import Common.Database.SQLITE.SQLiteDatabase;
import Common.Global;
import Common.UI.DataSetControlForm;
import GlobalData.Account.Account;
import GlobalData.Account.AccountsDBTable;
import GlobalData.Compiler.CompilersDBTable;
import GlobalData.Credentials.Credentials;
import GlobalData.Credentials.CredentialsDBTable;
import GlobalData.DBLastProject.LastProjectsDBTable;
import GlobalData.DVMParameter.DVMParameterDBTable;
import GlobalData.EnvironmentValue.EnvironmentValuesDBTable;
import GlobalData.FormsParams.FormsDBTable;
import GlobalData.FormsParams.MainFormParamsDBTable;
import GlobalData.Grid.GridsDBTable;
import GlobalData.Machine.MachinesDBTable;
import GlobalData.Makefile.MakefilesDBTable;
import GlobalData.Module.ModulesDBTable;
import GlobalData.RemoteSapfor.RemoteSapforsDBTable;
import GlobalData.RunConfiguration.RunConfigurationsDBTable;
import GlobalData.SapforProfile.SapforProfile;
import GlobalData.SapforProfile.SapforProfilesDBTable;
import GlobalData.SapforProfileSetting.SapforProfileSetting;
import GlobalData.SapforProfileSetting.SapforProfileSettingsDBTable;
import GlobalData.Settings.DBSetting;
import GlobalData.Settings.SettingName;
import GlobalData.Settings.SettingsDBTable;
import GlobalData.Splitter.SplittersDBTable;
import GlobalData.Tasks.CompilationTask.CompilationTasksDBTable;
import GlobalData.Tasks.RunTask.RunTasksDBTable;
import GlobalData.User.UsersDBTable;
import Repository.Component.ComponentType;
import TestingSystem.Sapfor.SapforTask.SapforTaskResult;
import TestingSystem.Sapfor.SapforTask.SapforTask_2023;
import TestingSystem.Sapfor.SapforTask.SapforTasksDBTable;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackage_2023;
import TestingSystem.Sapfor.SapforTasksPackage.SapforTasksPackagesDBTable;
import TestingSystem.TaskKey.TaskKeysDBTable;
import Visual_DVM_2021.PassStats.PassStatsDBTable;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.nio.file.Paths;
import java.util.Date;
import java.util.LinkedHashMap;

import static Common.UI.Tables.TableRenderers.RendererStatusEnum;
public class GlobalDatabase extends SQLiteDatabase {
    //---------СЕАНС----------------------------------------------
    public MachinesDBTable machines;
    public UsersDBTable users;
    public CompilersDBTable compilers;
    public RemoteSapforsDBTable remoteSapfors;
    public MakefilesDBTable makefiles;
    public ModulesDBTable modules;
    public CompilationTasksDBTable compilationTasks;
    public RunTasksDBTable runTasks;
    public RunConfigurationsDBTable runConfigurations;
    public EnvironmentValuesDBTable environmentValues;
    public DVMParameterDBTable dvmParameters;
    public CredentialsDBTable credentials;
    //----- ДАННЫЕ ВИЗУАЛИЗАТОРА---------------------------------
    public FormsDBTable forms;
    public MainFormParamsDBTable mainFormParams;
    public SettingsDBTable settings;
    public LastProjectsDBTable lastProjects;
    public AccountsDBTable accounts;
    public PassStatsDBTable passStats;
    public SplittersDBTable splitters;
    public GridsDBTable grids;
    //-
    public TaskKeysDBTable tasksKeys;
    //---------
    public SapforTasksPackagesDBTable sapforTasksPackages;
    public SapforTasksDBTable sapforTasks = null;
    public SapforProfilesDBTable sapforProfiles = null;
    //---------
    public SapforProfileSettingsDBTable sapforProfilesSettings = null;
    //-
    public GlobalDatabase() {
        super(Paths.get(System.getProperty("user.dir"), "Data", Global.properties.GlobalDBName).toFile());
    }
    @Override
    protected void initAllTables() throws Exception {
        addTable(machines = new MachinesDBTable());
        addTable(users = new UsersDBTable());
        addTable(compilers = new CompilersDBTable());
        addTable(remoteSapfors = new RemoteSapforsDBTable());
        addTable(makefiles = new MakefilesDBTable());
        addTable(modules = new ModulesDBTable());
        addTable(compilationTasks = new CompilationTasksDBTable());
        addTable(runTasks = new RunTasksDBTable());
        addTable(runConfigurations = new RunConfigurationsDBTable());
        addTable(environmentValues = new EnvironmentValuesDBTable());
        addTable(credentials = new CredentialsDBTable());
        addTable(forms = new FormsDBTable());
        addTable(settings = new SettingsDBTable());
        addTable(mainFormParams = new MainFormParamsDBTable());
        addTable(lastProjects = new LastProjectsDBTable());
        addTable(accounts = new AccountsDBTable());
        addTable(passStats = new PassStatsDBTable());
        addTable(splitters = new SplittersDBTable());
        addTable(dvmParameters = new DVMParameterDBTable());
        addTable(grids = new GridsDBTable());
        addTable(tasksKeys = new TaskKeysDBTable());
        //--
        addTable(sapforTasksPackages = new SapforTasksPackagesDBTable());
        addTable(sapforTasks = new SapforTasksDBTable());
        //--
        addTable(sapforProfiles = new SapforProfilesDBTable());
        addTable(sapforProfilesSettings = new SapforProfileSettingsDBTable());
    }
    //---------------------------------------------------------------------------------
    @Override
    public void Init() throws Exception {
        Current.set(Current.Account,
                accounts.Data.isEmpty() ? Insert(new Account()) :
                        accounts.getFirstRecord()
        );
        Current.set(Current.Credentials,
                credentials.Data.isEmpty() ? Insert(new Credentials()) :
                        credentials.getFirstRecord());
        //настройки компонент
        settings.AddAll();
        runConfigurations.Patch();
    }
    public void SaveCredentials() throws Exception {
        Credentials credentials = (Credentials) Current.get(Current.Credentials);
        if (Current.HasMachine())
            credentials.machine_id = Current.getMachine().id;
        if (Current.HasUser())
            credentials.user_id = Current.getUser().id;
        if (Current.HasCompiler())
            credentials.compiler_id = Current.getCompiler().id;
        if (Current.HasMakefile())
            credentials.makefile_id = Current.getMakefile().id;
        if (Current.HasRunConfiguration())
            credentials.runconfiguration_id = Current.getRunConfiguration().id;
        if (Current.HasRemoteSapfor())
            credentials.remotesapfor_id = Current.getRemoteSapfor().id;
        Update(credentials);
    }
    public void UpdateCredentials() {
        try {
            Global.db.Update((DBObject) Current.get(Current.Credentials));
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
    }
    public long IncSapforMaxTaskId() {
        long res = Global.properties.SapforTaskMaxId;
        Global.properties.SapforTaskMaxId++;
        Global.properties.Update();
        return res;
    }
    public LinkedHashMap<Long, SapforTask_2023> getSapforPackageTasks(int package_id) throws Exception {
        LinkedHashMap<Long, SapforTask_2023> res = new LinkedHashMap<>();
        for (SapforTask_2023 task : sapforTasks.Data.values()) {
            if (task.sapfortaskspackage_2023_id == package_id) {
                res.put(task.id, task);
            }
        }
        return res;
    }
    public DataSet<String, SapforTaskResult> getSapforPackagesMasterDataSet(SapforTasksPackage_2023 package_in) throws Exception {
        DataSet<String, SapforTaskResult> res = new DataSet<String, SapforTaskResult>(String.class, SapforTaskResult.class) {
            @Override
            protected DataSetControlForm createUI() {
                return new DataSetControlForm(this) {
                    @Override
                    protected void AdditionalInitColumns() {
                        columns.get(1).setRenderer(RendererStatusEnum);
                    }
                    @Override
                    public void MouseAction2() throws Exception {
                        Pass_2021.passes.get(PassCode_2021.OpenSapforTest).Do(Current.SapforEtalonTaskResult);
                    }
                    @Override
                    public void ShowCurrentObject() throws Exception {
                        System.out.println(Current.get(Current.SapforEtalonTaskResult));
                    }
                };
            }
            @Override
            public String[] getUIColumnNames() {
                return new String[]{
                        "Статус"
                };
            }
            @Override
            public Object getFieldAt(SapforTaskResult object, int columnIndex) {
                switch (columnIndex) {
                    case 1:
                        return object.task.state;
                    default:
                        return null;
                }
            }
            @Override
            public Current CurrentName() {
                return Current.SapforEtalonTaskResult;
            }
        };
        for (SapforTask_2023 task : sapforTasks.Data.values())
            if (task.sapfortaskspackage_2023_id == package_in.id)
                res.put(task.test_description, new SapforTaskResult(package_in, task));
        return res;
    }
    //--
    public DataSet<String, SapforTaskResult> getSapforPackagesSlaveDataSet(SapforTasksPackage_2023 package_in) throws Exception {
        DataSet<String, SapforTaskResult> res = new DataSet<String, SapforTaskResult>(String.class, SapforTaskResult.class) {
            @Override
            protected DataSetControlForm createUI() {
                return new DataSetControlForm(this) {
                    @Override
                    protected void AdditionalInitColumns() {
                        columns.get(1).setRenderer(RendererStatusEnum);
                        columns.get(2).setRenderer(RendererStatusEnum);
                    }
                    @Override
                    public void MouseAction2() throws Exception {
                         Pass_2021.passes.get(PassCode_2021.OpenSapforTest).Do(Current.SapforTaskResult);
                    }
                    @Override
                    public void ShowCurrentObject() throws Exception {
                        System.out.println(Current.get(Current.SapforTaskResult));
                    }
                };
            }
            @Override
            public String[] getUIColumnNames() {
                return new String[]{
                        "Статус",
                        "Совпадение"
                };
            }
            @Override
            public Object getFieldAt(SapforTaskResult object, int columnIndex) {
                switch (columnIndex) {
                    case 1:
                        return object.task.state;
                    case 2:
                        return object.match_state;
                    default:
                        return null;
                }
            }
            @Override
            public Current CurrentName() {
                return Current.SapforTaskResult;
            }
        };
        for (SapforTask_2023 task : sapforTasks.Data.values())
            if (task.sapfortaskspackage_2023_id == package_in.id)
                res.put(task.test_description, new SapforTaskResult(package_in, task));
        return res;
    }
    //--
    public LinkedHashMap<SettingName, String> getSapforSettingsForProfile() {
        LinkedHashMap<SettingName, String> res = new LinkedHashMap<>();
        for (DBSetting setting : Global.db.settings.getSettingsByOwner(ComponentType.SapforOptions))
            if (setting.Visible)
                res.put(setting.Name, setting.Value);
        return res;
    }
    //проверить, есть ли профиль с таким же набором настроек.
    public SapforProfile checkProfileForCurrentSettings() {
        LinkedHashMap<SettingName, String> current_values = getSapforSettingsForProfile();
        for (SapforProfile profile : sapforProfiles.Data.values()) {
            //--получить все настройки профиля
            LinkedHashMap<SettingName, String> profileValues = new LinkedHashMap<>();
            for (SapforProfileSetting setting : sapforProfilesSettings.Data.values())
                if (setting.sapforprofile_id == profile.id) profileValues.put(setting.name, setting.value);
            //--
            if (current_values.equals(profileValues)) return profile;
        }
        return null;
    }
    public void insertProfileSettings(SapforProfile profile) throws Exception {
        LinkedHashMap<SettingName, String> current_values = getSapforSettingsForProfile();
        for (SettingName name : current_values.keySet()) {
            //--
            SapforProfileSetting sapforProfileSetting = new SapforProfileSetting();
            sapforProfileSetting.name = name;
            sapforProfileSetting.value = current_values.get(name);
            sapforProfileSetting.sapforprofile_id = profile.id;
            //--
            Insert(sapforProfileSetting);
        }
    }
    public void saveCurrentProfile(String name_in) throws Exception {
        if (Global.db.checkProfileForCurrentSettings() == null) {
            SapforProfile profile = new SapforProfile();
            profile.description = name_in;
            profile.creationDate = new Date().getTime();
            Global.db.Insert(profile);
            Global.db.insertProfileSettings(profile);
        }
    }
    public void rewriteProfileByDescription(String description_in) throws Exception {
        SapforProfile old = null;
        //ищем профиль с именем бага
        for (SapforProfile p : Global.db.sapforProfiles.Data.values())
            if (p.description.equalsIgnoreCase(description_in)) {
                old = p;
                break;
            }
        if (old != null) {
            //удалить профиль с именем бага,если есть, чтобы не множить кроликов.
            //при условии что не видно окна профилей!!
            Global.db.Delete(old);
            Global.db.DeleteByFK(old, SapforProfileSetting.class);
        }
        //---
        SapforProfile profile = new SapforProfile();
        profile.description = description_in;
        profile.creationDate = new Date().getTime();
        Global.db.Insert(profile);
        Global.db.insertProfileSettings(profile);
    }
}

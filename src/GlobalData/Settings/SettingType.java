package GlobalData.Settings;
public enum SettingType {
    Undefined,
    SapforFlag,
    PercentField,
    StringField,
    IntField
}

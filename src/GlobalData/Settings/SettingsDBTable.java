package GlobalData.Settings;
import Common.Database.DBTable;
import Common.Global;
import Common.Utils.Utils;
import Repository.Component.ComponentType;

import java.util.Vector;
import java.util.stream.Collectors;
public class SettingsDBTable extends DBTable<SettingName, DBSetting> {
    public SettingsDBTable() {
        super(SettingName.class, DBSetting.class);
    }
    @Override
    public String getSingleDescription() {
        return "настройка";
    }
    public void AddSetting(DBSetting s) throws Exception {
        if (containsKey(s.Name)) {
            DBSetting old = get(s.Name);
            old.settingType = s.settingType;
            old.Visible = s.Visible;
            //патч на всегда включить.
            if (s.Name.equals(SettingName.STATIC_PRIVATE_ANALYSIS) && !old.Value.equals("1")) {
                old.Value = "1";
                getDb().Update(s);
            }
            //---------------------------------------->>
        } else
            this.getDb().Insert(s);
    }
    public void AddAll() throws Exception {
        //проверка списка всех настроек.
        //<editor-fold desc="Global">
        AddSetting(new DBSetting(SettingName.ProjectsSearchDirectory, "", SettingType.StringField, ComponentType.Visualiser, false));
        AddSetting(new DBSetting(SettingName.EditorFontSize, "14", SettingType.StringField, ComponentType.Visualiser, false));
        AddSetting(new DBSetting(SettingName.DarkThemeOn, 0, SettingType.SapforFlag, ComponentType.Visualiser, false));
    //    AddSetting(new DBSetting(SettingName.FocusPassesResult, 1, SettingType.SapforFlag, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.ShowFullArraysDeclarations, 0, SettingType.SapforFlag, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.ShowFullTabsNames, 0, SettingType.SapforFlag, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.FastAccessPassesCount, 5, SettingType.IntField, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.LastOpenedProjectsCount, 5, SettingType.IntField, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.Workspace, "", SettingType.StringField, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.Kernels, Utils.getHalfKernels(), SettingType.IntField, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.AutoBugReportsLoad, 0, SettingType.SapforFlag, ComponentType.Visualiser, true));
        AddSetting(new DBSetting(SettingName.AutoTestsLoad, 0, SettingType.SapforFlag, ComponentType.Visualiser, true));
        if (Global.isWindows)
            AddSetting(new DBSetting(SettingName.LocalMakePathWindows, "C:\\MinGW\\msys\\1.0\\bin\\make.exe", SettingType.StringField, ComponentType.Visualiser));
        AddSetting(new DBSetting(SettingName.SmallScreen, 0, SettingType.SapforFlag, ComponentType.Visualiser));
        //совместимость. указываем явно чтобы не были видны в меню.
        AddSetting(new DBSetting(SettingName.AutoSubscribeTest, 0, SettingType.SapforFlag, ComponentType.Visualiser, false));
        //</editor-fold>
        //--
        //<editor-fold desc="Sapfor">
        //при добавлении новых. учесть SapforTasksPackage (добавить поле)
        //и Global.packSapforSettings        (добавить запаковку поля в конец)
        AddSetting(new DBSetting(SettingName.STATIC_SHADOW_ANALYSIS, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.STATIC_PRIVATE_ANALYSIS, 1, SettingType.SapforFlag, ComponentType.SapforOptions, false));
        AddSetting(new DBSetting(SettingName.FREE_FORM, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.KEEP_DVM_DIRECTIVES, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.KEEP_SPF_DIRECTIVES, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.PARALLELIZE_FREE_LOOPS, 0, SettingType.SapforFlag, ComponentType.SapforOptions, false));
        AddSetting(new DBSetting(SettingName.MAX_SHADOW_WIDTH, 50, SettingType.PercentField, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.OUTPUT_UPPER, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.TRANSLATE_MESSAGES, 1, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.KEEP_LOOPS_CLOSE_NESTING, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.KEEP_GCOV, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.ANALYSIS_OPTIONS, " ", SettingType.StringField, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.DEBUG_PRINT_ON, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.MPI_PROGRAM, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.IGNORE_IO_SAPFOR, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.KEEP_SPF_DIRECTIVES_AMONG_TRANSFORMATIONS, 1, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.PARSE_FOR_INLINE, 0, SettingType.SapforFlag, ComponentType.SapforOptions, false));
        //--> косвенные, анализам не передаются.
        AddSetting(new DBSetting(SettingName.Precompilation, 0, SettingType.SapforFlag, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.GCOVLimit, 10, SettingType.PercentField, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.DVMConvertationOptions, " ", SettingType.StringField, ComponentType.SapforOptions));
        AddSetting(new DBSetting(SettingName.SaveModifications, 1, SettingType.SapforFlag, ComponentType.SapforOptions));
        //</editor-fold>
        //-
        //<editor-fold desc="Comparsion">
        AddSetting(new DBSetting(SettingName.ExtensionsOn, 0, SettingType.SapforFlag, ComponentType.ComparsionOptions));
        AddSetting(new DBSetting(SettingName.RegisterOn, 0, SettingType.SapforFlag, ComponentType.ComparsionOptions));
        AddSetting(new DBSetting(SettingName.SpacesOn, 1, SettingType.SapforFlag, ComponentType.ComparsionOptions));
        AddSetting(new DBSetting(SettingName.EmptyLinesOn, 1, SettingType.SapforFlag, ComponentType.ComparsionOptions));
        AddSetting(new DBSetting(SettingName.FortranWrapsOn, 1, SettingType.SapforFlag, ComponentType.ComparsionOptions));
        AddSetting(new DBSetting(SettingName.ComparsionDiffMergeOn, 0, SettingType.SapforFlag, ComponentType.ComparsionOptions));
        //</editor-fold>
    }
    public Vector<DBSetting> getSettingsByOwner(ComponentType owner) {
        return Data.values().stream().filter(setting -> setting.Owner.equals(owner)).collect(Collectors.toCollection(Vector::new));
    }
    public String getSapforSettingsText() {
        return "настройки SAPFOR\n" + String.join("\n",
                getSettingsByOwner(ComponentType.SapforOptions).stream().map(s -> "    " + s.Name.getDescription() + "=" + s.Value).collect(Collectors.toCollection(Vector::new)));
    }
}
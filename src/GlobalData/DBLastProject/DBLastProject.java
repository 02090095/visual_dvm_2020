package GlobalData.DBLastProject;
import Common.Database.DBObject;
import ProjectData.Project.db_project_info;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Date;
public class DBLastProject extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String HomePath = "";
    public long lastOpened = 0;
    public DBLastProject() {
    }
    public DBLastProject(db_project_info project) {
        HomePath = project.Home.getAbsolutePath();
        RefreshOpenTime();
    }
    public void RefreshOpenTime() {
        lastOpened = new Date().toInstant().getEpochSecond();
    }
    @Override
    public Object getPK() {
        return HomePath;
    }
}
package GlobalData.RemoteSapfor;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import Common.Utils.Validators.PathValidator;
import GlobalData.RemoteSapfor.UI.RemoteSapforFields;
import ProjectData.LanguageName;
public class RemoteSapforsDBTable extends iDBTable<RemoteSapfor> {
    public RemoteSapforsDBTable() {
        super(RemoteSapfor.class);
    }
    @Override
    public String getSingleDescription() {
        return "SAPFOR";
    }
    @Override
    public String getPluralDescription() {
        return "SAPFOR";
    }
    @Override
    public Current CurrentName() {
        return Current.RemoteSapfor;
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"описание", "команда вызова", "версия"};
    }
    @Override
    public Object getFieldAt(RemoteSapfor object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.description;
            case 2:
                return object.call_command;
            case 3:
                return object.version;
        }
        return null;
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
            }
        };
    }
    //---
    @Override
    public DBObjectDialog<RemoteSapfor, RemoteSapforFields> getDialog() {
        return new DBObjectDialog<RemoteSapfor, RemoteSapforFields>(RemoteSapforFields.class) {
            @Override
            public void validateFields() {
                //<editor-fold desc="расположение">
                String home = fields.tfHome.getText();
                if (!home.isEmpty()) {
                    if (home.startsWith("/")) {
                        if (Utils.ContainsCyrillic(home))
                            Log.Writeln("Расположение системы SAPFOR не может содержать кириллицу");
                        else {
                            new PathValidator(home, "Расположение системы SAPFOR", Log).Validate();
                        }
                    } else
                        Log.Writeln("Расположение системы SAPFOR может быть либо пустой строкой, либо путём к файлу");
                }
                //</editor-fold>
                //<editor-fold desc="команда вызова">
                String call_command = fields.tfCallCommand.getText();
                if (call_command.isEmpty()) Log.Writeln("Команда вызова системы SAPFOR не может быть пустой");
                else if (Utils.ContainsCyrillic(call_command))
                    Log.Writeln("Команда вызова системы SAPFOR не может содержать кириллицу");
                else {
                    switch (call_command.charAt(0)) {
                        case ' ':
                            Log.Writeln("Команда вызова системы SAPFOR не может начинаться с пробела.");
                            break;
                        case '/':
                            if (call_command.endsWith("/"))
                                Log.Writeln("Каталог не может быть указан в качестве команды.");
                            else new PathValidator(call_command, "Команда вызова системы SAPFOR", Log).Validate();
                            break;
                        default:
                            //это команда.
                            //самое опасное место. теоретически тут можно ввести любую команду ОС, в том числе rm -rf
                            if (call_command.contains(" "))
                                Log.Writeln("Прямая команда вызова системы SAPFOR не может содержать пробелы");
                            if (!call_command.contains("+") && Utils.ContainsForbiddenName(call_command))
                                Log.Writeln("Прямая команда вызова системы SAPFOR содержит запрещённые символы");
                            else {
                                if (Utils.isLinuxSystemCommand(call_command))
                                    Log.Writeln(Utils.DQuotes(call_command) + " является системной командой Linux");
                            }
                            break;
                    }
                }
                //</editor-fold>
            }
            @Override
            public void fillFields() {
                fields.tfDescription.setText(Result.description);
                fields.tfCallCommand.setText(Result.call_command);
                fields.tfHome.setText(Result.home_path);
                UI.TrySelect(fields.cbLanguageName, Result.languageName);
                fields.tfVersion.setText(Result.version);
                fields.events_on = true;
            }
            @Override
            public void ProcessResult() {
                Result.machine_id = Current.getMachine().id;
                Result.description = fields.tfDescription.getText();
                Result.call_command = fields.tfCallCommand.getText();
                Result.home_path = fields.tfHome.getText();
                Result.languageName = (LanguageName) fields.cbLanguageName.getSelectedItem();
                Result.version = fields.tfVersion.getText();
            }
            @Override
            public int getDefaultHeight() {
                return 300;
            }
        };
    }
}

package GlobalData.RemoteSapfor.UI;
import Common.Current;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import ProjectData.LanguageName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
public class RemoteSapforFields implements DialogFields {
    private JPanel content;
    public JTextField tfDescription;
    public JTextField tfHome;
    private JButton bBrowse;
    public JTextField tfCallCommand;
    public JComboBox<LanguageName> cbLanguageName;
    public JTextField tfVersion;
    public boolean events_on = false;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfDescription = new StyledTextField();
        tfHome = new StyledTextField();
        tfCallCommand = new StyledTextField();
        cbLanguageName = new JComboBox<>();
        cbLanguageName.addItem(LanguageName.fortran);
        tfVersion = new StyledTextField();
    }
    public RemoteSapforFields() {
        bBrowse.addActionListener(e -> {
            //  LanguageName languageName = (LanguageName) cbLanguageName.getSelectedItem();
            String dst = null;
            if (Pass_2021.passes.get(PassCode_2021.SelectRemoteFile).Do(true))
                dst = Current.getRemoteFile().full_name;
            if (dst != null)
                tfHome.setText(dst);
        });
        tfHome.getDocument().addDocumentListener(new DocumentListener() {
                                                     @Override
                                                     public void insertUpdate(DocumentEvent e) {
                                                         TryRestoreCallCommand();
                                                     }
                                                     @Override
                                                     public void removeUpdate(DocumentEvent e) {
                                                         TryRestoreCallCommand();
                                                     }
                                                     @Override
                                                     public void changedUpdate(DocumentEvent e) {
                                                     }
                                                 }
        );
    }
    public void TryRestoreCallCommand() {
        if (events_on && (cbLanguageName.getSelectedItem() != null))
            tfCallCommand.setText(tfHome.getText() +
                    (tfHome.getText().endsWith("/") ? "" : "/") +
                    "Sapfor_F");
    }
}

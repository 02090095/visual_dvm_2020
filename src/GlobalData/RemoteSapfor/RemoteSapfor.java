package GlobalData.RemoteSapfor;
import Common.Current;
import Common.Database.iDBObject;
import Common.Utils.Utils;
import GlobalData.Machine.Machine;
import ProjectData.LanguageName;
import com.sun.org.glassfish.gmbal.Description;
public class RemoteSapfor extends iDBObject {
    //---------------------------------------------------------------------
    @Description("IGNORE")
    public static String version_command = "-ver";//команда запроса версии компилятора.
    @Description("IGNORE")
    public static String help_command = "-help";// команда запроса help
    //----------------------------------------------------------------------
    public int machine_id = -1;
    public String description = "";
    public LanguageName languageName = LanguageName.fortran;
    public String home_path = ""; //домашняя папка.
    public String call_command = ""; //полная команда вызова.
    public String version = "?";
    public RemoteSapfor() {
    }
    public RemoteSapfor(Machine machine,
                        String description_in,
                        LanguageName languageName_in,
                        String call_command_in
    ) {
        machine_id = machine.id;
        description = description_in;
        languageName = languageName_in;
        call_command = call_command_in;
    }
    @Override
    public boolean isVisible() {
        return Current.HasMachine() && Current.getMachine().id == machine_id;
    }
    @Override
    public String toString() {
        return call_command;
    }
    public String getDescription() {
        return description;
    }
    public String getVersionCommand() {
        return Utils.DQuotes(call_command) + " " + version_command;
    }
}

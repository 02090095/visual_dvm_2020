package GlobalData.Credentials;
import Common.Database.iDBObject;
import Common.Utils.Utils;
import com.sun.org.glassfish.gmbal.Description;
public class Credentials extends iDBObject {
    @Description("DEFAULT -1")
    public int machine_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int user_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int compiler_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int makefile_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int runconfiguration_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int remotesapfor_id = Utils.Nan;
}

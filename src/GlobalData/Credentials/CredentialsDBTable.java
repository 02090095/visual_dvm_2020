package GlobalData.Credentials;
import Common.Database.iDBTable;
public class CredentialsDBTable extends iDBTable<Credentials> {
    public CredentialsDBTable() {
        super(Credentials.class);
    }
}

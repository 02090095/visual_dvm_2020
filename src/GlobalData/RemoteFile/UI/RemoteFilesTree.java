package GlobalData.RemoteFile.UI;
import Common.Current;
import Common.UI.Trees.DataTree;
import Common.UI.Trees.TreeRenderers;
import Common.UI.UI;
public class RemoteFilesTree extends DataTree {
    public RemoteFilesTree() {
        super(UI.getRemoteFileChooser().root);
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererRemoteFile;
    }
    @Override
    public Current getCurrent() {
        return Current.RemoteFile;
    }
    @Override
    public void ShowCurrentObject() {
        UI.getRemoteFileChooser().ShowCurrentRemoteFile();
    }
    @Override
    public void LeftMouseAction2() {
        if (Current.HasRemoteFile() && Current.getRemoteFile().isDirectory()) {
            UI.getRemoteFileChooser().Refresh(Current.getRemoteFile().full_name);
        }
    }
}

package GlobalData.Account;
public enum AccountSubscribeState {
    Undefined,
    None,
    Active;
    public String getDescription() {
        switch (this) {
            case Undefined:
                return "Подписка: нет данных";
            case None:
                return "Подписка: аннулирована";
            case Active:
                return "Подписка: активна";
            default:
                break;
        }
        return "";
    }
}

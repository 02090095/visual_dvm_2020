package GlobalData.Account;
public enum AccountRole {
    Undefined,
    User,
    Developer,
    Admin;
    public String getDescription() {
        switch (this) {
            case Undefined:
                return "не зарегистрирован";
            case User:
                return "Пользователь";
            case Developer:
                return "Разработчик";
            case Admin:
                return "Администратор";
            default:
                break;
        }
        return "";
    }
}

package GlobalData.Account;
import Common.Database.DBObject;
import Common.UI.UI;
import Common.Utils.TextLog;
import Repository.BugReport.BugReport;
import com.sun.org.glassfish.gmbal.Description;
public class Account extends DBObject {
    @Description("PRIMARY KEY,NOT NULL")
    public int id = 0;
    public String name = "?";
    public String email = "?";
    //--
    @Description("IGNORE")
    public AccountRole role = AccountRole.Undefined; //роль незареганного пользователя
    //--
    public boolean CheckRegistered(TextLog Log) {
        if (role.equals(AccountRole.Undefined)) {
            if (Log != null) {
                Log.Writeln("Пользователь не зарегистрирован");
                if (UI.HasNewMainWindow())
                    UI.getMainWindow().FocusCallback();
            }
            return false;
        }
        return true;
    }
    public boolean isRegistered() {
        return !role.equals(AccountRole.Undefined);
    }
    public boolean CheckAuthorship(String address_in, TextLog log) {
        if (!isAdmin() && (!email.equals(address_in))) {
            if (log != null)
                log.Writeln_("Вы не являетесь автором или администратором");
            return false;
        }
        return true;
    }
    public boolean CheckAccessRights(String address_in, TextLog log) {
        return (CheckRegistered(log) && CheckAuthorship(address_in, log));
    }
    public boolean ExtendedCheckAccessRights(BugReport bugReport, TextLog log) {
        if (CheckRegistered(log)) {
            if (email.equals(bugReport.executor_address)) {
                //исполнитель.
                return true;
            } else {
                if (!isAdmin() && (!email.equals(bugReport.sender_address))) {
                    log.Writeln_("Вы не являетесь автором, исполнителем, или администратором");
                    return false;
                } else return true;
            }
        }
        return false;
    }
    public boolean isAdmin() {
        return role.equals(AccountRole.Admin);
    }
    @Override
    public Object getPK() {
        return id;
    }
}


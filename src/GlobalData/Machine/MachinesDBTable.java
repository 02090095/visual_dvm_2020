package GlobalData.Machine;
import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.Compiler.Compiler;
import GlobalData.DVMParameter.DVMParameter;
import GlobalData.EnvironmentValue.EnvironmentValue;
import GlobalData.Machine.UI.MachineFields;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import GlobalData.RemoteSapfor.RemoteSapfor;
import GlobalData.RunConfiguration.RunConfiguration;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.User.User;

import java.util.LinkedHashMap;
public class MachinesDBTable extends iDBTable<Machine> {
    public MachinesDBTable() {
        super(Machine.class);
    }
    @Override
    public String getSingleDescription() {
        return "машина";
    }
    @Override
    public String getPluralDescription() {
        return "машины";
    }
    @Override
    public DBObjectDialog<Machine, MachineFields> getDialog() {
        return new DBObjectDialog<Machine, MachineFields>(MachineFields.class) {
            @Override
            public int getDefaultHeight() {
                return 250;
            }
            @Override
            public void validateFields() {
                if (fields.tfAddress.getText().isEmpty())
                    Log.Writeln("Адрес машины не может быть пустым");
            }
            @Override
            public void fillFields() {
                fields.tfName.setText(Result.name);
                fields.tfAddress.setText(Result.address);
                fields.sPort.setValue(Result.port);
                UI.TrySelect(fields.cbMachineType, Result.type);
            }
            @Override
            public void ProcessResult() {
                Result.name = fields.tfName.getText();
                Result.address = fields.tfAddress.getText();
                Result.port = (int) fields.sPort.getValue();
                Result.type = (MachineType) fields.cbMachineType.getSelectedItem();
            }
            @Override
            public void SetEditLimits() {
                fields.cbMachineType.setEnabled(false);
            }
        };
    }
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        //-
        res.put(User.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(Compiler.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(RemoteSapfor.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(Makefile.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(RunConfiguration.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        //-
        res.put(Module.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.PASSIVE));
        res.put(EnvironmentValue.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.PASSIVE));
        res.put(DVMParameter.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.PASSIVE));
        res.put(CompilationTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(RunTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.PASSIVE));
        return res;
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                UI.getMainWindow().getTestingWindow().ShowCurrentMachine();
                UI.getMainWindow().getTestingWindow().ShowCredentials();
            }
            @Override
            public void ShowNoCurrentObject() throws Exception {
                super.ShowNoCurrentObject();
                UI.getMainWindow().getTestingWindow().ShowNoCurrentMachine();
                UI.getMainWindow().getTestingWindow().ShowCredentials();
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
            }

        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"URL"};
    }
    @Override
    public Object getFieldAt(Machine object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.getURL();
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.Machine;
    }
    public boolean LocalMachineExists() {
        return Data.values().stream().anyMatch(machine -> machine.type.equals(MachineType.Local));
    }
}

package Repository.Server;
public enum ServerCode {
    Undefined,
    ReadFile,
    SendFile,
    ReceiveFile,
    //--
    SynchronizeTests,
    //-
    RegisterSubscriber,
    CheckSubscriberRole,
    //-
    GetComponentsBackups,
    //-
    UpdateBugReportField,
    UpdateBugReport,
    //-
    Email,
    ReceiveAllArchives,
    //-
    DownloadTest,
    //-
    OK,
    //-
    GetTestProject,
    StartTests,
    //-
    RefreshDVMTests, //- для админа. получение тестов из репозитория.
    //-
    PublishObject,
    CopyObjects, //использовать с осторожностью. проверять CopyAction на тему первичного ключа.
    EditObject,
    DeleteObject,
    GetObjectCopyByPK,
    DeleteObjects,
    CheckObjectExistense, //
    //--
    PublishAccountObjects,
    EditAccountObject,
    DeleteAccountObjects,
    //--
    EXIT,
    //--
    FAIL,
    GetAccountObjectCopyByPK,
    CheckAccountObjectExistense,
    GetAccountQueueSize,
    //--
    GetAccountObjectsCopiesByPKs,
    GetFirstActiveAccountPackage, //с задачами!
    GetQueueSize,
    CheckPackageToKill,
    //--
    ReceiveBugReportsDatabase,
    ReceiveTestsDatabase,
    ReceiveTestsTasksDatabase,
    PublishComponent,
    UpdateComponentMinimalVersion, //возможно потом, слить воедино с публикацией?
    ReceiveComponent,
    ReceiveBugReport,
    SendBugReport,
    GetComponentsVersions,
    GetComponentsMinimalVersions,
    GetComponentChangesLog,
    //--
    CheckURLRegistered,
    DVMConvertProject,
    SetRole, OLD
}

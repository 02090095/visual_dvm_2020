package Repository;
import Common.Database.SQLITE.SQLiteDatabase;
import Common.Global;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportsDBTable;
import Repository.SubscriberWorkspace.SubscriberWorkspaceDBTable;
import Repository.Subscribes.SubsribersDBTable;

import java.nio.file.Paths;
import java.util.Vector;
public class BugReportsDatabase extends SQLiteDatabase {
    public BugReportsDBTable bugReports;
    public SubsribersDBTable subscribers;
    public SubscriberWorkspaceDBTable workspaces; //рабочие пространства для машин.
    public BugReportsDatabase() {
        super(Paths.get(System.getProperty("user.dir"), "Data", Global.properties.BugReportsDBName).toFile());
    }
    @Override
    protected void initAllTables() throws Exception {
        addTable(bugReports = new BugReportsDBTable());
        addTable(subscribers = new SubsribersDBTable());
        addTable(workspaces = new SubscriberWorkspaceDBTable());
    }
    @Override
    public void Init() throws Exception {
        DeleteDrafts();
    }
    public void DeleteDrafts() throws Exception {
        Vector<BugReport> drafts = bugReports.getAllDrafts();
        for (BugReport draft : drafts)
            Delete(draft);
    }
}

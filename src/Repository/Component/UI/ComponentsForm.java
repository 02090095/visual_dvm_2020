package Repository.Component.UI;
import Common.Global;
import Common.UI.Windows.Dialog.Dialog;

import java.awt.*;
public class ComponentsForm extends Dialog<Object, ComponentsFields> {
    public ComponentsForm() {
        super(ComponentsFields.class);
    }
    @Override
    public boolean NeedsScroll() {
        return false;
    }
    @Override
    public int getDefaultWidth() {
        return Global.properties.ComponentsWindowWidth;
    }
    @Override
    public int getDefaultHeight() {
        return Global.properties.ComponentsWindowHeight;
    }
    @Override
    public void CreateButtons() {
    }
    @Override
    public void Init(Object... params) {
        Global.Components.ShowUI();
    }

    @Override
    public void LoadSize() {
        setMinimumSize(new Dimension(650, 250));
        Dimension dimension = new Dimension(getDefaultWidth(), getDefaultHeight());
        setPreferredSize(dimension);
        setSize(dimension);
    }
    @Override
    public void onClose() {
        super.onClose();
        Global.properties.ComponentsWindowWidth=getWidth();
        Global.properties.ComponentsWindowHeight=getHeight();
        Global.properties.Update();
    }
}

package Repository.Component.UI;
import Common.Global;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class ComponentsFields implements DialogFields {
    public JPanel content;
    private JPanel componentsPanel;
    public ComponentsFields() {
        Global.Components.mountUI(componentsPanel);
    }
    @Override
    public Component getContent() {
        return content;
    }
}

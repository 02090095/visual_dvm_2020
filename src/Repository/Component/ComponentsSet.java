package Repository.Component;
import Common.Current;
import Common.Database.DataSet;
import Common.UI.DataSetControlForm;

import java.util.Vector;

import static Common.UI.Tables.TableRenderers.RendererMaskedInt;
import static Common.UI.Tables.TableRenderers.RendererStatusEnum;
public class ComponentsSet extends DataSet<ComponentType, Component> {
    public ComponentsSet() {
        super(ComponentType.class, Component.class);
    }
    @Override
    public String getSingleDescription() {
        return "компонент";
    }
    @Override
    public String getPluralDescription() {
        return "компоненты";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
                columns.get(3).setRenderer(RendererMaskedInt);
                columns.get(4).setRenderer(RendererMaskedInt);
                columns.get(6).setRenderer(RendererStatusEnum);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Компонент", "Текущая версия", "Актуальная версия", "Дата сборки", "Статус"};
    }
    @Override
    public Object getFieldAt(Component object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.getComponentType().getDescription();
            case 3:
                return object.version;
            case 4:
                return object.actual_version;
            case 5:
                return object.date_text;
            case 6:
                return object.getState();
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.Component;
    }
    @Override
    public Vector<Component> getCheckedItems() {
        Vector<Component> target = new Vector<>();
        Component visualiser = null;
        Component server = null;
        //------------------------
        for (Component component : super.getCheckedItems()) {
            switch (component.getComponentType()) {
                case Visualizer_2:
                    server = component;
                    break;
                case Visualiser:
                    visualiser = component;
                    break;
                default:
                    target.add(component);
                    break;
            }
        }
        if (visualiser != null)
            target.add(visualiser);
        if (server != null)
            target.add(server);
        return target;
    }
}

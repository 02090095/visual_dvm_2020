package Repository.Component.Sapfor;
import Common.Global;
import Repository.Component.ComponentType;

import java.io.File;
import java.nio.file.Paths;
public class Sapfor_F extends Sapfor {
    @Override
    public ComponentType getComponentType() {
        return ComponentType.Sapfor_F;
    }
    @Override
    public String getAssemblyCommand() {
        return "cd Repo/sapfor/experts/Sapfor_2017/_bin\n" +
                "cmake ../\n" +
                "make -j 4\n";
    }
    @Override
    public File getAssemblyFile() {
        return Paths.get(
                Global.RepoDirectory.getAbsolutePath(),
                "sapfor/experts/Sapfor_2017/_bin/Sapfor_F").toFile();
    }
    @Override
    public String getUpdateCommand() {
        return "update_spf: ";
    }
    @Override
    public String getRestartCommand() {
        return "restart: ";
    }
}

package Repository;
public class RepositoryRefuseException extends Exception{
    //исключение для "штатных" отказов. например отсутствие объекта с заданным ключом.
    public RepositoryRefuseException(String message_in){
        super(message_in);
    }
}

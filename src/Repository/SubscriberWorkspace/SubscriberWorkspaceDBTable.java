package Repository.SubscriberWorkspace;
import Common.Database.iDBTable;
public class SubscriberWorkspaceDBTable extends iDBTable<SubscriberWorkspace> {
    public SubscriberWorkspaceDBTable() {
        super(SubscriberWorkspace.class);
    }
    public SubscriberWorkspace findWorkspace(String email, String machineURL, String login) {
        return this.Data.values().stream().filter(subscriberWorkspace ->
                subscriberWorkspace.email.equals(email) &&
                subscriberWorkspace.URL.equals(machineURL) &&
                subscriberWorkspace.login.equals(login)).findFirst().orElse(null);
    }
}

package Repository.BugReport;
import Common.Current;
import Common.Database.DBObject;
import Common.Database.rDBObject;
import Common.Global;
import Repository.Component.ComponentType;
import com.sun.org.glassfish.gmbal.Description;

import java.io.File;
import java.util.Date;
public class BugReport extends rDBObject {
    public String project_version = "";
    public long visualiser_version = -1;
    public long sapfor_version = -1;
    public String sapfor_settings = "";
    public String comment = "";
    public String targets = "";
    public String executor = "";
    @Description("DEFAULT ''")
    public String executor_address = "";
    public BugReportState state;
    public int percentage = 0;
    //-
    @Description("IGNORE")
    public String descriptionAdditionDraft = "";
    @Description("IGNORE")
    public String commentAdditionDraft = "";
    @Description("IGNORE")
    public File owner = null;
    public BugReport() {
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        BugReport b = (BugReport) src;
        change_date = b.change_date;
        description = b.description;
        comment = b.comment;
        targets = b.targets;
        state = b.state;
        percentage = b.percentage;
        //-
        executor = b.executor;
        executor_address = b.executor_address;
        project_version = b.project_version;
        visualiser_version = b.visualiser_version;
        sapfor_version = b.sapfor_version;
        sapfor_settings = b.sapfor_settings;
        //-
        descriptionAdditionDraft = b.descriptionAdditionDraft;
        commentAdditionDraft = b.commentAdditionDraft;
        owner = b.owner;
    }
    public BugReport(BugReport src) {
        this.SynchronizeFields(src);
    }

    public BugReport(String sender_name_in, String sender_address_in, String description_in, String version_in) {
        genName();
        sender_name = sender_name_in;
        sender_address = sender_address_in;
        project_version = version_in;
        visualiser_version = Global.visualiser.version;
        sapfor_version = Global.Components.get(ComponentType.Sapfor_F).version;
        sapfor_settings = Global.db.settings.getSapforSettingsText();
        percentage = 0;
        description = description_in;
        date = new Date().getTime();
        change_date = new Date().getTime();
        state = BugReportState.draft;
        owner = Current.getProject().Home;
    }
    @Override
    public boolean isVisible() {
        return BugReportInterface.isVisible(this);
    }
}

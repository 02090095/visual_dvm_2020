package Repository.BugReport;
import Common.Current;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Comparator;
import java.util.Vector;
import java.util.stream.Collectors;

import static Common.UI.Tables.TableRenderers.*;
public class BugReportsDBTable extends DBTable<String, BugReport> {
    public BugReportsDBTable() {
        super(String.class, BugReport.class);
    }
    @Override
    public String getSingleDescription() {
        return "отчёт об ошибке";
    }
    @Override
    public String getPluralDescription() {
        return "отчёты об ошибках";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                UI.getMainWindow().getCallbackWindow().ShowCurrentBugReport();
            }
            @Override
            public void ShowNoCurrentObject() throws Exception {
                super.ShowNoCurrentObject();
                UI.getMainWindow().getCallbackWindow().ShowNoCurrentBugReport();
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(1).setMaxWidth(600);
                columns.get(5).setRenderer(RendererProgress);
                columns.get(6).setRenderer(RendererDate);
                columns.get(7).setRenderer(RendererDate);
                columns.get(8).setRenderer(RendererStatusEnum);
            }
            @Override
            public void MouseAction2() throws Exception {
                Pass_2021.passes.get(PassCode_2021.OpenBugReportTestProject).Do();
            }
            @Override
            public void CreateControl() {
                //https://stackoverflow.com/questions/9091208/jtable-enter-key
                super.CreateControl();
                final String solve = "Solve";
                KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
                control.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(enter, solve);
                control.getActionMap().put(solve, new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Pass_2021.passes.get(PassCode_2021.OpenBugReportTestProject).Do();
                    }
                });
            }
        };
    }
    @Override
    public Comparator<BugReport> getComparator() {
        return (o1, o2) -> -(o1.getDate().compareTo(o2.getDate()));
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Описание",
                "Отправитель",
                "Исполнитель",
                "Проект",
                "Завершенность",
                "Дата создания",
                "Дата изменения",
                "Статус"};
    }
    @Override
    public Object getFieldAt(BugReport object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return BugReportInterface.getDescriptionHeader(object);
            case 2:
                return object.sender_name;
            case 3:
                return object.executor;
            case 4:
                return object.project_version;
            case 5:
                return object.percentage;
            case 6:
                return object.getDate();
            case 7:
                return object.getChangeDate();
            case 8:
                return object.state;
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.BugReport;
    }
    public Vector<BugReport> getAllDrafts() throws Exception {
        return Data.values().stream().filter(bugReport -> bugReport.state.equals(BugReportState.draft)).collect(Collectors.toCollection(Vector::new));
    }
}

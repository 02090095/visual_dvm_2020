package Repository.BugReport;
import Common.Current;
import Common.Global;
import Common.Utils.TextLog;
import Common.Utils.Utils;
import Repository.RepositoryServer;
import Repository.Subscribes.Subscriber;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Vector;
public class BugReportInterface {
    public static String filterKey = "";
    public static String filterSenderName = "";
    public static String filterDescription = "";
    public static String filterComment = "";
    public static String filterExecutor = "";
    public static String filterVersion = "";
    public static boolean filterOpenedOnly = false;
    public static boolean filterMyOnly = false;
    public static boolean isVisible(BugReport object) {
        return
                object.state.equals(BugReportState.draft) ||
                        object.id.toUpperCase().contains(filterKey.toUpperCase())
                                && object.sender_name.toUpperCase().contains(filterSenderName.toUpperCase())
                                && object.description.toUpperCase().contains(filterDescription.toUpperCase())
                                && object.comment.toUpperCase().contains(filterComment.toUpperCase())
                                && object.executor.toUpperCase().contains(filterExecutor.toUpperCase())
                                && object.project_version.toUpperCase().contains(filterVersion.toUpperCase())
                                && (!filterOpenedOnly || object.state.equals(BugReportState.active))
                                && (!filterMyOnly ||
                                (object.sender_address.equalsIgnoreCase(Current.getAccount().email) ||
                                        object.executor_address.equalsIgnoreCase(Current.getAccount().email)
                                )
                        );
    }
    public static String getPackedTargets() {
        Vector<String> selected = new Vector<>();
        for (Subscriber subscriber : Global.componentsServer.db.subscribers.Data.values())
            if (subscriber.isSelected()) selected.add(subscriber.address);
        return String.join("\n", selected);
    }
    public static File getArchiveFile(BugReport object) {
        return Paths.get(System.getProperty("user.dir"), "Bugs", object.id + ".zip").toFile();
    }
    public static String getDescriptionHeader(BugReport object) {
        if (object.description != null) {
            String[] data = object.description.split("\n");
            return (data.length > 0) ? data[0] : "";
        } else return "";
    }
    public static void CheckSubscribers(BugReport object) {
        for (Subscriber subscriber : Global.componentsServer.db.subscribers.Data.values())
            subscriber.Select(object.targets.contains(subscriber.address));
    }
    public static boolean CheckNotDraft(BugReport object, TextLog log) {
        if (object.state.equals(BugReportState.draft)) {
            log.Writeln_("Отчёт об ошибке является черновиком");
            return false;
        }
        return true;
    }
    public static String getMailTitlePrefix(BugReport object) {
        return "Ошибка " + Utils.Brackets(object.id) + ", автор " + Utils.Brackets(object.sender_name) + " : ";
    }
    public static Vector<String> getRecipients(BugReport object, boolean add_defaults) {
        Vector<String> res = new Vector<>();
        String[] data = object.targets.split("\n");
        for (String a : data)
            if (a.length() > 0)
                res.add(a);
        if (add_defaults) {
            // res.add(Email.full_address); //служебный ящик
            //добавить админов если их там нет.
            Arrays.stream(Global.admins_mails).filter(adm -> !res.contains(adm)).forEach(res::add);
            // и себя
            if (!res.contains(Current.getAccount().email))
                res.add(Current.getAccount().email);
        }
        return res;
    }
    public static File[] getAttachements(BugReport object) {
        File[] project_attachements = Current.getProject().getAttachmentsDirectory().listFiles();
        File[] res = new File[project_attachements.length + 1];
        res[0] = getArchiveFile(object);
        for (int i = 0; i < project_attachements.length; ++i)
            res[i + 1] = project_attachements[i];
        return res;
    }
    public static boolean CheckDraft(BugReport object, TextLog log) {
        if (!object.state.equals(BugReportState.draft)) {
            log.Writeln("Отчёт об ошибке не является черновиком. Он уже опубликован");
            return false;
        }
        return true;
    }
    public static String getNewMailText(BugReport object) {
        String res = String.join("\n",
                "Описание:", object.description,
                getPassport(object)
        );
        return res;
    }
    public static String getSettingsSummary(BugReport object) {
        return
                (Current.HasAccount() ? (Current.getAccount().isAdmin() ? ("Адрес отправителя: " + object.sender_address + "\n") : "") : "") +
                        "Версия SAPFOR: " + object.sapfor_version + "\n" +
                        "Версия визуализатора: " + object.visualiser_version + "\n" +
                        "----------------------------------\n" +
                        object.sapfor_settings;
    }
    public static String getPassport(BugReport object) {
        return String.join("\n",
                RepositoryServer.separator,
                "Отправитель: " + object.sender_name,
                "Исполнитель: " + object.executor,
                "Проект: " + object.project_version,
                RepositoryServer.separator,
                getSettingsSummary(object),
                RepositoryServer.separator);
    }
    public static boolean isNoneProject(BugReport object) {
        return object.project_version.isEmpty();
    }
}

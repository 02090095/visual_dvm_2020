package Repository.Subscribes;
import Common.Database.DBObject;
import GlobalData.Account.AccountRole;
import com.sun.org.glassfish.gmbal.Description;
public class Subscriber extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String address = "";
    @Description("DEFAULT ''")
    public String name = "";
    @Description("DEFAULT 'User'")
    public AccountRole role = AccountRole.User;  //права доступа
    @Description("DEFAULT 1")
    public int mailOn = 1;
    //---
    public Subscriber() {
    }
    public Subscriber(String address_in) {
        address = address_in;
    }
    @Override
    public Object getPK() {
        return address;
    }
    @Override
    public void SynchronizeFields(DBObject src) {
        super.SynchronizeFields(src);
        Subscriber s = (Subscriber) src;
        name= s.name;
        role = s.role;
        mailOn= s.mailOn;
    }
}

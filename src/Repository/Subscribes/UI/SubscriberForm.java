package Repository.Subscribes.UI;
import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import GlobalData.Account.AccountRole;
import Repository.Subscribes.Subscriber;
public class SubscriberForm extends DBObjectDialog<Subscriber, SubscriberFields> {
    public SubscriberForm() {
        super(SubscriberFields.class);
    }
    @Override
    public int getDefaultHeight() {
        return 250;
    }
    @Override
    public int getDefaultWidth() {
        return 450;
    }
    @Override
    public void validateFields() {
        if (fields.tfName.getText().isEmpty())
            Log.Writeln("Имя учётной записи не может быть пустым");
        Utils.validateEmail(fields.tfAddress.getText(), Log);
        if (fields.tfAddress.getText().isEmpty())
            Log.Writeln_("Адрес электронной почты не может быть пустым");
        if (!title_text.equals("Регистрация") && (fields.tfAddress.isEditable() && Global.componentsServer.db.subscribers.Data.containsKey(fields.tfAddress.getText()))) {
            Log.Writeln_("Адрес электронной почты " + Utils.Brackets(fields.tfAddress.getText()) + " уже есть в списке.");
        }
        if (Result.role.equals(AccountRole.Admin) && !getSelectedRole().equals(AccountRole.Admin)) {
            Log.Writeln_("Администратор не может разжаловать сам себя.");
        }
    }
    @Override
    public void fillFields() {
        fields.tfName.setText(Result.name);
        fields.tfAddress.setText(Result.address);
        fields.cbMail.setSelected(Result.mailOn!=0);
        UI.TrySelect(fields.cbRole, Result.role);
    }
    @Override
    public void SetEditLimits() {
        fields.tfAddress.setEditable(false);
    }
    private AccountRole getSelectedRole() {
        return (AccountRole) fields.cbRole.getSelectedItem();
    }
    @Override
    public void ProcessResult() {
        Result.name = fields.tfName.getText();
        Result.address = fields.tfAddress.getText();
        Result.mailOn = fields.cbMail.isSelected()?1:0;
        Result.role = getSelectedRole();
    }
};


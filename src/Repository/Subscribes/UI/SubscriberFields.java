package Repository.Subscribes.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import GlobalData.Account.AccountRole;

import javax.swing.*;
import java.awt.*;
public class SubscriberFields implements DialogFields {
    private JPanel content;
    public JTextField tfName;
    public JTextField tfAddress;
    public JComboBox<AccountRole> cbRole;
    public JCheckBox cbMail;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName = new StyledTextField();
        tfAddress = new StyledTextField();
        cbRole = new JComboBox<>();
        cbRole.addItem(AccountRole.User);
        cbRole.addItem(AccountRole.Developer);
        cbRole.addItem(AccountRole.Admin);
    }
}

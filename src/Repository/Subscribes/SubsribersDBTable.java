package Repository.Subscribes;
import Common.Current;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.Account.AccountRole;
import Repository.Subscribes.UI.SubscriberFields;
import Repository.Subscribes.UI.SubscriberForm;
public class SubsribersDBTable extends DBTable<String, Subscriber> {
    public SubsribersDBTable() {
        super(String.class, Subscriber.class);
    }
    @Override
    public String getSingleDescription() {
        return "Адресат";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                boolean admin = Current.getAccount().role.equals(AccountRole.Admin);
                columns.get(0).setVisible(admin);
                columns.get(1).setVisible(Current.getBugReport() != null);
                columns.get(3).setVisible(admin);
                columns.get(4).setVisible(admin);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Имя", "Роль", "Рассылка"};
    }
    @Override
    public Object getFieldAt(Subscriber object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.name;
            case 3:
                return object.role.getDescription();
            case 4:
                return (object.mailOn==0)?"выключена":"включена";
        }
        return object.name;
    }
    @Override
    public Current CurrentName() {
        return Current.Subscriber;
    }
    @Override
    public DBObjectDialog<Subscriber, SubscriberFields> getDialog() {
        return new SubscriberForm();
    }
}

package ProjectData;
public enum ProjectView {
    Files,
    Includes,
    FunctionsCallsPoints,
    FunctionsHierarchy;
    public String getIcon(){
        switch (this){
            case Files:
                return "/icons/Editor/Paste.png";
            case Includes:
                return "/icons/Transformations/SPF_InsertIncludesPass.png";
            case FunctionsHierarchy:
                return "/icons/FunctionsH.png";
            case FunctionsCallsPoints:
                return "/icons/Function.png";
            default:
                return "";
        }
    }
    public String getDescription(){
        switch (this){
            case Files:
                return "Файлы";
            case Includes:
                return "Зависимости по включениям";
            case FunctionsHierarchy:
                return "Иерархия процедур";
            case FunctionsCallsPoints:
                return "Точки вызовов процедур";
            default:
                return "";
        }
    }
}

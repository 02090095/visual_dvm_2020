package ProjectData.DBArray;
import Common.Database.DBObject;
import ProjectData.SapforData.Arrays.ArrayState;
import ProjectData.SapforData.Arrays.ProjectArray;
import com.sun.org.glassfish.gmbal.Description;

import javax.swing.*;
public class DBArray extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String UniqKey;
    public String shortName;
    public ArrayState State;
    public DBArray() {
    }
    public DBArray(ProjectArray array) {
        UniqKey = array.UniqKey;
        shortName = array.shortName;
        State = array.State;
    }
    @Override
    public Object getPK() {
        return UniqKey;
    }
    @Override
    public ImageIcon GetSelectionIcon() {
        return State.GetIcon();
    }
}

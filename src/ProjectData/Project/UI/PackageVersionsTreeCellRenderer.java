package ProjectData.Project.UI;
import Common.UI.Trees.StyledTreeCellRenderer;
import ProjectData.Project.db_project_info;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.net.URL;
public class PackageVersionsTreeCellRenderer extends StyledTreeCellRenderer {
    public java.awt.Component getTreeCellRendererComponent(
            JTree tree, Object value,
            boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        URL imageUrl = null;
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        if (o instanceof db_project_info) {
            /*
            db_project_info version = (db_project_info) o;
            String type_image_key = "";
            if (version.Home.getParent().equals(Current.getSapforTasksPackage().getWorkspace().getAbsolutePath()))
                type_image_key = "Root";
            else if (version.IsMCopy())
                type_image_key = "M";
            else
                type_image_key = "Version";
            imageUrl = getClass().getResource("/icons/versions/" +
                    type_image_key +
                    ".png");
            if (imageUrl != null) {
                setIcon(new ImageIcon(imageUrl));
            }
            setForeground(tree.getForeground());
            setFont(getFont().deriveFont((float) 14.0));
            setText(version.getTitle());

             */
        }else {
            setForeground(tree.getForeground());
            setFont(getFont().deriveFont((float) 14.0));
        }
        return this;
    }
}

package ProjectData.Project.UI;
import Common.Current;
import Common.UI.Trees.DataTree;
import Common.UI.Trees.TreeRenderers;
import Common.UI.UI;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
public class PackageVersionsTree extends DataTree {
    public PackageVersionsTree() {
        super(null);
              //  Current.getSapforTasksPackage().root);
        //  setRootVisible(false);
        ExpandAll();
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererPackageVersion;
    }
    @Override
    public Current getCurrent() {
        return Current.PackageVersion;
    }
    @Override
    protected int getStartLine() {
        return 1;
    }
    @Override
    public void SelectionAction(TreePath path) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
        Object object = node.getUserObject();
        if (object instanceof db_project_info) {
            Current.set(getCurrent(), object);
        }else {
            Current.set(getCurrent(), null);
        }
    }
    @Override
    public void LeftMouseAction2() {
        if ((Current.HasPackageVersion())&&(UI.Question("Открыть версию пакета, как текущий проект"))){
            //Открываем как папку, чтобы было отдельное дерево версий, уже как для нормального проекта.
            //? Запретить удалять ее (?). копировать куда то как времянку мб.
            Pass_2021.passes.get(PassCode_2021.OpenCurrentProject).Do(Current.getPackageVersion().Home);
        }
    }
}

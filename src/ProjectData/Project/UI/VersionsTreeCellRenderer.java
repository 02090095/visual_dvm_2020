package ProjectData.Project.UI;
import Common.Current;
import Common.Global;
import Common.UI.Selectable;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.Trees.StyledTreeCellRenderer;
import ProjectData.Project.db_project_info;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.net.URL;
//https://docs.oracle.com/javase/7/docs/api/javax/swing/tree/DefaultMutableTreeNode.html
//https://java.hotexamples.com/ru/examples/java.awt/JTree/-/java-jtree-class-examples.html
public class VersionsTreeCellRenderer extends StyledTreeCellRenderer {
    public java.awt.Component getTreeCellRendererComponent(
            JTree tree, Object value,
            boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        db_project_info version = (db_project_info) o;
        if (Global.versions_multiselection) {
            setIcon(((Selectable) o).GetSelectionIcon());
        } else {
            URL imageUrl = null;
            boolean current = Current.HasProject() && version.Home.equals(Current.getProject().Home);
            String type_image_key = "";
            if (version.Home.equals(Current.getRoot().Home))
                type_image_key = "Root";
            else if (version.IsMCopy())
                type_image_key = "M";
            else
                type_image_key = "Version";
            if (current)
                type_image_key = "current" + type_image_key;
            imageUrl = getClass().getResource("/icons/versions/" +
                    type_image_key +
                    ".png");
            if (imageUrl != null) {
                setIcon(new ImageIcon(imageUrl));
            }
        }
        setForeground(tree.getForeground());
        setFont(Current.getTheme().Fonts.get(
                version.isNew ? VisualiserFonts.NewVersion : VisualiserFonts.TreePlain));
        setText(version.getTitle());
        return this;
    }

    /*
       Fonts.put(VisualiserFonts.Version, new Font(
                    new HashMap<TextAttribute, Object>() {
                        {
                            put(TextAttribute.FAMILY, "Times New Roman");
                            put(TextAttribute.FOREGROUND, Color.BLACK);
                            put(TextAttribute.BACKGROUND, Color.WHITE);
                            put(TextAttribute.SIZE, 14);
                        }
                    }

            ));
            Fonts.put(VisualiserFonts.NewVersion, new Font(
                    new HashMap<TextAttribute, Object>() {
                        {
                            put(TextAttribute.FAMILY, "Times New Roman");
                            put(TextAttribute.FOREGROUND, Color.BLACK);
                            put(TextAttribute.BACKGROUND, Color.YELLOW);
                            put(TextAttribute.SIZE, 14);
                        }
                    }

            ));
    */
}

package ProjectData.SapforData.Loops;
import Common.UI.Themes.VisualiserFonts;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;
public class EGoto extends FileObjectWithMessages {
    public EGoto(DBProjectFile father_in, int lineNum_in) {
        super(father_in, lineNum_in);
    }
    @Override
    public String Description() {
        return "вход внутрь цикла";
    }
    @Override
    public VisualiserFonts getFont() {
        return VisualiserFonts.BadState;
    }
}

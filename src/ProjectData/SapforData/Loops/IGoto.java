package ProjectData.SapforData.Loops;
import Common.UI.Themes.VisualiserFonts;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;
public class IGoto extends FileObjectWithMessages {
    public IGoto(DBProjectFile father_in, int lineNum_in) {
        super(father_in, lineNum_in);
    }
    @Override
    public String Description() {
        return "выход за пределы цикла";
    }
    @Override
    public VisualiserFonts getFont() {
        return VisualiserFonts.BadState;
    }
}

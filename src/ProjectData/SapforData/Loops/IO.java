package ProjectData.SapforData.Loops;
import Common.UI.Themes.VisualiserFonts;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;
public class IO extends FileObjectWithMessages {
    public IO(DBProjectFile father_in, int lineNum_in) {
        super(father_in, lineNum_in);
    }
    @Override
    public String Description() {
        return "оператор ввода-вывода";
    }
    @Override
    public VisualiserFonts getFont() {
        return VisualiserFonts.BadState;
    }
}

package ProjectData.SapforData.Loops;
import Common.UI.Themes.VisualiserFonts;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;
public class NonRectIter extends FileObjectWithMessages {
    public NonRectIter(DBProjectFile father_in, int lineNum_in) {
        super(father_in, lineNum_in);
    }
    @Override
    public String Description() {
        return "непрямоугольное пространство итераций";
    }
    @Override
    public VisualiserFonts getFont() {
        return VisualiserFonts.BadState;
    }
}

package ProjectData.SapforData.Arrays.Distribution;
import ProjectData.SapforData.Arrays.ArrayLocation;
import ProjectData.SapforData.Arrays.ProjectArray;
import javafx.util.Pair;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Vector;
public class DataDirective extends Directive {
    public Vector<AlignRule> alignRules;
    public String rules_ = "";
    public Vector<String> rules = new Vector<>();
    public void genRules_(LinkedHashMap<Long, ProjectArray> Arrays) {
        rules_ = "";
        int maxLen = 0;
        for (AlignRule a : alignRules)
            maxLen = Math.max(maxLen, a.GetLenString());
        LinkedHashMap<String, Vector<String>> toPrint = new LinkedHashMap<>();
        for (AlignRule a : alignRules) {
            if (Arrays.get(a.alignArray_address).location != ArrayLocation.parameter) {
                Pair<String, String> result = a.GenRule(maxLen);
                if (!toPrint.containsKey(result.getKey()))
                    toPrint.put(result.getKey(), new Vector<>());
                toPrint.get(result.getKey()).add(result.getValue());
            }
        }
        for (Vector<String> v : toPrint.values()) {
            rules_ += String.join("\n", v);
        }
    }
    public void genRules(LinkedHashMap<BigInteger, ProjectArray> Arrays) {
        rules.clear();
        int maxLen = 0;
        for (AlignRule a : alignRules)
            maxLen = Math.max(maxLen, a.GetLenString());
        LinkedHashMap<String, Vector<String>> toPrint = new LinkedHashMap<>();
        for (AlignRule a : alignRules) {
            if (Arrays.get(a.alignArray_address).location != ArrayLocation.parameter) {
                Pair<String, String> result = a.GenRule(maxLen);
                if (!toPrint.containsKey(result.getKey()))
                    toPrint.put(result.getKey(), new Vector<>());
                toPrint.get(result.getKey()).add(result.getValue());
            }
        }
        for (Vector<String> v : toPrint.values())
            for (String r : v)
                rules.add(r);
    }
}

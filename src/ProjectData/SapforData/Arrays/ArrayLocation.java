package ProjectData.SapforData.Arrays;
public enum ArrayLocation {
    local, common, module, parameter, structure, local_save, unknown;
    public static ArrayLocation fromInt(int i) {
        switch (i) {
            case 0:
                return local;
            case 1:
                return common;
            case 2:
                return module;
            case 3:
                return parameter;
            case 4:
                return structure;
            case 5:
                return local_save;
            default:
                return unknown;
        }
    }
    public String getDescription() {
        switch (this) {
            case local:
                return "локальный";
            case common:
                return "глобальный";
            case module:
                return "модульный";
            case parameter:
                return "параметр";
            case structure:
                return "структура";
            case local_save:
                return "локальный сохраняемый";
            default:
                return this.toString();
        }
    }
}

package ProjectData.SapforData.Arrays;
import Common.Current;
import Common.Database.DataSet;
import Common.Global;
import Common.UI.DataSetControlForm;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;

import static Common.UI.Tables.TableEditors.EditorHyperlinks;
import static Common.UI.Tables.TableRenderers.RendererHiddenList;
import static Common.UI.Tables.TableRenderers.RendererHyperlinks;
public class ArraysSet extends DataSet<Long, ProjectArray> {
    public ArraysSet() {
        super(Long.class, ProjectArray.class);
    }
    @Override
    public String getSingleDescription() {
        return "массив";
    }
    @Override
    public String getPluralDescription() {
        return "объявленные массивы";
    }
    @Override
    public Current CurrentName() {
        return Current.ProjectArray;
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
                if (Global.db.settings.get(SettingName.ShowFullArraysDeclarations).toBoolean()) {
                    columns.get(4).setRenderer(RendererHyperlinks);
                    columns.get(4).setEditor(EditorHyperlinks);
                } else {
                    columns.get(4).setRenderer(RendererHiddenList);
                    columns.get(4).setMaxWidth(200);
                }
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Имя", "Область описания", "Файлы объявления", "Размерность", "Размер элемента(байт)", "Область распараллеливания"
        };
    }
    @Override
    public Object getFieldAt(ProjectArray object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.State;
            case 2:
                return object.GetShortNameWithDim();
            case 3:
                return object.locName + " : " + object.location;
            case 4:
                return object.GetDeclPlacesList();
            case 5:
                return object.dimSize;
            case 6:
                return object.typeSize;
            case 7:
                return object.GetRegionsText();
            default:
                return null;
        }
    }
    @Override
    public void CheckAll(boolean flag) {
        Pass_2021.passes.get(PassCode_2021.MassSelectArrays).Do(flag,new Vector(Current.getProject().declaratedArrays.Data.values()));
    }
}

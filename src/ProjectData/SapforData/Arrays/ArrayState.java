package ProjectData.SapforData.Arrays;
import javax.swing.*;
import java.net.URL;
public enum ArrayState {
    Selected, None, SpfPrivate, IOPrivate, Unknown;
    public static ArrayState fromInt(int i) {
        switch (i) {
            case 0:
                return Selected;
            case 1:
                return None;
            case 2:
                return SpfPrivate;
            case 3:
                return IOPrivate;
            default:
                return Unknown;
        }
    }
    public ImageIcon GetIcon() {
        URL imageUrl = getClass().getResource("/icons/Arrays/" + this + ".png");
        return new ImageIcon(imageUrl);
    }
}

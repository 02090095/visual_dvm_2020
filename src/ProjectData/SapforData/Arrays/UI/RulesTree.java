package ProjectData.SapforData.Arrays.UI;
import Common.Current;
import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.StyledTree;
import Common.UI.Trees.TreeRenderers;
import ProjectData.SapforData.Regions.ParallelRegion;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import static Common.UI.Trees.TreeRenderers.RendererRule;
public class RulesTree extends StyledTree {
    public RulesTree() {
        super(Current.getProject().align_rules_root);
        setRootVisible(false);
        expandRow(0);
        ExpandAll();
        Current.set(Current.ParallelRegion, null);
    }
    @Override
    protected GraphMenu createMenu() {
        return new DistributionMenu(this);
    }
    @Override
    public TreeRenderers getRenderer() {
        return RendererRule;
    }
    @Override
    public void SelectionAction(TreePath e) {
        ParallelRegion region = null;
        if (e != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getLastPathComponent();
            Object o = node.getUserObject();
            if (o instanceof String) {
                region = (ParallelRegion) ((DefaultMutableTreeNode) node.getParent()).getUserObject();
            }
            if (o instanceof ParallelRegion) {
                region = (ParallelRegion) o;
            }
        }
        Current.set(Current.ParallelRegion, region);
    }
}

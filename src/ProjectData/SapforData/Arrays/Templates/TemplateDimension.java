package ProjectData.SapforData.Arrays.Templates;
import Common.Database.DBObject;
import ProjectData.SapforData.Arrays.ProjectArray;
import ProjectData.SapforData.Regions.ParallelRegion;
//для генерации вариантов и для управления распределением измерений.
public class TemplateDimension extends DBObject {
    public int num = -1;
    public ProjectArray template = null;
    public ParallelRegion region = null;
    public TemplateDimensionState state;
    public TemplateDimension(int num_in, ProjectArray template_in, ParallelRegion region_in) {
        num = num_in;
        template = template_in;
        region = region_in;
        state = isBlocked() ? TemplateDimensionState.multiplied : TemplateDimensionState.distributed;
    }
    public boolean isBlocked() {
        return (num >= template.dimSize) || template.DimDisabled(num);
    }
    public void SwitchState() {
        switch (state) {
            case distributed:
                state = TemplateDimensionState.multiplied;
                break;
            case multiplied:
                state = TemplateDimensionState.distributed;
                break;
        }
    }
    @Override
    public Object getPK() {
        return num;
    }
}

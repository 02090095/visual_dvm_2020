package ProjectData.SapforData;
import Common.UI.Themes.VisualiserFonts;
import ProjectData.Files.DBProjectFile;
import ProjectData.Messages.Errors.MessageError;
import ProjectData.Messages.Message;
import ProjectData.Messages.Notes.MessageNote;
import ProjectData.Messages.Warnings.MessageWarning;
public abstract class FileObjectWithMessages extends FileObject {
    public String messages_presence = "OK";
    public FileObjectWithMessages() {
    }
    //использовать только если хотим проверять сообщения.
    public FileObjectWithMessages(DBProjectFile father_in, int line_in) {
        file = father_in.name;
        line = line_in;
        CheckMessagesPresence();
    }
    public boolean HasMessage(Message message) {
        return message.line == line;
    }
    public void CheckMessagesPresence() {
        messages_presence = "OK";
        DBProjectFile father = getFather();
        for (MessageError message : father.father.db.errors.Data.values()) {
            if (message.file.equals(father.name) && message.line == line) {
                messages_presence = "HasErrors";
                return;
            }
        }
        for (MessageWarning message : father.father.db.warnings.Data.values()) {
            if (message.file.equals(father.name) && message.line == line) {
                messages_presence = "HasWarnings";
                return;
            }
        }
        for (MessageNote message : father.father.db.notes.Data.values()) {
            if (message.file.equals(father.name) && message.line == line) {
                messages_presence = "HasNotes";
                return;
            }
        }
    }
    public String TypeKey() {
        return getClass().getSimpleName();
    }
    public String ImageKey() {
        return "/icons/loops/" + messages_presence + TypeKey() + ".png";
    }
    public VisualiserFonts getFont() {
        return VisualiserFonts.TreePlain;
    }
    //-
    public abstract String Description();
    @Override
    public String toString() {
        return Description() + " в строке " + line;
    }
}

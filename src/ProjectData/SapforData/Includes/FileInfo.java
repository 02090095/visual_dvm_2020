package ProjectData.SapforData.Includes;
import Common.Utils.Utils;
import ProjectData.SapforData.FileObject;

import javax.swing.*;
import java.util.Vector;
public class FileInfo extends FileObject {
    public Vector<DependencyInfo> dependencies = new Vector<>();
    public FileInfo(String file_in) {
        super(file_in);
    }
    @Override
    public String getSelectionText() {
        return file;
    }
    @Override
    public boolean isSelectionEnabled() {
        return false;
    }
    @Override
    public ImageIcon GetDisabledIcon() {
        return Utils.getIcon("/icons/File.png");
    }
    @Override
    public void SelectAllChildren(boolean select) {
        for (DependencyInfo di : dependencies)
            di.Select(select);
    }
}

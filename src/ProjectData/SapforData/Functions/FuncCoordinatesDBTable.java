package ProjectData.SapforData.Functions;
import Common.Database.DBTable;
public class FuncCoordinatesDBTable extends DBTable<String, FuncCoordinates> {
    public FuncCoordinatesDBTable() {
        super(String.class, FuncCoordinates.class);
    }
}

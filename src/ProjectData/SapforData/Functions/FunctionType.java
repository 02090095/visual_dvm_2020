package ProjectData.SapforData.Functions;
public enum FunctionType {
    Default, //обычное объявление
    Main, //ГПЕ
    //для графа функций
    Standard, //стандартная функция
    NotFound; //функция не найдена в проекте
    //-
    public String getDescription() {
        switch (this) {
            case Default:
                return "объявление";
            case Main:
                return "главная программная единица";
            case NotFound:
                return "не найдена";
            case Standard:
                return "стандартная";
            default:
                return toString();
        }
    }
}

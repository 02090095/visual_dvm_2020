package ProjectData.SapforData.Functions;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;
public class FuncCoordinates extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String name = "";
    @Description("DEFAULT 0.0")
    public Double X = 0.0;
    @Description("DEFAULT 0.0")
    public Double Y = 0.0;
    @Override
    public Object getPK() {
        return name;
    }
}

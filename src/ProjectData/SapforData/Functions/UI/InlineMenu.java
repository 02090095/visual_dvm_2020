package ProjectData.SapforData.Functions.UI;
import Common.Current;
import Common.UI.Menus.SelectionTreeMenu;
import Common.UI.Trees.SelectableTree;
import ProjectData.SapforData.Functions.FuncInfo;
public class InlineMenu extends SelectionTreeMenu {
    public InlineMenu(SelectableTree tree_in) {
        super(tree_in);
    }
    @Override
    public Class getTargetClass() {
        return FuncInfo.class;
    }
    @Override
    public void SelectAll(boolean select) {
        for (FuncInfo fi : Current.getProject().allFunctions.values())
            fi.SelectAllChildren(select);
    }
}

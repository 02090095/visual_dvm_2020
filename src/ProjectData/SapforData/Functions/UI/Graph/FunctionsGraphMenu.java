package ProjectData.SapforData.Functions.UI.Graph;
import Common.Current;
import Common.UI.Menus.VisualiserMenuItem;
import Common.UI.Menus.StyledPopupMenu;
import Common.UI.UI;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.All.SPF_GetGraphFunctionPositions;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FunctionsGraphMenu extends StyledPopupMenu {
    JMenuItem changeCurrent;
    public FunctionsGraphMenu() {
        changeCurrent = new VisualiserMenuItem("Назначить выбранный узел текущей функцией");
        changeCurrent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Current.HasSelectedFunction()) {
                    Current.set(Current.Function, Current.getSelectionFunction());
                    UI.getMainWindow().getProjectWindow().getFunctionsWindow().ShowCurrentFunction();
                    if (SPF_GetGraphFunctionPositions.showByCurrentFunction) {
                        Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
                    }
                }
            }
        });
        add(changeCurrent);
    }
    @Override
    public void CheckElementsVisibility() {
        if (Current.HasSelectedFunction()) {
            changeCurrent.setText("Назначить процедуру " + Utils.DQuotes(Current.getSelectionFunction().funcName) + " текущей.");
            changeCurrent.setEnabled(true);
        } else {
            changeCurrent.setText("Невозможно назначить текущую процедуру: узел графа не выбран");
            changeCurrent.setEnabled(false);
        }
    }
}

package ProjectData.SapforData.Functions.UI.Graph;
import Common.Current;
import Common.UI.ControlForm;
import Common.UI.UI;
import com.mxgraph.swing.mxGraphComponent;

import java.awt.*;
public class FunctionsGraphForm extends ControlForm<mxGraphComponent> {
    public FunctionsGraphForm() {
        super(mxGraphComponent.class);
    }
    @Override
    public void CreateControl() {
        control = Current.getProject().DrawFunctionsGraph();
    }
    @Override
    public void Show() {
        super.Show();
        content.add(scroll, BorderLayout.CENTER);
        content.updateUI();
    }
    @Override
    public void Clear() {
        super.Clear();
        UI.Clear(content);
    }
}

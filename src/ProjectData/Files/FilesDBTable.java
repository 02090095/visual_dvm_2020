package ProjectData.Files;
import Common.Database.DBTable;
public class FilesDBTable extends DBTable<String, DBProjectFile> {
    public FilesDBTable() {
        super(String.class, DBProjectFile.class);
    }
    @Override
    public String getSingleDescription() {
        return "файл";
    }
}

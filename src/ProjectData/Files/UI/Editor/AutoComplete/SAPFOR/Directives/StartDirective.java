package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
public class StartDirective extends BaseDirective {
    public StartDirective(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in);
    }
    @Override
    public boolean Check() {
        return getOwner().flags.contains(DirectiveName.START);
    }
}

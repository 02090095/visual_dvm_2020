package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
public class Spec2Directive extends SpecDirective {
    public Spec2Directive(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in);
    }
    @Override
    public boolean Check() {
        return (getOwner().flags.size() > 1)
                && getOwner().flags.get(0).equals(DirectiveName.SPF)
                && getOwner().flags.get(1).equals(DirectiveName.PARALLEL)
                && getOwner().nearestGroup.equals(DirectiveName.PARALLEL);
    }
}

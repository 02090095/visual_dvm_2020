package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import Common.UI.Editor.CaretInfo;
import Common.Utils.Utils;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
import org.fife.ui.autocomplete.BasicCompletion;
//определяет, должна ли отображаться в меню автозаполнения директива данного типа.
public class BaseDirective extends BasicCompletion {
    public DirectiveName name;
    public boolean visible = false; //должна ли быть в меню.
    public BaseDirective(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in.getText(), name_in.getDescription());
        name = name_in;
    }
    //есть ли совпадение с началом директивы в строке
    public boolean isMatch() {
        return
                getCaretInfo().suffix_word.isEmpty() &&
                        name.getText().startsWith(getCaretInfo().prefix_word)
                        && (!name.getText().equals(getCaretInfo().prefix_word))
                        && !Utils.isBracketsBalanced(getCaretInfo().before);
    }
    //итоговая функция, определяющая наличие директивы в автозаполнении
    public boolean Check() {
        return visible = isMatch();
    }
    protected CaretInfo getCaretInfo() {
        // System.out.println(getOwner());
        // System.out.println(getOwner().getEditor());
        return getOwner().getEditor().getCaretInfo();
    }
    protected SapforAutoComplete getOwner() {
        return ((BaseProvider) getProvider()).getOwner();
    }
}

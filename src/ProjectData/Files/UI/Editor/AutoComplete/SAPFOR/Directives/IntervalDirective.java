package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import Common.Utils.Utils;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
public class IntervalDirective extends BaseDirective {
    protected String prefix;
    protected String word;
    protected String suffix;
    public IntervalDirective(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in);
    }
    @Override
    public boolean Check() {
        return (getOwner().flags.size() > 2) &&
                getOwner().flags.get(0).equals(DirectiveName.SPF) &&
                getOwner().flags.get(1).equals(DirectiveName.CHECKPOINT) &&
                getOwner().nearestGroup.equals(DirectiveName.INTERVAL)
                && !getOwner().flags.contains(DirectiveName.TIME)
                && !getOwner().flags.contains(DirectiveName.ITER);
    }
    @Override
    public String getReplacementText() {
        prefix = Utils.pack(getCaretInfo().before.substring(0, getCaretInfo().before.length() - getCaretInfo().prefix_word.length()));
        word = name.getText();
        suffix = Utils.pack(getCaretInfo().after.substring(getCaretInfo().suffix_word.length()));
        String pp = prefix.replace(" ", "");
        String ps = suffix.replace(" ", "");
        if ((!ps.isEmpty()) && (ps.charAt(0) != ','))
            word = (word + ",");
        return word;
    }
}

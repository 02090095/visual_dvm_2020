package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.BaseDirective;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
public class BaseProvider extends DefaultCompletionProvider {
    protected static char[] separators = new char[]{'!', '$', '*', ' ', ',', '(', ')'};
    protected SapforAutoComplete owner;//владелец.
    public BaseProvider(SapforAutoComplete owner_in) {
        owner = owner_in;
    }
    public SapforAutoComplete getOwner() {
        return owner;
    }
    protected boolean isSeparator(char ch) {
        for (char s : separators)
            if (ch == s) return true;
        return false;
    }
    @Override
    protected boolean isValidChar(char ch) {
        return super.isValidChar(ch) || isSeparator(ch);
    }
    //получить текст от каретки и до разделителя
    public void addDirective(BaseDirective directive) {
        addCompletion(directive);
        owner.directives.put(directive.name, directive);
    }
}

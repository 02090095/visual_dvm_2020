package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.RegionDirective;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.TypeDirective;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class TypeProvider extends PrefixWordProvider {
    public TypeProvider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new TypeDirective(this, DirectiveName.ANALYSIS));
        addDirective(new TypeDirective(this, DirectiveName.PARALLEL));
        addDirective(new TypeDirective(this, DirectiveName.TRANSFORM));
        addDirective(new TypeDirective(this, DirectiveName.CHECKPOINT));
        addDirective(new RegionDirective(this, DirectiveName.PARALLEL_REG));
        addDirective(new RegionDirective(this, DirectiveName.END_PARALLEL_REG));
    }
}

package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.StartDirective;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class StartProvider extends BaseProvider {
    public StartProvider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new StartDirective(this, DirectiveName.SPF));
    }
}

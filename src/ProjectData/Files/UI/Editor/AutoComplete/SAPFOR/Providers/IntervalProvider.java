package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.IntervalDirective;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class IntervalProvider extends PrefixWordProvider {
    public IntervalProvider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new IntervalDirective(this, DirectiveName.TIME));
        addDirective(new IntervalDirective(this, DirectiveName.ITER));
    }
}

package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.CheckPointTypeDirective;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class CheckPointTypeProvider extends PrefixWordProvider {
    public CheckPointTypeProvider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new CheckPointTypeDirective(this, DirectiveName.ASYNC));
        addDirective(new CheckPointTypeDirective(this, DirectiveName.FLEXIBLE));
    }
}

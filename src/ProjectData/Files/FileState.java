package ProjectData.Files;
public enum FileState {
    Undefined, //состояние файла сброшено.
    OK, //никаких сообщений нет, зеленая галочка.
    HasNotes, //есть только note
    HasWarnings, //есть warning, нет error
    HasErrors, //есть error
    Excluded //исключен из рассмотрения
}

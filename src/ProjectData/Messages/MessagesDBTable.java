package ProjectData.Messages;
import Common.Current;
import Common.Database.DataSet;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Tables.ColumnFilter;
import Common.UI.Tables.TableRenderers;

import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.util.Comparator;
//https://stackoverflow.com/questions/2026965/can-i-add-a-button-to-a-jtable-column-header
//https://stackoverflow.com/questions/7137786/how-can-i-put-a-control-in-the-jtableheader-of-a-jtable/29963916#29963916
//https://stackoverflow.com/questions/7137786/how-can-i-put-a-control-in-the-jtableheader-of-a-jtable   ->>
public class MessagesDBTable<M extends Message> extends iDBTable<M> {
    public MessagesDBTable(Class<M> d_in) {
        super(d_in);
    }
    @Override
    protected DataSetControlForm createUI() {
        DataSet dataset = this;
        return new DataSetControlForm(this) {
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                Current.getFile().form.getEditor().gotoLine(getCurrent().line);
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
                columns.get(3).setMinWidth(700);
                columns.get(3).setRenderer(TableRenderers.RendererWrapText);
            }
            @Override
            public void MouseAction2() throws Exception {
                ShowCurrentObject();
            }
            @Override
            public void CreateControl() {
                super.CreateControl();
                columnsFilters.put(3, new ColumnFilter(dataset, 3));
                control.setRowSorter(null);
                TableRowSorter<TableModel> sorter = new TableRowSorter<>(control.getModel());
                sorter.setSortable(3, false);
                control.setRowSorter(sorter);
                /*
                List<RowSorter.SortKey> sortKeys = new ArrayList<>();
                for (int i = 0; i < 6; ++i)
                    sorter.setSortable(i, false);
                sortKeys.add(new RowSorter.SortKey(6, SortOrder.DESCENDING));
                sortKeys.add(new RowSorter.SortKey(7, SortOrder.DESCENDING));
                 */

              //  sorter.setSortKeys(sortKeys);
              //  sorter.sort();
            }
        };
    }
    @Override
    public Object getFieldAt(M object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.group_s;
            case 2:
                return object.line;
            case 3:
                return object.value;
            default:
                return null;
        }
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"группа", "строка", "текст"};
    }
    @Override
    public Comparator<M> getComparator() {
        return Comparator.comparingInt(o -> o.line);
    }
}

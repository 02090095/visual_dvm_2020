package ProjectData.Messages.Errors;
import Common.Current;
import ProjectData.Messages.MessagesDBTable;
public class ErrorsDBTable extends MessagesDBTable<MessageError> {
    public ErrorsDBTable() {
        super(MessageError.class);
        //   setUIContent(UI.getMainWindow().errorsPanel);
    }
    @Override
    public String getSingleDescription() {
        return "сообщение об ошибке";
    }
    @Override
    public Current CurrentName() {
        return Current.Errors;
    }
    public void changeColumnFilterValue(int columnIndex, String text) {
        if (columnIndex == 3)
            MessageError.filterValue = text;
    }
    public Object getColumnFilterValue(int columnIndex) {
        return MessageError.filterValue;
    }

}

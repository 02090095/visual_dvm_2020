package ProjectData.Messages.Warnings;
import Common.Current;
import ProjectData.Messages.MessagesDBTable;
public class WarningsDBTable extends MessagesDBTable<MessageWarning> {
    //https://stackoverflow.com/questions/13079777/editable-jtableheader
    public WarningsDBTable() {
        super(MessageWarning.class);
    }
    @Override
    public String getSingleDescription() {
        return "предупреждение";
    }
    @Override
    public Current CurrentName() {
        return Current.Warnings;
    }
    // применить значение фильтра к фильру объекта  напирмер Message.filterValue = text;
    public void changeColumnFilterValue(int columnIndex, String text) {
        if (columnIndex == 3)
            MessageWarning.filterValue = text;
    }
    public Object getColumnFilterValue(int columnIndex) {
        return MessageWarning.filterValue;
    }
}
package ProjectData.Messages.Recommendations;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Tables.TableRenderers;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
public class RecommendationsDBTable extends iDBTable<MessageRecommendation> {
    //group=1 - настройка
    //group=2 - преобразование
    public RecommendationsDBTable() {
        super(MessageRecommendation.class);
        //   setUIContent(UI.getMainWindow().errorsPanel);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this){
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
                columns.get(1).setMinWidth(700);
                columns.get(1).setRenderer(TableRenderers.RendererWrapText);
            }

        };

    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"текст"};
    }
    @Override
    public Object getFieldAt(MessageRecommendation object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.text;
            default:
                return null;
        }
    }
    @Override
    public String getSingleDescription() {
        return "рекомендация";
    }
    @Override
    public Current CurrentName() {
        return Current.Recommendations;
    }
    public void addRecommendation(int group_in) {
        MessageRecommendation result = null;
        switch (group_in) {
            /*
            case 1013:
                //процедура. понять как извлекать ее из сообщения, и сунуть как аргумент преобразованию.
                break;
             */
            case 1015:
                result = new MessageRecommendation(PassCode_2021.SPF_RemoveUnusedFunctions);
                break;
            case 1018:
                result = new MessageRecommendation(PassCode_2021.SPF_LoopEndDoConverterPass);
                break;
            case 1020:
                result = new MessageRecommendation(SettingName.KEEP_DVM_DIRECTIVES, "1");
                break;
            case 1027:
            case 1060:
                result = new MessageRecommendation(PassCode_2021.SPF_CorrectCodeStylePass);
                break;
            case 1041:
            case 3012:
            case 3013:
            case 3014:
            case 3015:
            case 3017:
            case 3018:
                result = new MessageRecommendation(PassCode_2021.SPF_ResolveParallelRegionConflicts);
                break;
            case 1061:
                result = new MessageRecommendation(PassCode_2021.SPF_InsertIncludesPass);
                break;
            case 3020:
                result = new MessageRecommendation(PassCode_2021.SPF_DuplicateFunctionChains);
                break;
        }
        MessageRecommendation finalResult = result;
        if ((result != null) && (Data.values().stream().noneMatch(recommendation -> recommendation.isMatch(finalResult)))) {
            try {
                getDb().Insert(result);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}

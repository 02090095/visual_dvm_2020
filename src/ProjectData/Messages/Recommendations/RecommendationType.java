package ProjectData.Messages.Recommendations;
public enum RecommendationType {
    Setting,
    Analysis,
    Transformation,
    Text;
}

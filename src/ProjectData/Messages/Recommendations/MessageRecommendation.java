package ProjectData.Messages.Recommendations;
import Common.Database.iDBObject;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import com.sun.org.glassfish.gmbal.Description;
public class MessageRecommendation extends iDBObject {
    //рекомендация может касаться, либо настройки либо прохода. так же может быть просто текстовой.
    @Description("DEFAULT 'Text'")
    public RecommendationType type = RecommendationType.Text;
    @Description("DEFAULT 'Undefined'")
    public String argName = "Undefined";//либо имя настройки либо имя прохода
    @Description("DEFAULT ''")
    public String argValue = ""; //Либо значение настройки либо аргумент для прохода
    @Description("DEFAULT ''")
    public String text = ""; //текст
    public MessageRecommendation() {
    }
    public MessageRecommendation(PassCode_2021 passCode_in) {
        type = RecommendationType.Transformation;
        argName = passCode_in.toString();
        text = "Выполните преобразование " + Utils.Quotes(passCode_in.getDescription());
    }
    public MessageRecommendation(SettingName settingName_in, String settingValue_in) {
        type = RecommendationType.Setting;
        argName = settingName_in.toString();
        argValue = settingValue_in;
        if (argValue.equals("1"))
            text = "Включите настройку SAPFOR " + Utils.Quotes(settingName_in.getDescription());
        else if (argValue.equals("0"))
            text = "Отключите настройку SAPFOR " + Utils.Quotes(settingName_in.getDescription());
        else
            text = "Задайте значение " + Utils.DQuotes(argValue) + " для настройки SAPFOR " + Utils.Quotes(settingName_in.getDescription());
    }
    public MessageRecommendation(String text_in) {
        type = RecommendationType.Text;
        text = text_in;
    }
    public boolean isMatch(MessageRecommendation recommendation_in) {
        return type.equals(recommendation_in.type) &&
                argName.equals(recommendation_in.argName) &&
                argValue.equals(recommendation_in.argValue);
    }
    @Override
    public boolean isVisible() {
        return true;
    }
}

package ProjectData.Messages.Notes;
import Common.Current;
import ProjectData.Messages.MessagesDBTable;
public class NotesDBTable extends MessagesDBTable<MessageNote> {
    public NotesDBTable() {
        super(MessageNote.class);
        // setUIContent(UI.getMainWindow().notesPanel);
    }
    @Override
    public String getSingleDescription() {
        return "примечание";
    }
    @Override
    public Current CurrentName() {
        return Current.Notes;
    }
    public void changeColumnFilterValue(int columnIndex, String text) {
        if (columnIndex == 3)
            MessageNote.filterValue = text;
    }
    public Object getColumnFilterValue(int columnIndex) {
        return MessageNote.filterValue;
    }

}
